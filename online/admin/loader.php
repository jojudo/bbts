<?php 
    defined('INSTANCE') or die("No Direct access allowed!");
    
class loader{
        
    function __construct(){
        $this->__boot();
    }
        
    private function __boot(){
       
        $this->ui  = load_class('ui');
        $this->datastore = load_class('datastore');
        
        $this->__authenticate();
        
        //send to ui the profile array
        $this->ui->open_cabinet('profile',$this->datastore->get_config("profile")[0]);
        $mod = $this->datastore->pick("mod")?$this->datastore->pick("mod"):'cpanel';
        $this->datastore->add_config("mod",$mod);		
		$this->__module_process($mod);   
        
    }
    
    private function __authenticate(){
       $this->auth = load_class('auth');
       $this->auth->authenticate();
    }

    
    private function __module_process($mod){
        if(file_exists(MODPATH.$mod.DS."helper.php")) 
        {
            include MODPATH.$mod.DS."helper.php";
            $module = new moduleHelper;
        }
        else
        {
            $this->ui->load_theme_page('404');
        }
    }	
}
