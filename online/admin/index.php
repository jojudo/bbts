<?php
	session_start();
	define('INSTANCE',1);
	error_reporting(E_ALL);
    
	//define environment
	define('ENVIRONMENT', 'development');	
	
    //let us define the folders
	$system_path = "system";
	$app_path = "admin";
    
    chdir('../');
    
    //save individual global values	
	$server_name = rtrim($_SERVER['SERVER_NAME']);
	$addr_arr = explode('/',$_SERVER['SCRIPT_NAME']);
	array_pop($addr_arr);
	array_pop($addr_arr);
	
	$current_folder = implode('/',$addr_arr);	
	$current_folder = LTRIM($current_folder,DIRECTORY_SEPARATOR);
    
    $base_arr = explode(DIRECTORY_SEPARATOR,dirname(__FILE__));
	array_pop($base_arr);
    define('BASE',implode(DIRECTORY_SEPARATOR,$base_arr));
    
	require_once $system_path."\core.php";	
	
?>

