<?php 
	/*---- container for helper functions for use within the subfolder-- */

	if ( ! defined('INSTANCE')) exit('No direct script access allowed');
    include(CLASSPATH."class.widget.php");
	function widget($widgetName,$method=""){
		if(file_exists(BASE.DS.'widgets'.DS.$widgetName.DS.'widget.php')){
			$ui = load_class("ui");
			
			$ui->ui_start();
			include(BASE.DS.'widgets'.DS.$widgetName.DS.'widget.php');
			$widget = new widget();
            if($method!="") $widget->$method();
			echo $ui->ui_end(false);
		}
	}
	
	function system_error($option=''){
		//load_class("datastore")->vomit();
		$error_messages = load_class("datastore")->get_config('system_message');
		if($option=='COUNT'){
			return count($error_messages);
		}else{
			foreach($error_messages as $msg){
				echo $msg[0].'<br>';
			}
		}
		
	}
	
	function add_template($template){
		$ui = load_class("ui");
		
		$ui->load_theme_page_solo($template);		
	}

    function transform_url(){
		$numargs = func_num_args();
		$params = func_get_args();
        //param1 = mod param 2 = action
        $datastore = load_class("datastore");
        $link =  $datastore->get_config("folder_url").US.'index.php?mod='.$params[0];
        if(isset($params[1])) $link.='&action='.$params[1];
		if($numargs>2){
			for($i=2;$i<$numargs;$i++){
				$link.='&p'.($i-1).'='.$params[$i];
			}
		}
        
        return $link;
    }
    
    function mod_url(){
        $datastore = load_class("datastore");
        return $datastore->get_config("mod_url").US.$datastore->get_config("mod");
    } 
    
    function admin_url(){
        $datastore = load_class("datastore");
        return $datastore->get_config("full_url").US."admin";
    } 
    
    function theme_url(){
        $datastore = load_class("datastore");
        return $datastore->get_config("full_url").US."themes".US."admin".US.$datastore->get_config("theme");
    } 
	
	function vendor_url(){
        $datastore = load_class("datastore");
        return $datastore->get_config("full_url").US."themes".US."vendors";
    } 
	
	//question: why are the vendors in the themes? - themes can use vendors? - probably better to move them into a vendor folder outside themes.
	function add_vendor($folder,$filename,$ext='js'){
		$ui = load_class("ui");
		$datastore = load_class("datastore");
		if($ext=='js'){
			$ui->set_javascript($datastore->get_config("full_url").US."themes".US."vendors".US.$folder.US.$filename.".$ext");
		}
		else{
			$ui->set_stylesheet($datastore->get_config("full_url").US."themes".US."vendors".US.$folder.US.$filename.".$ext");
		}
	}
	
    function library_url(){
        $datastore = load_class("datastore");
        return $datastore->get_config("lib_url");
    }
    
    function module(){
        return load_class("datastore")->get_config("mod");
    }
	
	function pick($paramNum){
		return load_class("datastore")->pick('p'.$paramNum);
	}
	
   