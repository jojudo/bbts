<?php
	/**
	* Millennium Express INC.
	*
	* Internal Tracking Sysytem
	*
	* @package		MEX
	* @author		Joey Toga
	* @copyright	Copyright (c) 2009 - 2015, Millennium Express INC..
	* @license		---
	* @link		http://www.millenniumexpress.biz
	* @since		Version 3.0
	* @filesource 
	* @description	processor file
	*/
	
	if ( ! defined('INSTANCE')) exit('No direct script access allowed');
   
    //reference the module class Helper 
	require_once CLASSPATH.DS.'class.helper.php';
		
	//this can be made dynamic
	$datastore->add_config('theme',$config['theme']['admin']);
	$datastore->add_config('theme_folder',BASE.DS."themes".DS."admin".DS.$config['theme']['admin']);
    $datastore->add_config('mod_folder',MODPATH);
    
	$datastore->add_config('theme_url',$datastore->get_config("full_url").US."themes".US."admin".US.$config['theme']['admin']);
	//$datastore->add_config('lib_url',$datastore->get_config("full_url").US."libraries");
	$datastore->add_config('mod_url',$datastore->get_config("full_url").US."modules");
	$datastore->add_config('folder_url',$datastore->get_config("full_url").US."admin");
 
	//Front end vendor libraries to be used in this folder -- will be called everytime
	add_vendor('jquery','dist/jquery.min');
	add_vendor('jquery-ui','jquery-ui.min');
	add_vendor('mustache.js','mustache.min');
	add_vendor('bootstrap','dist/js/bootstrap.min');
	add_vendor('mexpress','js/framework');
	add_vendor('pnotify','dist/pnotify');
	
	//Front end vendor stylesheets
	add_vendor('pnotify','dist/pnotify','css');
	add_vendor('jquery-ui','themes/ui-lightness/jquery-ui.min','css');

	$loader = new loader();
?>