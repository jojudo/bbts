<div id="middle_content">
	<p class="page_header"><img src="images/deliver.gif" style="vertical-align:bottom;">Job Orders</p>
	<form id="jobForm" name="jobForm" onsubmit="return false;">
	<?=$message!=""?"<div class=\"alert_msg\">$message</div>":"";?>
	<table width="95%" cellpadding="3" cellspacing="0" class="table_bordered" >
		<tr>
			<td width="50%" valign="top" style="border-right:1px solid;">
			<div id="clientDetail">
				<table cellpadding="3" cellspacing="0" width="100%" border="0">
				<tr>
					<td colspan="2" class="table_headers">Client Details</td>
				</tr>
				<tr>
					<td colspan="2" style="padding-left:20px;">
					<div id="client_details">
						<input type="hidden" name="cid" id="cid" value="">
						<table width="100%" border="0" cellpadding="3" cellspacing="0">
							<tr>
								<td valign="top"  width="150px"><b>Last Name</b></td>
								<td>
									<input type="text" name="lname" id="lname" size="40" class="matchMe" autocomplete="off">
									<div id="matchLoader" style="display:inline;"></div>
								</td>
							</tr>
							<tr>
								<td><b>First Name</b></td>
								<td>
									<div><input type="text" id="fname" name="fname" size="40" class="matchMe" autocomplete="off">
									<div id="nameMatch" class="nameMatch"></div>
								</td>
							</tr>
							<tr>
								<td><b>Home</b></td>
								<td>
									(<input class="phone_start" type="text" name="addr_tel[]" id="addr_tel1" maxlength="3" size="4">)
									<input type="text" name="addr_tel[]" maxlength="3" id="addr_tel2" size="5"> - 
									<input type="text" name="addr_tel[]" maxlength="4" id="addr_tel3" size="5">
								</td>
							</tr>
							<tr>
								<td><b>Mobile</b></td>
								<td>
									(<input class="phone_start" type="text" name="addr_cell[]" id="addr_cell1" maxlength="3" size="4">)
									<input type="text" name="addr_cell[]" id="addr_cell2" maxlength="3" size="5"> - 
									<input type="text" name="addr_cell[]" id="addr_cell3" maxlength="4" size="5">
								</td>
							</tr>
							<tr>
								<td><b>Work</b></td>
								<td>
									(<input class="phone_start" type="text" name="addr_work[]" id="addr_work1" maxlength="3" value="" size="4">)
									<input type="text" name="addr_work[]" maxlength="3" id="addr_work2" size="5"> - 
									<input type="text" name="addr_work[]" maxlength="4" id="addr_work3" size="5">
								</td>
							</tr>
							<tr>
								<td><b>Email Address</b></td>
								<td><input type="text" id="addr_email" name="addr_email" size="40"></td>
							</tr>
							<tr>
								<td colspan="2"><b>Client Address</b></td>
							</tr>
							<tr>
								<td style="padding-left:15px;">Country<font color="red">*</font></td>
								<td>
									<select name="addr_country" id="addr_country" onchange="loadAreas();">
									<option value=""> --- </option>
									<?php
										while($p=$this->db->fetchAssoc($country_rs))
										{
											echo "<option value='$p[country_id]'>$p[country]</option>";
										}
									?>
								</td>
							</tr>	
							<tr>
								<td style="padding-left:15px;">House Number</td>
								<td><input type="text" id="addr_num" name="addr_num" size="20"></td>
							</tr>	
							<tr>
								<td style="padding-left:15px;">Street</td>
								<td><input type="text" name="addr_st" id="addr_st" size="40"></td>
							</tr>
							<tr>
								<td style="padding-left:15px;">Neighborhood</td>
								<td><input type="text" name="addr_hood" id="addr_hood" size="40"></td>
							</tr>	
							<tr>
								<td style="padding-left:15px;">City<font color="red">*</font></td>
								<td><input type="text" name="addr_city" id="addr_city" size="40"></td>
							</tr>	
							<tr>
								<td style="padding-left:15px;">State / Province<font color="red">*</font></td>
								<td><input type="text" name="addr_state" id="addr_state" size="40" value="HI"></td>
							</tr>	
							
							<tr>
								<td style="padding-left:15px;">Route Area</td>
								<td>
									<select name="addr_area" id="addr_area" onchange="loadAreaZip();">
									<option value=""> --- </option>
									</select>
									<a href="#" id="addArea" onclick="addAreas();return false;">Add Area</a>
								</td>
							</tr>
							<tr>
								<td style="padding-left:15px;">ZIP<font color="red">*</font></td>
								<td><div id="div_zip">
									<select name="addr_zip" id="addr_zip">
									<option value=""> --- </option>
									</select>
									<a href="#" id="addZip" onclick="addZip();return false;">Add Zip</a>
									</div>
								</td>
							</tr>
							<tr valign="top">
								<td><b>Client Notes</b></td>
								<td><textarea rows="3" cols="30" name="addr_clientNotes" id="addr_clientNotes"></textarea></td>
							</tr>
							<tr valign="top" id="referrer_line">
								<td><b>Referrer No#</b></td>
								<td>
									<div id="referral-input">
										<input type="text" id="referral_acct_no" name="referral_acct_no" onblur="check_referralid();">
										<span id="referalDiv"></span>
									</div>
									<div id="referral-search">[ <a href="#" onclick="findReferrals();return false">Search Names</a> ]</div>
								</td>
							</tr>
						</table>
						<div id="referred">
							
							
						</div>
					</div>
					</td>
				</tr>
				</table>
			</td>
			<td width="50%" valign="top">
				<table width="100%" border="0" cellspacing="0" cellpadding="3">
				<tr>
					<td colspan="2" class="table_headers">Job Order Details</td>
				</tr>
				<tr>
					<td width="200px"><b>Job Date</b><font color="red">*</font></td>
					<td>
						<input type="text" name="job_date" id="job_date" class="pickers" value="<?php echo date("m/d/Y");?>" default="<?php echo date("m/d/Y");?>">
					</td>
				</tr>
				<tr>
					<td width="120px"><b>No. of Boxes</b><font color="red">*</font></td>
					<td>
						<input type="text" size="4" value="1" style="text-align:center;" id="box" name="boxes" maxlength="4">
					</td>
				</tr>
				<tr>
					<td><b>Job Type</b><font color="red">*</font></td>
					<td>
						<select name="job">
							<option value="DEL">Delivery</option>
							<option value="PU">Pick-up</option>
						</select>
					</td>
				</tr>
				<tr>
					<td width="120px"><b>Packing Nos.</b></td>
					<td>
						<input type="text" id="packing" name="packing" >
						<br><font size="1">(Separate each packing number with a comma "<b>,</b>" ex. <b>1035,1034</b>)</font>
					</td>
				</tr>
				<tr>
					<td><b>Product type</b></td>
					<td>
						<select name="product" id="product">
						<?php
							while($p=$this->db->fetchAssoc($product_rs))
							{
								echo "<option value='$p[product_id]'>$p[product_name]</option>";
							}
						?>
					</td>
				</tr>
				<tr>
					<td width="120px"><b>Consignee Name</b></td>
					<td>
						<input type="text" id="packing" name="consignee" size="40" >
					</td>
				</tr>
				<tr>
					<td><b>Landline</b></td>
					<td>
						(<input type="text" name="landline[]" maxlength="3" size="4">)
						<input type="text" name="landline[]" maxlength="3" size="5"> - 
						<input type="text" name="landline[]" maxlength="4" size="5">
					</td>
				</tr>
				<tr>
					<td><b>cellphone</b></td>
					<td>
						<input class="cellphone" type="text" name="mobile"  size="20">
					</td>
				</tr>
				<tr>
					<td><b>Destination Country</b><font color="red">*</font></td>
					<td>
						<select name="addr_countryDest" id="addr_countryDest" onchange="loadDest();">
						<option value=""> --- </option>
						<?php
							mysql_data_seek($country_rs,0);
							while($p=$this->db->fetchAssoc($country_rs))
							{
								echo "<option value='$p[country_id]'>$p[country]</option>";
							}
						?>
					</td>
				</tr>
				<tr>
					<td valign="top"><b>Destination Address</b></td>
					<td><textarea name="addr" rows="3" cols="35"></textarea></td>
				</tr>
				
				<tr>
					<td><b>Destination State/Province</b></td>
					<td>
						<select name="destiny" id="destiny">
							<option value=""> --- Not Specified ---</option>
							<?php
							while($p=$this->db->fetchAssoc($dest_rs))
							{
								echo '<option value='.$p['code_id'].'>'.$p['destination'].'</option>';
							}
						?>	
						</select>
					</td>
				</tr>
				<tr>
					<td><b>Business Source</b></td>
					<td>
						<select name="branch" id="branch">
							<option value="">---</option>
						</select>
					</td>
				</tr>			
				<tr>
					<td><b>Assigned Driver / Agent</b></td>
					<td><div id="div_agent">
						<select name="driver_id" id="driver_id">
							<option value=""> --- any driver in route --- </option>
							<?
							while($p=$this->db->fetchAssoc($driver_rs))
							{
								echo "<option value='$p[user_id]'>$p[name]</option>";
							}
						?>
						</select>
						</div>
					</td>
				</tr>
				<tr>
					<td valign="top"><b>Job Order Comments</b></td>
					<td><textarea name="comments" rows="3" cols="35"></textarea></td>
				</tr>
				<tr>
					<td colspan="2">
						<input type="checkbox" name="sendSMS" id="sendSMS" value="yes"> Send Me SMS Alerts (for box pick-ups only)
					</td>
				</tr>
			</table>
			<p align="right"><input type="button" id="submitButton" name="btnSave" value="Record Job Order" class="buttons" onclick="submitJobOrder();"></p>
			<font color="red">* Required Fields</font>
			</td>
		</tr>
		<?php
		if($this->config->map_active){
		?>
		<tr>
			<td colspan="2" class="table_headers">Map Plotting</td>
		</tr>
		<tr>
			<td colspan="2" align="center" style="border-top:1px solid;padding:10px;">
				<p align="left"><input type="checkbox" id="autoplot" name="autoplot" checked> Autoplot (<a href="#" onclick="centerMarker();return false;">Center Marker</a>)</p>
				<div id="map" style="width: 95%; height: 500px;">
					<img src="http://maps.google.com/maps/api/staticmap?center=21.2893344,-157.8246546&zoom=13&size=900x300&sensor=false">
				</div>
				<input type="hidden" name="geotxt" id="geoTxt">
			</td>
		</tr>
		<?php }?>
	</table>
	</form>
</div>
