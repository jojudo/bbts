<div id="middle_content">
	<p class="page_header"><img src="images/deliver.gif">Job Order Reports</p>
	<div id="breadcrumb"><a href="index.php">Home</a> >> <b>Job Done Reports</b></div>
	<form method="POST" action="" id="statusForm">
	<table cellpadding="3" cellspacing="0" width="100%" border="0">
			<tr>
				<td width="10%"><b>Dispatch Date</b></td>
				<td><input type="text" name="job_date" class="pickers" value="<?php echo $dispatch_date;?>"></td>
			</tr>
			<tr>
				<td><b>Driver</b></td>
				<td>
						<select name="driver_id">
							<option value=""> --- All Drivers --- </option>
							<?php
								$ctr=0;
								while($p=$this->db->fetchAssoc($driver_rs)){
								$ctr++;
									echo '<option value="'.$p['user_id'].'"'.($driver_id==$p['user_id']?" selected":"").'>'.$p['name'].'</option>';
								}
							?>
						</select>
				</td>
			</tr>
			<tr valign="top">
				<td><b>Area List</b></td>
			</tr>
			<tr>
				<td colspan="2" style="padding-left:10px;">
					<input type="checkbox" class="chkParent" checked> Check / Uncheck All 
					<p><a href="#">hide/unhide</a></p>
					<div id="areaList">
					<fieldset>
						<?php
						if(mysql_num_rows($area_rs))
						{
							echo '<table cellpadding="3" cellspacing="0" width="100%"><tr>';
							$ctr=0;
							if(count($areas)==0)
								$countall = true;
								
							while($p=$this->db->fetchAssoc($area_rs)){$ctr++;
								if($countall)
									$areas[] = $p['area_id'];
								?>
								<td>
									<input type="checkbox" name="areas[]" value="<?php echo $p['area_id'];?>"  <?php echo in_array($p['area_id'],$areas)?" checked":"";?>>
									<?php echo $p['area_name'];?>
								</td>
							<?php
							if($ctr%8==0)
								echo "</tr><tr>";
							}
							echo "</tr></table>";
						}
						?>
						</fieldset>
						</div>
					
				</td>
			</tr>
			<tr>
				<td colspan="2"><input type="submit" name="action" id="btnSubmit" value="View Report"></td>
			</tr>
		</table>
		<div id="reportArea"></div>
	</form>
</div>