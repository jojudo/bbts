<div id="middle_content">
	<p class="page_header"><img src="images/deliver.gif">Job Order Reports</p>
	<div id="breadcrumb"><a href="index.php">Home</a> >> <b>Dispatch Report</b></div>
	<form method="POST" action="index.php?mod=jobOrder">
		<table cellpadding="3" cellspacing="0" width="100%" border="0">
			<tr>
				<td width="10%"><b>Dispatch Date</b></td>
				<td><input type="text" name="job_date" class="pickers" value="<?php echo $dispatch_date;?>"></td>
			</tr>
			<tr>
				
			</tr>
			<tr>
				<td><b>Driver</b></td>
				<td>
						<select name="driver_id">
							<option value="0"> --- All Drivers --- </option>
							<?php
								$ctr=0;
								while($p=$this->db->fetchAssoc($driver_rs)){
								$ctr++;
									echo '<option value="'.$p['user_id'].'"'.($driver_id==$p['user_id']?" selected":"").'>'.$p['name'].'</option>';
								}
							?>
						</select>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<fieldset>
						<legend><b>ENCODED AREAS</b></legend>
						<?php
						if(mysql_num_rows($area_rs))
						{
							echo '<table cellpadding="3" cellspacing="0"><tr>';
							$ctr=0;
							if(count($areas)==0)
								$countall = true;
								
							while($p=$this->db->fetchAssoc($area_rs)){$ctr++;
								if($countall)
									$areas[] = $p['area_id'];
								?>
								<td width="25px" valign="top">
									<input type="checkbox" name="areas[]" value="<?php echo $p['area_id'];?>"  <?php echo in_array($p['area_id'],$areas)?" checked":"";?>>
								</td><td valign="top">
									<?php echo $p['area_name'];?>
								</td>
							<?php
							if($ctr%12==0)
								echo "</tr><tr>";
							}
							echo "</tr></table>";
						}
						?>
					</fieldset>
				</td>
			</tr>
			<tr>
				<td colspan="2"><input type="submit" name="action" id="btnSubmit" value="Search Dispatch"></td>
			</tr>
		</table>
		<div id="dispatchResults">
		<?php
			foreach ($areas as $area) 
			{
				$dispatch_rs = $this->getDispatchRS($area,$dispatch_date,$driver_id);
				if(mysql_num_rows($dispatch_rs)!=0){
					echo '<p class="sub_header">'.strtoupper($this->getAreaname($area)).'</p>';	
					echo '<table cellpadding="3" cellspacing="0" border="0" class="table_bordered" width="95%">';
					echo '<tr class="table_headers" align="center"><td width="2%"></td>';
					echo '<td width="8%;">&nbsp;<b>Job Date</b></td>';
					echo '<td width="15%"><b>Caller</b></td>';
					echo '<td width="15%"><b>Contact Nos#</b></td>';
					echo '<td width="5%">Boxes</td>';
					echo '<td width="25%"><b>Job Address</b></td>';
					echo '<td width="8%"><b>Job Type</b></td>';
					echo '<td width="28%"><b>Notes</b></td></tr>';
						
					$i=0;
					while($p=$this->db->fetchAssoc($dispatch_rs)){
						$i++;
						$phone_arr = explode("|",$p['phones']);
						echo '<tr valign="top">';
						echo '<td class="cell_border_right cell_border_bottom"><b>'.$i.'</b>.</td>';
						echo '<td class="cell_border_right cell_border_bottom" style="min-height:40px;"><a href="'.mex::long_addr('index.php?mod=jobOrder&action=edit&jid='.$p['job_id']).'" target="editPage" title="Click to modify in a new page">'.$p['jobDate'].'</a></td>';
						echo '<td class="cell_border_right cell_border_bottom">'.($p['name']!=""?utf8_decode($p['name']):"--- NO NAME ---").'</td>';
						echo '<td class="cell_border_right cell_border_bottom">';
						echo '<table cellspacing="0" cellpadding="0">';
						echo $phone_arr[0]!=""?"<tr bgcolor='$bg'><td><b>Home</b> </td><td>: ".mex::phoneFormat("-",$phone_arr[0])."</td></tr>":"";
						echo $phone_arr[1]!=""?"<tr bgcolor='$bg'><td><b>Work</b> </td><td>: ".mex::phoneFormat("-",$phone_arr[1])."</td></tr>":"";
						echo $phone_arr[2]!=""?"<tr bgcolor='$bg'><td><b>Cell</b> </td><td>: ".mex::phoneFormat("-",$phone_arr[2])."</td></tr>":"";
						echo '<tr bgcolor="'.$bg.'"><td colspan="2">&nbsp;</tr></table></td>';						
						echo '<td class="cell_border_right cell_border_bottom" align="center">'.$p['box_num'].'</td>';
						echo '<td class="cell_border_right cell_border_bottom">'.mex::textToParagraph($p['address']).'</td>';
						
						echo '<td class="cell_border_right cell_border_bottom" align="center"><b>'.mex::job_type($p['type']).'</b></td>';
						
						echo '<td class="cell_border_bottom">'.($p['driver']!=""?'<b><font color="red">Assigned to : '.$p['driver'].'</font></b><br>':'').mex::textToParagraph($p['comments']).'</td>';
						echo '</tr>';
					}
					echo '</table>';
					
				}
				
				$this->db->freeMe($dispatch_rs);
			}
		?>
			<table cellpadding="3" cellspacing="0">
				<tr>
					
				</tr>
			</table>
		</div>
	</form>
</div>