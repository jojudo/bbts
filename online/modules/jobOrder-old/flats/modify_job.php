<div id="middle_content">
	<p class="page_header"><img src="images/clipper.png" style="vertical-align:bottom;">Job Orders</p>
	<div id="breadcrumb"><a href="index.php">Home</a> >> <a href="<?php echo mex::long_addr("index.php?mod=jobOrder&action=list_jobs");?>">Job Orders</a> >> <b>Modify Job Order</b></div>
	<form id="jobForm" name="jobForm" onsubmit="return false;">
	<?php echo $message!=""?"<div class=\"alert_msg\">$message</div>":"";?>
	<table width="100%" cellpadding="3" cellspacing="0" class="table_bordered" >
		<tr>
			<td width="50%" valign="top" style="border-right:1px solid;">
			<div id="clientDetail">
				<table cellpadding="3" cellspacing="0" width="100%" border="0" height="100%">
				<tr>
					<td colspan="2" class="table_headers">Client Details</td>
				</tr>
				<tr>
					<td colspan="2" style="padding-left:20px;">
					<div id="client_details">
						<table width="100%" border="0" cellpadding="3" cellspacing="0" >
							<tr>
								<td valign="top"  width="150px"><b>Customer Name</b></td>
								<td>
									<?php echo $client_arr['name'];?> <!--[<a href="#">change</a>]-->
									<input type="hidden" name="cid" value="<?php echo $job_arr['customer_id'];?>" >
									<input type="hidden" name="jid" value="<?php echo $jid;?>" > 
								</td>
							</tr>
							<?php if($client_arr['phone_home']){?>
							<tr>
								<td><b>Home</b></td>
								<td><?php echo mex::phoneFormat("-",$client_arr['phone_home']);?></td>
							</tr>
							<?php
							} if($client_arr['phone_cell']){
							?>
							<tr>
								<td><b>Mobile</b></td>
								<td><?php echo mex::phoneFormat("-",$client_arr['phone_cell']);?></td>
							</tr>
							<?php } if($client_arr['phone_work']){?>
							<tr>
								<td><b>Work</b></td>
								<td><?php echo mex::phoneFormat("-",$client_arr['phone_work']);?></td>
							</tr>
							<?php } if($client_arr['email']){?>
							<tr>
								<td><b>Email Address</b></td>
								<td><?php echo $client_arr['email'];?></td>
							</tr>
							<?php }if($client_arr['notes']){?>
							<tr valign="top">
								<td><b>Client Notes</b></td>
								<td><?php echo mex::textToParagraph($client_arr['notes']);?></td>
							</tr>	
							<?php }?>
							<tr>
								<td colspan="2"><b>Job Address</b></td>
							</tr>
							<tr>
								<td style="padding-left:15px;">Country<font color="red">*</font></td>
								<td>
									<select name="addr_country" id="addr_country">
									<option value=""> --- </option>
									<?
										while($p=$this->db->fetchAssoc($country_rs))
										{
											if($job_arr['country_source']==$p['country_id'])
												$selected=" selected";
											else
												$selected="";
												
											echo "<option value='$p[country_id]' $selected>$p[country]</option>";
										}
									?>
								</td>
							</tr>	
							<tr>
								<td style="padding-left:15px;">House Number</td>
								<td><input type="text" id="addr_num" name="addr_num" size="20" value="<?php echo $job_arr['number'];?>"></td>
							</tr>	
							<tr>
								<td style="padding-left:15px;">Street</td>
								<td><input type="text" name="addr_st" id="addr_st" size="40" value="<?php echo $job_arr['street'];?>"></td>
							</tr>
							<tr>
								<td style="padding-left:15px;">Neighborhood</td>
								<td><input type="text" name="addr_hood" id="addr_hood" size="40" value="<?php echo $job_arr['hood'];?>"></td>
							</tr>	
							<tr>
								<td style="padding-left:15px;">City<font color="red">*</font></td>
								<td><input type="text" name="addr_city" id="addr_city" size="40" value="<?php echo $job_arr['city'];?>"></td>
							</tr>	
							<tr>
								<td style="padding-left:15px;">State / Province<font color="red">*</font></td>
								<td><input type="text" name="addr_state" id="addr_state" size="40" value="<?php echo $job_arr['state'];?>"></td>
							</tr>	
							
							<tr>
								<td style="padding-left:15px;">ZIP<font color="red">*</font></td>
								<td><div id="div_zip">
									<select name="addr_zip" id="addr_zip">
									<option value=""> --- </option>
									<?php
										while($p=$this->db->fetchAssoc($zip_rs)){
											if($job_arr['zip_id']==$p['code'])
												$selected = " selected";
											else
												$selected="";
												
											echo '<option value="'.$p['code'].'" '.$selected.'>'.$p['code'].'</option>';
										}
									?>
									</select>
									</div>
								</td>
							</tr>
						</table>
					</div>
					</td>
				</tr>
				</table>
				<p style="padding-top:50px">
					<font color="red" size="1">*Denotes Required Fields</font><br>
					<font color="red" size="1">Client Details can be modified <b><u>ONLY</u></b> in the clients page</font>
				</p>
			</td>
			<td width="50%" valign="top">
				<table width="100%" border="0" cellspacing="0" cellpadding="3">
				<tr>
					<td colspan="2" class="table_headers">Job Order Details</td>
				</tr>
				<tr>
					<td width="200px"><b>Job Date</b><font color="red">*</font></td>
					<td>
						<input type="text" name="job_date" class="pickers" value="<?php echo $job_arr['job_date'];?>">
					</td>
				</tr>
				<tr>
					<td width="120px"><b>No. of Boxes</b><font color="red">*</font></td>
					<td>
						<input type="text" size="4" value="<?php echo $job_arr['box_num'];?>" style="text-align:center;" id="box" name="boxes" maxlength="4">
					</td>
				</tr>
				<tr>
					<td><b>Job Type</b></td>
					<td>
						<select name="type">
							<option value="1" <?php echo $job_arr['type']==1?"selected":"";?> >Box Delivery</option>
							<option value="2" <?php echo $job_arr['type']==2?"selected":"";?>>Box Pick-up</option>
						</select>
					</td>
				</tr>
				<tr>
					<td><b>Job Done</b></td>
					<td><input type="radio" name="done" value="1" <?php echo $job_arr['job_done']==1?"checked":"";?> >YES <input type="radio" name="done" value="0" <?php echo $job_arr['job_done']!=1?"checked":"";?>>NO</td>
				</tr>
				<tr>
					<td width="120px"><b>Packing Nos.</b></td>
					<td>
						<input type="text" id="packing" name="packing" value="<?php echo $job_arr['packing_nos'];?>">
						<br><font size="1">(Separate each packing number with a comma "<b>,</b>" ex. <b>103A,103B</b>)</font>
					</td>
				</tr>
				<tr>
					<td><b>Destination Country</b><font color="red">*</font></td>
					<td>
						<select name="addr_countryDest" id="addr_countryDest">
							<option value=""> --- </option>
						<?php
							mysql_data_seek($country_rs,0);
							while($p=$this->db->fetchAssoc($country_rs))
							{
								if($job_arr['country_dest']==$p['country_id'])
									$selected= " selected";
								else
									$selected = "";
									
								echo "<option value='$p[country_id]' $selected>$p[country]</option>";
							}
						?>
					</td>
				</tr>
				<tr>
					<td valign="top"><b>Destination Address</b></td>
					<td><textarea name="addr" rows="3" cols="35"><?php echo $job_arr['destination'];?></textarea></td>
				</tr>
				
				<tr>
					<td><b>Destination State/Province</b></td>
					<td>
						<select name="destiny" id="destiny">
							<option value=""> --- Not Specified ---</option>
							<?php
							while($p=$this->db->fetchAssoc($dest_rs))
							{
								if($p['code_id']==$job_arr['code_id'])
									$selected = " selected";
								else
									$selected = "";
									
								echo '<option value="'.$p['code_id'].'" '.$selected.'>'.'('.$p['code'].') '.$p['destination'].'</option>';
							}
						?>	
						</select>
					</td>
				</tr>
				<tr>
					<td><b>Business Source</b></td>
					<td>
						<select name="branch" id="branch">
							<option value="">---</option>
							<?php
							while($p=$this->db->fetchAssoc($branch_rs))
							{
								if($p['branch_id']==$job_arr['branch_id'])
									$selected = " selected";
								else
									$selected = "";
									
								echo '<option value="'.$p['branch_id'].'" '.$selected.'>'.$p['branch_name'].'</option>';
							}
						?>	
						</select>
					</td>
				</tr>			
				<tr>
					<td><b>Assigned Driver / Agent</b></td>
					<td><div id="div_agent">
						<select name="driver_id" id="driver_id">
							<option value=""> --- Any Driver in Route --- </option>
							<?php
							while($p=$this->db->fetchAssoc($driver_rs))
							{
								if($p['user_id']==$job_arr['driver_id'])
									$selected = " selected"; 
								else
									$selected = "";
								echo "<option value='$p[user_id]' $selected>$p[name]</option>";
							}
						?></div>
						</select>
					</td>
				</tr>
				<tr>
					<td valign="top"><b>Job Order Comments</b></td>
					<td><textarea name="comments" rows="5" cols="40"><?php echo strip_tags($job_arr['comments']);?></textarea></td>
				</tr>
				<tr valign="top">
					<td><b>Payment Details</b></td>
					<td>
						<?php
						if(mysql_num_rows($payment_rs)!=0){
							$payment_arr = $this->db->fetchAssoc($payment_rs);
							$total = $payment_arr['cash'] + $payment_arr['check'] + $payment_arr['credit'];
							echo '<table cellpadding="3" cellspacing="0">';
							echo '<tr>';
							echo '<td width="100px"><b>Cash</b></td>';
							echo '<td>'.number_format($payment_arr['cash'],2).'</td>';
							echo '</tr>';
							echo '<tr>';
							echo '<td><b>Check</b></td>';
							echo '<td>'.number_format($payment_arr['check'],2).'</td>';
							echo '</tr>';
							echo '<tr>';
							echo '<td><b>Credit Card</b></td>';
							echo '<td>'.number_format($payment_arr['credit'],2).'</td>';
							echo '</tr>';
							echo '<tr>';
							echo '<td><b>Total Payment</b></td>';
							echo '<td>'.number_format($total,2).'</td>';
							echo '</tr>';
							if($payment_arr['notes']){
								echo '<tr valign="top">';
								echo '<td><b>Payment Notes</b></td>';
								echo '<td>'.mex::textToParagraph($payment_arr['notes']).'</td>';
								echo '</tr>';
							}
							echo '</table>';
						}else{
							echo '<input type="radio" name="paid" value="1" > Mark as Paid';
							echo '<input type="radio" name="paid" value="2" > Partial Payment';
							echo '<input type="radio" name="paid" value="0" checked> Mark as unpaid';
						}
					?>
					</td>
				</tr>
			</table>
			</td>
		</tr>
	</table>
	<?php
		if(mysql_num_rows($box_rs)!=0){
			echo '<p class="sub_header">Included Boxes</p>';
			echo '<table cellpadding="3" cellspacing="0" width="100%" class="table_bordered">';
			echo '<tr class="table_headers">';
			echo '<td width="20px"></td>';
			echo '<td>Packing No</td>';
			echo '<td>Consignee</td>';
			echo '<td>Delivery Address</td>';
			echo '<td>Status</td>';
			echo '<td></td>';
			echo '</tr>';
			$i=0;
			while($b=$this->db->fetchAssoc($box_rs)){
				$i++;
					
				echo '<tr class="'.$className.'">';
				echo '<td><b>'.$i.'</b></td>';
				echo '<td>'.$b['packing_no'].'</td>';
				echo '<td>'.$b['consignee'].'</td>';
				echo '<td>'.$b['delivery_address'].'</td>';
				echo '<td>'.mex::boxStatus($b['box_id']).'</td>';
				echo '<td><a href="'.mex::long_addr("index.php?mod=boxes&action=modify&bid=".$b['box_id']).'" title="Edit Box"><img src="images/edit.png" border="0"></a> <img src="images/delete.png"></td>';
				echo '</tr>';	
			}
			
			echo '</table>';
		}
		
	?>
	<p align="right">
		<input type="button" class="btn" id="submitButton" name="btnSave" value="Update Job Order" class="buttons" onclick="updateJobOrder();" style="height:50px;width:150px;">
	</p>
	</form>
</div>
