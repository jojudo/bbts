<?php 
		function __construct(&$db){
			$action = mex::request("action");
			$action = $action?$action:"main";
			$this->action = $action;
			$this->db = $db;
			$this->render();
			
		}
				
		function render(){
			switch ($this->action){
				case "receive" :
					require("modules".DS.$this->mod.DS."flats".DS."container.php");
					break;
				case "save_job" :
					$this->save_job();
					break;
				case "updateJob" :
					$this->updateJob();
					break;
				case "edit" :
					$this->edit_job();
					break;
				case "delete" :
					$this->removeJob();
					break;
				//reports
				case "dispatch_report":
					$this->dispatch_report();
					break;
				case "Search Dispatch":
					$this->dispatch_report();
					break;
				case "status_report":
					$this->status_report();
					break;
				case "carryOver_report":
					$this->carryOver_report();
					break;
				case "get_status_report":
					$this->get_status_report();
					break;
				//management
				case "list":
					$this->list_jobs();
					break;
				case "search":
					$this->jobSearch();
					break;
				case "view":
					$this->viewDetails();
					break;
				case "referral_search":
					$this->referral_search();
					break;
					
				case "consigneeCopy";
					$this->consigneeCopy();
					break;
					
				default:
					$this->new_job();
					break;
			}
		}
?>