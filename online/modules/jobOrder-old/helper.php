<?php
	class moduleHelper extends helperController{
		
		function __construct($db){
			parent::__construct();
			$action = $this->action;
			$this->$action();
			
		}
		
		function main(){
			$this->new_job();
		}
		
		//----------------------------- creation of new job ----------------------------------------
		
		function new_job(){
			//google map loaders
			UI::set_js($this->js.'new-jobOrder.js');
			UI::set_stylesheet($this->styles.DS."jobOrder.css");
			
			//select all countries being served
			$sql = "select country_id,country from ".PREFIX."countries order by country ASC";
			$arg['country_rs'] = $this->db->queryMe($sql);
			
			//select all products
			$sql = "select product_id,product_name from ".PREFIX."products order by product_name DESC";
			$arg['product_rs'] = $this->db->queryMe($sql);
			
			//require("modules".DS.$this->mod.DS."flats".DS."new-job.php");
			$this->load_flat('new-job',$arg);
			$this->db->freeMe($arg['country_rs']);
			$this->db->freeMe($arg['product_rs']);
		}
		
		function name_match(){
			//matching search for generic purpose
			$lname = mex::secure_get("lname");
			$fname = mex::secure_get("fname");
			$start = intval(mex::secure_get("start"));
			$fld = mex::secure_get("fld");
			$limit = 10;
			
			$start=$start?$start:$start;
			
			//count total search match
			$sql = "select count(customer_id) as total 
							from ".PREFIX."customer 
							where lname like '$lname%' and 
							fname like '$fname%'";
			
			$arr= $this->db->queryFirst($sql);
			$total = $arr['total'];
			
			$sql = "select 
						lname,
						fname,
						customer_id
					from 
						".PREFIX."customer
					where
						lname like '$lname%' and
						fname like '$fname%'
						
					order by
						lname ASC
					limit
						$start,$limit";
			
			$rs = $this->db->queryMe($sql);
			//this is an xml type output
			$prev = $start-$limit;
			$next = $start+$limit;
			$next = $next<$total?$next:0;
			
			$con = "\n";
			$str .= '<names total="'.$total.'" previous="'.$prev.'" next="'.$next.'" fld="'.$fld.'" lname="'.$lname.'" fname="'.$fname.'">';	
			if(mysql_num_rows($rs))
			{	
				while($p=$this->db->fetchAssoc($rs)){
					$str.='<name>'.$con;
					$str.='<firstname>'.htmlentities(ucfirst(strtolower($p['fname']))).'</firstname>'.$con;
					$str.='<lastname>'.htmlentities(ucfirst(strtolower($p['lname']))).'</lastname>'.$con;
					$str.='<ID>'.$p['customer_id'].'</ID>'.$con;
					$str.='</name>'.$con;
					
				}
			
			}
			$str.='</names>';
			
			$this->db->freeMe($rs);
			
			header("Content-Type:text/xml");
			echo '<?xml version="1.0" encoding="iso-8859-1"?>'.$con;
			echo $str.$con;
			return ;
		}
		
		function save_job(){

			$cid = mex::secure_post("cid");
			$status = 1;
			$continue = true;
			
			
			$form_arr = array();
			$prefix = "addr_";
			
			//select the address
			$addr_num = mex::secure_post($prefix."num");
			$addr_st = mex::secure_post($prefix."st");
			$addr_hood = mex::secure_post($prefix."hood");
			$addr_city = mex::secure_post($prefix."city");
			$addr_state = mex::secure_post($prefix."state");
			$addr_zip = mex::secure_post($prefix."zip");
			
			//generic variables
			$email = mex::secure_post($prefix."email");
			$geotxt = str_replace(array('(',')'),'',mex::secure_post("geotxt"));
			$entry_date = date("Y-m-d");
			$clientNotes = mex::secure_post("addr_clientNotes");
			
			//select the phones
			$tel = $_POST[$prefix."tel"];
			$cell = $_POST[$prefix."cell"];
			$work = $_POST[$prefix."work"];
			
			$phone_tel = "";
			$phone_cell = "";
			$phone_work = "";
			
			if($tel[0]!="" && $tel[1]!="" && $tel[2]!="")
				$phone_tel = "$tel[0]-$tel[1]-$tel[2]";
			if($cell[0]!="" && $cell[1]!="" && $cell[2]!="")
				$phone_cell = "$cell[0]-$cell[1]-$cell[2]";
			if($work[0]!="" && $work[1]!="" && $work[2]!="")
				$phone_work = "$work[0]-$work[1]-$work[2]";
			
			if(is_numeric($cid))
			{
				if($cid==0)
				{
					$continue = false;
					$message = "Please Select Existing Client.";
				}
				else
				{
					$change = 	mex::secure_post("addr_change");
					//if follow changes set to on
					if($change=="on"){					
						$sql = "update ".PREFIX."customer set 
									number = '$addr_num',
									street = '$addr_st',
									hood = '$addr_hood',
									city = '$addr_city',
									state = '$addr_state',
									zip = '$addr_zip',
									email = '$email',
									phone_home = '$phone_tel',
									phone_work = '$phone_work',
									phone_cell = '$phone_cell',
									geocode='$geotxt',
									notes='$clientNotes'
								where customer_id = '$cid'";
						$this->db->queryMe($sql);
					}
				}
			}
			else if($cid=="")
			{
				$lname = mex::secure_post("lname");
				$fname = mex::secure_post("fname");
				if($lname=="" && $fname==""){
					$continue = false;
					$message = "Please enter name of New Client";
				}
				else{
					//insert it into customer table
					$sql = "insert into ".PREFIX."customer 
								(lname,fname,email,
									number,street,
									hood,city,state,
									zip,phone_home,
									phone_work,phone_cell,
									date_added,user_id,notes,geocode
									)
								values(
									'$lname','$fname','$email',
									'$addr_num','$addr_st',
									'$addr_hood','$addr_city',
									'$addr_state','$addr_zip',
									'$phone_tel','$phone_cell',
									'$phone_work','$entry_date',
									'$_SESSION[user_id]','$clientNotes','$geotxt'
								)";
					
					$this->db->queryMe($sql);
					$cid = mysql_insert_id();
					
					//new feature .. track client referrals
					$referralid = mex::secure_post("referralid");
					if($referralid){
						include_once($this->includes.'class.referral.php');
						$referrals = new referral;
						
						//create a referrer account for the referrer if one does not exist
						$referrals->createreferrals($referralid,0);
						
						//create refferal id for the referred also
						$referrals->createreferrals($cid,$referralid);	
						
					}
				}
				
			}
			else{
				$message =  "Unable to determine action";
				$continue = false;
			}
			
			//start to input new job order
			if($continue){
				//insert new job order
				$boxes = mex::secure_post("boxes");
				$job_date = mex::reformatDate(mex::secure_post("job_date"));
				$branch = mex::secure_post("branch");
				$destiny = mex::secure_post("destiny");
				$status = mex::secure_post("job");
				$driver_id = mex::secure_post("driver_id");
				$comments = mex::secure_post("comments");
				$packing = mex::secure_post("packing");
				$addr = mex::secure_post("addr");
				$type= $status=="DEL"?1:2;
				$landline_arr = $_POST['landline'];
				
				if(is_array($landline_arr))
					$landline = implode("-",$landline_arr);
					
				$mobile = mex::secure_post("mobile");
				$consignee = mex::secure_post("consignee");
				$product = mex::secure_post("product");
				
				//insert new job details
				$sql = "insert into ".PREFIX."joborder
							(	customer_id,branch_id,
								job_date,code_id,
								box_num,comments,status,
								type,user_id,entry_date,
								packing_nos,destination,
								consignee,landline,mobile,
								product_id,
								number,street,city,state,
								zip_id,hood,driver_id,geocode
							)values(
								'$cid','$branch',
								'$job_date','$destiny',
								'$boxes','$comments','$status',
								'$type','$_SESSION[user_id]','$entry_date',
								'$packing','$addr',
								'$consignee','$landline','$mobile',
								'$product',
								'$addr_num','$addr_st','$addr_city','$addr_state',
								'$addr_zip','$addr_hood','$driver_id','$geotxt'
							)";
				$this->db->queryMe($sql);
				
				$job_id =  mysql_insert_id();
				$smsNotice = mex::secure_post("sendSMS");
			
				if($smsNotice=="yes"){
					//insert sms notice confirmation
					$sql = "insert into ".PREFIX."joborder_alerts (job_id,send_sms) values ($job_id,1)";
					$this->db->queryMe($sql);
				}
			
				
				$status =  1;
			}
			
			$json_arr['status'] = $status;
			$json_arr['msg'] = $message;
			$json_arr['cid'] = $cid;
			
			echo json_encode($json_arr);
		}
		
		//-------------------------------------------------- Box Direct Encoding Start ----------------------------
		function direct_encode(){
			//show encode form
			UI::set_js($this->js.'new-jobOrder.js');
			UI::set_stylesheet($this->styles.DS."jobOrder.css");
			
			$sql = "select country_id,country from ".PREFIX."countries order by country ASC";
			$arg['country_rs'] = $this->db->queryMe($sql);
			
			$this->load_flat('direct_encode',$arg);			
			$this->db->freeMe($arg['country_rs']);
		}
		
		
		function encodeBoxForm(){
			$args['boxes'] = mex::secure_get("boxes");
			
			//select destinationation code
			$sql = "select code,destination,code_id from ".PREFIX."destinationcodes order by left(code,1) ASC, substring(code,2)+0 ASC";
			$args['dest_rs'] = $this->db->queryMe($sql);
			
			//select available products
			$sql = "select price_full,product_id,product_name from ".PREFIX."products order by product_id ASC";
			$args['product_rs'] = $this->db->queryMe($sql);

			//select containers
			$sql = "select container_no,container_id,trip_code,trip_no
					from 
						".PREFIX."container as c left join
						".PREFIX."shipment as s using (shipment_id)
					order by est_departure DESC
					limit 0,10";
			
			$args['shipment_rs'] = $this->db->queryMe($sql);
			$this->load_flat("encode-final",$args);
			//$this->db->freeMe($dest_rs);
		}
		
		function consigneeCheck(){
			$cid = mex::secure_get("cid");
			$key = mex::secure_get("key");
			
			$sql = "select distinct b.consignee,box_id 
					from ".PREFIX."boxes as b left join 
						".PREFIX."joborder using (job_id)
					where b.consignee like '$key%' and customer_id=$cid";
			
							
			$rs = $this->db->queryMe($sql);
			
			$json_arr = array();
			while($p=$this->db->fetchAssoc($rs)){
				$json_arr[] = $p;
			}
			
			echo json_encode($json_arr);
			return ;
		}
		
		function chooseConsignee(){
			$bid = mex::secure_get("bid");
			
				$sql = "select distinct b.consignee,box_id,b.contact_no,b.mobile_no,b.delivery_address,b.code_id 
					from ".PREFIX."boxes as b
					where b.box_id=$bid";
			
							
			$arr = $this->db->queryFirst($sql);
			
			echo json_encode($arr);
			return ;
		}
		
		function encode_check(){
			//checkboxes first if there are no duplicates
			$str = mex::secure_post("str");
			$strConvert = str_replace(",","','",$str);
			$strConvert ="'$strConvert'";
			$status = 1;
			$msg = '';
			$sql = "select packing_no
						from ".PREFIX."boxes 
						where 
							packing_no in ($strConvert)";
							
			$str_rs = $this->db->queryMe($sql);
			if(mysql_num_rows($str_rs)!=0){
				//if packing nos exists .. inform user
				$msg = 'Packing Nos# ';
				$i=0;
				while($p = $this->db->fetchAssoc($str_rs)){
					if($i!=0)
						$msg.= ", ";
						
					$msg.= $p['packing_no'];
					$i++;
				}
				$msg.= ' already exists';
				$status=0;
			}else{
				//check if there is an emport packing list
				$str_arr = explode(",",$str);
				if(in_array("",$str_arr)){
					$msg =  "a packing no is empty";
					break;
				}
				
				//check for duplicates
				$end_arr = array();
				$duplicateStr = "";
				$i=0;
				foreach ($str_arr as $no){
					if(in_array($no,$end_arr)){
						$i++;
						if($i!=1)
							$duplicateStr.=", ";
						$duplicateStr.=$no;
					}
					$end_arr[] = $no;
				}
				
				if(!empty($duplicateStr)){
					$msg.= 'There are duplicate entries for Packing Nos# '.$duplicateStr;
					$status = 0;
				}
			}

			$this->db->freeMe($str_rs);
			$json_arr['msg'] = $msg;
			$json_arr['status'] = $status;
			
			echo json_encode($json_arr);
			
			
		}
		
		function finalize_encoding(){
			//save boxes = final encode -- checking supposedly over .. no checking required 
				$pack = $_POST['packing'];
				$prod = $_POST['product'];
				$collected = $_POST['collected'];
				$status = $_POST['boxStatus'];
				$container = $_POST['container'];
				$consignee = $_POST['consignee'];
				$received = $_POST['received'];
				$addr = $_POST['addr'];
				$dest = $_POST['destination'];
				$details = $_POST['details'];
				$total = mex::secure_post("boxes");
				$jid = mex::secure_post("jid");
				
				$today_is=date("Y-m-d H:i:s");
				$ctr=0;
				foreach ($pack as $packing) {
					//start insert boxes
					$home = $_POST['home'.($ctr+1)];
					
					$homeArr = implode("-",$home);
					$homeNum =mex::phoneFormat("-",$homeArr);
					$mobile = mex::secure_post("mob".($ctr+1));
					$boxCount = ($ctr+1)." of ".$total;
					if($status[$ctr]=='WH' && $container[$ctr]==0){
						$date_stored = mex::reformatDate($received[$ctr]);
					}else if ($status[$ctr]=='WH' && $container[$ctr]!=0) {
						$date_loaded = mex::reformatDate($received[$ctr]);
					}elseif ($status[$ctr]=='ARR'){
						$date_arrived = mex::reformatDate($received[$ctr]);
					}else{
						;
					}
					
					$sql = "insert into ".PREFIX."boxes(
										job_id,container_id,packing_no,consignee,
										contact_no,mobile_no,delivery_address,
										product_id,collected,details,image,status,
										date_added,last_updated,user_id,date_arrived,
										recipient,relation,code_id,date_stored,date_loaded,
										box_count,notes
									)values(
										'$jid','$container[$ctr]','$packing','$consignee[0]',
										'$homeNum','$mobile','$addr[$ctr]',
										'$prod[$ctr]','$collected[$ctr]','$details[$ctr]','','$status[$ctr]',
										'$today_is','$today_is','$GLOBALS[current_user]','$date_arrived',
										'','','$dest[$ctr]','$date_stored','$date_loaded',
										'$boxCount','$details[$ctr]'							
									)";
					$this->db->queryMe($sql);
					
					$ctr++;
				}
				
				//new MLM verification system
				//MLM system 
				include_once($this->includes.'class.referral.php');
				$referral = new referral();
				$referral->award_check_referral_jobid($jid);
				
				$json_arr['jid'] = $jid;
				$json_arr['status'] = 1;
				
				echo json_encode($json_arr);
				
		}
		//-------------------------------------------------- Box Direct Encoding End ------------------------------------
		
		function createreferrals($cid,$referrer){
			//get the id of the upline
			if($referrer){
				$sql = "select referrer_id from ".PREFIX."referrer where customer_id = $referrer";
				$arr = $this->db->queryFirst($sql);
				$upline_id = $arr['referrer_id'];
			}else{
				$upline_id = 0;
			}
			
			
			//check if the referrer already has a referrer account
			$sql = "select referrer_id from ".PREFIX."referrer where customer_id = $cid";
			$arr = $this->db->queryFirst($sql);
			if(!$arr['referrer_id']){
				//let us create a referral acct
				$sql = "insert into ".PREFIX."referrer values 
						('',$upline_id,$cid,'','',now(),'')";
				$this->db->queryMe($sql);
				$referrer_id = mysql_insert_id();
				//uniqueID format - R12-001 R(y)-id_no
				$referral_no = "R".date("y")."-".str_pad($referrer_id,5,0,STR_PAD_LEFT);				
				
				//insert the customer into the referrers records
				$sql = "update ".PREFIX."referrer
							set acct_no = '$referral_no' 
							where referrer_id = $referrer_id";
							
				$this->db->queryMe($sql);
				
			}
		}
		
		function removeJob(){
			$jid = mex::secure_post("jid");
					
			$sql = "delete from ".PREFIX."joborder where job_id='$jid'";
			$this->db->queryMe($sql);

			$sql = "select file_name from ".PREFIX."files as f 
					where exists(select box_id from ".PREFIX."boxes where 
									box_id = f.box_id and 
									job_id = '$jid')";
			
			$file_rs = $this->db->queryMe($sql);
			while($p=$this->db->fetchAssoc($file_rs)){
				unlink(MEX_BASE.DS."images".DS."boxes".DS.$p['file_name']);
			}
			$this->db->freeMe($file_rs);
			
			$sql="delete from  ".PREFIX."boxes where job_id='$jid';";
			$this->db->queryMe($sql);
			
			
			echo 1;
		}
		
		function edit(){
			UI::set_js("modules".DS.$this->mod.DS."js".DS."modify-job.js");
			$jid = mex::secure_get("jid");
			$sql = "select 
						customer_id,branch_id,
						DATE_FORMAT(job_date,'%m/%d/%Y') job_date,
						code_id,destination,box_num,type,job_done,
						comments,number,street,packing_nos,
						city,state,zip_id,hood,status,
						(select country_id from ".PREFIX."zipcodes where code=j.zip_id limit 0,1)
						country_source,(select country_id from ".PREFIX."destinationcodes where code_id=j.code_id)
						country_dest,driver_id
					from 
						".PREFIX."joborder as j
					where
						job_id = '$jid'";
			
			$job_rs = $this->db->queryMe($sql);
			$job_arr = $this->db->fetchAssoc($job_rs);
			
			//client details
			$sql = "select  concat(fname,' ',lname) as name,
							phone_home,phone_work,phone_cell,
							email,notes
						from ".PREFIX."customer 
						where 
							customer_id = '$job_arr[customer_id]'";
			$client_rs = $this->db->queryMe($sql);
			$client_arr = $this->db->fetchAssoc($client_rs);
			
			//select all countries being served
			$sql = "select country_id,country from ".PREFIX."countries order by country ASC";
			$country_rs = $this->db->queryMe($sql);
			
			//select all the zip
			$sql = "select code from ".PREFIX."zipcodes	where country_id = '$job_arr[country_source]'";	
			$zip_rs = $this->db->queryMe($sql);
			
			//select all the destinationcodes
			$sql = "select code_id,code,destination from ".PREFIX."destinationcodes	where country_id = '$job_arr[country_dest]'";	
			$dest_rs = $this->db->queryMe($sql);
			
			//select all the branches
			$sql = "select branch_name,branch_id from ".PREFIX."branches	where country_id = '$job_arr[country_source]'";	
			$branch_rs = $this->db->queryMe($sql);
			
			//select all the drivers
			$sql = "select concat(user_lname,', ',user_fname) name,user_id from ".PREFIX."users	
						where country_id = '$job_arr[country_source]' and user_level='2'";	
			$driver_rs = $this->db->queryMe($sql);

			//select all included boxes
			$sql= "select 
						box_id,packing_no,
						delivery_address,consignee
					from 
						".PREFIX."boxes
					where 
						job_id = '$jid'";		
			
			$box_rs = $this->db->queryMe($sql);
			
			//select the payments
			$sql = "select `payment_id`,`cash`,`check`,`credit`,`notes` from ".PREFIX."payments where job_id = '$jid'";
			$payment_rs = $this->db->queryMe($sql);
			
			require("modules".DS.$this->mod.DS."flats".DS."modify_job.php");
			$this->db->freeme($job_rs);
			$this->db->freeme($client_rs);
			$this->db->freeMe($zip_rs);
			$this->db->freeMe($dest_rs);
			$this->db->freeMe($branch_rs);
			$this->db->freeMe($box_rs);
			$this->db->freeMe($payment_rs);
		}
		
		function updateJob(){
			$job_id =mex::secure_post("jid");
			$client_id = mex::secure_post("cid");
			$country_id = mex::secure_post("country_id");
			$number = mex::secure_post("addr_num");
			$street = mex::secure_post("addr_st");
			$hood = mex::secure_post("addr_hood");
			$city = mex::secure_post("addr_city");
			$state = mex::secure_post("addr_state");
			$zip = mex::secure_post("addr_zip");
			
			$job_date = mex::reformatDate(mex::secure_post("job_date"));
			$boxes = mex::secure_post("boxes");
			$packing = mex::secure_post("packing");
			$type = mex::secure_post("type");
			$destination = mex::secure_post("addr");
			$code_id = mex::secure_post("destiny");
			$branch_id = mex::secure_post("branch");
			$driver_id = mex::secure_post("driver_id");
			$comments = mex::secure_post("comments");
			$done = mex::secure_post("done");
						
			$sql = "update ".PREFIX."joborder set
							customer_id = '$client_id',
							job_date = '$job_date',
							code_id = '$code_id',
							destination='$destination',
							box_num = '$boxes',
							comments = '$comments',
							packing_nos = '$packing',
							number = '$number',
							street = '$street',
							hood = '$hood',
							city = '$city',
							state = '$state',
							zip_id= '$zip',
							branch_id = '$branch_id',
							driver_id= '$driver_id',
							job_done='$done',
							type='$type'
						where
							job_id = '$job_id'";
			
			$this->db->queryMe($sql);
			echo 1;
			
		}
		//-------------------------------------- reports -------------------------//
		function dispatch_report(){
			UI::set_js("modules".DS.$this->mod.DS."js".DS."dispatch.js");
			$dispatch_date = mex::secure_post("job_date");
			$driver_id = mex::secure_post("driver_id");
			
			if(!$dispatch_date)
				$dispatch_date = date("m/d/Y");
			
			
			if($driver_id!=0){
				//select driver areas
				$sql = "select group_concat(area_id) as areas from ".PREFIX."driveroute where user_id='$driver_id'";
				$dr_areas = $this->db->queryMe($sql);
				$dr_arr = $this->db->fetchAssoc($dr_areas);
				
				$drs = $dr_arr['areas'];
				$areas = explode(",",$drs);
			}else{
				$areas = $_POST["areas"];
			}
				
			if(!is_array($areas)){
				$areas = array();
			}

						
			//get all driver names
			$sql = "select user_id,concat(user_lname,', ',user_fname) name from ".PREFIX."users where user_level='2'";
			$driver_rs = $this->db->queryMe($sql);
			
			//select area
			$sql = "select area_id,area_name from ".PREFIX."area order by area_name ASC";
			$area_rs = $this->db->queryMe($sql);
			
			require("modules".DS.$this->mod.DS."flats".DS."dispatch.php");
			
			$this->db->freeMe($driver_rs);
			$this->db->freeMe($area_rs);
		}
		
		function status_report(){
			UI::set_js("modules".DS.$this->mod.DS."js".DS."status-report.js");
			$type = mex::secure_post("type");
			$today_is = date("Y-m-d");
			$dispatch_date = date("m/d/Y");
			
			//start with areas 
			$sql = "select area_name,area_id from ".PREFIX."area order by area_name ASC";
			$area_rs = $this->db->queryMe($sql);
			
			//get all driver names
			$sql = "select user_id,concat(user_lname,', ',user_fname) name from ".PREFIX."users where user_level='2' order by user_lname ASC";
			$driver_rs = $this->db->queryMe($sql);
			
			require("modules".DS.$this->mod.DS."flats".DS."status-report.php");
			$this->db->freeMe($area_rs);
			$this->db->freeMe($driver_rs);
			
		}
		
		function get_status_report(){
			//this is a compliment function with the one above
			$areas = $_POST['areas'];
			$jobDate = mex::reformatDate(mex::secure_post("job_date"));
			$driver = mex::secure_post("driver_id");
			
			$areasTxt = implode(",",$areas);
			if($driver==0)
				$insert = "";
			else
				$insert = "and (j.driver_id='$driver')";
				
			$sql = "select area_name,area_id from ".PREFIX."area as a
						where 
							exists(
								select job_id 
									from 
										".PREFIX."joborder as j,
										".PREFIX."zipcodes as z
									where
										j.zip_id = z.code and
										z.area_id = a.area_id and
										j.done_date = '$jobDate' and
										j.job_done='1' 
										$insert
									)
							and a.area_id in ($areasTxt)
									order by area_name ASC";
			
			$actualAreas_rs = $this->db->queryMe($sql);
			//echo mysql_num_rows($actualAreas_rs);
			if(mysql_num_rows($actualAreas_rs)!=0){
				while ($p=$this->db->fetchAssoc($actualAreas_rs)) {
					$sql = "select j.job_id,j.box_num,j.status,j.type,
									DATE_FORMAT(j.job_date,'%m/%d/%Y') jobDate,j.comments,
									DATE_FORMAT(j.done_date,'%m/%d/%Y') jobDone,
									concat(j.number,' ',j.street,' ',j.hood,' ',j.city,' ',j.state,' ',j.zip_id) address,
									(select concat(fname,' ',lname) from ".PREFIX."customer where
										customer_id = j.customer_id) name,
									(select concat(phone_home,'|',phone_work,'|',phone_cell) as phones
									from ".PREFIX."customer where customer_id=j.customer_id) phones,
									(select concat(user_fname,' ',user_lname) from ".PREFIX."users where
										user_id = j.driver_id) driver
									from 
										".PREFIX."joborder as j,
										".PREFIX."zipcodes as z
									where
										j.zip_id = z.code and
										z.area_id = '$p[area_id]' and
										j.done_date = '$jobDate' and
										j.job_done='1' $insert";
					$jobs_rs = $this->db->queryMe($sql);
					
					echo '<p class="sub_header">'.$p['area_name'].'</p>';
					echo '<table cellpadding="3" cellspacing="0" border="0" class="table_bordered" width="100%">';
					echo '<tr class="table_headers" align="center"><td width="2%"></td>';
					echo '<td width="8%;">&nbsp;<b>Date Done</b></td>';
					echo '<td width="15%"><b>Customer Details</b></td>';
					echo '<td width="30%"><b>Job Details</b></td>';
					echo '<td width="10%">Accomplished by (Driver/Agent)</td>';
					echo '<td ><b>Notes</b></td></tr>';

					$i=0;
					while($p = $this->db->fetchAssoc($jobs_rs)){
						$i++;
						$phone_arr = explode("|",$p['phones']);

						echo '<tr valign="top">';
						echo '<td class="cell_border_right cell_border_bottom" style="min-height:40px;"><b>'.$i.'</b>.</td>';
						echo '<td class="cell_border_right cell_border_bottom">'.$p['jobDone'].'</td>';
						echo '<td class="cell_border_right cell_border_bottom"><font size="2"><a href="#">'.utf8_decode($p['name']).'</a></font>';
							echo '<table cellspacing="0" cellpadding="0" style="padding-left:5px;">';
							echo $phone_arr[0]!=""?"<tr bgcolor='$bg'><td><b>Home No</b> </td><td>: ".mex::phoneFormat("-",$phone_arr[0])."</td></tr>":"";
							echo $phone_arr[1]!=""?"<tr bgcolor='$bg'><td><b>Work No</b> </td><td>: ".mex::phoneFormat("-",$phone_arr[1])."</td></tr>":"";
							echo $phone_arr[2]!=""?"<tr bgcolor='$bg'><td><b>Cell No</b> </td><td>: ".mex::phoneFormat("-",$phone_arr[2])."</td></tr>":"";
							echo '<tr bgcolor="'.$bg.'"><td colspan="2">&nbsp;</tr></table>';
						echo '</td>';						
						echo '<td class="cell_border_right cell_border_bottom">';
						echo "<b>Job Type</b>: ".mex::job_type($p['type']).'<br>';
						echo "<b>Boxes</b>: ".number_format($p['box_num']).'<br>';
						echo "<b>Address</b>: ";
						echo '<div style="padding-left:10px;">'.mex::textToParagraph($p['address']).'</div>';
						echo '</td>';
						echo '<td class=" cell_border_bottom cell_border_right" align="center">'.$p['driver'].'&nbsp;</td>';
						echo '<td class="cell_border_bottom ">'.mex::textToParagraph($p['comments']).'</td>';
						echo '</tr>';
					}
					echo '</table>';
					//get the jobs of the same date and the same categories
					$this->db->freeMe($jobs_rs);
				}
			}else{
				echo '<p align="center"><span  class="alert_msg">There were no records found</span></p>';
			}
			
			$this->db->freeMe($actualAreas_rs);
		}
		
		function carryOver_report(){
			$today_is = date("Y-m-d");
			$sql = "select area_name,area_id from ".PREFIX."area as a
						where 
							exists(
								select job_id 
									from 
										".PREFIX."joborder as j,
										".PREFIX."zipcodes as z
									where
										j.zip_id = z.code and
										z.area_id = a.area_id and
										j.done_date < '$today_is' and
										j.job_done!='1' 
									)
						order by area_name ASC";
			
			$area_rs = $this->db->queryMe($sql);
			require("modules".DS.$this->mod.DS."flats".DS."carry-over.php");
			$this->db->freeMe($area_rs);
		}
		
		function getNotDoneRS($area){
			$today_is = date("Y-m-d");
			$sql = "select 
						box_num,comments,status,job_id,customer_id,j.type,j.job_done,j.status,
						DATE_FORMAT(j.entry_date,'%m/%d/%Y') call_date,
						(select concat(lname,', ',fname) as name from ".PREFIX."customer where
							customer_id = j.customer_id) as name,
						(select concat(phone_home,'|',phone_work,'|',phone_cell) as phones
									from ".PREFIX."customer where customer_id=j.customer_id) phones,
									
						DATE_FORMAT(job_date,'%m/%d/%Y') as jobDate,
						concat(number,' ',street,' ',hood,' ',city,' ',state,' ',j.zip_id) as address,
						(select concat(user_fname,' ',user_lname) from ".PREFIX."users where user_id=j.driver_id) driver
						from
							".PREFIX."joborder as j,
							".PREFIX."zipcodes as z
							
						where
							j.zip_id = z.code and
							z.area_id='$area' and
							j.job_date < '$today_is' and 
							j.job_done!='1' and 
							(j.type='1' or j.type='2')
							";
			
			$job_rs = $this->db->queryMe($sql);
			return $job_rs;
			
		}
		//-------------------------------- end reports
		
		
		
		function getDispatchRS($area_id,$dispatch_date,$driver_id=0){
			//select all orders to be dispatched
			$start_date = mex::reformatDate($dispatch_date);
			if($driver_id!=0)
				$insert =" and (j.driver_id = '$driver_id' or j.driver_id=0) ";
			else
				$insert='';
					
			$sql = "select 
						box_num,comments,status,job_id,customer_id,j.type,j.status,
						DATE_FORMAT(j.entry_date,'%m/%d/%Y') call_date,
						(select concat(lname,', ',fname) as name from ".PREFIX."customer where
							customer_id = j.customer_id) as name,
						(select concat(phone_home,'|',phone_work,'|',phone_cell) as phones
									from ".PREFIX."customer where customer_id=j.customer_id) phones,
									
						DATE_FORMAT(job_date,'%m/%d/%Y') as jobDate,
						concat(number,' ',street,' ',hood,' ',city,' ',state,' ',zip_id) as address	,
						(select concat(user_fname,' ',user_lname) from ".PREFIX."users
								where user_id = j.driver_id) driver				
					from 
						".PREFIX."joborder as j
					where 	
						j.job_date<='$start_date' and
						job_done!='1' and 
						exists(select zip_id from ".PREFIX."zipcodes where area_id = '$area_id' and 
							code = j.zip_id) 
						$insert
			
					order by
						job_date ASC,
						j.street ASC,
						j.number ASC,
						j.hood ASC";
			
			$dispatch_rs = $this->db->queryMe($sql);
			
			return $dispatch_rs;
		}
		
		function list_jobs(){
			UI::set_js("modules".DS.$this->mod.DS."js".DS."list-orders.js");
			UI::set_stylesheet("modules".DS.$this->mod.DS."styles".DS."jobOrder.css");
			$limit = 50;
			$start = mex::secure_get("start");
			$key = mex::secure_get("key");
			$order = mex::secure_get("order");
			$start=$start?$start:0;
			
			$sql = "select 
						j.box_num,j.comments,j.status,j.job_id,j.customer_id,j.type,j.job_done,
						DATE_FORMAT(j.entry_date,'%m/%d/%Y') call_date,
						concat(c.lname,', ',c.fname)  as customer,
						DATE_FORMAT(j.job_date,'%m/%d/%Y') as jobDate,
						DATE_FORMAT(j.done_date,'%m/%d/%Y') as jobDone,
						concat(j.number,' ',j.street,' ',j.hood,' ',j.city,' ',j.state,' ',j.zip_id) as address
					from 
						".PREFIX."joborder as j,
						".PREFIX."customer as c
					where
						j.customer_id = c.customer_id and
						(j.job_id like '$key%' or c.lname like '$key%' or c.fname like '$key%')
					order by
						j.job_date DESC
					limit $start, $limit";
			
			$job_rs = $this->db->queryMe($sql);
			
			//count total boxes
			$sql = "select count(job_id) total 
					from 
						".PREFIX."joborder as j,
						".PREFIX."customer as c
					where
						j.customer_id = c.customer_id and
						(j.job_id like '$key%' or c.lname like '$key%' or c.fname like '$key%')";
			
			$count_rs = $this->db->queryMe($sql);
			$count_arr = $this->db->fetchAssoc($count_rs);
			$total = $count_arr['total'];
			
			require("modules".DS.$this->mod.DS."flats".DS."jobs_list.php");
			$this->db->freeMe($job_rs);
			$this->db->freeMe($count_rs);
		}
		
		function search(){
			$limit = 20;
			$start = mex::secure_get("start");
			$key = mex::secure_get("key");
			$order = mex::secure_get("order");
			$start=$start?$start:0;
			
			$sql = "select 
						j.box_num,j.comments,j.status,j.job_id,j.customer_id,j.type,j.job_done,
						DATE_FORMAT(j.entry_date,'%m/%d/%Y') call_date,
						concat(c.lname,', ',c.fname)  as customer,
						DATE_FORMAT(j.job_date,'%m/%d/%Y') as jobDate,
						DATE_FORMAT(j.done_date,'%m/%d/%Y') as jobDone,
						concat(j.number,' ',j.street,' ',j.hood,' ',j.city,' ',j.state,' ',j.zip_id) as address
					from 
						".PREFIX."joborder as j,
						".PREFIX."customer as c
					where
						j.customer_id = c.customer_id and
						(j.job_id like '$key%' or c.lname like '$key%' or c.fname like '$key%')
						
					order by
						j.job_date DESC
					limit $start, $limit";
			
			$job_rs = $this->db->queryMe($sql);
			
			//count total boxes
			$sql = "select count(job_id) total 
					from 
						".PREFIX."joborder as j,
						".PREFIX."customer as c
					where
						j.customer_id = c.customer_id and
						(j.job_id like '$key%' or c.lname like '$key%' or c.fname like '$key%')";
			$count_rs = $this->db->queryMe($sql);
			$count_arr = $this->db->fetchAssoc($count_rs);
			$total = $count_arr['total'];
			
			if(mysql_num_rows($job_rs)!=0){
				echo '<p><b>Total Records :</b>'.number_format($total).'</p>';
				echo '<table cellpadding="3" cellspacing="0" width="100%" class="table_bordered">';
				echo '<tr class="table_headers">';
				echo '<td width="30px"></td>';
				echo '<td width="60px">Job ID</td>';
				echo '<td width="200px">Customer</td>';
				echo '<td>Address</td>';
				echo '<td>Job Date</td>';
				echo '<td width="100px">Type</td>';
				echo '<td width="100px">Status</td>';
				echo '<td width="150px"></td>';
				echo '</tr>';
			
				$ctr=$start;
				while($p=$this->db->fetchAssoc($job_rs)){
					$ctr++;
					if($ctr%2==0)
						$className="even";
					else
						$className="odd";
						
					echo '<tr class="'.$className.'" id="tr_'.$p['job_id'].'">';
					echo '<td><b>'.$ctr.'</b>.</td>';
					echo '<td><a href="#" class="jobLink">'.$p['job_id'].'</a></td>';
					echo '<td><a href="#" class="clientsLink" cid="'.$p['customer_id'].'">'.$p['customer'].'</a></td>';
					echo '<td>'.$p['address'].'</td>';
					echo '<td>'.$p['jobDate'].'</td>';
					echo '<td>'.mex::job_type($p['type']).'</td>';
					echo '<td>'.mex::jobStatus($p['job_id']).'</td>';
					echo '<td align="right"><a href="'.mex::long_addr("index.php?mod=jobOrder&action=edit&jid=".$p['job_id']).'";><img src="images/btn-modify.png" border="0"></a> ';
					echo '<img src="images/btn-del.png" border="0" jid="'.$p['job_id'].'" class="delMe" style="cursor:pointer;"></td>';
					echo '</tr>';	
				}
				echo '</table>';
				echo '<p>';
				echo mex::classBasedPaging('pagingClass',$total,$limit,$start);
				echo '</p>';
			}else{
				echo '<p align="center">There are no Job Orders listed</p>';
			}
			$this->db->freeMe($job_rs);
			$this->db->freeMe($count_rs);
		}
		function view(){
			$jid = mex::secure_post("jid");
			$sql = "select 	
							j.job_id,concat(c.lname,', ',c.fname) as customer,j.type,
							DATE_FORMAT(j.job_date,'%m/%d/%Y') as jobDate,
							DATE_FORMAT(j.done_date,'%m/%d/%Y') as jobDone,
							(select concat(user_fname,' ',user_lname) from ".PREFIX."users
								where user_id = j.user_id) encoder,
							(select concat(user_fname,' ',user_lname) from ".PREFIX."users
								where user_id = j.driver_id) driver,
							concat(j.number,' ',j.street,' ',j.hood,' ',j.city,' ',j.state,' ',j.zip_id) as address,	
							j.comments,j.status,j.job_done
						from 
							".PREFIX."joborder as j,
							".PREFIX."customer as c
						where
							j.customer_id = c.customer_id and
							j.job_id='$jid'";
			
			$job_rs = $this->db->queryMe($sql);
			
			if(mysql_num_rows($job_rs)!=0){
				$job = $this->db->fetchAssoc($job_rs);
				echo '<table cellpadding="3" cellspacing="0" style="background:inherit;">';
				echo '<tr>';
				echo '<td width="100px"><b>Job ID</b></td>';
				echo '<td>'.$job['job_id'].'</td>';
				echo '</tr>';
				echo '<tr>';
				echo '<td ><b>Customer</b></td>';
				echo '<td>'.$job['customer'].'</td>';
				echo '</tr>';	
				echo '<tr>';
				echo '<td ><b>Job Address</b></td>';
				echo '<td>'.$job['address'].'</td>';
				echo '</tr>';	
				echo '<tr>';
				echo '<td ><b>Job Type</b></td>';
				echo '<td>'.mex::job_type($job['type']).'</td>';
				echo '</tr>';
				echo '<tr>';
				echo '<td ><b>Job Date</b></td>';
				echo '<td>'.$job['jobDate'].'</td>';
				echo '</tr>';
				echo '<tr>';
				echo '<td ><b>Date Done</b></td>';
				echo '<td>'.mex::empty_date("/",$job['jobDone']).'</td>';
				echo '</tr>';
				echo '<tr>';
				echo '<td ><b>Encoded By</b></td>';
				echo '<td>'.$job['encoder'].'</td>';
				echo '</tr>';
				if($job['driver']!=""){
				echo '<tr>';
				echo '<td ><b>Driver</b></td>';
				echo '<td>'.$job['driver'].'</td>';
				echo '</tr>';
				}
				echo '<tr>';
				echo '<td ><b>Job Status</b></td>';
				echo '<td>'.mex::jobStatus($job['job_id']).'</td>';
				echo '</tr>';
				if($job['comments']!=""){
				echo '<tr>';
				echo '<td  valign="top"><b>Comments</b></td>';
				echo '<td valign="top">'.mex::textToParagraph($job['comments']).'</td>';
				echo '</tr>';
				}
				echo '</table>';
				
				///check the boxes
				$sql = "select box_id,packing_no,consignee,delivery_address,status 
							from ".PREFIX."boxes 
							where job_id='$jid'";
				$box_rs  = $this->db->queryMe($sql);
				if(mysql_num_rows($box_rs)!=0){
					echo '<p class="sub_header">Included Boxes</p>';
					echo '<table cellpadding="3" cellspacing="0" width="100%" class="table_bordered">';
					echo '<tr class="table_headers">';
					echo '<td>#</td>';
					echo '<td>Packing No#</td>';
					echo '<td>Consignee</td>';
					echo '<td>Status</td>';
					echo '</tr>';
					$ctr=0;
					while($p=$this->db->fetchAssoc($box_rs)){
						$ctr++;
						if($ctr%2==0)
							$className="even";
						else
							$className="odd";
						echo '<tr class="'.$className.'">';
						echo '<td><b>'.$ctr.'</b>.</td>';
						echo '<td>'.$p['packing_no'].'</td>';
						echo '<td>'.$p['consignee'].'</td>';
						echo '<td>'.mex::boxStatus($p['box_id']).'</td>';
						echo '</tr>';
					}
					echo '</table>';
				}
				$this->db->freeMe($box_rs);
			}else{
				echo '<p align="center">Job Order #'.$jid.' cannot be found</p>';
			}
			$this->db->freeMe($job_rs);
		}
		
		
		
		function consigneeCopy(){
			$bid = mex::secure_post("bid");
			$sql = "select consignee,mobile_no,delivery_address,contact_no,code_id,
						(select country_id from ".PREFIX."destinationcodes  where code_id = b.code_id) country_id
						from ".PREFIX."boxes as b
						where
							box_id = '$bid'";
							
			$cons_rs = $this->db->queryMe($sql);
			header ("Content-Type:text/xml");
			
				$con_arr = $this->db->fetchAssoc($cons_rs);
				
				echo '<?xml version="1.0" encoding="ISO-8859-1"?>';
				echo '<parameters>';
				echo '<option value="consignee">'.$con_arr['consignee'].'</option>';
				echo '<option value="landline">'.$con_arr['contact_no'].'</option>';
				echo '<option value="mobile">'.$con_arr['mobile_no'].'</option>';
				echo '<option value="country">'.$con_arr['country_id'].'</option>';
				echo '</parameters>';
			
			
		}
		
		/*----------------------- ajax functions --------------------------- */
		
		//creating of job orders
		function client_details(){
			$cid = intval(mex::secure_get("cid"));
			$arr = array();
			if($cid){
				$sql = "select 
							lname,fname,
							customer_id,
							phone_home,
							phone_cell,
							phone_work,
							number,street,
							hood,city,email,
							state,zip,notes,
							z.country_id,z.area_id
						from 
							".PREFIX."customer as c left join
							".PREFIX."zipcodes as z on (c.zip=z.code)
							
						where
							customer_id='$cid'";
				
				$basic = $this->db->queryFirst($sql);
				
				//select the latest job made by this customer
				$sql = "select branch_id,driver_id,d.code_id,d.country_id countryid,j.driver_id
							from 
								".PREFIX."joborder as j left join 
								".PREFIX."destinationcodes as d using (code_id)
							where
								customer_id = $cid 
							order by job_date DESC 
							limit 0,1";
				$secondary = $this->db->queryFirst($sql);
				
				if(!is_array($secondary))
					$secondary=array();
					
				$arr = array_merge($basic,$secondary);
			}
			
			echo json_encode($arr);
		}
		
		function selectAreas(){
		
			//select areas of a country .. show only <option>
			$country_id = mex::secure_get("country_id");
			$sql = "select area_id,area_name from ".PREFIX."area where country_id='$country_id' order by area_name ASC";
			$area_rs = $this->db->queryMe($sql);
			$arr = array();
			if(mysql_num_rows($area_rs)!=0){
				while($p=$this->db->fetchAssoc($area_rs)){
					$arr[] = $p;
				}
			}
			$this->db->freeMe($area_rs);
			echo json_encode($arr);
		}
		
		//show all the zipcodes by area
		function selectAreasZip(){
			//select zips of an area.
			$area_id = mex::secure_get("area");
			$sql = "select zip_id,code from ".PREFIX."zipcodes where area_id='$area_id' order by code ASC";
			$area_rs = $this->db->queryMe($sql);
			$arr = array();
			if(mysql_num_rows($area_rs)!=0){
				while($p=$this->db->fetchAssoc($area_rs)){
					$arr[] = $p;
				}
			}
			$this->db->freeMe($area_rs);
			echo json_encode($arr);
		}
		
		//show all the branches
		function getBranches(){
			//set zip into select format
			$country_id =mex::secure_get("country_id");
			$sql = "select branch_id,branch_name from ".PREFIX."branches where country_id = '$country_id' order by branch_name ASC";
			$branch_rs = $this->db->queryMe($sql);
						
			$arr = array();
			while($p=$this->db->fetchAssoc($branch_rs)){
				$arr[] = $p;
			}
			$this->db->freeMe($branch_rs);
			echo json_encode($arr);
		}
		
		//get all drivers based on countryid
		function getDrivers(){
			//get the drivers for the job order	
			$cid = intval(mex::secure_get("cid"));
			$cid = $cid?$cid:1;
			$sql = "select u.user_id,user_username,user_lname,user_fname
					from 
						".PREFIX."users as u 
						
					where user_level = 2";
					
			$rs = $this->db->queryMe($sql);
			$arr = array();
			while($p=$this->db->fetchAssoc($rs)){
				$arr[] = $p;
			}
			$this->db->freeMe($rs);
			echo json_encode($arr);
		}
		
		//get all the areas of the destination country
		function loaDest(){
			//set zip into select format
			$country_id =mex::secure_get("country_id");
			$sql = "select code_id,code,destination from 
						".PREFIX."destinationcodes
					where 
						country_id = '$country_id' 
					order by code ASC";
			
			$zip_rs = $this->db->queryMe($sql);
			$arr = array();
			while($p=$this->db->fetchAssoc($zip_rs)){
				$arr[] = $p;
			}
			$this->db->freeMe($zip_rs);
			echo json_encode($arr);
		}
		
		//ajax referral
		function referralIDNames(){
			//ajax request - output json
			$referralid = mex::secure_get("referralid");
			$json_arr = array();
			if($referralid){
				//select the name of the referral
				$sql = "select lname,fname,customer_id from 
							".PREFIX."referral left join 
							".PREFIX."customer using (customer_id)
							where acct_no = '$referralid'";
							
				$json_arr = $this->db->queryFirst($sql);
				
			}
			
			echo json_encode($json_arr);
			
		}
		
		
		function referral_search(){
			$keyword = mex::secure_get('keyword');
			$rows = mex::secure_get('rows');
			
			$rows=$rows?$rows:50;
			if(is_numeric($rows)){
				$limit = 'limit 0,'.$rows;
			}else{
				$limit = '';
			}
			
			$sql = "select count(distinct customer_id) as total 
						from 
							".PREFIX."customer left join 
							".PREFIX."referral using (customer_id)
						where 
							(lname like '%$keyword%' or fname like '%$keyword%')"; 
						
			$total_arr = $this->db->queryFirst($sql);
			$total = $total_arr['total'];
			
			$sql = "select customer_id,lname,fname,acct_no 
						from 
							".PREFIX."customer left join 
							".PREFIX."referral using (customer_id)
						where 
							(lname like '%$keyword%' or fname like '%$keyword%') 
						order by lname ASC $limit";
						
			$search_rs = $this->db->queryMe($sql);
			$count = mysql_num_rows($search_rs);	
			
			echo '<p>'.$count.' of '.$total.' records found</p>';
			if(mysql_num_rows($search_rs)!=0){
				echo '<table cellpadding="3" cellspacing="0" width="100%">';
				echo '<thead>';
				echo '<tr>';
				echo '<td>#</td>';
				echo '<th>Customer Name</th>';
				echo '<th>Acct No#</th>';
				echo '<th></th>';
				echo '</tr>';
				echo '</thead>';
				echo '<tbody>';
				$ctr=0;
				while($p=$this->db->fetchAssoc($search_rs)){
				$ctr++;
					if($ctr%2==0)
						$classname = "even";
					else
						$classname = "odd";
					
					$name = $p['lname'].', '.$p['fname'];
					$name = str_replace($keyword,'<b>'.$keyword.'</b>',$name);
					$name = str_replace(strtoupper($keyword),'<b>'.strtoupper($keyword).'</b>',$name);
					$name = str_replace(ucfirst($keyword),'<b>'.ucfirst($keyword).'</b>',$name);
					echo '<tr class="'.$classname.'">';
					echo '<td>'.$ctr.'. </td>';
					echo '<td>'.$name.'</td>';
					echo '<td><b>'.$p['acct_no'].'</b></td>';
					echo '<td><input type="button" value="USE THIS" onclick="useReferral(\''.$p['fname'].' '.$p['lname'].'\','.$p['customer_id'].');"></td>';
					echo '</tr>';
				}
				echo '</tbody>';
				echo '<table>';
			}else{
				echo '<p align="center">No Match Found</p>';
			}
			
			$this->db->freeme($search_rs);
			
		}
		/*----------------------------------------- generic calls -------------------------------------------------*/
		
		function getAreaName($area_id){
			$sql = "select area_name from ".PREFIX."area where area_id='$area_id'";
			
			$area_rs = $this->db->queryMe($sql);
			$area_arr = $this->db->fetchAssoc($area_rs);
			
			$this->db->freeMe($area_rs);
			return $area_arr['area_name'];
		}
		
		
	}
?>