function showJob(jobID){
	if(!$("#local_modal").length){
		 createInfo('local_modal','...','Job Order Details',400,550);
	}else{
		$("#local_modal").dialog('open');
		$("#local_modal").dialog('option','title','Job Order Details');
	}	
	fetchJobDetails(jobID);
}

function showClients(cid){
	if(!$("#local_modal").length){
		 createInfo('local_modal','...','Client Details',400,550);
	}else{
		$("#local_modal").dialog('open');
		$("#local_modal").dialog('option','title','Client Details');
	}	
	fetchClientDetails(cid);
}

function fetchJobDetails(jobID){
	$("#local_modal").html('<p class="loader"><img src="images/loader.gif"> Loading Details...');
	$.ajax({
		  type: "POST",
		  url: "index.php?mod=jobOrder",
		  async: false,
		  data: "ptype=ajax&action=view&jid="+jobID,
		   success: function(data){
				if(data==0){
					sess_failure();
				}else{
					$("#local_modal").html(data);
				}
		   },
		 error: function(XMLHttpRequest, status, error){alert("There was a problem ... please try again");}
	})
	
}

function fetchClientDetails(cid){
	$("#local_modal").html('<p class="loader"><img src="images/loader.gif"> Loading Details...');
	$.ajax({
		  type: "POST",
		  url: "index.php?mod=clients",
		  async: false,
		  data: "ptype=ajax&action=view&cid="+cid,
		   success: function(data){
				if(data==0){
					sess_failure();
				}else{
					$("#local_modal").html(data);
				}
		   },
		 error: function(XMLHttpRequest, status, error){alert("There was a problem ... please try again");}
	})
	
}
function searchOrders(param){
	var key = $("#keyword").val();
	
	$("#jobsArea").html('<p class="loader"><img src="images/loader.gif"> Searching Orders...');
	$.ajax({
		  type: "GET",
		  url: "index.php?mod=jobOrder",
		  async: false,
		  data: "ptype=ajax&action=search&key="+key+"&"+param,
		   success: function(data){
				if(data==0){
					sess_failure();
				}else{
					$("#jobsArea").html(data);
				}
		   },
		 error: function(XMLHttpRequest, status, error){alert("There was a problem ... please try again");}
	})
	
}
function removeJob(obj){
		var jid = obj.attr("jid");
	 var answer = confirm("Deleting job orders will delete all related boxes\nAre you sure you wish to delete?")
	if (answer){
		$.ajax({
				  type: "POST",
				  url: "index.php?mod=jobOrder",
				  async: false,
				  data: "ptype=ajax&action=delete&jid="+jid,
				   success: function(data){
						if(data==0){
							sess_failure();
						}else if(data==1){
							$("#tr_"+jid).empty();
							$("#tr_"+jid).remove();
						}else{
							alert(data);
						}
				   },
				 error: function(XMLHttpRequest, status, error){alert("There was a problem ... please try again");}
			})
	}
}
$(function() {
		searchOrders('');
		$(".pagingClass").live('click',function(){searchOrders('start='+$(this).attr("href"));return false;})
		$("#btnSearch").click(function(){searchOrders();});
		$("#jobForm").submit(function(){searchOrders();return false;});
		$(".jobLink").live('click',function(){showJob($(this).html());return false;});
		$(".clientsLink").live('click',function(){showClients($(this).attr("cid"));return false;});
		$(".delMe").live('click',function(){removeJob($(this));return false;});
});