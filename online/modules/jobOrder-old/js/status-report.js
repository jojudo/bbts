function showReport(){
	var dataSet = $("#statusForm").serialize();
	$("#reportArea").html("<p class=\"loader\"><img src='images/loader.gif'> Loading Report...</p>")
	$.ajax({
		  type: "POST",
		  url: "index.php?mod=jobOrder",
		  async: false,
		  data: "ptype=ajax&action=get_status_report&"+dataSet,
		   success: function(data){
				if(data==0){
					sess_failure();
				}else{
					$("#reportArea").html(data);
				}
		   },
		 error: function(XMLHttpRequest, status, error){alert("There was a problem ... please try again");}
	})
	
}

$(function() {
		$('.pickers').datepicker({
			numberOfMonths: 1,
			showButtonPanel: true,
			showOn: 'both', 
			buttonImage: 'images/icn-calendar.png',
			buttonImageOnly: true
		});
		showReport();
		$("#statusForm").submit(function(){return false;});
		$("#btnSubmit").click(function(){showReport();});

});