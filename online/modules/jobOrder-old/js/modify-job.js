
function updateJobOrder(){
	
	var base_arr = new Array();
	var form_arr = new Array();
	var ctr;
	
	var cid = $("#cid").val();
	var prefix = "addr_";
	var autoplot = $('#autoplot').attr("checked");
	var plotted;
	var cont= true;
	
	if(!IsNumeric(cid)){	
		form_arr[0] = "lname";
		form_arr[1] = "fname";
	}
		
	base_arr[0] = 'num';
	base_arr[1] = 'st';
	base_arr[2] = 'city';
	base_arr[3] = 'state';
	base_arr[4] = 'zip';
	
	
	var i=form_arr.length;
	for(var ctr=0;ctr<base_arr.length;ctr++){
		form_arr[i] = prefix+base_arr[ctr];
		i++;
	}
	
	if(checkForm(form_arr)){
		var param = $('#jobForm').serialize();
		var autoplot =$('#autoplot').attr("checked");
		if(autoplot){
			plotted = plotAddress();
			if(plotted==0){
				alert("unable to find addres on map");
				cont= false;
			}else if(plotted==2){
				alert("Map Geocoding is offline");
				cont= false;
			}else{
				cont= true;
			}
		}
			
		if(cont){
		//send to ajax processing
			$.ajax({
			  type: "POST",
			  mode:"ABORT",
			  url: "index.php?mod=jobOrder",
			  beforeSend:function(info){
			  	 $('#jobForm :input').attr("disabled","disabled");
			  },
			  data: "action=updateJob&ptype=ajax&"+param,
			   success: function(data,status){			   
			   	if(data==1){
			   		alert("Job Order was successfully updated");
			   	}else
			   		alert(data);
			   		
			   	$('#jobForm :input').attr("disabled","");
			   }
			   
			 })
		}
	}
}

function loadDrivers(country){
	var htm = '<select name="driver_id">';
	htm+='<option value=""> --- Any Driver in Route --- </option>';
	$("#driver_id").attr("disabled","disabled");
	$.ajax({
		  type: "POST",
		  mode:"ABORT",
		  data: "action=getUsers&utype=2&ptype=ajax&country_id="+country,
		  success: function(data,status){
		  	htm+=data;
		  	htm+='</select>';
		  	$("#div_agent").html(htm); 
		  },
		   url: "index.php?mod=users",
		   error:function(hhtp,url,error){alert("error code "+error)}
		 })	
}

function loadDest(){
	//load zip codes -- default loading all with country (bypass aarea)
	var objs = document.getElementById('addr_countryDest');
	var eVal = objs[objs.selectedIndex].value
	//populate zipcodes based on country
	$.ajax({
		  type: "POST",
		  mode:"ABORT",
		  data: "action=loaDest&ptype=ajax&country_id="+eVal,
		  success: function(data,status){
		  	$("#destiny").children().remove(); 
		  	$("#destiny").append(data); 
		  },
		   url: "index.php?mod=zipcodes"
		 })
	
}

function loadBranch(country){
	$.ajax({
		  type: "POST",
		  mode:"ABORT",
		  data: "action=getBranches&ptype=ajax&country_id="+country,
		  success: function(data,status){
		  	$("#branch").children().remove(); 
		  	$("#branch").append(data); 
		  	loadDrivers(country);
		  },
		   url: "index.php?mod=branches"
		 })
	
}

function loadZip(){
	//load zip codes -- default loading all with country (bypass aarea)
	var objs = document.getElementById('addr_country');
	var eVal = objs[objs.selectedIndex].value
	//populate zipcodes based on country
	$.ajax({
		  type: "POST",
		  mode:"ABORT",
		  data: "action=zipcodes&ptype=ajax&country_id="+eVal,
		  success: function(data,status){
		  	$("#addr_zip").children().remove(); 
		  	$("#addr_zip").append(data); 
		  	loadBranch(eVal);
		  	
		  },
		   url: "index.php?mod=zipcodes"
		 })
	
}

$(function() {
		$('.pickers').datepicker({
			showButtonPanel: true,
			showOn: 'both', 
			buttonImage: 'images/icn-calendar.png',
			buttonImageOnly: true
		});
		
		$('#addr_country').live("change",(function(){loadZip();}));
		$('#addr_country').change((function(){loadZip();}));
		$('#addr_countryDest').change((function(){loadDest();}));

});