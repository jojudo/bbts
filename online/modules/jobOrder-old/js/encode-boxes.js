var mod = 'jobOrder';

function match_client(posName,param){
	/*search clients based on entries in the lname and fname field*/
	
	var lname = $("#lname").val();
	var fname = $("#fname").val();
	var posObj = $("#"+posName);
	var str = '';
	
	$("#nameMatch").css("top",posObj.position().top+20);	
	$("#nameMatch").css("left",posObj.position().left);	
	
	$("#matchLoader").html('<img src="images/mini-loader.gif">');	
	
	if(param.length!=0)
		param = "&"+param;
	 
	$.ajax({
		  type: "GET",
		  data: "lname="+lname+"&fname="+fname+"&action=name_match&mod=jobOrder&ptype=ajax&fld="+posName+"&"+param,
		  error:function(obj,msg){
		  	alert("Request Error : "+msg);
		  },
		  //dataType:'xml',
		  success: function(xml) { 
		  	if(xml==0){
		  		sess_failure();
		  	}else{
				
				if($(xml).find('names').attr('total')!=0){
					str = '<ul id="matchUL">';
					$(xml).find('name').each(function(){
						str+='<li><a href="#" class="match_name" onclick="client_details('+$(this).find('ID').text()+');return false;\">';
						str+=$(this).find('lastname').text()+', '+$(this).find('firstname').text()+'</a> </li>';						
					})
					str += '</ul>';
					str+='<p>';
					var prev = $(xml).find('names').attr('previous');
					var next = $(xml).find('names').attr('next');
					var fld = $(xml).find('names').attr('fld');
					
					if(prev>=0)
						str +='<a class="prev" href="#" onclick="match_client(\''+fld+'\',\'start='+prev+'\');return false;"><< Previous</a>';
					
					if(next>0)	
						str += '<a class="next" href="#" onclick="match_client(\''+fld+'\',\'start='+next+'\');return false;">Next >></a>';
					
					str+='</p>';
					
					$("#nameMatch").html(str);
					$("#nameMatch").show();	
					
					
					//hide when document click
					$(document).click(function(){
							//alert("outside click");
						 $("#nameMatch").hide();
					});
					
					//do not hide if clicked in list
					$("#nameMatch").click(function(e){
						e.stopPropagation();
					});
					
				}else{
					$("#nameMatch").hide();
					
				}
				//hide the loader
				$("#matchLoader").html("");
		  	}
		  }, 
		  url: "index.php"
	});
	
}


function client_details(cid)
{
	//assign the details to the form
	var objs;
	var eVal;
	
	$.ajax({
	  type: "GET",
	  mode:"ABORT",
	  beforeSend:function(info){
	  	 $(":input").attr("disabled","disabled");
	  },
		url: "index.php",
		data: "cid="+cid+"&mod="+mod+"&action=client_details&ptype=ajax",
		dataType:"json",
		success: function(data,status){
			if(data.customer_id){
				//set all values
				$("#lname").val(data.lname);
				$("#fname").val(data.fname);
				$("#cid").val(data.customer_id);
				$("#nameMatch").hide();
				$("#matchLoader").html('[ <a href="#" onclick="clearSlate();return false;">Clear</a> ]');
				
				//set the address
				//country selection
				$('#addr_country option[value="'+data.country_id+'"]').attr("selected","selected");
				
				//load the areas of the country -- setting values to be marked as 
				loadAreas(data.area_id,data.zip);
				
				$("#addr_num").val(data.number);
				$("#addr_st").val(data.street);
				$("#addr_hood").val(data.hood);
				$("#addr_city").val(data.city);
				$("#addr_state").val(data.state);
				
				//phone numbers
				if(data.phone_home){
					var telp = data.phone_home.split("-");
					$("#addr_tel1").val(telp[0]);
					$("#addr_tel2").val(telp[1]);
					$("#addr_tel3").val(telp[2]);
				}
				
				if(data.phone_cell){
					var celp = data.phone_cell.split("-");
					$("#addr_cell1").val(celp[0]);
					$("#addr_cell2").val(celp[1]);
					$("#addr_cell3").val(celp[2]);
				}
				
				if(data.phone_work){
					var workp = data.phone_work.split("-");
					$("#addr_work1").val(workp[0]);
					$("#addr_work2").val(workp[1]);
					$("#addr_work3").val(workp[2]);
				}
				
				$("#referrer_line").hide();
			}
		
		//load map if it is active
		if(typeof(mapActive) != "undefined"){
			var plot;
			plot = plotAddress();
			$('#autoplot').attr("checked","");
			if(plot==0){
				alert("unable to find address on the map");
			}else if(plot==2){
				alert("Geocoder is offline");
			}else{
				;
			}
		}
		
	    $(":input").attr("disabled","");
	    $('.matchMe').attr("disabled","disabled");
	   }
	 })
}

function clearSlate(){
	$(":text").val("");
	$("#cid").val('');
	$("#boxes").val('1');
	$("#matchLoader").html("");
	$("option:nth-child(1)").attr("selected","selected");
	$(":input").attr("disabled","");
	$("#makePermanent").remove();
	$("#addr_area").children().remove();
	$("#addr_zip").children().remove();
	$("#branch").children().remove();
	$("#driver_id").children().remove();
	$("#job_date").val($("#job_date").attr("default"));
	$(":input[type=textarea]").val();
}

function set_existing(cid){
	$("#submitButton").attr("disabled","disabled");
	$.ajax({
		  type: "POST",
		  url: "index.php?mod=USDelivery",
		  async: false,
		  data: "ptype=ajax&action=set_existing&cid="+cid,
		   success: function(data){
		   	if(data==0)
		   		sess_failure();
		   	else{
		   		$("#jobCust").html(data);
		   		$("#submitButton").attr("disabled","");
		   		$("#flatNew").css("display","none");
		   		$("#fname").keyup(function(){match_client("","fname")});
				$("#lname").keyup(function(){match_client("","lname")});
				$("#clearCust").click(function(){clearJobOrder();return false;});
		   	}
		   }
		})
}


function clearJobOrder(){
	$(":text[name!=job_date][name!=boxes][name!=addr_state]").val("");
	$(".phone_start").val("808");
	if($("#flatExist").length){
		$("#flatExist").css("display","none");
		$("#flatNew").css("display","block");	
	}
}

function checkJobOrder(){
	var base_arr = new Array();	
	var boxes = $("#box").val();
	base_arr[0] = 'addr_num';
	base_arr[1] = 'addr_st';
	base_arr[2] = 'addr_city';
	base_arr[3] = 'addr_state';
	base_arr[4] = 'addr_zip';
	base_arr[5] = 'lname';
	base_arr[6] = 'fname';
	
	if(checkForm(base_arr)){
		if(IsNumeric(boxes) && boxes!=0){
			startBoxEncode(boxes);
		}else{
			alert('Please enter a number for number of boxes');
			$("#box").focus();
		}
	}
}

function startBoxEncode(num){
	if(!$("#local_modal").length){
		 createInfo('local_modal','...','Box Encoding',500,800);
	}
	else{
		$("#local_modal").dialog('option','title','Box Encoding');
		$("#local_modal").dialog('option','width','700');
		$("#local_modal").dialog('option','height','500');
		$("#local_modal").dialog('open');
	}
	$("#local_modal").html('<div class="loader"><img src="images/loader.gif"> Loading Content...</div>');
	$.ajax({
	  type: "POST",
	  url: "index.php?mod=boxes",
	  async: false,
	  data: "ptype=ajax&action=encodeBox&boxes="+num,
	   success: function(data){
	   	if(data==0){
	   		sess_failure();
	   	}else{
	   		$("#local_modal").html(data)
	   		$("#box_form").submit(function(){boxChecking();return false})
	   		$("#btnSub").click(function(){boxChecking();})
	   		$('.pickers').datepicker({
				showButtonPanel: true,
				showOn: 'both', 
				buttonImage: 'images/icn-calendar.png',
				buttonImageOnly: true
			});
			$(".packing").keyup(function(){checkExisting($(this))})
			$("#btnDup").click(function(){boxDuplicate()})
	   	}
	   }
	})
		
}

//function used to duplicate values
function boxDuplicate(){
	var con = $(".consignee").val();
	var cont1 = $(".contact1").val();
	var cont2 = $(".contact2").val();
	var cont3 = $(".contact3").val();
	var cont4 = $(".contact4").val();
	var det = $(".details").val();
	var add = $(".addr").val();
	var collected = $(".collected").val();
	var received = $(".received").val();
	
	//alert($("#destination :selected").val())
	
	
	$(".consignee").val(con);
	$(".contact1").val(cont1);
	$(".contact2").val(cont2);
	$(".contact3").val(cont3);
	$(".contact4").val(cont4);
	$(".details").val(det);
	$(".addr").val(add);
	$(".collected").val(collected);
	$(".received").val(received);
	
	var obj = document.getElementById("destination1");
	var dest = obj[obj.selectedIndex].index+1;
	$(".destination option:nth-child("+dest+")").attr("selected","selected");
		
	obj = document.getElementById("container1");
	dest = obj[obj.selectedIndex].index+1;
	
	$(".container option:nth-child("+dest+")").attr("selected","selected");
	
	obj = document.getElementById("boxStatus1");
	dest = obj[obj.selectedIndex].index+1;
	
	$(".boxStatus option:nth-child("+dest+")").attr("selected","selected");
	
	obj = document.getElementById("product1");
	dest = obj[obj.selectedIndex].index+1;
	
	$(".product option:nth-child("+dest+")").attr("selected","selected");
	
	
}

function boxChecking(){
	var boxStr = "";
	var i=0;
	var con = true;

	$('.packing').each(function(index) {
		if($(this).val()==""){
			$(this).attr("style","background:yellow");
			$(this).focus();
			con = false;
		}
		$(this).attr("css","background:");
		
		if(i!=0)
   	 		boxStr +=",";
   	 	boxStr+= $(this).val();
   	 	i++;
  	});
  	
  	if(con){
	  	$("#box_form :input").attr("disabled","disabled");	
	  	$.ajax({
			  type: "POST",
			  url: "index.php?mod=boxes",
			  async: false,
			  data: "ptype=ajax&action=checkBoxes&str="+boxStr,
			   success: function(data){
			   	if(data==0){
			   		sess_failure();
			   	}else if(data==1){
			   		//start submit Boxes
					submitForm();
			   	}else{
			   		alert(data);
			   		$("#box_form :input").attr("disabled","");
			   	}		   	
			   }
			})
  	}else{
  		alert("Please enter Packing Nos#");
  	}
}

function submitForm(){	
	
	//1.) submit the job details	
	var dataSet = $("#job_form").serialize();
	$("#job_form :input").attr("disabled","disabled");
	$.ajax({
	  type: "POST",
	  url: "index.php?mod=boxes",
	  async: false,
	  data: "ptype=ajax&action=encodeJob&job=WH&"+dataSet,
	   success: function(data){
	   	if(data==0){
	   		sess_failure();
	   	}else if(IsNumeric(data)){
	   		//start submit Boxes
	   		$("#local_modal").bind( "dialogbeforeclose", function(event, ui) {
			   alert("Please wait, processing boxes ...'");return false;
			});
			$("#box_form :input").attr("disabled","");	
			finalBoxEncode(data);
	   	}else{
	   		alert(data);
	   	}
	   }
	})
	
}

function finalBoxEncode(jid){
	//1.) submit boxes job details	
	var dataSet = $("#box_form").serialize();
	var total = $("#box").val();
	$.ajax({
		  type: "POST",
		  url: "index.php?mod=boxes",
		  async: false,
		  data: "ptype=ajax&action=finalizeEncode&jid="+jid+"&boxes="+total+"&"+dataSet,
		   success: function(data){
		   	if(data==0){
		   		sess_failure();
		   	}else if(IsNumeric(data)){
		   		//start submit Boxes
		   		//finalBoxEncode();
		   		alert("Job Order "+jid+" was successfully encoded");
		   		$("#local_modal").unbind("dialogbeforeclose");
		   		$("#local_modal").dialog("close");
		   		
		   		clearSlate();
		   	}else{
		   		alert(data + "final encode");
		   		$("#box_form :input").attr("disabled","");
		   	}
		   }
		})
}

function checkExisting(obj){
	//check exsiting	
	var divName = obj.attr("id")+"Div";
	
	if(obj.val().length>2){
		$('#'+divName).load('index.php?mod=boxes&ptype=ajax&action=checkPacking&packing='+obj.val());
	}else{
		$('#'+divName).html("");
	}
}

function loadAreas(){
	//load area routes of country to make it easier to encode
	var objs = document.getElementById('addr_country');
	var eVal = objs[objs.selectedIndex].value
	//populate zipcodes based on country
	$("#addr_area").attr("disabled","disabled");
	$.ajax({
		  type: "POST",
		  mode:"ABORT",
		  data: "action=selectAreas&ptype=ajax&country_id="+eVal,
		  success: function(data,status){
		  	$("#addr_area").children().remove(); 
		  	$("#addr_area").append(data); 
		  	$("#addr_area").attr("disabled","");
		  	loadZip();
		  },
		   url: "index.php?mod=zipcodes"
		 })
}

function loadZip(){
	//load zip codes -- default loading all with country (bypass aarea)
	var objs = document.getElementById('addr_country');
	var eVal = objs[objs.selectedIndex].value
	//populate zipcodes based on country
	$("#addr_zip").attr("disabled","disabled");
	$.ajax({
		  type: "POST",
		  mode:"ABORT",
		  data: "action=zipcodes&ptype=ajax&country_id="+eVal,
		  success: function(data,status){
		  	$("#addr_zip").children().remove(); 
		  	$("#addr_zip").append(data); 
		  	$("#addr_zip").attr("disabled","");
		  	loadBranch(eVal);
		  },
		   url: "index.php?mod=zipcodes"
		 })
}

function loadAreaZip(){
	//load area routes of country to make it easier to encode
	var objs = document.getElementById('addr_area');
	var area = objs[objs.selectedIndex].value
	//populate zipcodes based on country
	$("#addr_zip").attr("disabled","disabled");
	$.ajax({
		  type: "POST",
		  mode:"ABORT",
		  data: "action=selectAreasZip&ptype=ajax&area="+area,
		  success: function(data,status){
		  	$("#addr_zip").children().remove(); 
		  	$("#addr_zip").append(data); 
		  	$("#addr_zip").attr("disabled","");
		  },
		   url: "index.php?mod=zipcodes"
		 })
}

function loadBranch(country){
	
	$("#branch").attr("disabled","disabled");
	$.ajax({
		  type: "POST",
		  mode:"ABORT",
		  data: "action=getBranches&ptype=ajax&country_id="+country,
		  success: function(data,status){
		  	$("#branch").children().remove(); 
		  	$("#branch").append(data); 
		  	$("#branch").attr("disabled","");
		  	loadDrivers(country);
		  },
		   url: "index.php?mod=branches"
		 })
	
}

function loadDrivers(country){
	var htm = '<select name="driver_id" id="driver_id">';
	htm+='<option value=""> --- Any Driver in Route --- </option>';
	$("#driver_id").attr("disabled","disabled");
	$.ajax({
		  type: "POST",
		  mode:"ABORT",
		  data: "action=getUsers&utype=2&ptype=ajax&country_id="+country,
		  success: function(data,status){
		  	htm+=data;
		  	htm+='</select>';
		  	$("#div_agent").html(htm); 
		  },
		   url: "index.php?mod=users",
		   error:function(hhtp,url,error){alert("error code "+error)}
		 })	
}

//add area and zip handling
function addAreas(){
	createInfo('local_modal','...','New Area',150,400);
	$("#local_modal").html('<div class="loaders"><img src="images/mini-loader.gif"> Loading Content ... </div>');
		$.ajax({
		  type: "POST",
		  url: "index.php?mod=zipcodes",
		  async: false,
		  data: "ptype=ajax&action=newArea",
		   success: function(data){
				if(data==0){
					sess_failure();
				}else{
					$("#local_modal").html(data);
					$("#areaForm").submit(function(){submitArea();return false;})
				}
		   },
		   error:function(html,url,error){alert("Problem Loading page, please try again");$("#local_modal").dialog('close');}
		})
}

function submitArea(){
	var form_arr = new Array();
	form_arr[0] = 'area_name';
	form_arr[1] = 'country';
	
	if(checkForm(form_arr)){
		var dataSet = $("#areaForm").serialize();
		var cid = $("#country").val();
		$("#areaForm :input").attr("disabled","disabled");
		$.ajax({
		  type: "POST",
		  url: "index.php?mod=zipcodes",
		  async: false,
		  data: "ptype=ajax&action=saveArea&"+dataSet,
		   success: function(data){
				if(data==0){
					sess_failure();
				}else if(data==1){
					alert("Area successfully Saved")
					$("#local_modal").dialog('close');
					loadAreas();
				}else{
					alert(data)
				}
		   },
		   error:function(html,url,error){alert("Problem Loading page, please try again");$("#local_modal").dialog('close');}
		})
		
	}
}

function addZip(){
 	createInfo('local_modal','...','New Zip Code',150,400);
	$("#local_modal").html('<div class="loaders"><img src="images/mini-loader.gif"> Loading Content ... </div>');
		$.ajax({
		  type: "POST",
		  url: "index.php?mod=zipcodes",
		  async: false,
		  data: "ptype=ajax&action=newZip",
		   success: function(data){
				if(data==0){
					sess_failure();
				}else{
					$("#local_modal").html(data);
					$("#areaForm").submit(function(){submitZip();return false;})
					$("#country").change(function(){ZiploadAreas();})
				}
		   },
		   error:function(html,url,error){alert("Problem Loading page, please try again");$("#local_modal").dialog('close');}
		})
}

function ZiploadAreas(){
	//load area routes of country to make it easier to encode
	var objs = document.getElementById('country');
	var eVal = objs[objs.selectedIndex].value
	//populate zipcodes based on country
	$("#area").attr("disabled","disabled");
	$.ajax({
		  type: "POST",
		  mode:"ABORT",
		  data: "action=selectAreas&ptype=ajax&country_id="+eVal,
		  success: function(data,status){
		  	$("#area").children().remove(); 
		  	$("#area").append(data); 
		  	$("#area").attr("disabled","");
		  },
		   url: "index.php?mod=zipcodes"
		 })
}

function submitZip(){
	var form_arr = new Array();
	form_arr[0] = 'zip';
	form_arr[1] = 'country';
	form_arr[2] = 'area';
	
	if(checkForm(form_arr)){
		var dataSet = $("#areaForm").serialize();
		var cid = $("#country").val();
		$("#areaForm :input").attr("disabled","disabled");
		$.ajax({
		  type: "POST",
		  url: "index.php?mod=zipcodes",
		  async: false,
		  data: "ptype=ajax&action=saveZip&"+dataSet,
		   success: function(data){
				if(data==0){
					sess_failure();
				}else if(data==1){
					alert("Area successfully Saved")
					$("#areaForm :input").attr("disabled","");
					$("#local_modal").dialog('close');
					 loadAreaZip();
				}else{
					alert(data)
					$("#areaForm :input").attr("disabled","");
				}
		   },
		   error:function(html,url,error){alert("Problem Loading page, please try again");$("#local_modal").dialog('close');}
		})
		
	}
}

//add area and zip handling end

$(function() {
		$('.pickers').datepicker({
			showButtonPanel: true,
			showOn: 'both', 
			buttonImage: 'images/icn-calendar.png',
			buttonImageOnly: true
		});
		$('#addr_country').live("change",(function(){loadAreas();}));
		$('#addr_area').live('change',function(){loadAreaZip()});
		$('#addArea').live('click',function(){addAreas();return false;});
		$('#addZip').live('click',function(){addZip();return false;});
		$("#submitButton").click(function(){checkJobOrder()});
		$('.matchMe').live('keyup',(function(){match_client($(this),'nameMatch')}));
});