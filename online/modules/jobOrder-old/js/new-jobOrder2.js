var mod = 'jobOrder_beta';

function match_client(posName,param){
	/*search clients based on entries in the lname and fname field*/
	
	var lname = $("#lname").val();
	var fname = $("#fname").val();
	var posObj = $("#"+posName);
	var str = '';
	
	$("#nameMatch").css("top",posObj.position().top+20);	
	$("#nameMatch").css("left",posObj.position().left);	
	
	$("#matchLoader").html('<img src="images/mini-loader.gif">');	
	
	if(param.length!=0)
		param = "&"+param;
	 
	$.ajax({
		  type: "GET",
		  data: "lname="+lname+"&fname="+fname+"&action=name_match&mod="+mod+"&ptype=ajax&fld="+posName+"&"+param,
		  error:function(obj,msg){
		  	alert("Request Error : "+msg);
		  },
		  //dataType:'xml',
		  success: function(xml) { 
		  	if(xml==0){
		  		sess_failure();
		  	}else{
				
				if($(xml).find('names').attr('total')!=0){
					str = '<ul id="matchUL">';
					$(xml).find('name').each(function(){
						str+='<li><a href="#" class="match_name" onclick="client_details('+$(this).find('ID').text()+');return false;\">';
						str+=$(this).find('lastname').text()+', '+$(this).find('firstname').text()+'</a> </li>';						
					})
					str += '</ul>';
					str+='<p>';
					var prev = $(xml).find('names').attr('previous');
					var next = $(xml).find('names').attr('next');
					var fld = $(xml).find('names').attr('fld');
					
					if(prev>=0)
						str +='<a class="prev" href="#" onclick="match_client(\''+fld+'\',\'start='+prev+'\');return false;"><< Previous</a>';
					
					if(next>0)	
						str += '<a class="next" href="#" onclick="match_client(\''+fld+'\',\'start='+next+'\');return false;">Next >></a>';
					
					str+='</p>';
					
					$("#nameMatch").html(str);
					$("#nameMatch").show();	
					
					
					//hide when document click
					$(document).click(function(){
							//alert("outside click");
						 $("#nameMatch").hide();
					});
					
					//do not hide if clicked in list
					$("#nameMatch").click(function(e){
						e.stopPropagation();
					});
					
				}else{
					$("#nameMatch").hide();
					
				}
				//hide the loader
				$("#matchLoader").html("");
		  	}
		  }, 
		  url: "index.php"
	});
	
}

function client_details(cid)
{
	//assign the details to the form
	var objs;
	var eVal;
	
	$.ajax({
	  type: "GET",
	  mode:"ABORT",
	  beforeSend:function(info){
	  	 $(":input").attr("disabled","disabled");
	  },
		url: "index.php",
		data: "cid="+cid+"&mod="+mod+"&action=client_details&ptype=ajax",
		dataType:"json",
		success: function(data,status){
			if(data.customer_id){
				//set all values
				$("#lname").val(data.lname);
				$("#fname").val(data.fname);
				$("#cid").val(data.customer_id);
				$("#nameMatch").hide();
				$("#matchLoader").html('[ <a href="#" onclick="clearSlate();return false;">Clear</a> ]');
				
				//set the address
				//country selection
				$('#addr_country option[value="'+data.country_id+'"]').attr("selected","selected");
				
				//load the areas of the country -- setting values to be marked as 
				loadAreas(data.area_id,data.zip);
				
				$("#addr_num").val(data.number);
				$("#addr_st").val(data.street);
				$("#addr_hood").val(data.hood);
				$("#addr_city").val(data.city);
				$("#addr_state").val(data.state);
				
				//phone numbers
				if(data.phone_home){
					var telp = data.phone_home.split("-");
					$("#addr_tel1").val(telp[0]);
					$("#addr_tel2").val(telp[1]);
					$("#addr_tel3").val(telp[2]);
				}
				
				if(data.phone_cell){
					var celp = data.phone_cell.split("-");
					$("#addr_cell1").val(celp[0]);
					$("#addr_cell2").val(celp[1]);
					$("#addr_cell3").val(celp[2]);
				}
				
				if(data.phone_work){
					var workp = data.phone_work.split("-");
					$("#addr_work1").val(workp[0]);
					$("#addr_work2").val(workp[1]);
					$("#addr_work3").val(workp[2]);
				}
				
				$("#referrer_line").hide();
			}
		
		//load map if it is active
		if(typeof(mapActive) != "undefined"){
			var plot;
			plot = plotAddress();
			$('#autoplot').attr("checked","");
			if(plot==0){
				alert("unable to find address on the map");
			}else if(plot==2){
				alert("Geocoder is offline");
			}else{
				;
			}
		}
		
	    $(":input").attr("disabled","");
	    $('.matchMe').attr("disabled","disabled");
	   }
	 })
}

//load all areas through the addr_country  field
function loadAreas(areaid,zipid){
	
	var objs = $('#addr_country');
	var eVal = objs.val();
	//populate zipcodes based on country
	$("#addr_area").attr("disabled","disabled");
	$.ajax({
		  type: "GET",
		  mode:"ABORT",
		  url: "index.php?mod="+mod,
		  data: "action=selectAreas&ptype=ajax&country_id="+eVal,
		  dataType:"json",
		  success: function(data,status){
		  	$("#addr_area").children().remove(); 
			$.each(data,function(){
				//alert(this.area_id);
				$("#addr_area").append('<option value="'+this.area_id+'">'+this.area_name+'</option>'); 
			})
			//set the default value
			$('#addr_area option[value="'+areaid+'"]').attr("selected","selected");
			loadAreaZip(zipid);
		  }   
	})
	$("#addr_area").attr("disabled","");
}

function loadAreaZip(zipid){
	//load zip codes of area
	var objs = $('#addr_area');
	var area = objs.val();
	
	//populate zipcodes based on area
	$("#addr_zip").attr("disabled","disabled");
	$.ajax({
		  type: "GET",
		  mode:"ABORT",
		  url: "index.php?mod="+mod,
		  data: "action=selectAreasZip&ptype=ajax&area="+area,
		  dataType:"json",
		  success: function(data,status){
		  	$("#addr_zip").children().remove(); 
			$.each(data,function(){
				//alert(this.area_id);
				$("#addr_zip").append('<option value="'+this.code+'">'+this.code+'</option>'); 
			})
			//set the default value
		  	$("#addr_zip").attr("disabled","");
			$('#addr_zip option[value="'+zipid+'"]').attr("selected","selected");
			
			$('#addr_clientNotes').val(this.notes);
			
			//load all branches for choosing where this job was encoded
			loadBranch();
		  }
	})
}

//load all business sourcess (branches)
function loadBranch(){
	var objs = $('#addr_country');
	var country = objs.val();
	
	$("#branch").attr("disabled","disabled");
	$.ajax({
		  type: "GET",
		  mode:"ABORT",
		  url: "index.php?mod="+mod,
		  data: "action=getBranches&ptype=ajax&country_id="+country,
		  dataType:"json",
		  success: function(data,status){
		  	$("#branch").children().remove(); 
			$("#branch").append('<option value=""> --- select business source --- </option>'); 
			$.each(data,function(){
				//alert(this.area_id);
				$("#branch").append('<option value="'+this.branch_id+'">'+this.branch_name+'</option>'); 
			})
		  	$("#branch").attr("disabled","");
		  	loadDrivers();
		  }
	})
	
}

//get all drivers in this country
function loadDrivers(){
	var objs = $('#addr_country');
	var country = objs.val();
	
	var htm = '<select name="driver_id" id="driver_id">';
	htm+='<option value=""> --- Any Driver in Route --- </option>';
	$("#driver_id").attr("disabled","disabled");
	
	$.ajax({
		  type: "GET",
		  mode:"ABORT",
		  url: "index.php?mod="+mod,
		  data: "action=getDrivers&ptype=ajax&cid="+country,
		  dataType:'json',
		  success: function(data,status){
			$("#driver_id").children().remove(); 
			$("#driver_id").append('<option value="">--- any driver in route ---</option>'); 
			$.each(data,function(){
				$("#driver_id").append('<option value="'+this.user_id+'">'+this.user_lname+', '+this.user_fname+'</option>'); 
			})
		  	$("#driver_id").attr("disabled","");
			
		  },
		error:function(hhtp,url,error){alert("There was a problem loading drivers")}
		 })	
}

//get all destination country areas
function loadDest(){

	//load zip codes -- default loading all with country (bypass aarea)
	var objs =$('#addr_countryDest');
	var eVal = objs.val();
	
	//populate zipcodes based on country
	$("#destiny").attr("disabled","disabled");
	$.ajax({
		  type: "GET",
		  mode:"ABORT",
		  url: "index.php?mod="+mod,
		  data: "action=loaDest&ptype=ajax&country_id="+eVal,
		  dataType:'json',
		  success: function(data,status){
		  	$("#destiny").children().remove(); 
		  	$("#destiny").append('<option value="">--- select destination area ---</option>'); 
			$.each(data,function(){
				$("#destiny").append('<option value="'+this.code_id+'">'+this.destination+'</option>'); 
			})
			
		  	$("#destiny").attr("disabled","");
		  }
		 })
	
}

function clearSlate(cid){
	$(":text").val("");
	$("textarea").val("");
	$("#cid").val('');
	$("#matchLoader").html("");
	$("option:nth-child(1)").attr("selected","selected");
	$(":input").attr("disabled","");
	$("#makePermanent").remove();
	
	if(!$("#referred").length)
		$("#client_details").append('<div id="referred"><b>Referred by : </b> <span id="referral-div"><a href="#" id="referral">View List</a></span></div>');
	else
		 $("#referral-div").html('<a href="#" id="referral">View List</a>');
			
	$(":input[type=textarea]").val("");
	//remove auto populating
	$("#addr_area").children().remove();
	$("#addr_zip").children().remove();
	$("#destiny").children().remove();
	$("#driver_id").children().remove();
	$("#branch").children().remove();
	$("#job_date").val($("#job_date").attr("default"));
	$("#box").val('1');
	$("#extraFeatures").empty();
	$("#extraFeatures").remove();
	$("#referrer_line").show();
	useReferral('',0);
	/*checkPlot();
	defaultMarker();*/
	
}

function checkPlot(){

	if($("#radioExisting").attr("checked")){
		 $('#autoplot').attr("checked","");
	}else{
		$('#autoplot').attr("checked","checked");
	}
}

function submitJobOrder(){
	
	var base_arr = new Array();
	var existClient = $('#radioExisting').attr("checked");
	var form_arr = new Array();
	var ctr;
	
	var cid = $("#cid").val();
	var prefix = "addr_";
	var autoplot = $('#autoplot').attr("checked");
	var plotted;
	var cont= true;
	
	if(!IsNumeric(cid)){	
		form_arr[0] = "lname";
		form_arr[1] = "fname";
	}
		
	base_arr[0] = 'num';
	base_arr[1] = 'st';
	base_arr[2] = 'city';
	base_arr[3] = 'state';
	base_arr[4] = 'zip';
	
	//evaluate
	check_referralid();
	
	var i=form_arr.length;
	for(var ctr=0;ctr<base_arr.length;ctr++){
		form_arr[i] = prefix+base_arr[ctr];
		i++;
	}
	
	if(checkForm(form_arr)){
		var param = $('#jobForm').serialize();
		var autoplot =$('#autoplot').attr("checked");
		if(autoplot){
			plotted = plotAddress();
			if(plotted==0){
				alert("unable to find addres on map");
				cont= false;
			}else if(plotted==2){
				alert("Map Geocoding is offline");
				cont= false;
			}else{
				cont= true;
			}
		}
			
		if(cont){
	
		//send to ajax processing
			$.ajax({
			  type: "POST",
			  mode:"ABORT",
			  beforeSend:function(info){
			  	$("#submitButton").attr("disabled","disabled");
			  	$("#submitButton").val("Submitting Form ...");
			  },
			  data: "mod="+mod+"&action=save_job&ptype=ajax&"+param,
			  dataType:"json",
			   success: function(data,status){
			   	$("#submitButton").val("Record Job Order");
			   	$("#submitButton").attr("disabled","");
			   
			   	if(data.status==1){
			   		alert("Job was successfully posted");
			   		clearSlate(cid);
			   	}else
			   		alert(data.msg);
			   },
			   url: "index.php?mod="+mod
			 })
		}
	}
}

function copyAndPaste()
{
	//using maps.google.js
	var addr = "";
	var objs;
	base_arr = new Array();
	
	base_arr[0] = 'num';
	base_arr[1] = 'st';
	base_arr[2] = 'hood';
	base_arr[3] = 'city';
	base_arr[4] = 'state';
	base_arr[5] = 'zip';
	
	prefix = "addr_";
		
	for(var ctr=0;ctr<base_arr.length;ctr++){
		objs = document.getElementById(prefix+base_arr[ctr]);
		
		if(objs.type=="select-one")
		{
			eVal = objs[objs.selectedIndex].value
			if(eVal==0)
				eVal="";
		}
		else
		{
			eVal = objs.value;
		}
		
		if(eVal!="")
			addr+=eVal+" ";
	}
	
	//document.getElementById(address).value = addr;
	
	if(addr=="")
	{
		alert("Please Enter Job Address");
	}
	else{
		;//plotAddr(addr,'geoTxt','0');
		//alert(addr);
	}
}



//map manilpulation
function encodingMap(lat,lng){
	 	geocoder = new google.maps.Geocoder();
	 	infoWindowHtml = new google.maps.InfoWindow({maxWidth:300});
		
	    var latlng = new google.maps.LatLng(lat,lng);
	    defaultPosition = latlng;
	    var myOptions = {
	      zoom: 16,
	      center: latlng,
	      mapTypeId: google.maps.MapTypeId.ROADMAP
	    };
	    map = new google.maps.Map(document.getElementById("map"), myOptions);
	    encodingMarkerStart(latlng);

}

function defaultMarker(){
	 infoWindowHtml.setContent("Input address to Plot on Map");
	 infoWindowHtml.open(map,currentMarker);	  
	 currentMarker.setPosition(defaultPosition);
	 map.setCenter(defaultPosition);
}

function encodingMarkerStart(position,hiddenID){
	var latlng;
	var addr;
	 currentMarker = new google.maps.Marker({
	      map: map, 
	      position: position,
	      draggable:true
	 });
     google.maps.event.addListener(currentMarker, 'dragend', function() {
     	latlng = currentMarker.getPosition();
     	$("#geoTxt").val(latlng);
     	latlngtoAddress(latlng);
      	infoWindowHtml.open(map, currentMarker);
     });   
     
     google.maps.event.addListener(currentMarker, 'click', function() {
      	infoWindowHtml.open(map, currentMarker);
     });  
     
	  infoWindowHtml.setContent("Input address to Plot on Map");
	  infoWindowHtml.open(map,currentMarker);	  
}

function centerMarker(){
	map.setCenter(currentMarker.getPosition());
}
function makeDialog(){
	$("#mapContainer").dialog({
		resizeable:true
	});
}
function destroyDialog(){
	//$("#mapContainer").html($("#map").html());
	$("#mapContainer").dialog("destroy");
	
}
function plotAddress(){
	var num = $("#addr_num").val();
	var street = $("#addr_st").val();
	var hood = $("#addr_hood").val();
	var city = $("#addr_city").val();
	var state = $("#addr_state").val();
	var zip = $("#addr_zip").val();
	var objs = document.getElementById('addr_country');
	var country = objs[objs.selectedIndex].text
	
	var address = num+" "+street+" "+hood+" "+city+" "+state+" "+zip+" "+country+" ";	
	if (geocoder) {
	      geocoder.geocode( { 'address': address}, function(results, status) {
	        if (status == google.maps.GeocoderStatus.OK) {
	         	currentMarker.setPosition(results[0].geometry.location);
	         	$("#geoTxt").val(results[0].geometry.location);
	         	map.setCenter(results[0].geometry.location);
	           	infoWindowHtml.setContent(address);
	  			infoWindowHtml.open(map,currentMarker);	  
	  			return 1;
	        }else{
	          //unable to find address
	          return 0;
	        }
	      });
	}else{
		//geocoder is offline
		return 2;
	}
}

//add area and zip handling
function addAreas(){
	createInfo('local_modal','...','New Area',150,400);
	$("#local_modal").html('<div class="loaders"><img src="images/mini-loader.gif"> Loading Content ... </div>');
		$.ajax({
		  type: "POST",
		  url: "index.php?mod=zipcodes",
		  async: false,
		  data: "ptype=ajax&action=newArea",
		   success: function(data){
				if(data==0){
					sess_failure();
				}else{
					$("#local_modal").html(data);
					$("#areaForm").submit(function(){submitArea();return false;})
				}
		   },
		   error:function(html,url,error){alert("Problem Loading page, please try again");$("#local_modal").dialog('close');}
		})
}

function showGroups(objs,grpObj,manage){
	var cid = objs.val();
	var obj = $("#"+grpObj);
	var str = '';
	obj.html('<option value="0">Loading Groups</option>');
	$.ajax({
		  type: "GET",
		  url: "index.php?mod=zipcodes",
		  async: false,
		  dataType:'xml',
		  data: "ptype=ajax&action=loadGroups&cid="+cid,
		  
		   success: function(xml){
				if(xml==0){
					sess_failure();
				}else{
					//process xml	
					if(manage)
						str = '<option value="0">All from Country</option>';
						
					if($(xml).find('country').attr('total')){
						$(xml).find('groups').each(function(){
							str+='<option value="'+$(this).find('areaid').text()+'">'+$(this).find('name').text()+'</option>';
						});
						
						if(manage)
							str+='<option value="0" onclick="manageGroup('+cid+');">-- MANAGE GROUP --</option>';
					}
					obj.html(str);
				}
		   },
		   error:function(html,url,error){alert("Problem Loading page");}
	})
}


function submitArea(){
	var form_arr = new Array();
	form_arr[0] = 'area_name';
	form_arr[1] = 'country';
	
	if(checkForm(form_arr)){
		var dataSet = $("#areaForm").serialize();
		var cid = $("#country").val();
		$("#areaForm :input").attr("disabled","disabled");
		$.ajax({
		  type: "POST",
		  url: "index.php?mod=zipcodes",
		  async: false,
		  data: "ptype=ajax&action=saveArea&"+dataSet,
		   success: function(data){
				if(data==0){
					sess_failure();
				}else if(data==1){
					alert("Area successfully Saved")
					$("#local_modal").dialog('close');
					loadAreas();
				}else{
					alert(data)
				}
		   },
		   error:function(html,url,error){alert("Problem Loading page, please try again");$("#local_modal").dialog('close');}
		})
		
	}
}

function addZip(){
 	createInfo('local_modal','...','New Zip Code',150,400);
	$("#local_modal").html('<div class="loaders"><img src="images/mini-loader.gif"> Loading Content ... </div>');
		$.ajax({
		  type: "POST",
		  url: "index.php?mod=zipcodes",
		  async: false,
		  data: "ptype=ajax&action=newZip",
		   success: function(data){
				if(data==0){
					sess_failure();
				}else{
					$("#local_modal").html(data);
					$("#areaForm").submit(function(){submitZip();return false;})
					$("#country").change(function(){ZiploadAreas();})
				}
		   },
		   error:function(html,url,error){alert("Problem Loading page, please try again");$("#local_modal").dialog('close');}
		})
}

function ZiploadAreas(){
	//load area routes of country to make it easier to encode
	var objs = document.getElementById('country');
	var eVal = objs[objs.selectedIndex].value
	//populate zipcodes based on country
	$("#area").attr("disabled","disabled");
	$.ajax({
		  type: "POST",
		  mode:"ABORT",
		  data: "action=selectAreas&ptype=ajax&country_id="+eVal,
		  success: function(data,status){
		  	$("#area").children().remove(); 
		  	$("#area").append(data); 
		  	$("#area").attr("disabled","");
		  },
		   url: "index.php?mod=zipcodes"
		 })
}

function submitZip(){
	var form_arr = new Array();
	form_arr[0] = 'zip';
	form_arr[1] = 'country';
	form_arr[2] = 'area';
	
	if(checkForm(form_arr)){
		var dataSet = $("#areaForm").serialize();
		var cid = $("#country").val();
		$("#areaForm :input").attr("disabled","disabled");
		$.ajax({
		  type: "POST",
		  url: "index.php?mod=zipcodes",
		  async: false,
		  data: "ptype=ajax&action=saveZip&"+dataSet,
		   success: function(data){
				if(data==0){
					sess_failure();
				}else if(data==1){
					alert("Area successfully Saved")
					$("#areaForm :input").attr("disabled","");
					$("#local_modal").dialog('close');
					 loadAreaZip();
				}else{
					alert(data)
					$("#areaForm :input").attr("disabled","");
				}
		   },
		   error:function(html,url,error){alert("Problem Loading page, please try again");$("#local_modal").dialog('close');}
		})
		
	}
}



function transactionHistory(){
	var cid = $("#cid").val();
	
	createInfo('historyDiv','<div class="loader"><img src="images/loader.gif"> Loading Content...</div>','Transaction History',400,500);	
	
	$.ajax({
		  type: "POST",
		  url: "index.php?mod=clients",
		  async: false,
		  data: "ptype=ajax&action=transactionHistory&cid="+cid,
		   success: function(data){
				if(data==0){
					sess_failure();
				}else{
					$("#historyDiv").html(data);
				}
		   },
		   error:function(html,url,error){$("#historyDiv").html("Problem Loading page, please close this window and try again");}
		})
}

function consigneeCopy(obj){
	var bid = obj.attr("bid");
	createInfo('loaderDiv','<div class="loader"><img src="images/loader.gif"> please wait...</div>','',100,300);	
	$.ajax({
		  type: "POST",
		  url: "index.php?mod="+mod,
		  async: false,
		  data: "ptype=ajax&action=consigneeCopy&bid="+bid,
		   success: function(data){
				if(data==0){
					sess_failure();
					$("#loaderDiv").dialog('close');
				}else{
					var doms;
					$(data).find("option").each(function(){
						doms =$(this); 
						alert(doms.attr("value"));
						//$("#"+doms.attr("value")).val(doms.text());
						//alert(doms.attr("value")+" = "+doms.text());
					});
					$("#loaderDiv").dialog('close');
				}
		   },
		   error:function(html,url,error){$("#historyDiv").html("Problem Loading page, please close this window and try again" + error + html +url);}
		})
}

/*--------------------------------------- 	referral functions ---------------------------------- */

function findReferrals(){
	createInfo('referralWindow','','MLM Accounts',500,400);	
	var html = '<form name="form1" onsubmit="referralSearch();return false;">Name : <input type="text" name="term" id="referralterm">';
	html+='<select name="referralrows" id="referralrows"><option selected>50</option><option>100</option><option>200</option><option>500</option><option>all</option></select>'
	html+='<input type="submit" value="Search"></form>';
	$("#referralWindow").html(html+'<div id="referralArea"><p class="loaders"><img src="images/loader.gif"> Loading Content</p></div>');
	referralSearch();
	
}

function referralSearch(){
	var term = $("#referralterm").val();
	var rows = $("#referralrows").val();
	
	$("#referralArea").html('<img src="images/loader.gif"> Loading Content</div>');
	$.ajax({
		  type: "GET",
		  url: "index.php?mod="+mod,
		  async: false,
		  data: "ptype=ajax&action=referral_search&keyword="+term+"&rows="+rows,
		   success: function(data){
				
				if(data==0){
					sess_failure();
				}else{
					$("#referralArea").html(data);
				}
		   },
		   error:function(html,url,error){$("#referralWindow").html("Problem Loading page, please close this window and try again");}
		})
}

function useReferral(name,id){
	if(id){
		var str = 
		$("#referral-search").html(name+'<input type="hidden" name="referralid" value="'+id+'"> [<a href="#" onclick="findReferrals();return false;">change</a>] [<a href="#" onclick="useReferral(\'\',0);return false">remove</a>]');
		$("#referralWindow").dialog('close');
		$("#referral-input").hide();
		$("#referral_acct_no").val('');
	}else{
		$("#referral-input").show();
		$("#referral-search").html('[<a href="#" onclick="findReferrals();return false;">Search Accounts</a>]');
	}
}


function check_referralid(){
	//get the name of the referral id
	var obj  = $("#referral_acct_no");
	var referralid = obj.val();
	if(referralid){
		$("#referalDiv").html('<img src="images/loader.gif"> Checking...');
		$.ajax({
			  type: "GET",
			  url: "index.php?mod="+mod,
			  async: false,
			  data: "ptype=ajax&action=referralIDNames&referralid="+referralid,
			  datatype:'json',
			  success: function(data){
					if(data==0){
						sess_failure();
					}else{
						var obj = jQuery.parseJSON(data);
						if(obj.customer_id){
							str = '<input type="hidden" name="referralid" value="'+obj.customer_id+'">';
							$("#referalDiv").html('*'+obj.lname+', '+obj.fname+'</i></b></i>'+str);
							obj.css("background","");
						}else{
							$("#referalDiv").html("Referral ID does not exist");
							obj.css("background","yellow");
						}
					}
			   },
			   error:function(html,url,error){$("#referalDiv").html('Something is wrong');}
			})
	}
	
}

/*-------------------------- Job Encoding -------------------------------------*/
function submitBoxEncoding(){
	
	var base_arr = new Array();	
	var boxes = $("#box").val();
	base_arr[0] = 'addr_num';
	base_arr[1] = 'addr_st';
	base_arr[2] = 'addr_city';
	base_arr[3] = 'addr_state';
	base_arr[4] = 'addr_zip';
	base_arr[5] = 'lname';
	base_arr[6] = 'fname';
	
	if(checkForm(base_arr)){
		if(IsNumeric(boxes) && boxes!=0){
			startBoxEncode(boxes);
		}else{
			alert('Please enter a number for number of boxes');
			$("#box").focus();
		}
	}
}

function startBoxEncode(num){
	//fetch the encoding form to start box encoding
	createInfo('local_modal','...','Box Encoding',500,800);
	 
	$("#local_modal").html('<div class="loader"><img src="images/loader.gif"> Loading Content...</div>');
	$.ajax({
	  type: "GET",
	  url: "index.php?mod="+mod,
	  async: false,
	  data: "ptype=ajax&action=encodeBoxForm&boxes="+num,
	   success: function(data){
	   	if(data==0){
	   		sess_failure();
	   	}else{
	   		$("#local_modal").html(data)
	   		$("#box_form").submit(function(){boxChecking();return false})
	   		$("#btnSub").click(function(){boxChecking();})
	   		$('.pickers').datepicker({
				showButtonPanel: true,
				showOn: 'both', 
				buttonImage: 'images/icn-calendar.png',
				buttonImageOnly: true
			});
			//$(".packing").keyup(function(){checkExisting($(this))})
			//$("#btnDup").click(function(){boxDuplicate()})
	   	}
	   }
	})
		
}

function fetch_price(obj){
	//fetch the price of the item to textares
	$("#collected"+obj.parent().attr("num")).val(obj.attr('price'));
	
}

function consignee_check(obj){
	var cid = $("#cid").val();

	if(cid){
		var key = obj.val();
		//continue with the checking
		obj.parent().append('<span id="con_loading">loading...</span>');
		//get the list
		$.ajax({
		  type: "GET",
		  mode:"ABORT",
		  url: "index.php?mod="+mod,
		  data: "action=consigneeCheck&ptype=ajax&cid="+cid+"&key="+key,
		  dataType:"json",
		  success: function(data,status){
			$(".consigneeMatch").html('');
			var str = '<ul>';
			var ctr=0;
			$.each(data,function(){				
				str+='<li><a href="#" onclick="chooseConsignee('+this.box_id+');return false;">'+this.consignee+'</a></li>';
				ctr++;
			})
			str+= '<ul>';
			$(".consigneeMatch").html(str);
			$(".consigneeMatch").show();
			$("#con_loading").remove();
			
			//hide when document click
			$(document).click(function(){
					//alert("outside click");
				 $(".consigneeMatch").hide();
			});
			
			//do not hide if clicked in list
			$(".consigneeMatch").click(function(e){
				e.stopPropagation();
			});
					
		  }   
	})
	}
}

function chooseConsignee(bid){
		$.ajax({
		  type: "GET",
		  mode:"ABORT",
		  url: "index.php?mod="+mod,
		  data: "ptype=ajax&action=chooseConsignee&bid="+bid,
		  dataType:"json",
		  success: function(data,status){
			$(".consigneeMatch").hide();
			$("#consignee1").val(data.consignee);	
			$("#mob1").val(data.mobile_no);	
			$("#con_addr1").val(data.delivery_address);	
			var telp = data.contact_no.split("-");
			$("#home1_1").val(telp[0]);
			$("#home2_1").val(telp[1]);
			$("#home3_1").val(telp[2]);
			$('#con_area1 option[value="'+data.code_id+'"]').attr("selected","selected");
		  }   
	})
}

function boxDuplicate(){
	// copy all the details to other boxes
	var con = $(".consignee").val();
	var cont1 = $(".contact1").val();
	var cont2 = $(".contact2").val();
	var cont3 = $(".contact3").val();
	var cont4 = $(".contact4").val();
	var area = $("#con_area1").val();
	var det = $(".details").val();
	var add = $(".addr").val();
	var collected = $(".collected").val();
	var received = $(".received").val();
	var destiny = $("#con_area1").val();
	var conts = $("#container1").val();
	var status = $("#boxStatus1").val();
	var packing =  $("#packing1").val();
	
	//get the base packing number .. if first is numeric add numbers to each
	var lastchar = packing.charAt(packing.length-1 );
	
	if(lastchar=='a' || lastchar=='A'){
		var ext = 'abcdefghijklmnopqrstuvwxyz';
		var i =0;
		var mainpack = packing.slice(0, -1);
		$.each($(".packing"),function(index,value){
			if(i){
				//index.val(mainpack+ext[i]);
				$(this).val(mainpack+ext[i]);
				//alert(index)
			}
			i++;
			
		})
	}
	//alert($("#destination :selected").val())
	
	
	$(".consignee").val(con);
	$(".contact1").val(cont1);
	$(".contact2").val(cont2);
	$(".contact3").val(cont3);
	$(".contact4").val(cont4);
	$(".details").val(det);
	$(".addr").val(add);
	//$(".collected").val(collected);
	$(".received").val(received);
	

	$('.destination option[value="'+destiny+'"]').attr("selected","selected");
	$('.container option[value="'+conts+'"]').attr("selected","selected");
	$('.boxStatus option[value="'+status+'"]').attr("selected","selected");
		
}


function boxChecking(){
	var boxStr = "";
	var i=0;
	var con = true;
		
	lock_encoding_dialogs();
	
	//verify that all packing nos are not empty
	$('.packing').each(function(index) {
		if($(this).val()==""){
			$(this).attr("style","background:yellow");
			$(this).focus();
			con = false;
		}
		$(this).attr("css","background:");
		
		if(i!=0)
   	 		boxStr +=",";
   	 	boxStr+= $(this).val();
   	 	i++;
  	});
  	
	//if not empty check if there no duplicates
  	if(con){
	  	$("#box_form :input").attr("disabled","disabled");	
	  	$.ajax({
			  type: "POST",
			  url: "index.php?mod="+mod,
			  async: false,
			  dataType:'json',
			  data: "ptype=ajax&action=encode_check&str="+boxStr,
			   success: function(data){
			   	if(data==0){
			   		sess_failure();
			   	}else{
			   		//start submit Boxes
					if(data.status==1){
						//submit_encoded_boxes();
						dialog_close_button("encoding_loader","Creating Job Order");
						encode_job_submit();
					}else{
						unlock_encoding_dialogs(0);
						dialog_close_button("encoding_loader",data.msg);
						$("#box_form :input").attr("disabled","");
					}
			   	}
					
			   },error:function(var1,var2,var3){
					unlock_encoding_dialogs(0);
					dialog_close_button("encoding_loader","There was a problem while checking Box Record");
				
			   }
			})
  	}else{
		dialog_close_button("encoding_loader","Please make sure all Packing No# are filled-up");
		unlock_encoding_dialogs(0);
		$("#box_form :input").attr("disabled","");
  	}
}


function encode_job_submit(){	
	//1.) save the job details first ... 
	var dataSet = $("#jobForm").serialize();
	$("#jobForm :input").attr("disabled","disabled");
	$.ajax({
	  type: "POST",
	  url: "index.php?mod="+mod,
	  async: false,
	  dataType:'json',
	  data: "ptype=ajax&action=save_job&"+dataSet,
	   success: function(data){
	   	if(data==0){
	   		sess_failure();
	   	}else if(data.status==1){
	   		//start submit Boxes
			$("#box_form :input").attr("disabled","");	
			dialog_close_button("encoding_loader","Recording Boxes, please wait...");
			finalBoxEncode(data.cid);
	   	}else{
	   		//alert(data.msg);
			unlock_encoding_dialogs(0);
			$("#encoding_loader").html(data.msg);
			$("#box_form :input").attr("disabled","");
	   	}
	   }
	})
	
}


function finalBoxEncode(jid){
	//1.) submit boxes job details	
	var dataSet = $("#box_form").serialize();
	var total = $("#box").val();
	$.ajax({
		  type: "POST",
		  url: "index.php?mod=boxes",
		  async: false,
		  data: "ptype=ajax&action=finalizeEncode&jid="+jid+"&boxes="+total+"&"+dataSet,
		   success: function(data){
		   	if(data==0){
		   		sess_failure();
		   	}else if(IsNumeric(data)){
		   		//start submit Boxes
		   		//finalBoxEncode();
				dialog_close_button("encoding_loader","Job was successfully encoded");
				unlock_encoding_dialogs(0);
				$("#local_modal").dialog('close');
				clearSlate();
				
		   	}else{
		   		alert(data + "final encode");
		   		$("#box_form :input").attr("disabled","");
		   	}
		   }
		})
}


function lock_encoding_dialogs(){
	loadingInfo('encoding_loader','Encoding Boxes, Please wait,','',100,300);
	
	$("#encoding_loader").bind( "dialogbeforeclose", function(event, ui) {
		alert("Please wait until processing is done!'");return false;
	});
	
	$("#local_modal").bind( "dialogbeforeclose", function(event, ui) {
		alert("Please wait until processing is done!'");return false;
	});
}

function unlock_encoding_dialogs(closeLoader){
	$("#encoding_loader").unbind("dialogbeforeclose");
	$("#local_modal").unbind("dialogbeforeclose");
	
	if(closeLoader){
		$("#encoding_loader").dialog("close");
		$("#local_modal").dialog("close");
		clearSlate();
	}
}
/*-------------------------------------------- End Direct Encoding ------------------------------- */

$(function() {	
		$('.matchMe').live('keyup',(function(){match_client($(this).attr("id"),'nameMatch')}));
		
		if(typeof(mapActive) != "undefined"){
			encodingMap('map','21.2893344','-157.8246546','geoTxt','Please plot job location');
			//encodingMap('21.2893344','-157.8246546');
		}
		
		//$("#map").resizable();
		//$("#transactionHistory").live('click',function(){transactionHistory();return false;});
		clearSlate();
		$('.pickers').datepicker({
			showButtonPanel: true,
			showOn: 'both', 
			buttonImage: 'images/icn-calendar.png',
			buttonImageOnly: true
		});

});