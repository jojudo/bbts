function listUsers (page){
	//step 1 = request Data from Server
	//callback to function that handles the template
	framework.ajaxGeneric({param:"action=usersPage",callback:"loadUserTemplate",submitType:'get',dataType:'json'});
}

function loadUserTemplate (data){
	framework.loadTemplate({template:'userlist',callback:'populateUserData',passover:data})
}

function populateUserData (Obj){
	var content = framework.fillTemplate(Obj.template,Obj.data);
	$("#search_results").html(content);
	//$('#datatable').DataTable();
	
}

function testunction(){
    var tester = test;
    tester.myvar();
}
function userListInit(template){
    listUsers(page);
}

/*function listUsers(page){
    param = "action=users_page&page="+page;
    //now define the ajax call and callback function
    ajaxCallJson(param,'GET','buildResultTable');
}*/

function buildResultTable(data){
	var str = '';
	var ctr=0;
	if(data.page!=1) ctr=(data.items*(data.page-1));
	$.each(data.users,function(index,value){
		ctr++;
		str+='<tr>';
		str+='<td>'+ctr+'</td>';
		str+='<td>'+value.user_id+'</td>';
		str+='<td>'+value.user_lname+', '+value.user_fname+'</td>';
		str+='<td>'+value.user_username+'</td>';
		str+='<td>'+value.level_name+'</td>';
		str+='<td>'+value.branch_name+'</td>';
		str+='<td>'+value.last_login+'</td>';
		str+='<td>';
		str+='<a href="#" userid="'+value.user_id+'" class="button edit">Modify</a>'
		str+='<a href="#" class="button delete">Delete</a>'
		str+='</td>';
		str+='<tr>';
	})	
	
	
	$("#datatable").find("tbody").html(str);
	$("#user_count").html(data.rows);
	$("#user_paging").html(classNamePaging('user_page',data.page,data.pages));
	$(".user_page").click(function(){listUsers($(this).html());return false;})
	
	$(".edit").click(function(){loadEditForm($(this).attr("userid"));return false;})
	
	window.prev = data.prev;
	window.next = data.next
	
}


function loadEditForm(cid){
	jsTemplate('editForm','fillEditForm','','');
}

function fillEditForm(template){0
	createInfo('EditForm',template,'Update User',300,500);
}

function silentCallBack(){
	//do nothing
	;
}

$(function() {
	listUsers(0);
});