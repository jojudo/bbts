{{DATASTART}}
<table id="datatable" class="table table-striped table-bordered">
  <thead>
	<tr>
	  <th>UserID</th>
	  <th>Name</th>
	  <th>username</th>
	  <th>user_level</th>
	  <th>Branch</th>
	  <th>Date Created</th>
	  <th>Last Log in</th>
	  <th>Action</th>
	</tr>
  </thead>
  <tbody>
    {{LOOPSTART}}
        <tr>
            <td>{UserID}</td>
            <td>{name}</td>
            <td>{username}</td>
            <td>{user_level}</td>
            <td>{branch}</td>
            <td>{date_created}</td>
            <td>{last_login}</td>
            <td>
                <a href="#" product-id="{product_id}" class="btn btn-default edit-product"><i class="fa fa-pencil fa-fw" ></i>Edit</a>
                
                <a href="#" product-id="{product_id}" class="btn btn-default delete-product"><i class="fa fa-remove fa-fw"></i>Delete</a>
            </td>
        </tr>
     {{LOOPEND}}   
    </tbody>
</table>
{{DATAEND}}