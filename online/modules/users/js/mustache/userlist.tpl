<table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
  <thead>
	<tr>
	  <th>Name</th>
	  <th>username</th>
	  <th>user_level</th>
	  <th>Branch</th>
	  <th>Last Log in</th>
	  <th>Action</th>
	</tr>
  </thead>
  <tbody>
    {{#users}}
        <tr>
            <td>{{user_fname}} {{user_lname}}</td>
            <td>{{user_username}}</td>
            <td>{{level_name}}</td>
            <td>{{branch_name}}</td>
					<td>{{last_login}}</td>
            <td>
                <a href="#" product-id="{{userid}}" class="btn btn-default edit-user"><i class="fa fa-pencil fa-fw" ></i>Edit</a>
                
                <a href="#" product-id="{product_id}" class="btn btn-default delete-user"><i class="fa fa-remove fa-fw"></i>Delete</a>
            </td>
        </tr>
     {{/users}}   
    </tbody>
</table>

<button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg">Large modal</button>

                  <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                      <div class="modal-content">

                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                          </button>
                          <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                        </div>
                        <div class="modal-body">
                          <h4>Text in a modal</h4>
                          <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.</p>
                          <p>Aenean lacinia bibendum nulla sed consectetur. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Donec sed odio dui. Donec ullamcorper nulla non metus auctor fringilla.</p>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          <button type="button" class="btn btn-primary">Save changes</button>
                        </div>

                      </div>
                    </div>
                  </div>
