<?php
	load_interface('insertSet');
	class moduleHelper extends helperController implements insertSet{
				
		function __construct(){
			parent::__construct();
		}
		
		protected function main(){
			$this->set_mod_javascript();
			$this->use_module_template('user_list');
		}
		
		public function usersPage(){
			$this->args = $this->listUsers();
			$this->json_output($this->args);
		}
		
		private function listUsers(){
			$page = intval($this->datastore->pick('page'));
			$page = $page?$page:1;
			$limit = 10;
			
			if($page>1)
				$start = (($page-1)*$limit);
			else
				$start =0;
				
			$start=$start?$start:0;
			
			$this->db->set_table("users u");
            $this->db->set_join("userlevels l","u.user_level = l.level_id");
            $this->db->set_join("branches b","u.branch_id = b.branch_id");
            $this->db->set_fields("SQL_CALC_FOUND_ROWS u.user_id,user_fname,user_lname,user_username,last_action");
            $this->db->set_fields("if(last_log=0,'',date_format(last_log,'%m/%d/%Y')) last_login ,if(u.branch_id,branch_name,'') branch_name ,level_name");
            $this->db->set_orderby("last_log","DESC");
            $this->db->set_limit($start,$limit);
			
			$args['users'] = $this->db->select();
            $args['rows'] = $this->db->found_rows();
			
			$maxPages = ceil($args['rows']/$limit);
			$next = $page+1;
			$next = $next<=$maxPages?$next:0;
			$prev = $page-1;
			$prev = $prev>0?$prev:0;
			
			
			$args['pages'] = $maxPages;
			$args['page'] = $page;
			$args['items'] = $limit;
			$args['next'] = $next;
			$args['prev'] = $prev;
			
			return $args;
			
		}
		
		public function edit_data(){
			$userId = intval($this->datastore->pick('uid'));
			$table = PREFIX."users u left join ".PREFIX."branches b using (branch_id)";
			
			$this->args['user'] = $this->db->selectQueryFirst($table,"u.user_id,b.branch_id,b.country_id,user_lname,user_fname,user_username","u.user_id = $userId");
			$this->args['countries'] = $this->countries = $this->db->selectQueryArr(PREFIX."countries","country,country_id");
			
			$this->set_mod_javascript("user_edit");
			$this->set_mod_stylesheet();
			$this->ui->use_module_flat('user_edit',$this->args);
		}
		
		
		function update_data(){
			$this->datastore->set_parameter_source('POST');
			$container_no = $this->datastore->pick('container_no');
			$box_size = $this->datastore->pick('box_size');
			$customs = $this->datastore->pick('customs');
			
			$trip_no = $this->datastore->pick('trip_no');
			$vessel = $this->datastore->pick('vessel_name');
			$departure = $this->datastore->pick('departure');
			$arrival = $this->datastore->pick('arrival');
			
			$cid = $this->datastore->pick('cid');
			$this->args = array();
			
			//get the shipment id
			$shipment_arr = $this->db->selectQueryFirst(PREFIX."container","shipment_id","container_id = $cid");
			$sid = $shipment_arr['shipment_id'];
			
			//start the updateQuery
			$fields = array();
			
			$fields['container_no'] = $container_no;
			$fields['size'] = $box_size;
			$fields['date_released'] = date("Y-m-d",strtotime($customs));
			
			$result = $this->db->updateQuery(PREFIX."container",$fields,"container_id=$cid");
			$this->args['message'] = $result." records updated for container";
				
			//update shipment 
			$fields = array();
			
			$fields['trip_no'] = $trip_no;
			$fields['est_departure'] = date("Y-m-d",strtotime($departure));
			$fields['est_arrival'] = date("Y-m-d",strtotime($arrival));
			$fields['vessel_name'] = $vessel;
			
			$result = $this->db->updateQuery(PREFIX."shipment",$fields,"shipment_id=$sid");
			$this->args['message'] .= " ".$result." records updated for shipment";
			
			$this->args['success'] = true;
			$this->json_output($this->args);
		}
		
				
		public function add_data(){
			$this->datastore->set_parameter_source('GET');
			$adname = $this->datastore->pick('adname');
			$duration = $this->datastore->pick('duration');
			$enabled = $this->datastore->pick('enabled');
			$qqfile = $this->datastore->pick('qqfile');
			$browsefile = $this->datastore->pick('browsefile');
			
			
			$enabled  = ($enabled)?1:0;

			$continue = true;
			
			//name is required
			if($adname ==''){
				$continue = false;
				$args['message'] = 'Please complete all fields';
			}
			
			//do not save unsupported file extensions
			$filename = $qqfile?$qqfile:$browsefile;
			$file_check = $this->getFileType($filename);
			
			if($file_check[0]=='unsupported'){
				$continue = FALSE;
				$args['message'] = $file_check[1]." is not yet supported, please select another one";
			}
			
			if($continue && $qqfile){
				//upload the file using the uploader
				$uploader = load_class('fileUploader');
				$result = $uploader->get_status();
				if(!isset($result['success'])) $result['success'] = FALSE;
				if(!$result['success']){
					$continue = FALSE;
					$args['message']  = $result['error'];
					$args['success']  = FALSE;
				}else{
					$filename = $result['filename'].'_'.date("YmdHis").".".$result['ext'];
					$ext = $result['ext'];
					//on success we move the file from the upload folder to the media folder
					$uploader->move_file("media".DS.$filename);
				}
			}
			
			$ext = isset($ext)?$ext:$file_check[1];
			
			if($continue){
				//let us get the extensions 
				$adType_arr = $this->db->selectQueryArr('ad_type',"type_name,ad_type_id,extensions");
				foreach($adType_arr as $adTypes){
					$extensions = explode(",",$adTypes['extensions']);
					if(in_array($ext,$extensions)){
						$ad_type_id = $adTypes["ad_type_id"];
						$type_name = $adTypes["type_name"];
						break;
					}
					//if it pasrsed through this is an unknown file
					$ad_type_id = 3;
				}
				
				//let us insert the ad now		
				$insertData = array('ad_name'=>$adname,'position_id'=>1,'body_text'=>'','duration'=>$duration,'ad_type_id'=>$ad_type_id,'isEnabled'=>$enabled,'filename'=>$this->datastore->sanitizer($filename));
				
				$this->db->insertQuery('ads',$insertData);
				$insertData['fileType'] = $type_name;
				$args['message'] = 'Insert success';
				$insertData['ad_id'] = $this->db->getInsertId();
				$args['insertedData'] = $insertData;
			}
			
			$args['success'] = $continue;
			$this->json_output($args);
		}
		
			
		function delete_data(){
			$data_id = $this->datastore->pick('data_id');
			$this->db->deleteQuery("ads","ad_id=$data_id");
			
			$args['success'] = true;
			$args['data_id'] = $data_id;
			
			$this->json_output($args);
			
		}
	
	}
