<div id="breadcrumb">Users >> <a href="<?php echo UI::transform_url("users","");?>">List</a> >> Edit User</div>
<div id="user_div">
	<div id="user_form">
	<form id="frmEdit" >
		<label for="container_no">Username</label>
		<span id="username_span"><?php echo $user['user_username'];?></span>
		
		<label for="boxes">password</label>
		<input type="text" name="password" value = "">
		
		<label for="boxes">re-type password</label>
		<input type="text" name="retype_password" value = "">
		
		<label for="contact_home">Last Names</label>
		<input type="text" name="lname" id="trip_no" size="40"  value="<?php echo $user['user_lname'];?>" >
		
		<label for="vessel_name">first Name</label>
		<input type="text" name="fname" id="fname" ize="40" value="<?php echo $user['user_fname'];?>" >
		
		
		<label for="country" >Country</label>
		<select name="country">
			<?php 
				foreach($countries as $country){
			?>
				<option value="<?php echo $country['country_id'];?>"><?php echo $country['country'];?></option>
			<?php
				}
			?>
		</select>
	
		<label for="branch" >Branch</label>
		<select name="branch"></select>
	
		<label for="level">User Level</label>
		<select name="user_level"></select>
		
		<input type="hidden" name="cid" id="cid" value="<?php echo $users['user_id'];?>" >
		<p class="clear">
			<a href="<?php echo UI::transform_url("users","");?>" class="button">Cancel</a>
			<input id="btnUpdate" type="submit" name="btnUpdate" class="button" value="Update User" />
		</p>
	</form>
	</div>
	
	<div id="userExtra_div"></div>
</div>
