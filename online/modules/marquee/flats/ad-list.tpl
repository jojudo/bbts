<div id="breadcrumb">Admin >> <b>Users</b></div>
<form name="frmOrder" id="frmOrder">
<p id="top-controls">
	<a href="#" id="btnAdd" class="button add">Add Marquee Text</a>
</p>

	<table width="100%" class="data-table" cellspacing="0">
		<thead>
		<tr>
			<th></th>
			<th>Text</th>
			<th>Status</th>
			<th>Delete</th>
		</tr>
		<thead>
		<tbody id="sortable">
<?php
if($adverts['rows']){
		$ctr=0;
		foreach($adverts['data'] as $data){ $ctr++;
?>
			<tr id="data-row-<?php echo $data['data_id'];?>">
				<td class="holder">+</td>
				<td><?php echo $data['body_text'];?></td>
				<td><div class="buttongroup"><a href="#" line-id="<?php echo $data['data_id'];?>" class="button-toggle <?php echo $data['isEnabled']?'active':'inactive';?>"><?php echo $data['isEnabled']?'PUBLISHED':'NOT PUBLISHED';?></a></div></td>
				<td>
					<a href="#" line-id="<?php echo $data['data_id'];?>" class="link delete"></a>
					<input type="hidden" value="<?php echo $data['data_id'];?>" name="order[]" />
				</td>
			</tr>
<?php			
		}
}
?>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="6">&nbsp;</td>
			</tr>
		</tfoot>
	</table>

</form>