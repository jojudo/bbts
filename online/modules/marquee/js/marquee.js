function addForm(){	
	createInfo('addDiv','...','Add Ad',250,500);
	loadJsTemplate('addDiv',modPath+'js/templates/advert-add.tpl','setupAddForm');
}

function addFormCallback(){
	//request the teams list		
	param = "action=resources";
	//now define the ajax call and callback function
	ajaxCallJson(param,'POST','setupAddForm');
}

function setupAddForm(data){
	//add data to select field
	$("#frmAdd").submit(function(){submitAddform();return false;});
}

function submitAddform(){
	var form_arr = new Array();
	form_arr[0] = 'bodyText';
	
	if(checkForm(form_arr)){
		//passed password screening
		var dataSet = $("#frmAdd").serialize();
		param = "action=add_data&"+dataSet;
		
		//now do an ajax call
		ajaxCallJson(param,'GET','submitAddCallback');
	}
		
}

function submitAddCallback(data){
	if(data.success){
		createAlertAutoClose('New Marquee Text recorded successfully!',500);
		closeInfo('addDiv');
		appendData(data.insertedData);
	}else{
		alert(data.message);
		if(data.highlight)
			$("#"+data.highlight).css("background","#FFFF66");
			$("#"+data.highlight).focus();
	}
}

function appendData(data){
	
	var str = '<tr id="data-row-'+data.ad_id+'">';
		str+='<td class="holder">+</td>';
		str+='<td>'+data.body_text+'</td>';
		str+='<td>';
			str+='<div class="buttongroup"><a href="#" line-id="'+data.ad_id+'" class="button-toggle '+(data.isEnabled?'active':'inactive')+'">'+(data.isEnabled?'PUBLISHED':'NOT PUBLICHED')+'</a></div>';
		str+='</div>';
		str+='<td><a href="#" line-id="'+data.ad_id+'" class="link delete"></a></td>';
		str+='</tr>';
		
	$(".data-table").find("tbody").prepend(str);
	rebind();
}


//delete user
function deleteRecord(obj){
	var data_id = obj.attr("line-id");
	$("#data-row-"+data_id).css("background","red");
	if(confirm("Are you sure you wish to remove this Record?")){
		param = "action=delete_data&data_id="+data_id;
		ajaxCallJson(param,'GET','deleteCallback');
	}else{
		$("#data-row-"+data_id).css("background","");
	}
}

function deleteCallback(data){
	if(data.success){
		$("#data-row-"+data.data_id).animate({
			opacity: 0,
			height: "toggle"
		  }, 500, function() {
			$(this).remove();
		  });
		
	}
}

//Save order
function saveOrder(){
	var dataSet = $("#frmOrder").serialize();
	param = "action=order_data&"+dataSet;
	
	//now do an ajax call
	ajaxCallJson(param,'POST','orderCallback','orderCallback');
}


function orderCallback(){
	//createAlertAutoClose('Media Order Successfully Updated!',1000);
	console.log("we are sorting, but being silent");
}


//activat account
function activateAccount(obj){
	var data_id = obj.attr("line-id");
	param = "action=activate_ad&data_id="+data_id;
	ajaxCallJson(param,'GET','activateCallback');

}

function activateCallback(data){
	var obj = $("#data-row-"+data.data_id);
	var linkObj = obj.find(".inactive");
	linkObj.removeClass("inactive");
	linkObj.addClass("active");
	linkObj.html("PUBLISHED");
	rebind();
}


//deactivate account
function deactivateAccount(obj){
	var data_id = obj.attr("line-id");
	param = "action=deactivate_ad&data_id="+data_id;
	ajaxCallJson(param,'GET','deactivateCallback');

}

function deactivateCallback(data){
	var obj = $("#data-row-"+data.data_id);
	var linkObj = obj.find(".active");
	linkObj.removeClass("active");
	linkObj.addClass("inactive");
	linkObj.html("NOT PUBLISHED");
	rebind();
}


//deactivate bindings
function bindings(){
	//bind them to bindings
	$(".edit").bind("click",function(){editRecord($(this));return false;});		
	$(".delete").bind("click",function(){deleteRecord($(this));return false;});		
	$(".inactive").bind("click",function(){activateAccount($(this));return false;});		
	$(".active").bind("click",function(){deactivateAccount($(this));return false;});		
}

function unbind(){
	//clear anything connected
	$(".edit").unbind("click");
	$(".delete").unbind("click");
	$(".inactive").unbind("click");
	$(".active").unbind("click");
}

function rebind(){
	unbind();
	bindings();
}
$(function() {
	$("#btnAdd").bind("click",function(){addForm();return false;});		
	bindings();
	$("#sortable").sortable({
		placeholder: "holder",
		handle:".holder",
		stop:function(){ saveOrder(); }
	});
});