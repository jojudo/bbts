function searchManager(param){
	var key = $("#keyword").val();
	var source = $("#source").val();
	
	$("#resultArea").html('<p class="loader"><img src="images/loader.gif"> Searching Clients...');
	$.ajax({
		  type: "POST",
		  url: "index.php?mod=contactManager",
		  async: false,
		  data: "ptype=ajax&action=search&key="+key+"&source="+source+"&"+param,
		   success: function(data){
				if(data==0){
					sess_failure();
				}else{
					$("#resultArea").html(data);
				}
		   },
		 error: function(XMLHttpRequest, status, error){alert("There was a problem ... please try again");}
	})
}

function viewJobSummary(jobID){
	createInfo('local_modal','...','Job Order Summary',400,550);
	$("#local_modal").html('<p class="loader"><img src="images/loader.gif"> Loading Details...');
	$.ajax({
		  type: "POST",
		  url: "index.php?mod=jobOrder",
		  async: false,
		  data: "ptype=ajax&action=view&jid="+jobID,
		   success: function(data){
				if(data==0){
					sess_failure();
				}else{
					$("#local_modal").html(data);
				}
		   },
		 error: function(XMLHttpRequest, status, error){alert("There was a problem ... please try again");}
	})
	
}

$(function() {
		searchManager('');
		$(".jobLink").live('click',function(){viewJobSummary($(this).attr("href"));return false;})
		$("#managerForm").submit(function(){searchManager('');return false;});
		$(".pagingClass").live('click',function(){searchManager('start='+$(this).attr("href"));return false;})
		
});