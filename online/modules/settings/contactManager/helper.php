<?php
	class moduleHelper{
		var $action;
		var $db;
		var $mod = "contactManager";
		
		function __construct($db){
			$action = mex::request("action");
			$action = $action?$action:"main";
			$this->action = $action;
			$this->db = $db;
			$this->render();
			
		}
				
		function render(){
			switch ($this->action){		
				case "new":
					$this->getNew();
					break;
				case "search":
					$this->listResult();
					break;
				default:
					$this->listMain();
					break;
			}
		}
		
		function listMain(){
			UI::set_js("modules".DS.$this->mod.DS."js".DS."list-main.js");
			require("modules".DS.$this->mod.DS."flats".DS."list-main.php");		
		}
		
		function listResult(){
			//exit();
			$keyword = mex::secure_post("key");
			$source = mex::secure_post("source");
			
			$limit = 20;
			$start = mex::secure_post("start");
			$start = $start?$start:0;
			
			$sql = "select count(customer_id) as total
					from ".PREFIX."customer 
					where
						lname like '$keyword%' or 
						fname like '$keyword%'";
			
			$total_rs = $this->db->queryMe($sql);
			$total_arr = $this->db->fetchAssoc($total_rs);
			$this->db->freeMe($total_rs);
			$total = $total_arr['total'];
			
			$sql = "select  customer_id,
							concat(lname,', ',fname) name,
							concat(number,' ',street,' ',city,' ',state,' ',zip) as address,
							phone_home as landline,
							phone_work as work,
							phone_cell as mobile,
							notes,
							(select job_id from ".PREFIX."joborder 
								where customer_id = c.customer_id order by job_date DESC limit 0,1) job_id,
							(select DATE_FORMAT(job_date,'%m/%d/%Y') from ".PREFIX."joborder 
								where customer_id = c.customer_id order by job_date DESC limit 0,1) job_date
					from ".PREFIX."customer as c
					where
						lname like '$keyword%' or 
						fname like '$keyword%'
					order by job_date ASC
					limit $start,$limit";
			
			$rs = $this->db->queryMe($sql);
						
			if(mysql_num_rows($rs)!=0){
				echo '<table cellpadding="3" cellspacing="0" width="100%" class="table_bordered">';
				echo '<tr class="table_headers">';
				echo '<td width="10px"></td>';
				echo '<td>Name</td>';
				echo '<td>Address</td>';
				echo '<td>Contact Nos</td>';
				echo '<td width="100px">Latest Transaction</td>';
				echo '<td width="20%">Notes</td>';
				echo '<td width="100px"></td>';
				echo '</tr>';
				$i=$start;
				while($p=$this->db->fetchAssoc($rs)){
					$i++;
					if($i%2==0)
						$className = "even";
					else
						$className = "odd";
					
					if(trim($p['name'])==",")	
						$name="-- NO NAME ---";
					else 
						$name = $p['name'];
					if($p['job_date']!="")	
						$latest_job = $p['job_date'];
						
					else
						$latest_job = "removed / NA";
						
					echo '<tr valign="top" class="'.$className.'">';
					echo '<td class="cell_border_bottom cell_border_right"><b>'.$i.'.</b></td>';
					echo '<td class="cell_border_bottom cell_border_right"><a href="index.php?mod=clients&action=modify&cid='.$p['customer_id'].'">'.$name.'</a>&nbsp;</td>';
					echo '<td class="cell_border_bottom cell_border_right">'.$p['address'].'&nbsp;</td>';
					echo '<td class="cell_border_bottom cell_border_right">';
					echo $p['landline']!=''?'<b>Home:</b> '.mex::phoneFormat("-",$p['landline']).'<br>':'';
					echo $p['mobile']!=''?'<b>Mobile:</b> '.mex::phoneFormat("-",$p['mobile']).'<br>':'';
					echo $p['work']!=''?'<b>Work:</b> '.mex::phoneFormat("-",$p['work']).'<br>':'';
					echo '&nbsp;</td>';
					echo '<td class="cell_border_bottom cell_border_right">';
					if($p['job_date']!="")
						echo '<a href="'.$p['job_id'].'" class="jobLink" title="click to view summary">'.$latest_job.'</a>&nbsp;</td>';
					else
						echo '<u><i><b>removed / NA</b></i></u>';
					echo '<td class="cell_border_bottom cell_border_right">'.mex::textToParagraph($p['notes']).'&nbsp;</td>';
					echo '<td class="cell_border_bottom"><a href="" title="manage contact"><img src="images/btn-manage.png" border="0"></a></td>';
					echo '</tr>';
				}
			echo '</table>';
			echo '<p>';
			mex::classBasedPaging('pagingClass',$total,$limit,$start);
			echo '</p>';
			
			}else{
				echo '<p align="center">There are no matches found</p>';	
			}
		}
	}
?>