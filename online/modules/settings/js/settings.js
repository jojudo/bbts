function saveSettings(){
	var content = '<div class="loader">Updating, please wait.... <img src="images/bar-loader.gif"></div>';
	var dataSet = $("#settingsForm").serialize();
	$("#settingsForm :input").attr("disabled","disabled");
	
	createInfo('local_modal',content,'Info',100,300);
	$("#local_modal").parent().children(".ui-dialog-titlebar").css("display","none");
	$.ajax({
	  type: "POST",
	  url: "index.php?mod=settings",
	  async: false,
	  data: "ptype=ajax&action=save&"+dataSet,
	  success: function(data){
	  	if(data==0){
	  		sess_failure();
	  	}else if(data==1){
	  		//alert('Settings were successfully updated');
	  		//$("#local_modal").dialog('close')
			$("#local_modal").html('<div class="loader">Settings Updated successfully <br /><a href="#" id="modal_close">OK</a></div>');
			$("#modal_close").button();
			$("#modal_close").click(function(){$("#local_modal").dialog('close');return false;});
	  	}else{
	  		alert(data);
	  	}
	  	$("#settingsForm :input").attr("disabled","");
	   },
	   error:function(a,b,c){alert("unable to update, please try again");$("#settingsForm :input").attr("disabled","");}
	 })
}

$(function() {
		$("#settingsTabs").tabs();
		$("#settingsForm").submit(function(){saveSettings();return false;});
});
	