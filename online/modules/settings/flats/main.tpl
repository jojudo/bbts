<div id="middle_content">
	<p class="page_header"><img src="images/configuration.png">Site Settings</p>
	<div id="breadcrumb"><a href="index.php">Home</a> >> <b>settings</b></div>
	<form method="POST" id="settingsForm">
	<div id="settingsTabs">
		<ul>
			<li><a href="#statusTab">Site Status</a></li>
			<li><a href="#siteTab">Public Site</a></li>
			<li><a href="#databaseTab">Database Settings</a></li>
			<li><a href="#mailTab">Mail Settings</a></li>
			<li><a href="#smsTab">SMS Settings</a></li>
			<li><a href="#mapTab">Google Map Settings</a></li>
		</ul>
		<div id="statusTab">
			<table cellpadding="3" cellspacing="0" width="80%" >
				<tr>
					<td width="150px"><b>Site Offline</b></td>
					<td>
						<input type="radio" name="offline" value="1" <?php echo $config->offline==1?"checked":"";?> > yes
						<input type="radio" name="offline" value="0" <?php echo $config->offline==0?"checked":"";?>> no
					</td>
				</tr>
				<tr>
					<td><b>Offline Message</b></td>
					<td><textarea name="offline_msg" rows="3" cols="50"><?php echo $config->offline_msg;?></textarea></td>
				</tr>
				<tr>
					<td colspan="2" height="20px"></td>
				</tr>
			</table>
		</div>
		<div id="siteTab">
			<table cellpadding="3" cellspacing="0" width="80%" >
					<tr>
						<td width="150px"><b>Site Title</b></td>
						<td><input type="text" name="site_title" size="40px" value="<?php echo $config->site_title;?>"></td>
					</tr>
					<tr>
						<td><b>Rewrite URL</b></td>
						<td>
							<input type="radio" name="rewrite_url" value="1" <?php echo $config->rewrite_url==1?"checked":"";?> > yes
							<input type="radio" name="rewrite_url" value="0" <?php echo $config->rewrite_url==0?"checked":"";?> > no
						</td>
					</tr>
					<tr>
						<td><b>Site Keywords</b></td>
						<td><input type="text" name="meta_keywords" size="40px" value="<?php echo $config->meta_keywords;?>"></td>
					</tr>
					<tr>
						<td><b>Site Description</b></td>
						<td>
							<textarea name="meta_desc" rows="3" cols="50"><?php echo $config->meta_desc;?></textarea>
						</td>
					</tr>
					<tr>
						<td><b>Meta Author</b></td>
						<td><input type="text" name="meta_author" size="40px" value="<?php echo $config->meta_author;?>"></td>
					</tr>
					<tr>
						<td><b>Site Timezone</b></td>
						<td><input type="text" name="timezone" size="40px" value="<?php echo $config->timezone;?>"></td>
					</tr>
					<tr>
						<td colspan="2" height="10px">&nbsp</td>
					</tr>
				</table>
		</div>
		<div id="databaseTab">
			<table cellpadding="3" cellspacing="0" width="80%" >
					<tr>
						<td width="150px"><b>Hostname</b></td>
						<td><input type="text" name="db_host" size="40px" value="<?php echo $config->db_host;?>"></td>
					</tr>
					<tr>
						<td><b>Username</b></td>
						<td><input type="text" name="db_user" size="40px" value="<?php echo $config->db_user;?>"></td>
					</tr>
					<tr>
						<td><b>Password</b></td>
						<td><input type="text" name="db_pass" size="40px" value="<?php echo $config->db_pass;?>"></td>
					</tr>
					<tr>
						<td><b>Database</b></td>
						<td><input type="text" name="db_name" size="40px" value="<?php echo $config->db_name;?>"></td>
					</tr>
					<tr>
						<td><b>Database Prefix</b></td>
						<td><input type="text" name="db_prefix" size="40px" value="<?php echo $config->db_prefix;?>"></td>
					</tr>
					<tr>
						<td colspan="2" height="10px">&nbsp</td>
					</tr>
				</table>
		</div>
		<div id="mailTab">
			<table cellpadding="3" cellspacing="0" width="80%" >
					<tr>
						<td width="150px"><b>SMTP Server</b></td>
						<td><input type="text" name="smtp_server" size="40px" value="<?php echo $config->smtp_server;?>"></td>
					</tr>
					<tr>
						<td><b>SMTP User</b></td>
						<td><input type="text" name="smtp_user" size="40px" value="<?php echo $config->smtp_user;?>"></td>
					</tr>
					<tr>
						<td><b>SMTP Password</b></td>
						<td><input type="text" name="smtp_pass" size="40px" value="<?php echo $config->smtp_pass;?>"></td>
					</tr>
					<tr>
						<td><b>Admin Email</b></td>
						<td><input type="text" name="admin_email" size="40px" value="<?php echo $config->admin_email;?>"></td>
					</tr>
					<tr>
						<td><b>Email Errors</b></td>
						<td><input type="radio" name="email_errors" value="0" <?php echo $config->email_errors==0?" checked":"";?> >No 
							<input type="radio" name="email_errors" value="1" <?php echo $config->email_errors==1?" checked":"";?>>Yes</td>
					</tr>
					<tr>
						<td><b>Email Notifications</b></td>
						<td><input type="radio" name="email_notify" value="0" <?php echo $config->email_notify==0?" checked":"";?> >No 
							<input type="radio" name="email_notify" value="1" <?php echo $config->email_notify==1?" checked":"";?>>Yes</td>
					</tr>
					<tr>
						<td><b>Error Limit</b></td>
						<td><input type="text" name="error_limit" size="10" value="<?php echo $config->error_limit; ?>"></td>
					</tr>
					<tr>
						<td colspan="2" height="10px">&nbsp</td>
					</tr>
				</table>
		</div>
		<div id="smsTab">
			<table cellpadding="3" cellspacing="0" width="80%" >
					<tr>
						<td><b>SMS API URL</b></td>
						<td><input type="text" name="sms_url" size="100px" value="<?php echo $config->sms_url;?>"></td>
					</tr>
					<tr>
						<td><b>SMS PARAMS</b></td>
						<td><input type="text" name="sms_param" size="100px" value="<?php echo $config->sms_param;?>"></td>
					</tr>
					<tr>
						<td><b>SMS recipient PARAM</b></td>
						<td><input type="text" name="sms_toVar" size="100px" value="<?php echo $config->sms_toVar;?>"></td>
					</tr>
					<tr>
						<td><b>SMS Message PARAM</b></td>
						<td><input type="text" name="sms_msgVar" size="100px" value="<?php echo $config->sms_msgVar;?>"></td>
					</tr>
				</table>
		</div>
		<div id="mapTab">
			<table cellpadding="3" cellspacing="0" width="80%" >
					<tr>
						<td><input type="checkbox" name="map_active" size="100px" <?php echo $config->map_active?"checked":"";?> > Google Maps is Active</td>
					</tr>
					<tr>
						<td><b>Google Map Key</b></td>
						<td><input type="text" name="map_key" size="100px" value="<?php echo $config->map_key;?>"></td>
					</tr>
				</table>
		</div>
	</div>
	<p><input type="submit" name="action" value="Save Settings"></p>
	</form>
</div>