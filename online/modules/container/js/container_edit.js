function showContainerBoxes(){
	var data_start = window.start;
	var param = "action=container_boxes&cid="+$("#cid").val()+"&start="+data_start;
	
	//now define the ajax call and callback function
	ajaxCallJson(param,'GET','buildResultTable');
}

function buildResultTable(data){
	
	if(!$("#result-table").length){
		buildTablebase();
	}
	
	var str = '';
	var ctr=data.firstNumber;
	window.start = data.start;

	$.each(data.boxes,function(index,value){
		ctr++;
		str+='<tr>';
		str+='<td>'+ctr+'</td>';
		str+='<td>'+value.packing_no+'</td>';
		str+='<td>'+value.consignee+'</td>';
		str+='<td>'+value.delivery_address+'</td>';
		str+='<td>';
		str+='<a href="#" id="box-'+value.box_id+'" bid="'+value.box_id+'" class="button delete removeBox">Remove</a>'
		str+='</td>';
		str+='<tr>';
	})	
	
	$("#result-table").find("tbody").append(str);
	if(data.start==0)
		$(window).unbind("scroll");
		
	$(".removeBox").on("click",function(){removeBoxFromContainer($(this));return false;});

}

function buildTablebase(){
	var str ='<table class="data-table" id="result-table">';
	str+='<thead>';
	str+='<tr>';
	str+='<th>#</th>';
	str+='<th>Packing No#</th>';
	str+='<th>Consignee</th>';
	str+='<th>Delivery Address</th>';
	str+='<th></th>';
	str+='</tr>';
	str+='</thead>';
	str+='<tbody>';	
	str+='</tbody>';
	str+='</table>';
	
	$("#containerBoxes_div").html(str);
	$("#showBoxes").remove();
	
	
	
	$(window).scroll(function () {
        if ($(document).height() <= $(window).scrollTop() + $(window).height()) {
            showContainerBoxes();
        }
    });
}

//------------- box removal functions
function removeBoxFromContainer(obj){
	window.removeID= obj.attr("bid");
	addHighlightClass(obj.parent().parent());
	dialogYesNo("Are you sure you wish to remove This Box from the container?","callRemoveBox","uncallRemoveBox");
}

function uncallRemoveBox(){
	removeHighlightClass($("#box-"+removeID).parent().parent());
}

function callRemoveBox(){
	var param = "action=remove_box&bid="+removeID+"&cid="+$(cid).val();
	//now define the ajax call and callback function
	ajaxCallJson(param,'GET','removeBoxNotice');
}

function removeBoxNotice(data){
	if(data.success){
		$("#box-"+removeID).parent().parent().fadeIn(function(){
			$(this).remove();
		})
		
		createAlertAutoClose("Box successfully Removed from Container!",500);
	}else{
		errorAlert(data.message);
	}
}

//--------------- remove File from the list of file uploads
function removeFile(obj){
	window.removeID = obj.prev().attr("fid");
	dialogYesNo("Are you sure you wish to remove File?","callRemoveFile");
}

function callRemoveFile(){
	var param = "action=delete_file&fid="+removeID;
	//now define the ajax call and callback function
	ajaxCallJson(param,'GET','removeFileNotice');
}

function removeFileNotice(data){
	if(data.success){
		$("#file-"+data.file_id).parent().remove();
		createAlertAutoClose("File Successfully deleted!",500);
	}else{
		errorAlert(data.message);
	}
}

function updateContainer(){	
	var param = ""
	param = "action=update_data";
	param = param+"&"+$("#frmEdit").serialize();
	//now define the ajax call and callback function
	ajaxCallJson(param,'POST','updateNotice');
}

function updateNotice(data){
	var message = "";
	if(data.success)
		message = "Container details now updated"
	else	
		message = "Problem updating container";
		
	createAlertAutoClose("<p>"+message+"</p>",1000)
}

function setupUploader(){
	var uploader = new qq.FileUploader({
		element: document.getElementById("btnUpload"),
		action: 'index.php',
		minSizeLimit: 10,
		onComplete: function(id, fileName, responseJSON){
			if(responseJSON.session==0){
				quick_login_form(responseJSON);
			}else{
				if(responseJSON.success){
				  $("#frmAdd")[0].reset();
				  createAlertAutoClose('File Uploading successfull',1000);
				  submitAddCallback(responseJSON);
				}else{
					alert(responseJSON.message);
				}
			}
			
			$(".qq-upload-list").empty();
		},
		onSubmit:function (id, fileName){
			//check values first
			var form_arr = new Array();
				form_arr[0] = 'adname';
				form_arr[1] = 'adposition';
			if(!checkForm(form_arr))	
				return false;
			var isEnabled = 0;
			if(	$("#enabled").prop("checked"))
				isEnabled = 1;
				
			uploader.setParams({
				  action: 'add_data',
				  headerType:'none',
				  mod:mod,
				  adname:$("#adname").val(),
				  enabled:isEnabled,
				  duration:$("#duration").val()
			});
		}
	});
}

$(function() {
	initDate("dated");
	$("#showBoxes").click(function(){showContainerBoxes();return false;});
	$("#frmEdit").submit(function(){updateContainer();return false;})
	if($(".removeMe").length){
			$(".removeMe").click(function(){removeFile($(this));return false;});
	}
	setupUploader();
});