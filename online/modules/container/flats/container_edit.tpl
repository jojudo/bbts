<div id="breadcrumb">Containers >> <a href="<?php echo UI::transform_url("container","");?>">List</a> >> Edit Container</div>
<div id="container_div">
	<div id="container_form">
	<form id="frmEdit" >
		<label for="container_no">Container No#</label>
		<input type="text" name="container_no" id="container_no" size="40" class="container_no" value = "<?php echo $container['container_no'];?>" />
		
		<label for="boxes">Boxes</label>
		<input type="text" name="box_size" id="box_size" size="10" value = "<?php echo $container['size'];?>">
		
		<label for="contact_home">Trip No#</label>
		<input type="text" name="trip_no" id="trip_no" size="40"  value="<?php echo $container['trip_no'];?>" >
		
		<label for="vessel_name">Vessel Name</label>
		<input type="text" name="vessel_name" id="vessel_name" class="vessel_name" size="40" value="<?php echo $container['vessel_name'];?>" >
		<br class="clear" />
		<label for="departure" >Departure</label>
		<input type="text" name="departure" id="departure" size="15" autocomplete="off" value="<?php echo $container['depart'];?>" class="dated" >
		<br class="clear" />
		<label for="departure" >Arrival</label>
		<input type="text" name="arrival" id="arrival" size="20" autocomplete="off" value="<?php echo $container['arrive'];?>" class="dated">
		<br class="clear" />
		<label for="customs">Customs </label>
		<input type="text" name="customs" id="customs" size="20" autocomplete="off" value="<?php echo $container['release_date'];?>" class="dated">
		<input type="hidden" name="cid" id="cid" value="<?php echo $container['container_id'];?>" >
		<p><input id="btnUpdate" type="submit" name="btnUpdate" class="button" value="Update Container Details" /></p>
	</form>
	</div>
	<div id="container_files">
		<h3>Uploaded Files</h3>
		<?php if(count($files)){ ?>
		<div id="fileDiv">
		<ul>
			<?php foreach($files as $f){?>
				<li> 
					<a href="#" id="file-<?php echo $f['file_id'];?>" fid = "<?php echo $f['file_id'];?>" ><?php echo $f['file_name'];?></a>
					<a href="#" class="removeMe"></a>
				</li>
			<?php } ?>
		</ul>
		</div>
		<?php } ?>
		<a href="#" class="button" id="btnUpload">Upload File</a>
	</div>
	<p class="clear"></p>	
	<a href="#" class="button" id="showBoxes">Show All <?php echo $container['boxes'];?> Boxes in Container</a>
	<div id="containerBoxes_div"></div>
</div>
