<div id="breadcrumb">Containers >> <b>List</b></div>
<div id="container_div">
	<div id="search_result">
		<h2><b>Total Records : <span id="container_count"></span></b></h2>
		<table width="100%" class="data-table" border="0" cellspacing="0">
			<thead>
			<tr class="table_headers">
				<td height="30px" >#</td>
				<td>Container No#</td>
				<td>Vessel</td>
				<td>Trip No</td>
				<td>Departure - Arrival (estimated)</td>
				<td>Boxes</td>
				<td>&nbsp</td>
			</tr>
			</thead>
			<tbody>
			</tbody>
			<Tfoot></tfoot>
		</table>
	</div>
	<div id="container_paging" class="records_paging"></div>
</div>
