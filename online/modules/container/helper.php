<?php
	include CLASSPATH.DS.'interface.insertSet.php';
	class moduleHelper extends helperController implements insertSet{
				
		function __construct(){
			parent::__construct();
		}
		
		protected function main(){
			$this->set_mod_javascript();
			$this->ui->use_module_flat('container_list');
		}
		
		public function container_page(){
			$this->args = $this->list_containers();
			$this->json_output($this->args);
		}
		
		private function list_containers(){
			$page = intval($this->datastore->pick('page'));
			$page = $page?$page:1;
			$limit = 20;
			
			if($page>1)
				$start = (($page-1)*$limit);
			else
				$start =0;
				
			$start=$start?$start:0;
			
			$table = PREFIX."container C inner join ".PREFIX."shipment S using (shipment_id)";
			$fields = "c.container_id,c.container_no,";
			$fields.= "s.shipment_id,s.trip_no,s.vessel_name,";
			$fields.= "DATE_FORMAT(est_departure,'%b %d, %Y') depart,DATE_FORMAT(est_arrival,'%b %d, %Y') arrive,";
			$fields.= "(select count(box_id) from ".PREFIX."boxes where container_id = C.container_id) as boxes";
			
			$args['containers'] = $this->db->selectQueryArr($table,"SQL_CALC_FOUND_ROWS ".$fields,"","s.est_departure DESC","$start,$limit");
			$args['rows'] = $this->db->found_rows();
			
			$maxPages = ceil($args['rows']/$limit);
			$next = $page+1;
			$next = $next<=$maxPages?$next:0;
			$prev = $page-1;
			$prev = $prev>0?$prev:0;
			
			
			$args['pages'] = $maxPages;
			$args['page'] = $page;
			$args['items'] = $limit;
			$args['next'] = $next;
			$args['prev'] = $prev;
			
			return $args;
			
		}
		
		public function edit_data(){
			$containerId = intval($this->datastore->pick('cid'));
			$table = PREFIX."container C inner join ".PREFIX."shipment S using (shipment_id)";
			
			$fields = "c.container_id,c.container_no,";
			$fields.= "s.shipment_id,s.trip_no,s.vessel_name,c.size,";
			$fields.= "DATE_FORMAT(est_departure,'%m/%d/%Y') depart,DATE_FORMAT(est_arrival,'%m/%d/%Y') arrive,";
			$fields.= "DATE_FORMAT(date_released,'%m/%d/%Y') release_date,";
			$fields.= "(select count(box_id) from ".PREFIX."boxes where container_id = C.container_id) as boxes";
			
			$this->args['container'] = $this->db->selectQueryFirst($table,$fields,"container_id = $containerId");
			$this->args['files'] = $this->db->selectQueryArr(PREFIX."containerfiles","file_name,file_id,title","container_id = $containerId");
			
			$this->set_mod_javascript("container_edit");
			$this->set_mod_stylesheet();
			$this->ui->use_module_flat('container_edit',$this->args);
		}
		
		
		function update_data(){
			$this->datastore->set_parameter_source('POST');
			$container_no = $this->datastore->pick('container_no');
			$box_size = $this->datastore->pick('box_size');
			$customs = $this->datastore->pick('customs');
			
			$trip_no = $this->datastore->pick('trip_no');
			$vessel = $this->datastore->pick('vessel_name');
			$departure = $this->datastore->pick('departure');
			$arrival = $this->datastore->pick('arrival');
			
			$cid = $this->datastore->pick('cid');
			$this->args = array();
			
			//get the shipment id
			$shipment_arr = $this->db->selectQueryFirst(PREFIX."container","shipment_id","container_id = $cid");
			$sid = $shipment_arr['shipment_id'];
			
			//start the updateQuery
			$fields = array();
			
			$fields['container_no'] = $container_no;
			$fields['size'] = $box_size;
			$fields['date_released'] = date("Y-m-d",strtotime($customs));
			
			$result = $this->db->updateQuery(PREFIX."container",$fields,"container_id=$cid");
			$this->args['message'] = $result." records updated for container";
				
			//update shipment 
			$fields = array();
			
			$fields['trip_no'] = $trip_no;
			$fields['est_departure'] = date("Y-m-d",strtotime($departure));
			$fields['est_arrival'] = date("Y-m-d",strtotime($arrival));
			$fields['vessel_name'] = $vessel;
			
			$result = $this->db->updateQuery(PREFIX."shipment",$fields,"shipment_id=$sid");
			$this->args['message'] .= " ".$result." records updated for shipment";
			
			$this->args['success'] = true;
			$this->json_output($this->args);
		}
		
		public function container_boxes(){
			$containerId = intval($this->datastore->pick('cid'));
			$start = intval($this->datastore->pick('start'));
			
			$start = $start?$start:0;
			$limit = 10;
				
			$table = PREFIX."boxes";
			$fields = "box_id,packing_no,consignee,date_loaded,delivery_address";
			$this->args['boxes'] = $this->db->selectQueryArr($table,$fields,"container_id = $containerId","packing_No ASC","$start, $limit");
			$rows = $this->db->found_rows();
			
			if($start<$rows){
				$this->args['start'] = $start+$limit;
				$this->args['firstNumber'] = $start;
			}else{
				$this->args['start'] = 0;
			}
			
			$this->json_output($this->args);
		}
		
		public function delete_file(){
			$fileid = intval($this->datastore->pick('fid'));
			$upload_dir = BASE.DS.$this->settings =  $this->datastore->get_config('upload')['dir'].DS.$this->mod;
			//Select filename
			if($fileid ){
				$rs = $this->db->selectQueryFirst(PREFIX."containerfiles","file_name","file_id = $fileid");
				$filename = $rs['file_name'];
			}
			
			if($filename && file_exists($upload_dir.DS.$filename)){
				unlink($upload_dir.DS.$filename);
			}
			
			//wether file exists or not remove from list
			$result = $this->db->deleteQuery(PREFIX."containerfiles","file_id = $fileid");
			
			if($result){
				$this->args['success'] = true;
				$this->args['file_id'] = $fileid;
			}else{
				$this->args['success'] = false;
				$this->args['message'] = $this->db->getErrorMsg();
			}
			
			$this->json_output($this->args);
			
		}
		
		public function remove_box(){
			$box_id = intval($this->datastore->pick('bid'));
			$container_id = intval($this->datastore->pick('cid'));
			
			$fields['container_id'] = 0;
	
			$result = $this->db->updateQuery(PREFIX."boxes",$fields,"container_id=$container_id and box_id = $box_id");
			
			if($result==0){
				$this->args['success'] = false;
				$this->args['message'] = "Unable to remove Box from container";
			}else{
				$this->args['success'] = true;
				$this->args['affected_rows'] = $result;
			}
			
			$this->json_output($this->args);
		}
		
		public function add_data(){
			$this->datastore->set_parameter_source('GET');
			$adname = $this->datastore->pick('adname');
			$duration = $this->datastore->pick('duration');
			$enabled = $this->datastore->pick('enabled');
			$qqfile = $this->datastore->pick('qqfile');
			$browsefile = $this->datastore->pick('browsefile');
			
			
			$enabled  = ($enabled)?1:0;

			$continue = true;
			
			//name is required
			if($adname ==''){
				$continue = false;
				$args['message'] = 'Please complete all fields';
			}
			
			//do not save unsupported file extensions
			$filename = $qqfile?$qqfile:$browsefile;
			$file_check = $this->getFileType($filename);
			
			if($file_check[0]=='unsupported'){
				$continue = FALSE;
				$args['message'] = $file_check[1]." is not yet supported, please select another one";
			}
			
			if($continue && $qqfile){
				//upload the file using the uploader
				$uploader = load_class('fileUploader');
				$result = $uploader->get_status();
				if(!isset($result['success'])) $result['success'] = FALSE;
				if(!$result['success']){
					$continue = FALSE;
					$args['message']  = $result['error'];
					$args['success']  = FALSE;
				}else{
					$filename = $result['filename'].'_'.date("YmdHis").".".$result['ext'];
					$ext = $result['ext'];
					//on success we move the file from the upload folder to the media folder
					$uploader->move_file("media".DS.$filename);
				}
			}
			
			$ext = isset($ext)?$ext:$file_check[1];
			
			if($continue){
				//let us get the extensions 
				$adType_arr = $this->db->selectQueryArr('ad_type',"type_name,ad_type_id,extensions");
				foreach($adType_arr as $adTypes){
					$extensions = explode(",",$adTypes['extensions']);
					if(in_array($ext,$extensions)){
						$ad_type_id = $adTypes["ad_type_id"];
						$type_name = $adTypes["type_name"];
						break;
					}
					//if it pasrsed through this is an unknown file
					$ad_type_id = 3;
				}
				
				//let us insert the ad now		
				$insertData = array('ad_name'=>$adname,'position_id'=>1,'body_text'=>'','duration'=>$duration,'ad_type_id'=>$ad_type_id,'isEnabled'=>$enabled,'filename'=>$this->datastore->sanitizer($filename));
				
				$this->db->insertQuery('ads',$insertData);
				$insertData['fileType'] = $type_name;
				$args['message'] = 'Insert success';
				$insertData['ad_id'] = $this->db->getInsertId();
				$args['insertedData'] = $insertData;
			}
			
			$args['success'] = $continue;
			$this->json_output($args);
		}
		
		public function browse_media(){
			$folder_files = scandir("media/");
			$skip_files = array(".","..","ready.json","count.json");
			
			foreach($folder_files as $file){
				if(!in_array($file,$skip_files)){
					$file_check = $this->getFileType($file);
					if($file_check[0]!='unsupported')
						$args['media'][] = array($file,$file_check[0]);
				}
			}

			$args['success'] = TRUE;
			
			$this->json_output($args);
		}
		
		private function getFileType($filename){
			$img_arr = explode(".",$filename);
			$ct= count($img_arr);
			$ext = strtolower($img_arr[($ct-1)]);
			
			if(!isset($this->adType_arr)){
				$this->adType_arr = $this->db->selectQueryArr('ad_type',"type_name,extensions");
			}
			
			foreach($this->adType_arr as $adTypes){
				$ext_types = explode(",",$adTypes['extensions']);
				$extensions = explode(",",$adTypes['extensions']);
				if(in_array($ext,$extensions)){
					return array($adTypes["type_name"],$ext);
				}
			}
			
			return array('unsupported',$ext);
		}
		
		
		function delete_data(){
			$data_id = $this->datastore->pick('data_id');
			$this->db->deleteQuery("ads","ad_id=$data_id");
			
			$args['success'] = true;
			$args['data_id'] = $data_id;
			
			$this->json_output($args);
			
		}
		
		function resources(){
			$args['data'] = $this->db->selectQueryArr("ad_positions","position_id,position_name");
			$args['success'] = true;
			
			$this->json_output($args);
		}
		
		function activate_ad(){
			$data_id = $this->datastore->pick('data_id');
			$result = $this->db->updateQuery("ads",array("isEnabled"=>1),"ad_id=$data_id");
			if($result)
				$args['success'] = true;
			else
				$args['success'] = false;
				
			$args['data_id'] = $data_id;
			
			$this->json_output($args);
			
		}
		
		function deactivate_ad(){
			$data_id = $this->datastore->pick('data_id');('data_id');
			$result = $this->db->updateQuery("ads",array("isEnabled"=>0),"ad_id=$data_id");
			if($result)
				$args['success'] = true;
			else
				$args['success'] = false;
				
			$args['data_id'] = $data_id;
			
			$this->json_output($args);
			
		}
		
		function order_data(){
			//
			$this->datastore->set_parameter_source('POST');
			$order= $this->datastore->pick('order');
			$ctr=0;
			foreach($order as $ad_id){ $ctr++;
				$this->db->updateQuery("ads",array("`order`"=>$ctr),"ad_id=$ad_id");
			}
			
			$args['success'] = true;
			$this->json_output($args);
		}
		
	}
