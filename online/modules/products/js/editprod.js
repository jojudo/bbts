var mod = "products";

function checkprod(){
	var base_arr = new Array();	
	
	base_arr[0] = 'pname';
	base_arr[1] = 'ptitle';
	base_arr[2] = 'prodtype';
	base_arr[3] = 'pfprice';
	base_arr[4] = 'peprice';
	base_arr[5] = 'addr_country';
	
	
	if(checkForm(base_arr)){
		checkprodname($('#pname').val(),$('#pid').val());
	} 
}
function checkprodname(prodname,prodid) {
	$.ajax({
		  type: "POST",
		  url: "index.php?mod="+mod,
		  async: false,
		  data: "ptype=ajax&action=checkprodnameagain&prodname="+prodname+"&prodid="+prodid,
		   success: function(data){
		   	if(data==0){
		   		sess_failure();
		   	}else if(data==1){	
				submits();
		   	}else{
		   		alert("Product "+prodname+" exists in the database! Choose another name to be updated!");
		   		
		   	}
		   }
		})
}
function submits(){	
	
	//1.) submit the product details	
	loadingInfo('windowloader','<div class="loader">Updating Boxes...<br /><img src="images/bar-loader.gif"> <div>','',100,450);
	var dataSet = $("#edit_product").serialize();
	$("#edit_product :input").attr("disabled","disabled");
	$.ajax({
		  type: "POST",
		  url: "index.php?mod="+mod,
		  async: false,
		  data: "ptype=ajax&action=updateProd&"+dataSet,
		   success: function(data){
		   	if(data==0){
		   		sess_failure();
		   	}else if(IsNumeric(data)){
				$("#windowloader").dialog('close');
		   		alert("Successfully updated product details");
		   		$("#edit_product :input").attr("disabled","");
		   		
		   	}else{
		   		alert("Unable to update file");
		   		$("#edit_product :input").attr("disabled","");
		   	}
		   }
		})
	
}
function clearSlate(){
	$('input[type="text"]').val('');
	$("option:nth-child(1)").attr("selected","selected");
	$(":input").attr("disabled","");
}

$(function() {
		$("#pfprice,#peprice").keydown(function(event) {
				
				if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 110 || event.keyCode == 9 || event.keyCode == 27 || 
					 
					(event.keyCode == 65 && event.ctrlKey === true) || 
					 
					(event.keyCode >= 35 && event.keyCode <= 39)) {
						 
						 return;
				}
				else {
					
					if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
						event.preventDefault(); 
					}   
				}
		});
		$("#submitButton").click(function(){checkprod()});
		
		
});