{{DATASTART}}
<table class="table table-striped table-bordered table-hover" id="product-list">
    <thead>
        <tr>
            <th>#</th>
            <th>Name</th>
            <th>Code</th>
            <th>Type</th>
            <th>Price</th>
        </tr>
    </thead>
    <tbody>
    {{LOOPSTART}}
        <tr>
            <td>{num}</td>
            <td>{product_title}</td>
            <td>{product_name}</td>
            <td>{type}</td>
            <td>{price}</td>
            <td>
                <a href="#" product-id="{product_id}" class="btn btn-default edit-product"><i class="fa fa-pencil fa-fw" ></i>Edit</a>
                
                <a href="#" product-id="{product_id}" class="btn btn-default delete-product"><i class="fa fa-remove fa-fw"></i>Delete</a>
            </td>
        </tr>
     {{LOOPEND}}   
    </tbody>
</table>
{{DATAEND}}