function loadProducts(){
    framework.ajaxGeneric({param:"action=loadProducts",callback:"populate",submitType:'get',dataType:'json'});
}

function populate(data){
   framework.loadtemplate({"template":"list","callback":"showResults","passover":{'data':data.data}});
}

function showResults(data){
    $("#search-results").html(framework.loopStr(data.template,data.data.products));
    $(".edit-product").click(function(){editProduct($(this));return false;})
}

function editProduct(obj){
    //alert(obj.attr("product-id"));
    framework.createDialog({id:"edit-form",content:"",title:"Title "+obj.attr("product-id"),height:"500",width:"600",obj:obj})
}

$(function() {	
   /*$('#product-list').DataTable({
            responsive: true
    });*/
    //{template=text,callback = text,passover  = obj, callbackobj=obj}
    loadProducts();
   
});

