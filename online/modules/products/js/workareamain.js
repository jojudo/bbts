var mod = 'products';

function pagingProd(param){
	loadingInfo('windowloader','<div class="loader">Loading Products...<br /><img src="images/bar-loader.gif"> <div>','',100,450);
	$.ajax({
		  type: "GET",
		  url: "index.php?mod="+mod,
		  async: true,
		  data: "ptype=ajax&action=loaded&"+param,
		   success: function(data){
				if(data==0){
					sess_failure();
				}else{
					$("#loaded").html(data);
					$("#windowloader").dialog('close');
				}
		   },
		 error: function(XMLHttpRequest, status, error){alert("There was a problem ... please try again");}
	})
} 
function deleteProd(obj){
	var pid = obj.attr("href");
	var pname =  obj.attr("prodname");
	var answer = confirm("Are you sure you wish to delete product "+pname +"?")
	if (answer){
		$.ajax({
			  type: "POST",
			  url: "index.php?mod="+mod,
			  async: true,
			  data: "ptype=ajax&action=deleteprod&pid="+pid,
			   success: function(data){
					if(data==0){
						sess_failure();
					}else if(data==1){
						alert("Product "+pname+" was successfully deleted");
						$("#tr_"+pid).empty();
						$("#tr_"+pid).remove();
						pagingProd('start=0');
						
					}else{
						alert(data);
					}
			   },
			 error: function(XMLHttpRequest, status, error){alert("There was a problem ... please try again");$("#local_modal").dialog('close');}
		})
	}
}
$(function() {
		pagingProd('start=0');
		$(".prodDelete").live('click',function(e){deleteProd($(this));return false;});
		$(".pagingClass").live('click',function(){pagingProd('start='+$(this).attr("href"));return false;});
		
});