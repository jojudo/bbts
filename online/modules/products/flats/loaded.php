<?php

	if(mysql_num_rows($workarea_rs)!=0)
	{
		?>
	<p><b>Total Records : <?php echo number_format($total); ?></b></p>
			<table width="100%" border="0" cellpadding="3" cellspacing="0" class="table_bordered">
			<tr class="table_headers">
			<td width="20px"></td>
			<td width="150px"><b>Product Name</b></td>
			<td width="100px"><b>Product Title</b></td>
			<td width="100px"><b>Product Type</b></td>
			<td width="100px"><b>Price</b></td>
			<td width="150px"><b>Price Empty</b></td>
			<td width="100px"><b>For Country</b></td>
			<td width="100px"><b>Date Added</b></td>
			
			<td width="140px">&nbsp;</td>
			</tr>
			<?php
				$i=$start;
				while($p=$this->db->fetchAssoc($workarea_rs))
				{$i++;
					if($i%2==0)
						$className="even";
					else
						$className = "odd";
					?>
					<tr valign="top" class="<?php echo $className;?>" id="tr_<?php echo $p['product_id'];?>">
					<td class="cell_border_bottom cell_border_right"><?php echo $i;?>.</td>
					<td class="cell_border_bottom cell_border_right"><?php echo $p['product_name']; ?></td>
					<td class="cell_border_bottom cell_border_right"><?php echo $p['product_title']; ?></td>
					<td class="cell_border_bottom cell_border_right"><?php echo $p['type']; ?></td>
					<td class="cell_border_bottom cell_border_right" align="right"><?php echo $p['price_full']>0?'$ '.number_format($p['price_full'],2):'FREE'; ?></td>
					<td class="cell_border_bottom cell_border_right" align="right"><?php echo $p['price_full']>0?'$ '.number_format($p['price_empty'],2):'FREE'; ?></td>
					<td class="cell_border_bottom cell_border_right" align="center"><?php echo $p['country']; ?></td>
					<td class="cell_border_bottom cell_border_right" align="center"><?php echo $p['dated']; ?></td>
					<td class="cell_border_bottom">
					<a href="<?php echo mex::long_addr("index.php?mod=$this->mod&action=editprod&pid=$p[product_id]");?>"><img src="images/btn-modify.png" border="0"></a>
					<a href="<?php echo $p['product_id'];?>" class="prodDelete" prodname="<?php echo $p['product_name'];?>">
					<img src="images/btn-del.png" border="0"></a>
					</td>
					<tr/>
				<?php
				}
				?>
			</table>
			<p><?php mex::classBasedPaging('pagingClass',$total,$limit,$start);?></p>
				<?
			}
			else
			{
				?>
				<div align="center">No Data to be Displayed.</div>
				<?php
			}
			
			?>