<div id="middle_content">
<?php echo $this->breadcrumb("Edit Product");?>

<form name="edit_product" id="edit_product" method="POST">
<table cellpadding="3" cellspacing="0" width="100%" class="table_bordered">
<tr class="table_headers">
	<td>Edit Product</td>
	
</tr>
<tr><td valign="top">
	<div id="client_details">
	<table width="100%" border="0" cellpadding="3" cellspacing="0">
		<tr>
			<td><b>Product ID</b></td>
			<td><?php echo $editprod_arr['product_id']; ?></td>
		</tr>
		<tr>
			<td valign="top"  width="150px"><b>Product Name</b><font color="red">*</font></td>
			<td>
				<input type="text" class="matchMe" name="pname" value="<?php echo $editprod_arr['product_name']; ?>" size="40" id="pname" autocomplete="off">
				<div id="matchLoader" style="display:inline;"></div>
				<div id="matchBtn" style="display:inline;"></div>
			</td>
		</tr>
		<tr>
			<td valign="top"  width="150px"><b>Product Title</b><font color="red">*</font></td>
			<td>
				<input style="text-transform:uppercase" type="text" value="<?php echo $editprod_arr['product_title']; ?>" class="matchMe" name="ptitle" size="10" id="ptitle" autocomplete="off">
				<div id="matchLoader" style="display:inline;"></div>
				<div id="matchBtn" style="display:inline;"></div>
			</td>
		</tr>
		<tr>
			<td valign="top"  width="150px"><b>Product Type</b><font color="red">*</font></td>
			<td>
				<select name="prodtype" id="prodtype">
				<option value=""> --- </option>
				
				<?php
					while($p=$this->db->fetchAssoc($ptype_rs))
					{
						
						if($p['product_type_id']==$editprod_arr['product_type_id'])
								$selected = " selected";
						else
								$selected  = "";
								
						echo '<option value="'.$p['product_type_id'].'" '.$selected.'>'.$p['type'].'</option>';
					}
				?>
				
				</select>
			</td>
		</tr>
		<tr>
			<td valign="top"  width="150px"><b>Product Price Full</b><font color="red">*</font></td>
			<td>
				<input type="text" class="matchMe" name="pfprice" value="<?php echo $editprod_arr['price_full']; ?>" size="40" id="pfprice" autocomplete="off">
				<div id="matchLoader" style="display:inline;"></div>
				<div id="matchBtn" style="display:inline;"></div>
			</td>
		</tr>
		<tr>
			<td valign="top"  width="150px"><b>Product Price Empty</b><font color="red">*</font></td>
			<td>
				<input type="text" class="matchMe" name="peprice" value="<?php echo $editprod_arr['price_empty']; ?>" size="40" id="peprice" autocomplete="off">
				<div id="matchLoader" style="display:inline;"></div>
				<div id="matchBtn" style="display:inline;"></div>
			</td>
		</tr>
		<tr>
			<td style="padding-left:15px;"><b>Country</b><font color="red">*</font></td>
			<td>
				<select name="addr_country" id="addr_country">
				<option value=""> --- </option>
				<?php
					while($p=$this->db->fetchAssoc($country_rs))
					{
						
						if($p['country_id']==$editprod_arr['country_id'])
								$selected = " selected";
						else
								$selected  = "";
								
						echo '<option value="'.$p['country_id'].'" '.$selected.'>'.$p['country'].'</option>';
					}
				?>
				
				</select>
			</td>
		</tr>
		
	</table>
	</div>
	</td></tr></table>
	<p align="right"><input type="hidden" name="pid" value="<?php echo $editprod_arr['product_id']; ?>" id="pid" /><input type="button" class="buttons" id="submitButton" name="btnEdit" value="Update Product" class="btn"></p>
</form>
</div>