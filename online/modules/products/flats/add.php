<div id="middle_content">
<?php echo $this->breadcrumb("Add Product");?>
<form name="add_product" id="add_product" method="POST">
<table cellpadding="3" cellspacing="0" width="100%" class="table_bordered"">
<tr class="table_headers">
	<td>Add Products</td>
	
</tr>
<tr><td valign="top">
	<div id="client_details">
	<table width="100%" border="0" cellpadding="3" cellspacing="0">
		
		<tr>
			<td valign="top"  width="150px"><b>Product Name</b><font color="red">*</font></td>
			<td>
				<input type="text" class="matchMe" name="pname" size="40" id="pname" autocomplete="off">
				<div id="matchLoader" style="display:inline;"></div>
				<div id="matchBtn" style="display:inline;"></div>
			</td>
		</tr>
		<tr>
			<td valign="top"  width="150px"><b>Product Title</b><font color="red">*</font></td>
			<td>
				<input style="text-transform:uppercase" type="text" class="matchMe" name="ptitle" size="10" id="ptitle" autocomplete="off">
				<div id="matchLoader" style="display:inline;"></div>
				<div id="matchBtn" style="display:inline;"></div>
			</td>
		</tr>
		<tr>
			<td valign="top"  width="150px"><b>Product Type</b><font color="red">*</font></td>
			<td>
				<select name="prodtype" id="prodtype">
				<option value=""> --- </option>
				<?php
					while($p=$this->db->fetchAssoc($ptype_rs))
					{
						echo "<option value='$p[product_type_id]'>$p[type]</option>";
					}
				?>
				
				</select>
			</td>
		</tr>
		<tr>
			<td valign="top"  width="150px"><b>Product Price Full</b><font color="red">*</font></td>
			<td>
				<input type="text" class="matchMe" name="pfprice" size="40" id="pfprice" autocomplete="off">
				<div id="matchLoader" style="display:inline;"></div>
				<div id="matchBtn" style="display:inline;"></div>
			</td>
		</tr>
		<tr>
			<td valign="top"  width="150px"><b>Product Price Empty</b><font color="red">*</font></td>
			<td>
				<input type="text" class="matchMe" name="peprice" size="40" id="peprice" autocomplete="off">
				<div id="matchLoader" style="display:inline;"></div>
				<div id="matchBtn" style="display:inline;"></div>
			</td>
		</tr>
		<tr>
			<td style="padding-left:15px;"><b>Country</b><font color="red">*</font></td>
			<td>
				<select name="addr_country" id="addr_country">
				<option value=""> --- </option>
				<?php
					while($p=$this->db->fetchAssoc($country_rs))
					{
						echo "<option value='$p[country_id]'>$p[country]</option>";
					}
				?>
				</select>
			</td>
		</tr>
		
	</table>
	</div>
	</td></tr></table>
	<p align="right"><input type="button" class="buttons" id="submitButton" name="btnSave" value="Add Product" class="btn"></p>
</form>
</div>