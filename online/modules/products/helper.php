<?php
	class moduleHelper extends helperController{
		
        function __construct(){
			parent::__construct();
		}
		
		function main(){
            $this->set_libraries("bower_components/datatables/media/js","jquery.dataTables.min","js");    
            $this->set_mod_javascript();
            $this->use_module_template('product-list',array());
		}
		
        function loadProducts()
        {		
			$page = intval($this->datastore->pick('page'));
			$page = $page?$page:1;
			$limit = 10;
			
			if($page>1)
				$start = ($page*$limit)+1;
			else
				$start =0;
            
            $this->db->set_table("products p");
            $this->db->set_join("product_type t","t.product_type_id=p.product_type_id");
            $this->db->set_fields("SQL_CALC_FOUND_ROWS product_id,product_name,product_title,type");
            $this->db->set_fields("FORMAT(price_empty,2) price");
            $this->db->set_orderby("product_name","ASC");
            $this->db->set_limit($start,$limit);
               
			$args['products'] = $this->db->select();
        
            $args['rows'] = $this->db->found_rows();
		
			$maxPages = floor($args['rows']/$limit);
			$prev = $page-1;
			$next = $page+1;
			
			$args['prev'] = $prev;			
			$args['next'] = $next<=$maxPages?$next:0;
			
			$this->json_output($args);
		}
		
		function add(){
			
			UI::set_js("modules".DS.$this->mod.DS."js".DS."add.js");
			UI::set_stylesheet("modules".DS.$this->mod.DS."styles".DS."boxes.css");
			$sql = "select country_id,country from ".PREFIX."countries order by country ASC";
			$country_rs = $this->db->queryMe($sql);
			$sql = "select * from ".PREFIX."product_type order by product_type_id ASC";
			$ptype_rs = $this->db->queryMe($sql);
			
			
			require("modules".DS.$this->mod.DS."flats".DS."add.php");
			$this->db->freeMe($country_rs);
			$this->db->freeMe($ptype_rs);
			
		}
		
		
		function addProd(){
			
			if(mex::secure_post("action")=="addProd"){
				
				$pname = mex::secure_post("pname");
				$ptitle = strtoupper(mex::secure_post("ptitle"));
				$ptype = mex::secure_post("prodtype");
				$pfprice = mex::secure_post("pfprice");
				$peprice = mex::secure_post("peprice");
				$addr_country = mex::secure_post("addr_country");
				
				$entry_date = date("Y-m-d");
				
				$sql = "insert into ".PREFIX."products
							(	country_id,product_name,
								product_title,product_type_id,price_full,price_empty,
								date_added
							)values(
								'$addr_country','$pname',
								'$ptitle','$ptype','$pfprice','$peprice',
								'$entry_date'
							)";
				
				$this->db->queryMe($sql);
				echo mysql_insert_id();
			} else {
				echo "Unable to process request";
			}
		
		}
		
		function updateProd(){
			
			if(mex::secure_post("action")=="updateProd"){
			
				$pid = mex::secure_post("pid");
				$pname = mex::secure_post("pname");
				$ptitle = strtoupper(mex::secure_post("ptitle"));
				$ptype = mex::secure_post("prodtype");
				$pfprice = mex::secure_post("pfprice");
				$peprice = mex::secure_post("peprice");
				$addr_country = mex::secure_post("addr_country");
				
				$entry_date = date("Y-m-d");
				
				$sql = "update ".PREFIX."products set
							country_id = '$addr_country',
							product_name = '$pname',
							product_title = '$ptitle',
							product_type_id = '$ptype',
							price_full = '$pfprice',
							price_empty = '$peprice'
						where product_id = '$pid'";
				
				$this->db->queryMe($sql);
				echo $pid;
			} else {
				echo "Unable to process request";
			}
		
		}
		
		function editprod(){
		
			UI::set_js("modules".DS.$this->mod.DS."js".DS."editprod.js");
			UI::set_stylesheet("modules".DS.$this->mod.DS."styles".DS."boxes.css");
			
			$pid = mex::secure_get("pid");
			
			$sql = "select * from ".PREFIX."products where product_id = '$pid'";
			
			$editprod_arr = $this->db->queryFirst($sql);
			
			$sql = "select country_id,country from ".PREFIX."countries order by country ASC";
			
			$country_rs = $this->db->queryMe($sql);
			
			$sql = "select * from ".PREFIX."product_type order by product_type_id ASC";
			
			$ptype_rs = $this->db->queryMe($sql);
			
			require("modules".DS.$this->mod.DS."flats".DS."editprod.php"); 
			
			
			$this->db->freeMe($country_rs);
			$this->db->freeMe($ptype_rs);
			
			
		}		
		
		function deleteprod(){
			//delete
			$pid = mex::secure_post("pid");
			
			$sql = "delete from ".PREFIX."products where product_id='$pid'";
			$this->db->queryMe($sql);
			
			echo 1;
		}
		
		function checkprodname(){
			//check product name
			$prodname = mex::secure_post("prodname");
			
			$sql = "select * from ".PREFIX."products where product_name = '$prodname'";
			$checkprod_rs = $this->db->queryMe($sql);
			
			if(mysql_num_rows($checkprod_rs)!=0) {
				echo '2';
			} else {
				echo '1';
			}
			
			
		}
		
		function checkprodnameagain(){
			//check product name
			$prodname = mex::secure_post("prodname");
			$prodid = mex::secure_post("prodid");
			
			$sql = "select * from ".PREFIX."products where product_name = '$prodname' and product_id <> '$prodid'";
			$checkprodagain_rs = $this->db->queryMe($sql);
			
			if(mysql_num_rows($checkprodagain_rs)!=0) {
				echo '2';
			} else {
				echo '1';
			}
			
			
		}
		
}

?>