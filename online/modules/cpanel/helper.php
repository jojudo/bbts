<?php
	load_interface("insertSet");
	class moduleHelper extends helperController implements insertSet{
		function __construct(){
			parent::__construct();
		}
		
		function main(){
			
			//$this->set_mod_javascript();
			$this->set_mod_stylesheet();
			$this->set_mod_javascript();

			$this->args['panels'] = $this->__get_shortcuts();
			
			//changing location of folder
			$this->use_module_template('cpanel-2',$this->args);
			
		}
				
		private function __get_shortcuts(){
			
			$this->db->set_table("cpanel");
            $this->db->set_fields("icon_id,label,module,action,img,`order`");
            $this->db->set_orderby("`order`","ASC");
			$panels = $this->db->select(); 
			
			$allowed_panels = array();
			foreach($panels as $panel){
				if($this->permission->is_permitted($panel['module'],$panel['action'])){
					$allowed_panels[] = $panel;
				}
			}
			echo $this->db->getErrorMsg();
			return $allowed_panels;
		}
		
		function view(){
			$this->args['panels'] = $this->__get_shortcuts();
			$this->use_module_template('list-view',$this->args);
		}
		
		function moveUp(){
			$icon_id = pick(1);
			$this->db->set_table("cpanel");
            $this->db->set_fields("`order`");
			$this->db->set_condition("icon_id=$icon_id");
			$this->db->select(false);
			$order = $this->db->queryResults()[0]['order'];
			if($order>1){
				$order--;
				//we will replace the previous one in this number
				$this->db->set_table("cpanel");
				$this->db->set_fields('`order`',$order+1);
				$this->db->set_condition("`order`=$order");
				$this->db->update();
					
				$this->db->set_table("cpanel");
				$this->db->set_fields('`order`',$order);
				$this->db->set_condition("icon_id=$icon_id");
				$this->db->update();				
			}
			
			header("location:index.php?mod=cpanel&action=view");
			exit();
			
		}
		function moveDown(){
			$icon_id = pick(1);
			$this->db->set_table("cpanel");
            $this->db->set_fields("`order`");
			$this->db->set_condition("icon_id=$icon_id");
			$this->db->select(false);
			$order = $this->db->queryResults()[0]['order'];
			if($order){
				$order++;
				//we will replace the previous one in this number
				$this->db->set_table("cpanel");
				$this->db->set_fields('`order`',$order-1);
				$this->db->set_condition("`order`=$order");
				$this->db->update();
					
				$this->db->set_table("cpanel");
				$this->db->set_fields('`order`',$order);
				$this->db->set_condition("icon_id=$icon_id");
				$this->db->update();				
			}
			
			header("location:index.php?mod=cpanel&action=view");
			exit();
		}
		
		function edit(){}
		
		function update(){}
		
		function save(){}
		
		function delete(){}
        
        
	}
?>