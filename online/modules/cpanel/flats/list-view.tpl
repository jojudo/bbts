 <div class="page-title">
	<div class="title_left"><h3>Shortcuts</h3></div>
 </div>
 <div class="row">
	<div class="col-md-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Table View</h2>
				<ul class="nav navbar-right panel_toolbox">
				  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
				  </li>
				  <li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
					<ul class="dropdown-menu" role="menu">
					  <li><a href="<?php echo transform_url('cpanel');?>">Panel View</a>
					   <li><a href="<?php echo transform_url('cpanel','create');?>">Add Shortcut</a>
					  </li>
					  </li>
					</ul>
				  </li>
				  <li><a href="<?php echo transform_url('cpanel','create');?>"><i class="fa fa-plus"></i></a>
				  </li>
				</ul>
				<div class="clearfix"></div>
			</div>
			 <div class="x_content" >
				<table class="table table-striped">
					  <thead>
						<tr>
						  <th>#</th>
						  <th>Title</th>
						  <th>Module</th>
						  <th>Page</th>
						  <th>icon</th>
						  <th>order</th>
						  <th>action</th>
						</tr>
					  </thead>
					  <tbody>
					  <?php 
							$ctr=0;foreach($panels as $panel) { $ctr++; 
							extract($panel);
					  ?>
						<tr>
						  <th scope="row"><?php echo $ctr;?></th>
						  <td><?php echo $label;?></td>
						  <td><?php echo $module;?></td>
						  <td><?php echo $action;?></td>
						  <td><?php echo $img;?></td>
						  <td><?php echo $order;?></td>
						  <td>
						  <?php if($order!=1) { ?>
							<a href="<?php echo transform_url('cpanel','moveUp',$panel['icon_id']);?>" title="Move Up"><i class="fa fa-arrow-up"></i></a>
						  <?php } ?>
							<a href="<?php echo transform_url('cpanel','moveDown',$panel['icon_id']);?>" title="Move Down"><i class="fa fa-arrow-down"></i></a>
							<a href="<?php echo transform_url('cpanel','edit',$panel['icon_id']);?>" title="Edit"><i class="fa fa-edit fa-hover"></i></a>
							<a href="<?php echo transform_url('cpanel','delete',$panel['icon_id']);?>" title="Delete"><i class="fa fa-remove"></i></a>
							
						  </td>
						  
						</tr>
					  <?php } ?>
					  </tbody>
					</table>
				</div>
			</div>	
		</div>
	</div>
				