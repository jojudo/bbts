<div id="middle_content" class="container grid_12">
	<div class="grid_6 alpha omega">
		<div id="cpanel">
            <?php foreach($boxes as $box) { ?>
            <div class="icon">
                <a href = "<?php echo transform_url($box['module'],$box['action']);?>">
                    <img src="<?php echo mod_url();?>/images/<?php echo $box['module'];?>" border="0">
                    <span><?php echo $box['label'];?></span>
                </a>
            </div>
            <?php } ?>
        </div>
	</div>
	<div class="grid_6">
		<div id="accordion">
					<h3><a href="#">Messages</a></h3>
					<div>
						<div id="emailDiv">
							<div class="loader"><img src="images/loader.gif"> Loading Bulletins</div>
							<center><button onclick="requestMessages();">Reload Bulletin</button></center>
						</div>
					</div>
					<h3><a href="#" onclick="retreiveBulletin()">Warnings</a></h3>
					<div>
						<div id="bulletinDiv"><div class="loader"><img src="images/loader.gif"> Loading Bulletins</div></div>
					</div>
					<h3><a href="#">Online Users</a></h3>
					<div>
						<div id="bulletinDiv" style="padding:0px;width:100%;height:150px;"><div class="loader"><img src="images/loader.gif"> Loading Bulletins</div></div>
					</div>
		</div>
	</div>
	<br style="clear:both;" />
</div>