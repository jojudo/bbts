 <div class="page-title">
	<div class="title_left"><h3>Control Panel</h3></div>
 </div>
 <div class="row">
	<div class="col-md-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Shorcuts</h2>
				<ul class="nav navbar-right panel_toolbox">
				  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
				  </li>
				  <li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
					<ul class="dropdown-menu" role="menu">
					  <li><a href="<?php echo transform_url('cpanel','view');?>">Table View</a>
					  </li>
					  <li><a href="<?php echo transform_url('cpanel','create');?>">Add Shortcut</a>
					  </li>
					</ul>
				  </li>
				  <li><a class="close-link"><i class="fa fa-close"></i></a>
				  </li>
				</ul>
				<div class="clearfix"></div>
			</div>
            <div class="x_content" >
				<div class="col-md-9 col-sm-12 col-xs-12" id="cpanel">
					<?php foreach($panels as $panel) { ?>
					<div class="icon">
						<a href = "<?php echo transform_url($panel['module'],$panel['action']);?>">
							<img src="<?php echo mod_url();?>/images/<?php echo $panel['img'];?>" border="0">
							<span><?php echo $panel['label'];?></span>
						</a>
					</div>
					<?php } ?>
				</div>
            </div>
        </div>
	</div>
</div>
