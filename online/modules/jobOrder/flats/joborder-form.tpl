 <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Box Order Form</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
			
			<!-- Customer Info -->
			<div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Customer Info <small>different form elements</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                    <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">

                      <div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="flast-name">Last Name <span class="required">*</span></label>
						<div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
							<input type="text" class="form-control col-md-7 col-xs-12" required="required">
							<span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
						</div>
                      </div>
					  
					  <div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">First Name <span class="required">*</span></label>
						<div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
							<input type="text" class="form-control col-md-7 col-xs-12" required="required">
							<span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
						</div>
                      </div>
					  
					  <div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="Contact-num">Phone
                        </label>
						<div class="col-md-2 col-sm-2 col-xs-12 form-group has-feedback">
							<input type="text" class="form-control" id="inputSuccess5" placeholder="Home">
							<span class="fa fa-phone form-control-feedback right" aria-hidden="true"></span>
						</div>
						<div class="col-md-2 col-sm-2 col-xs-12 form-group has-feedback">
							<input type="text" class="form-control" id="inputSuccess5" placeholder="Office">
							<span class="fa fa-phone form-control-feedback right" aria-hidden="true"></span>
						</div>
						<div class="col-md-2 col-sm-2 col-xs-12 form-group has-feedback">
							<input type="text" class="form-control" id="inputSuccess5" placeholder="Mobile">
							<span class="fa fa-phone form-control-feedback right" aria-hidden="true"></span>
						</div>
                      </div>
					  
					    
					  <div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="email-address">Email
                        </label>
						<div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
							<input type="text" class="form-control" id="inputSuccess5" >
							<span class="fa fa-envelope form-control-feedback right" aria-hidden="true"></span>
						</div>
					  </div>
					  
					  <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Customer Notes
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <textarea class="form-control" rows="3" ></textarea>
                        </div>
                      </div>
					 <div class="form-group"> 
						 <div class="col-md-9 col-sm-9 col-xs-12">
						 <label class="control-label col-md-4 col-sm-4 col-xs-12" for="email-address"></label>
							
						  <div class="checkbox col-md-4 col-sm-4 col-xs-12">
							<label>
							  <input type="checkbox" value="" checked> Send Me Updates Via email
							</label>
						  </div>
						  <div class="checkbox col-md-4 col-sm-4 col-xs-12">
							<label>
							  <input type="checkbox" value="" checked> Send Me Updates Via SMS
							</label>
						  </div>
						 </div>
					 </div>
									
					</form>
                  </div>
                </div>
              </div>
            </div>
			<!-- /Customer Info -->
			
			
			<!-- /Job Details -->	
			 <div class="row">
				<div class="col-md-6 col-xs-12">
					<div class="x_panel">
					  <div class="x_title">
						<h2>Job Address <small>different form elements</small></h2>
						<ul class="nav navbar-right panel_toolbox">
						  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
						  </li>
						  <li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
							<ul class="dropdown-menu" role="menu">
							  <li><a href="#">Settings 1</a>
							  </li>
							  <li><a href="#">Settings 2</a>
							  </li>
							</ul>
						  </li>
						  <li><a class="close-link"><i class="fa fa-close"></i></a>
						  </li>
						</ul>
						<div class="clearfix"></div>
					  </div>
					  <div class="x_content form-horizontal form-label-left input_mask">
						<br />
						  <div class="form-group">
							<label class="control-label col-md-4 col-sm-4 col-xs-12">Address line 1</label>
							<div class="col-md-8 col-sm-8 col-xs-12">
							  <input type="text" class="form-control" placeholder="Default Input">
							</div>
						  </div>
						  <div class="form-group">
							<label class="control-label col-md-4 col-sm-4 col-xs-12">Address line 2</label>
							<div class="col-md-8 col-sm-8 col-xs-12">
							  <input type="text" class="form-control" disabled="disabled" placeholder="Disabled Input">
							</div>
						  </div>
						 <div class="form-group">
							<label class="control-label col-md-4 col-sm-4 col-xs-12">City</label>
							<div class="col-md-8 col-sm-8 col-xs-12">
							  <input type="text" class="form-control" disabled="disabled" placeholder="Disabled Input">
							</div>
						  </div>
						  <div class="form-group">
							<label class="control-label col-md-4 col-sm-4 col-xs-12">State</label>
							<div class="col-md-8 col-sm-8 col-xs-12">
							  <input type="text" class="form-control" disabled="disabled" placeholder="Disabled Input">
							</div>
						  </div>
						  <div class="form-group">
							<label class="control-label col-md-4 col-sm-4 col-xs-12">Zip</label>
							<div class="col-md-8 col-sm-8 col-xs-12">
							  <input type="text" class="form-control" disabled="disabled" placeholder="Disabled Input">
							</div>
						  </div>
						 <div class="form-group">
							<label class="control-label col-md-4 col-sm-4 col-xs-12">Country</label>
							<div class="col-md-8 col-sm-8 col-xs-12">
							  <input type="text" class="form-control" disabled="disabled" placeholder="Disabled Input">
							</div>
						  </div>
						  
						  <div class="form-group"> 
						 <div class="col-md-9 col-sm-9 col-xs-12">
						 <label class="control-label col-md-4 col-sm-4 col-xs-12" for="email-address"></label>
							
						  <div class="checkbox col-md-8 col-sm-8 col-xs-12">
							<label>
							  <input type="checkbox" value="" checked> Use as Primary address
							</label>
						  </div>
						 </div>
					 </div>
						
					  </div>
					</div>
					
					<!-- job Details -->
				</div>
				
				<div class="col-md-6 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Order Details <small>different form elements</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                    <form class="form-horizontal form-label-left">

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Job Date</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <input type="text" class="form-control" placeholder="Default Input">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Type</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <select class="form-control">
                            <option>Choose option</option>
                            <option>Option one</option>
                            <option>Option two</option>
                            <option>Option three</option>
                            <option>Option four</option>
                          </select>
                        </div>
                      </div>
					  
					   <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Regular Box</label>
						<div class="col-md-2 col-sm-2 col-xs-12">
						  <input type="text" class="form-control" placeholder="0">
						</div>
                      </div>
					  
					  <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Branch</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <select class="form-control">
                            <option>Choose option</option>
                            <option>Option one</option>
                            <option>Option two</option>
                            <option>Option three</option>
                            <option>Option four</option>
                          </select>
                        </div>
                      </div>
					  
					  <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Driver</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <select class="form-control">
                            <option>Choose option</option>
                            <option>Option one</option>
                            <option>Option two</option>
                            <option>Option three</option>
                            <option>Option four</option>
                          </select>
                        </div>
                      </div>
					  
					   <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Instructions
                        </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <textarea class="form-control" rows="3" ></textarea>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
			   
			 </div>
			
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
      </div>
    </div>
	
	
