<div class="container">
	<div class="row">
        <div class="col-lg-12">
            <h2> Job Order</h2>
        </div>
        <!-- /.col-lg-12 -->
    </div>
     <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
               <div class="panel-heading">
                    
                </div>
                
                <div class="panel-body">
                    <div class="col-lg-6">
                        <div>
							<h3>Customer Details</h3>
                            <div style="width:50%;float:left;padding-right:2px;">
                                <label for="lname">Last name</label>
                                <input type="text" name="lname" id="lname" class="matchMe form-control small" autocomplete="off">
                            </div>
                            <div style="width:50%;float:left;">
                                <label for="lname">First name</label>
                                <input type="text" name="fname" id="fname" class="matchMe form-control small" autocomplete="off">
                            </div>
                        </div>   
                        <label for="contact_home">Home No#</label>
                        <input type="text" name="contact_home" id="contact_home" class="form-control" autocomplete="off">
                        
                        <label for="contact_mobile">Mobile No#</label>
                        <input type="text" name="contact_mobile" id="contact_mobile" class="form-control"  autocomplete="off">
                        
                        <label for="contact_work">Work No#</label>
                        <input type="text" name="contact_work" id="contact_work" class="form-control"  autocomplete="off">
                        
                        <label for="email">Email</label>
                        <input type="text" name="contact_email" id="contact_email" class="form-control" autocomplete="off">
                        
                        
                        <label for="address1">Address 1</label>
                        <input type="text" name="address1" id="address1" class="form-control" autocomplete="off">

                        <label for="address2">Address 2</label>
                        <input type="text" name="address2" id="address2" class="form-control" autocomplete="off">
                        
                        <label for="address2">City</label>
                        <input type="text" name="city" id="city" class="form-control" autocomplete="off">
                        
                        <label for="state">State</label>
                        <input type="text" name="state" id="state" size="20" class="form-control" autocomplete="off">
                        
                        <label for="zip">zip code</label>
                        <input type="text" name="zip" id="zip" size="20" class="form-control"  autocomplete="off">
                        
                        <label for="address2">Country</label>
                        <input type="text" name="country" id="country" class="form-control" autocomplete="off">
                        
                    </div>
                    <div class="col-lg-6">
                        <h3>Additional Details</h3>
                        <label for="jobdate">Job Date</label>
                        <input type="text" name="jobDate" id="jobDate"  class="dated form-control" size="40" autocomplete="off" value="<?php echo date("m/d/Y");?>" >
                        
                        <label>Products</label>
                        <div id="item_details">
                            <a href="#" id="addItemLink" class="add"><i class="fa fa-plus-square"></i> Add Item</a>
                            <div id="ordersLine"></div>
                        </div>
                        
                        <div class="form-group">
                        <label for="bsource">Business Source</label>
                        <select name="bsource" id="bsource"  class="form-control">
                            <option value="0"> --- </option>
                            <?php foreach($sources as $source) { ?>
                                <option value="<?php echo $source['source_id'];?>"><?php echo $source['source_name'];?></option>
                            <?php } ?>
                        </select>
                        </div>
                        
                        <div class="form-group">
                        <label for="driver">Assigned Driver</label>
                        <select name="driver" id="driver" class="form-control">
                            <option value="0"> --- </option>
                            <?php foreach($drivers as $driver) { ?>
                                <option value="<?php echo $driver['user_id'];?>"><?php echo $driver['user_fname'].' '.$driver['user_lname'];?> </option>
                            <?php } ?>
                        </select>
                        </div>
                        
                        <div class="form-group">
                            <label>Additional Instructions</label>
                            <textarea name="instructions" id="instructions" class="form-control" rows="3"></textarea>
                        </div>
                        
                        
                                            
                    </div>
                </div>
				<div class="form-group panel-footers">
					<button type="submit" class="btn btn-standard">Create Job Order</button>
				</div>
            </div>
        </div>
     </div>
</div>

 