<form name="box_form" id="box_form" onsubmit="boxChecking();return false;">
<div style="height:400px;overflow:auto;">
<?php

//set destination select once here
$dest ='<option value="0">--- UNKNOWN ---</option>';
while($p=$this->db->fetchAssoc($dest_rs))
{	
	$dest.= '<option value="'.$p['code_id'].'">('.$p['code'].') '.$p['destination'].'</option>\n';
}
$dest.='</select>';

//set product type select once here
$prod = '';

while($p=$this->db->fetchAssoc($product_rs))
{			
	$default_price = $default_price?$default_price:number_format($p['price_full'],2);
	$prod.= '<option value="'.$p['product_id'].'" onclick="fetch_price($(this));" price="'.number_format($p['price_full'],2).'">'.$p['product_name'].'</option>\n';
}
$prod.='</select>';

//set shipment select once here
$ship = '';
$ship.= '<option value="0"> --- NO CONTAINER --- </option>';
while($p=$this->db->fetchAssoc($shipment_rs))
{				
	$ship.= '<option value="'.$p['container_id'].'">'.$p['container_no'].' ('.$p['trip_no'].')</option>\n';
}
$ship.='</select>';


for($i=1;$i<=$boxes;$i++){
	?>
		<fieldset>
			<legend>Box No#. <?php echo $i;?> of <?php echo $boxes;?> </legend>
				<table cellpadding="3" cellspacing="0" style="background:inherit;" width="100%">
				<tr>
					<td width="150px"><b>Packing No#.</b></td>
					<td>
						<input type="text" class="packing" name="packing[]" size="20" id="<?php echo 'packing'.$i;?>" value="<?php echo $packing_arr[$i-1];?>">
						<div id="<?php echo 'packing'.$i.'Div';?>"></idv>
					</td>
				</tr>
				<tr>
					<td width="100px"><b>Product Type</b></td>
					<td>
						<select name="product[]" class="product" id="<?php echo 'product'.$i;?>" num="<?php echo $i;?>">
							<?php echo $prod;?>
						</select>
					</td>
				</tr>
				<tr>
					<td><b>Price</b></td>
					<td>$ <input type="text" name="collected[]" class="collected" id="<?php echo 'collected'.$i;?>" size="20" value="<?php echo $default_price;?>"></td>
				</tr>
				<tr>
					<td><b>Status</b></td>
					<td>
						<select name="boxStatus[]" id="<?php echo 'boxStatus'.$i;?>" class="boxStatus">
							<option value="WH">In the Warehouse (Origin Country)</option>
							<option value="DRI">For Barcoding</option>
							<option value="WH" onclick="showEncodeContainer();">Loaded to Container</option>
							<option value="DWH" onclick="showEncodeContainer();">In the Warehouse</option>
							<option value="ARR" onclick="showEncodeContainer();">Received by Consignee</option>
						</select>
					</td>
				</tr>
				<tr>
					<td><b>Container</b></td>
					<td><select name="container[]" class="container" id="<?php echo 'container'.$i;?>"><?php echo $ship;?></td>
				</tr>
				<tr>
					<td><b>Date Received/loaded/stored</b></td>
					<td><input name="received[]" id="received<?php echo $i;?>" value="<?php echo date("m/d/Y");?>" class="pickers received"></td>
				</tr>
				<tr>
					<td><b>Consignee</b></td>
					<td>
						<input type="text" name="consignee[]" id="<?php echo 'consignee'.$i;?>" class="consignee" size="50" onkeyup="consignee_check($(this));">
						<div class="consigneeMatch"></div>
					</td>
				</tr>
				<tr>
					<td><b>Phone No#</b></td><td>
					(<input type="text" id="<?php echo 'home1_'.$i;?>" name="<?php echo 'home'.$i.'[]';?>" class="contact1" size="3" maxlength="3">) 
					<input type="text" id="<?php echo 'home2'.$i;?>" name="<?php echo 'home'.$i.'[]';?>" class="contact2" size="3"> - 
					<input type="text" id="<?php echo 'home3_'.$i;?>" name="<?php echo 'home'.$i.'[]';?>" class="contact3" size="5"> 
					(area code) + phone no#
				</td></tr>
				<tr>
					<td><b>Mobile No#</b></td>
					<td><input type="text" name="<?php echo 'mob'.$i;?>" class="contact4" size="20" maxlength="11"> (11-digit mobile no#)</td>
				</tr>
				<tr>
					<td><b>Address</b></td>
					<td><textarea id="con_addr<?php echo $i;?>" name="addr[]" rows="3" cols="50" class="addr"><?php echo $job_arr['destination'];?></textarea></td>
				</tr>
				<tr>
					<td><b>Province/Area</b></td>
					<td><select id="con_area<?php echo $i;?>" name="destination[]" class="destination" id="<?php echo 'destination'.$i;?>"><?php echo $dest;?></td>
				</tr>
				<tr>
					<td><b>Details</b></td>
					<td><textarea name="details[]" rows="3" cols="50" class="details">Personal Effects</textarea></td>
				</tr>
				<tr>
					<td colspan="2">
					<?php
					if($i==1 && $boxes!=1){
					?>
						<input type="button" name="btnDup" id="btnDup" value="Copy Values" onclick="boxDuplicate();">
						
					<?php }	?>
					</td>
				</tr>
	
			</table>
		</fieldset>
<?php 
}
?>
</div>
		<p align="right"><input type="button" class="btn" name="btnBox" id="btnSub" value="Record Boxes"></p>
	</form>