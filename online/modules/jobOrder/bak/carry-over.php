<div id="middle_content">
	<p class="page_header"><img src="images/deliver.gif">Job Order Reports</p>
	<div id="breadcrumb"><a href="index.php">Home</a> >> <b>Carried Over</b></div>
	<?php
			while($p=$this->db->fetchAssoc($area_rs)){
				$jobs_rs = $this->getNotDoneRS($p['area_id']);
				echo '<p class="sub_header">'.$p['area_name'].'</p>';
				echo '<table cellpadding="3" cellspacing="0" border="0" class="table_bordered" width="100%">';
				echo '<tr class="table_headers" align="center"><td width="2%"></td>';
				echo '<td width="8%;">&nbsp;<b>Job Date</b></td>';
				echo '<td width="15%"><b>Customer</b></td>';
				echo '<td width="15%"><b>Contact Nos#</b></td>';
				echo '<td width="5%">Boxes</td>';
				echo '<td width="25%"><b>Job Address</b></td>';
				echo '<td width="8%"><b>Job Type</b></td>';
				echo '<td width="28%"><b>Notes</b></td></tr>';
					$i=0;
					while($p=$this->db->fetchAssoc($jobs_rs)){
						$i++;
						$phone_arr = explode("|",$p['phones']);
						$age = mex::dateAge($p['jobDate']);
						echo '<tr valign="top">';
						echo '<td class="cell_border_right cell_border_bottom" style="min-height:40px;"><b>'.$i.'</b></td>';
						echo '<td class="cell_border_right cell_border_bottom"><a href="'.mex::long_addr("index.php?mod=jobOrder&action=edit&jid=".$p['job_id']).'" target="editPage" title="click to edit in new window">'.$p['jobDate'].'</a><br><font color="red"><b>'.$age.' day(s)</b></font></td>';
						echo '<td class="cell_border_right cell_border_bottom">'.utf8_decode($p['name']).'</td>';
						echo '<td class="cell_border_right cell_border_bottom">';
						echo '<table cellspacing="0" cellpadding="0">';
						echo $phone_arr[0]!=""?"<tr bgcolor='$bg'><td><b>Home</b> </td><td>: ".mex::phoneFormat("-",$phone_arr[0])."</td></tr>":"";
						echo $phone_arr[1]!=""?"<tr bgcolor='$bg'><td><b>Work</b> </td><td>: ".mex::phoneFormat("-",$phone_arr[1])."</td></tr>":"";
						echo $phone_arr[2]!=""?"<tr bgcolor='$bg'><td><b>Cell</b> </td><td>: ".mex::phoneFormat("-",$phone_arr[2])."</td></tr>":"";
						echo '<tr bgcolor="'.$bg.'"><td colspan="2">&nbsp;</tr></table></td>';						
						echo '<td class="cell_border_right cell_border_bottom" align="center">'.number_format($p['box_num']).'</td>';
						echo '<td class="cell_border_right cell_border_bottom">'.mex::textToParagraph($p['address']).'</td>';
						
						echo '<td class="cell_border_right cell_border_bottom" align="center"><b>'.mex::job_type($p['type']).'</b></td>';
						
						echo '<td class="cell_border_bottom">'.($p['driver']!=""?'<b><font color="red">Assigned to : '.$p['driver'].'</font></b><br>':'').mex::textToParagraph($p['comments']).'</td>';
						echo '</tr>';
					}
				echo '</table>';
			}
	?>
</div>