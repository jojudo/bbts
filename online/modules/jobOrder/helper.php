<?php
	class moduleHelper extends helperController{

		function __construct(){
			parent::__construct();
		}
		
		function main(){
			$this->new_job();
		}
		
		//----------------------------- creation of new job ----------------------------------------
		
		function new_job(){
            $this->set_mod_stylesheet();
			$this->set_mod_javascript();		
            $this->add_vendor("mexpress.js.dynamic-list","js");
			$this->add_vendor('jquery.inputmask/dist/min','jquery.inputmask.bundle.min');
            $this->use_module_template('job-form',array());
		}
		
        function name_match(){
			//matching search for generic purpose
			$lname = $this->datastore->pick('lname');
			$fname = $this->datastore->pick('fname');
			
			$fld = $this->datastore->pick('fld');
			$page = intval($this->datastore->pick('page'));
			$page = $page?$page:1;
			$limit = 10;
			
			if($page>1)
				$start = ($page*$limit)+1;
			else
				$start =0;
				
			$start=$start?$start:0;
            $this->db->set_table("customer");
            $this->db->set_fields("SQL_CALC_FOUND_ROWS lname,fname,customer_id");
            $this->db->set_condition("lname like '$lname%'");
            $this->db->set_condition("fname like '$fname%'");
            $this->db->set_condition("(fname!='' and lname!='')");
            $this->db->set_orderby("lname","ASC");
            $this->db->set_orderby("fname","ASC");
            $this->db->set_limit($start,$limit);
            
            $args['names'] = $this->db->select();
           
            $args['rows'] = $this->db->found_rows();
			//$args['names'] = $this->db->selectQueryArr(PREFIX."customer","SQL_CALC_FOUND_ROWS lname,fname,customer_id","lname like '$lname%' and fname like '$fname%' and (fname!='' and lname!='')","lname ASC, fname ASC","$start,$limit");
			//$args['rows'] = $this->db->found_rows();
			
			$maxPages = floor($args['rows']/$limit);
			$prev = $page-1;
			$next = $page+1;
			
			$args['prev'] = $prev;			
			$args['next'] = $next<=$maxPages?$next:0;
			
			$this->json_output($args);
		}
        
        //request details of client
		function client_details(){
			$cid  = $this->datastore->pick('cid');
            $this->db->set_table("customer c");
            $this->db->set_join("countries o","o.country_id=c.country_id");
            $this->db->set_fields("lname,fname,customer_id,phone_home,phone_cell,phone_work,number,street,hood,city,email,state,zip,notes,c.country_id");
            $this->db->set_fields("o.country");
            $this->db->set_condition("customer_id = $cid");
            
            $args['details'] = $this->db->select()[0];
            		
			$this->json_output($args);
		}
		
        
		function job_countries(){
			if($this->profile['country_id']){
				$this->countries = $this->db->selectQueryArr(PREFIX."countries","country,country_id","country_id=".$this->profile['country_id']);
				
			}else{
				return $this->countries= array();
			}
			
			return $this->countries;
		}
		
		function job_cities(){
			$countries_arr = array();
			if(count($this->countries)){
				foreach($this->countries as $country){
					$countries_arr[] = $country['country_id'];
				}
				
				$country_str = implode("','",$countries_arr);
				
				$cities = $this->db->selectQueryArr(PREFIX."area","area_name,area_id,country_id","country_id in ('$country_str')","area_name ASC");
				
				return $cities;
			}else{
				return array();
			}
		}
		
		function job_sources(){
			return $this->db->selectQueryArr(PREFIX."business_sources","source_name,source_id","branch_id=".intval($this->profile['branch_id']),"source_name ASC");
		}
		
		function job_drivers(){
			return $this->db->selectQueryArr(PREFIX."users","user_id,user_fname,user_lname","branch_id=".intval($this->profile['branch_id'])." and user_level=2","user_fname ASC");
		}
					
		
		
		
		function product_search(){
			$this->args['products'] = $this->db->selectQueryArr(PREFIX."products","product_id,product_name,product_title,price_full price1,price_empty price2","","product_name ASC");	
			$this->args['destination'] = $this->db->selectQueryArr(PREFIX."destinationcodes","code,destination","country_id!=".$this->profile['country_id'],"code ASC");
			
			$this->json_output();
		}
		
		function zip_populate(){
			$area = $this->datastore->pick('area');
			$table = PREFIX."zipcodes z inner join ".PREFIX."area a using (area_id)";
			$this->args['zips'] = $this->db->selectQueryArr($table,"code","area_name='$area'","area_name ASC");	
			
			$this->json_output();
			
		}
		
		function city_match(){
			$area = $this->datastore->pick('city');
			$table = PREFIX."area a inner join ".PREFIX."areagroups g using (group_id) left join ".PREFIX."countries as c on (c.country_id=g.country_id)";
			$this->args['city'] = $this->db->selectQueryArr($table,"area_id,area_name,group_name,c.country_id","area_name like '$area%'","area_name ASC",'0,3');	
			
			$this->json_output();
		}
		
		//----- create new job order
		function save_job(){
			$this->datastore->set_parameter_source('POST');			
			$cid = $this->datastore->pick('cid');			
			
			
			if(!$cid && is_numeric($cid)){
				$continue = $this->__create_Customer();
			}else{
				$this->cid = $cid;
				$continue = true;
			}
			
			if($continue){
				$continue = $this->__create_joborder();
			}
			
			if($continue){
				$continue = $this->__create_jobdetails();
			}
			
			$this->args['success'] = 1;
			$this->args['job_id'] = $this->job_id;
			$this->json_output();

		}
		
		
		private function __create_jobdetails(){
			$jobTypes = $this->datastore->pick('jobType');

			$ctr = 0;
			if(is_array($jobTypes)){
				foreach($jobTypes as $type){
					$insertData	= array();
					$insertData['job_id'] = $this->job_id;
					$insertData['job_type'] = $type;
					$insertData['product_id'] = $this->datastore->pick('productTypes')[$ctr];
					$insertData['price'] = $this->datastore->pick('cons_price')[$ctr];
					$insertData['packing_no'] = $this->datastore->pick('cons_packing')[$ctr];
					$insertData['consignee'] = $this->datastore->pick('cons_name')[$ctr];
					$insertData['landline'] = $this->datastore->pick('cons_landline')[$ctr];
					$insertData['mobile'] = $this->datastore->pick('cons_mobile')[$ctr];
					$insertData['address1'] = $this->datastore->pick('cons_address1')[$ctr];
					$insertData['address2'] = $this->datastore->pick('cons_address2')[$ctr];
					$insertData['city'] = $this->datastore->pick('cons_city')[$ctr];
					$insertData['province'] = $this->datastore->pick('cons_province')[$ctr];
					$insertData['country_id'] = $this->datastore->pick('cons_country')[$ctr];
					
					$this->db->insertQuery(PREFIX.'joborder_details',$insertData,false);
					
					$ctr++;
				}
				
				if($this->db->execute()){
					return true;
				}else{
					$this->error_msg = $this->db->getErrorMsg();
					return false;
				}
			}
		}
		
		private function __create_joborder(){
			
			$insertData = array();
			
			$insertData['customer_id'] = $this->cid;
			$insertData['branch_id'] = $this->profile['branch_id'];
			
			$insertData['mobile'] = $this->datastore->pick('contact_mobile');			
			$insertData['landline'] = $this->datastore->pick('contact_home');			
			
			$insertData['number'] = $this->datastore->pick('address1');			
			$insertData['street'] = $this->datastore->pick('address2');			
			$insertData['city'] = $this->datastore->pick('city');			
			$insertData['state'] = $this->datastore->pick('state');			
			$insertData['zip_id'] = $this->datastore->pick('zip');			
						
			$insertData['driver_id'] = $this->datastore->pick('zip');			
			$insertData['comments'] = $this->datastore->pick('instructions');			
			$insertData['entry_date'] = date("Y-m-d H:i:s");
			
			$insertData['job_done'] = 0;
			$insertData['user_id'] = $this->profile['user_id'];
			
			if($this->db->insertQuery(PREFIX.'joborder',$insertData)){
				$this->job_id = $this->db->getInsertId();
				return true;
			}else{	
				$this->error_msg = $this->db->getErrorMsg();
				return false;
			}
		}
		
		private function __create_customer(){
			$insertData	= array();
			
			$insertData['phone_cell'] = $this->datastore->pick('contact_mobile');			
			$insertData['phone_home'] = $this->datastore->pick('contact_home');			
			$insertData['phone_work'] = $this->datastore->pick('contact_work');			
			$insertData['email'] = $this->datastore->pick('contact_email');			
			$insertData['number'] = $this->datastore->pick('address1');			
			$insertData['street'] = $this->datastore->pick('address2');			
			$insertData['city'] = $this->datastore->pick('city');			
			$insertData['state'] = $this->datastore->pick('state');			
			$insertData['zip'] = $this->datastore->pick('zip');			
			$insertData['country'] = $this->datastore->pick('country');
			$insertData['branch_id'] = $this->profile['branch_id'] ;
			$insertData['user_id'] = $this->profile['user_id'];
			
			$insertData['notes'] = $this->datastore->pick('notes');
			
			if($this->db->insertQuery(PREFIX.'customer',$insertData)){
				$this->cid = $this->db->getInsertId();
				return true;
			}else{
				$this->error_msg = $this->db->getErrorMsg();
				return false;
			}
		
		}
				
		//-------------------------------------------------- Box Direct Encoding Start ----------------------------
		function direct_encode(){
			//show encode form
			UI::set_js($this->js.'new-jobOrder.js');
			UI::set_stylesheet($this->styles.DS."jobOrder.css");
			
			$sql = "select country_id,country from ".PREFIX."countries order by country ASC";
			$arg['country_rs'] = $this->db->queryMe($sql);
			
			$this->load_flat('direct_encode',$arg);			
			$this->db->freeMe($arg['country_rs']);
		}
		
		
		function encodeBoxForm(){
			$args['boxes'] = mex::secure_get("boxes");
			
			//select destinationation code
			$sql = "select code,destination,code_id from ".PREFIX."destinationcodes order by left(code,1) ASC, substring(code,2)+0 ASC";
			$args['dest_rs'] = $this->db->queryMe($sql);
			
			//select available products
			$sql = "select price_full,product_id,product_name from ".PREFIX."products order by product_id ASC";
			$args['product_rs'] = $this->db->queryMe($sql);

			//select containers
			$sql = "select container_no,container_id,trip_code,trip_no
					from 
						".PREFIX."container as c left join
						".PREFIX."shipment as s using (shipment_id)
					order by est_departure DESC
					limit 0,10";
			
			$args['shipment_rs'] = $this->db->queryMe($sql);
			$this->load_flat("encode-final",$args);
			//$this->db->freeMe($dest_rs);
		}
		
		function consigneeCheck(){
			$cid = mex::secure_get("cid");
			$key = mex::secure_get("key");
			
			$sql = "select distinct b.consignee,box_id 
					from ".PREFIX."boxes as b left join 
						".PREFIX."joborder using (job_id)
					where b.consignee like '$key%' and customer_id=$cid";
			
							
			$rs = $this->db->queryMe($sql);
			
			$json_arr = array();
			while($p=$this->db->fetchAssoc($rs)){
				$json_arr[] = $p;
			}
			
			echo json_encode($json_arr);
			return ;
		}
		
		function chooseConsignee(){
			$bid = mex::secure_get("bid");
			
				$sql = "select distinct b.consignee,box_id,b.contact_no,b.mobile_no,b.delivery_address,b.code_id 
					from ".PREFIX."boxes as b
					where b.box_id=$bid";
			
							
			$arr = $this->db->queryFirst($sql);
			
			echo json_encode($arr);
			return ;
		}
		
		function encode_check(){
			//checkboxes first if there are no duplicates
			$str = mex::secure_post("str");
			$strConvert = str_replace(",","','",$str);
			$strConvert ="'$strConvert'";
			$status = 1;
			$msg = '';
			$sql = "select packing_no
						from ".PREFIX."boxes 
						where 
							packing_no in ($strConvert)";
							
			$str_rs = $this->db->queryMe($sql);
			if(mysql_num_rows($str_rs)!=0){
				//if packing nos exists .. inform user
				$msg = 'Packing Nos# ';
				$i=0;
				while($p = $this->db->fetchAssoc($str_rs)){
					if($i!=0)
						$msg.= ", ";
						
					$msg.= $p['packing_no'];
					$i++;
				}
				$msg.= ' already exists';
				$status=0;
			}else{
				//check if there is an emport packing list
				$str_arr = explode(",",$str);
				if(in_array("",$str_arr)){
					$msg =  "a packing no is empty";
					break;
				}
				
				//check for duplicates
				$end_arr = array();
				$duplicateStr = "";
				$i=0;
				foreach ($str_arr as $no){
					if(in_array($no,$end_arr)){
						$i++;
						if($i!=1)
							$duplicateStr.=", ";
						$duplicateStr.=$no;
					}
					$end_arr[] = $no;
				}
				
				if(!empty($duplicateStr)){
					$msg.= 'There are duplicate entries for Packing Nos# '.$duplicateStr;
					$status = 0;
				}
			}

			$this->db->freeMe($str_rs);
			$json_arr['msg'] = $msg;
			$json_arr['status'] = $status;
			
			echo json_encode($json_arr);
			
			
		}
		
		function finalize_encoding(){
			//save boxes = final encode -- checking supposedly over .. no checking required 
				$pack = $_POST['packing'];
				$prod = $_POST['product'];
				$collected = $_POST['collected'];
				$status = $_POST['boxStatus'];
				$container = $_POST['container'];
				$consignee = $_POST['consignee'];
				$received = $_POST['received'];
				$addr = $_POST['addr'];
				$dest = $_POST['destination'];
				$details = $_POST['details'];
				$total = mex::secure_post("boxes");
				$jid = mex::secure_post("jid");
				
				$today_is=date("Y-m-d H:i:s");
				$ctr=0;
				foreach ($pack as $packing) {
					//start insert boxes
					$home = $_POST['home'.($ctr+1)];
					
					$homeArr = implode("-",$home);
					$homeNum =mex::phoneFormat("-",$homeArr);
					$mobile = mex::secure_post("mob".($ctr+1));
					$boxCount = ($ctr+1)." of ".$total;
					if($status[$ctr]=='WH' && $container[$ctr]==0){
						$date_stored = mex::reformatDate($received[$ctr]);
					}else if ($status[$ctr]=='WH' && $container[$ctr]!=0) {
						$date_loaded = mex::reformatDate($received[$ctr]);
					}elseif ($status[$ctr]=='ARR'){
						$date_arrived = mex::reformatDate($received[$ctr]);
					}else{
						;
					}
					
					$sql = "insert into ".PREFIX."boxes(
										job_id,container_id,packing_no,consignee,
										contact_no,mobile_no,delivery_address,
										product_id,collected,details,image,status,
										date_added,last_updated,user_id,date_arrived,
										recipient,relation,code_id,date_stored,date_loaded,
										box_count,notes
									)values(
										'$jid','$container[$ctr]','$packing','$consignee[0]',
										'$homeNum','$mobile','$addr[$ctr]',
										'$prod[$ctr]','$collected[$ctr]','$details[$ctr]','','$status[$ctr]',
										'$today_is','$today_is','$GLOBALS[current_user]','$date_arrived',
										'','','$dest[$ctr]','$date_stored','$date_loaded',
										'$boxCount','$details[$ctr]'							
									)";
					$this->db->queryMe($sql);
					
					$ctr++;
				}
				
				//new MLM verification system
				//MLM system 
				include_once($this->includes.'class.referral.php');
				$referral = new referral();
				$referral->award_check_referral_jobid($jid);
				
				$json_arr['jid'] = $jid;
				$json_arr['status'] = 1;
				
				echo json_encode($json_arr);
				
		}
		//-------------------------------------------------- Box Direct Encoding End ------------------------------------
		
		function createreferrals($cid,$referrer){
			//get the id of the upline
			if($referrer){
				$sql = "select referrer_id from ".PREFIX."referrer where customer_id = $referrer";
				$arr = $this->db->queryFirst($sql);
				$upline_id = $arr['referrer_id'];
			}else{
				$upline_id = 0;
			}
			
			
			//check if the referrer already has a referrer account
			$sql = "select referrer_id from ".PREFIX."referrer where customer_id = $cid";
			$arr = $this->db->queryFirst($sql);
			if(!$arr['referrer_id']){
				//let us create a referral acct
				$sql = "insert into ".PREFIX."referrer values 
						('',$upline_id,$cid,'','',now(),'')";
				$this->db->queryMe($sql);
				$referrer_id = mysql_insert_id();
				//uniqueID format - R12-001 R(y)-id_no
				$referral_no = "R".date("y")."-".str_pad($referrer_id,5,0,STR_PAD_LEFT);				
				
				//insert the customer into the referrers records
				$sql = "update ".PREFIX."referrer
							set acct_no = '$referral_no' 
							where referrer_id = $referrer_id";
							
				$this->db->queryMe($sql);
				
			}
		}
		
		function removeJob(){
			$jid = mex::secure_post("jid");
					
			$sql = "delete from ".PREFIX."joborder where job_id='$jid'";
			$this->db->queryMe($sql);

			$sql = "select file_name from ".PREFIX."files as f 
					where exists(select box_id from ".PREFIX."boxes where 
									box_id = f.box_id and 
									job_id = '$jid')";
			
			$file_rs = $this->db->queryMe($sql);
			while($p=$this->db->fetchAssoc($file_rs)){
				unlink(MEX_BASE.DS."images".DS."boxes".DS.$p['file_name']);
			}
			$this->db->freeMe($file_rs);
			
			$sql="delete from  ".PREFIX."boxes where job_id='$jid';";
			$this->db->queryMe($sql);
			
			
			echo 1;
		}
		
		function edit(){
			UI::set_js("modules".DS.$this->mod.DS."js".DS."modify-job.js");
			$jid = mex::secure_get("jid");
			$sql = "select 
						customer_id,branch_id,
						DATE_FORMAT(job_date,'%m/%d/%Y') job_date,
						code_id,destination,box_num,type,job_done,
						comments,number,street,packing_nos,
						city,state,zip_id,hood,status,
						(select country_id from ".PREFIX."zipcodes where code=j.zip_id limit 0,1)
						country_source,(select country_id from ".PREFIX."destinationcodes where code_id=j.code_id)
						country_dest,driver_id
					from 
						".PREFIX."joborder as j
					where
						job_id = '$jid'";
			
			$job_rs = $this->db->queryMe($sql);
			$job_arr = $this->db->fetchAssoc($job_rs);
			
			//client details
			$sql = "select  concat(fname,' ',lname) as name,
							phone_home,phone_work,phone_cell,
							email,notes
						from ".PREFIX."customer 
						where 
							customer_id = '$job_arr[customer_id]'";
			$client_rs = $this->db->queryMe($sql);
			$client_arr = $this->db->fetchAssoc($client_rs);
			
			//select all countries being served
			$sql = "select country_id,country from ".PREFIX."countries order by country ASC";
			$country_rs = $this->db->queryMe($sql);
			
			//select all the zip
			$sql = "select code from ".PREFIX."zipcodes	where country_id = '$job_arr[country_source]'";	
			$zip_rs = $this->db->queryMe($sql);
			
			//select all the destinationcodes
			$sql = "select code_id,code,destination from ".PREFIX."destinationcodes	where country_id = '$job_arr[country_dest]'";	
			$dest_rs = $this->db->queryMe($sql);
			
			//select all the branches
			$sql = "select branch_name,branch_id from ".PREFIX."branches	where country_id = '$job_arr[country_source]'";	
			$branch_rs = $this->db->queryMe($sql);
			
			//select all the drivers
			$sql = "select concat(user_lname,', ',user_fname) name,user_id from ".PREFIX."users	
						where country_id = '$job_arr[country_source]' and user_level='2'";	
			$driver_rs = $this->db->queryMe($sql);

			//select all included boxes
			$sql= "select 
						box_id,packing_no,
						delivery_address,consignee
					from 
						".PREFIX."boxes
					where 
						job_id = '$jid'";		
			
			$box_rs = $this->db->queryMe($sql);
			
			//select the payments
			$sql = "select `payment_id`,`cash`,`check`,`credit`,`notes` from ".PREFIX."payments where job_id = '$jid'";
			$payment_rs = $this->db->queryMe($sql);
			
			require("modules".DS.$this->mod.DS."flats".DS."modify_job.php");
			$this->db->freeme($job_rs);
			$this->db->freeme($client_rs);
			$this->db->freeMe($zip_rs);
			$this->db->freeMe($dest_rs);
			$this->db->freeMe($branch_rs);
			$this->db->freeMe($box_rs);
			$this->db->freeMe($payment_rs);
		}
		
		function updateJob(){
			$job_id =mex::secure_post("jid");
			$client_id = mex::secure_post("cid");
			$country_id = mex::secure_post("country_id");
			$number = mex::secure_post("addr_num");
			$street = mex::secure_post("addr_st");
			$hood = mex::secure_post("addr_hood");
			$city = mex::secure_post("addr_city");
			$state = mex::secure_post("addr_state");
			$zip = mex::secure_post("addr_zip");
			
			$job_date = mex::reformatDate(mex::secure_post("job_date"));
			$boxes = mex::secure_post("boxes");
			$packing = mex::secure_post("packing");
			$type = mex::secure_post("type");
			$destination = mex::secure_post("addr");
			$code_id = mex::secure_post("destiny");
			$branch_id = mex::secure_post("branch");
			$driver_id = mex::secure_post("driver_id");
			$comments = mex::secure_post("comments");
			$done = mex::secure_post("done");
						
			$sql = "update ".PREFIX."joborder set
							customer_id = '$client_id',
							job_date = '$job_date',
							code_id = '$code_id',
							destination='$destination',
							box_num = '$boxes',
							comments = '$comments',
							packing_nos = '$packing',
							number = '$number',
							street = '$street',
							hood = '$hood',
							city = '$city',
							state = '$state',
							zip_id= '$zip',
							branch_id = '$branch_id',
							driver_id= '$driver_id',
							job_done='$done',
							type='$type'
						where
							job_id = '$job_id'";
			
			$this->db->queryMe($sql);
			echo 1;
			
		}
		//-------------------------------------- reports -------------------------//
		function dispatch_report(){
			UI::set_js("modules".DS.$this->mod.DS."js".DS."dispatch.js");
			$dispatch_date = mex::secure_post("job_date");
			$driver_id = mex::secure_post("driver_id");
			
			if(!$dispatch_date)
				$dispatch_date = date("m/d/Y");
			
			
			if($driver_id!=0){
				//select driver areas
				$sql = "select group_concat(area_id) as areas from ".PREFIX."driveroute where user_id='$driver_id'";
				$dr_areas = $this->db->queryMe($sql);
				$dr_arr = $this->db->fetchAssoc($dr_areas);
				
				$drs = $dr_arr['areas'];
				$areas = explode(",",$drs);
			}else{
				$areas = $_POST["areas"];
			}
				
			if(!is_array($areas)){
				$areas = array();
			}

						
			//get all driver names
			$sql = "select user_id,concat(user_lname,', ',user_fname) name from ".PREFIX."users where user_level='2'";
			$driver_rs = $this->db->queryMe($sql);
			
			//select area
			$sql = "select area_id,area_name from ".PREFIX."area order by area_name ASC";
			$area_rs = $this->db->queryMe($sql);
			
			require("modules".DS.$this->mod.DS."flats".DS."dispatch.php");
			
			$this->db->freeMe($driver_rs);
			$this->db->freeMe($area_rs);
		}
		
		function status_report(){
			UI::set_js("modules".DS.$this->mod.DS."js".DS."status-report.js");
			$type = mex::secure_post("type");
			$today_is = date("Y-m-d");
			$dispatch_date = date("m/d/Y");
			
			//start with areas 
			$sql = "select area_name,area_id from ".PREFIX."area order by area_name ASC";
			$area_rs = $this->db->queryMe($sql);
			
			//get all driver names
			$sql = "select user_id,concat(user_lname,', ',user_fname) name from ".PREFIX."users where user_level='2' order by user_lname ASC";
			$driver_rs = $this->db->queryMe($sql);
			
			require("modules".DS.$this->mod.DS."flats".DS."status-report.php");
			$this->db->freeMe($area_rs);
			$this->db->freeMe($driver_rs);
			
		}
		
		function get_status_report(){
			//this is a compliment function with the one above
			$areas = $_POST['areas'];
			$jobDate = mex::reformatDate(mex::secure_post("job_date"));
			$driver = mex::secure_post("driver_id");
			
			$areasTxt = implode(",",$areas);
			if($driver==0)
				$insert = "";
			else
				$insert = "and (j.driver_id='$driver')";
				
			$sql = "select area_name,area_id from ".PREFIX."area as a
						where 
							exists(
								select job_id 
									from 
										".PREFIX."joborder as j,
										".PREFIX."zipcodes as z
									where
										j.zip_id = z.code and
										z.area_id = a.area_id and
										j.done_date = '$jobDate' and
										j.job_done='1' 
										$insert
									)
							and a.area_id in ($areasTxt)
									order by area_name ASC";
			
			$actualAreas_rs = $this->db->queryMe($sql);
			//echo mysql_num_rows($actualAreas_rs);
			if(mysql_num_rows($actualAreas_rs)!=0){
				while ($p=$this->db->fetchAssoc($actualAreas_rs)) {
					$sql = "select j.job_id,j.box_num,j.status,j.type,
									DATE_FORMAT(j.job_date,'%m/%d/%Y') jobDate,j.comments,
									DATE_FORMAT(j.done_date,'%m/%d/%Y') jobDone,
									concat(j.number,' ',j.street,' ',j.hood,' ',j.city,' ',j.state,' ',j.zip_id) address,
									(select concat(fname,' ',lname) from ".PREFIX."customer where
										customer_id = j.customer_id) name,
									(select concat(phone_home,'|',phone_work,'|',phone_cell) as phones
									from ".PREFIX."customer where customer_id=j.customer_id) phones,
									(select concat(user_fname,' ',user_lname) from ".PREFIX."users where
										user_id = j.driver_id) driver
									from 
										".PREFIX."joborder as j,
										".PREFIX."zipcodes as z
									where
										j.zip_id = z.code and
										z.area_id = '$p[area_id]' and
										j.done_date = '$jobDate' and
										j.job_done='1' $insert";
					$jobs_rs = $this->db->queryMe($sql);
					
					echo '<p class="sub_header">'.$p['area_name'].'</p>';
					echo '<table cellpadding="3" cellspacing="0" border="0" class="table_bordered" width="100%">';
					echo '<tr class="table_headers" align="center"><td width="2%"></td>';
					echo '<td width="8%;">&nbsp;<b>Date Done</b></td>';
					echo '<td width="15%"><b>Customer Details</b></td>';
					echo '<td width="30%"><b>Job Details</b></td>';
					echo '<td width="10%">Accomplished by (Driver/Agent)</td>';
					echo '<td ><b>Notes</b></td></tr>';

					$i=0;
					while($p = $this->db->fetchAssoc($jobs_rs)){
						$i++;
						$phone_arr = explode("|",$p['phones']);

						echo '<tr valign="top">';
						echo '<td class="cell_border_right cell_border_bottom" style="min-height:40px;"><b>'.$i.'</b>.</td>';
						echo '<td class="cell_border_right cell_border_bottom">'.$p['jobDone'].'</td>';
						echo '<td class="cell_border_right cell_border_bottom"><font size="2"><a href="#">'.utf8_decode($p['name']).'</a></font>';
							echo '<table cellspacing="0" cellpadding="0" style="padding-left:5px;">';
							echo $phone_arr[0]!=""?"<tr bgcolor='$bg'><td><b>Home No</b> </td><td>: ".mex::phoneFormat("-",$phone_arr[0])."</td></tr>":"";
							echo $phone_arr[1]!=""?"<tr bgcolor='$bg'><td><b>Work No</b> </td><td>: ".mex::phoneFormat("-",$phone_arr[1])."</td></tr>":"";
							echo $phone_arr[2]!=""?"<tr bgcolor='$bg'><td><b>Cell No</b> </td><td>: ".mex::phoneFormat("-",$phone_arr[2])."</td></tr>":"";
							echo '<tr bgcolor="'.$bg.'"><td colspan="2">&nbsp;</tr></table>';
						echo '</td>';						
						echo '<td class="cell_border_right cell_border_bottom">';
						echo "<b>Job Type</b>: ".mex::job_type($p['type']).'<br>';
						echo "<b>Boxes</b>: ".number_format($p['box_num']).'<br>';
						echo "<b>Address</b>: ";
						echo '<div style="padding-left:10px;">'.mex::textToParagraph($p['address']).'</div>';
						echo '</td>';
						echo '<td class=" cell_border_bottom cell_border_right" align="center">'.$p['driver'].'&nbsp;</td>';
						echo '<td class="cell_border_bottom ">'.mex::textToParagraph($p['comments']).'</td>';
						echo '</tr>';
					}
					echo '</table>';
					//get the jobs of the same date and the same categories
					$this->db->freeMe($jobs_rs);
				}
			}else{
				echo '<p align="center"><span  class="alert_msg">There were no records found</span></p>';
			}
			
			$this->db->freeMe($actualAreas_rs);
		}
		
		function carryOver_report(){
			$today_is = date("Y-m-d");
			$sql = "select area_name,area_id from ".PREFIX."area as a
						where 
							exists(
								select job_id 
									from 
										".PREFIX."joborder as j,
										".PREFIX."zipcodes as z
									where
										j.zip_id = z.code and
										z.area_id = a.area_id and
										j.done_date < '$today_is' and
										j.job_done!='1' 
									)
						order by area_name ASC";
			
			$area_rs = $this->db->queryMe($sql);
			require("modules".DS.$this->mod.DS."flats".DS."carry-over.php");
			$this->db->freeMe($area_rs);
		}
		
		function getNotDoneRS($area){
			$today_is = date("Y-m-d");
			$sql = "select 
						box_num,comments,status,job_id,customer_id,j.type,j.job_done,j.status,
						DATE_FORMAT(j.entry_date,'%m/%d/%Y') call_date,
						(select concat(lname,', ',fname) as name from ".PREFIX."customer where
							customer_id = j.customer_id) as name,
						(select concat(phone_home,'|',phone_work,'|',phone_cell) as phones
									from ".PREFIX."customer where customer_id=j.customer_id) phones,
									
						DATE_FORMAT(job_date,'%m/%d/%Y') as jobDate,
						concat(number,' ',street,' ',hood,' ',city,' ',state,' ',j.zip_id) as address,
						(select concat(user_fname,' ',user_lname) from ".PREFIX."users where user_id=j.driver_id) driver
						from
							".PREFIX."joborder as j,
							".PREFIX."zipcodes as z
							
						where
							j.zip_id = z.code and
							z.area_id='$area' and
							j.job_date < '$today_is' and 
							j.job_done!='1' and 
							(j.type='1' or j.type='2')
							";
			
			$job_rs = $this->db->queryMe($sql);
			return $job_rs;
			
		}
		//-------------------------------- end reports
		
		
		
		function getDispatchRS($area_id,$dispatch_date,$driver_id=0){
			//select all orders to be dispatched
			$start_date = mex::reformatDate($dispatch_date);
			if($driver_id!=0)
				$insert =" and (j.driver_id = '$driver_id' or j.driver_id=0) ";
			else
				$insert='';
					
			$sql = "select 
						box_num,comments,status,job_id,customer_id,j.type,j.status,
						DATE_FORMAT(j.entry_date,'%m/%d/%Y') call_date,
						(select concat(lname,', ',fname) as name from ".PREFIX."customer where
							customer_id = j.customer_id) as name,
						(select concat(phone_home,'|',phone_work,'|',phone_cell) as phones
									from ".PREFIX."customer where customer_id=j.customer_id) phones,
									
						DATE_FORMAT(job_date,'%m/%d/%Y') as jobDate,
						concat(number,' ',street,' ',hood,' ',city,' ',state,' ',zip_id) as address	,
						(select concat(user_fname,' ',user_lname) from ".PREFIX."users
								where user_id = j.driver_id) driver				
					from 
						".PREFIX."joborder as j
					where 	
						j.job_date<='$start_date' and
						job_done!='1' and 
						exists(select zip_id from ".PREFIX."zipcodes where area_id = '$area_id' and 
							code = j.zip_id) 
						$insert
			
					order by
						job_date ASC,
						j.street ASC,
						j.number ASC,
						j.hood ASC";
			
			$dispatch_rs = $this->db->queryMe($sql);
			
			return $dispatch_rs;
		}
		
		function list_jobs(){
			UI::set_js("modules".DS.$this->mod.DS."js".DS."list-orders.js");
			UI::set_stylesheet("modules".DS.$this->mod.DS."styles".DS."jobOrder.css");
			$limit = 50;
			$start = mex::secure_get("start");
			$key = mex::secure_get("key");
			$order = mex::secure_get("order");
			$start=$start?$start:0;
			
			$sql = "select 
						j.box_num,j.comments,j.status,j.job_id,j.customer_id,j.type,j.job_done,
						DATE_FORMAT(j.entry_date,'%m/%d/%Y') call_date,
						concat(c.lname,', ',c.fname)  as customer,
						DATE_FORMAT(j.job_date,'%m/%d/%Y') as jobDate,
						DATE_FORMAT(j.done_date,'%m/%d/%Y') as jobDone,
						concat(j.number,' ',j.street,' ',j.hood,' ',j.city,' ',j.state,' ',j.zip_id) as address
					from 
						".PREFIX."joborder as j,
						".PREFIX."customer as c
					where
						j.customer_id = c.customer_id and
						(j.job_id like '$key%' or c.lname like '$key%' or c.fname like '$key%')
					order by
						j.job_date DESC
					limit $start, $limit";
			
			$job_rs = $this->db->queryMe($sql);
			
			//count total boxes
			$sql = "select count(job_id) total 
					from 
						".PREFIX."joborder as j,
						".PREFIX."customer as c
					where
						j.customer_id = c.customer_id and
						(j.job_id like '$key%' or c.lname like '$key%' or c.fname like '$key%')";
			
			$count_rs = $this->db->queryMe($sql);
			$count_arr = $this->db->fetchAssoc($count_rs);
			$total = $count_arr['total'];
			
			require("modules".DS.$this->mod.DS."flats".DS."jobs_list.php");
			$this->db->freeMe($job_rs);
			$this->db->freeMe($count_rs);
		}
		
		function search(){
			$limit = 20;
			$start = mex::secure_get("start");
			$key = mex::secure_get("key");
			$order = mex::secure_get("order");
			$start=$start?$start:0;
			
			$sql = "select 
						j.box_num,j.comments,j.status,j.job_id,j.customer_id,j.type,j.job_done,
						DATE_FORMAT(j.entry_date,'%m/%d/%Y') call_date,
						concat(c.lname,', ',c.fname)  as customer,
						DATE_FORMAT(j.job_date,'%m/%d/%Y') as jobDate,
						DATE_FORMAT(j.done_date,'%m/%d/%Y') as jobDone,
						concat(j.number,' ',j.street,' ',j.hood,' ',j.city,' ',j.state,' ',j.zip_id) as address
					from 
						".PREFIX."joborder as j,
						".PREFIX."customer as c
					where
						j.customer_id = c.customer_id and
						(j.job_id like '$key%' or c.lname like '$key%' or c.fname like '$key%')
						
					order by
						j.job_date DESC
					limit $start, $limit";
			
			$job_rs = $this->db->queryMe($sql);
			
			//count total boxes
			$sql = "select count(job_id) total 
					from 
						".PREFIX."joborder as j,
						".PREFIX."customer as c
					where
						j.customer_id = c.customer_id and
						(j.job_id like '$key%' or c.lname like '$key%' or c.fname like '$key%')";
			$count_rs = $this->db->queryMe($sql);
			$count_arr = $this->db->fetchAssoc($count_rs);
			$total = $count_arr['total'];
			
			if(mysql_num_rows($job_rs)!=0){
				echo '<p><b>Total Records :</b>'.number_format($total).'</p>';
				echo '<table cellpadding="3" cellspacing="0" width="100%" class="table_bordered">';
				echo '<tr class="table_headers">';
				echo '<td width="30px"></td>';
				echo '<td width="60px">Job ID</td>';
				echo '<td width="200px">Customer</td>';
				echo '<td>Address</td>';
				echo '<td>Job Date</td>';
				echo '<td width="100px">Type</td>';
				echo '<td width="100px">Status</td>';
				echo '<td width="150px"></td>';
				echo '</tr>';
			
				$ctr=$start;
				while($p=$this->db->fetchAssoc($job_rs)){
					$ctr++;
					if($ctr%2==0)
						$className="even";
					else
						$className="odd";
						
					echo '<tr class="'.$className.'" id="tr_'.$p['job_id'].'">';
					echo '<td><b>'.$ctr.'</b>.</td>';
					echo '<td><a href="#" class="jobLink">'.$p['job_id'].'</a></td>';
					echo '<td><a href="#" class="clientsLink" cid="'.$p['customer_id'].'">'.$p['customer'].'</a></td>';
					echo '<td>'.$p['address'].'</td>';
					echo '<td>'.$p['jobDate'].'</td>';
					echo '<td>'.mex::job_type($p['type']).'</td>';
					echo '<td>'.mex::jobStatus($p['job_id']).'</td>';
					echo '<td align="right"><a href="'.mex::long_addr("index.php?mod=jobOrder&action=edit&jid=".$p['job_id']).'";><img src="images/btn-modify.png" border="0"></a> ';
					echo '<img src="images/btn-del.png" border="0" jid="'.$p['job_id'].'" class="delMe" style="cursor:pointer;"></td>';
					echo '</tr>';	
				}
				echo '</table>';
				echo '<p>';
				echo mex::classBasedPaging('pagingClass',$total,$limit,$start);
				echo '</p>';
			}else{
				echo '<p align="center">There are no Job Orders listed</p>';
			}
			$this->db->freeMe($job_rs);
			$this->db->freeMe($count_rs);
		}
		function view(){
			$jid = mex::secure_post("jid");
			$sql = "select 	
							j.job_id,concat(c.lname,', ',c.fname) as customer,j.type,
							DATE_FORMAT(j.job_date,'%m/%d/%Y') as jobDate,
							DATE_FORMAT(j.done_date,'%m/%d/%Y') as jobDone,
							(select concat(user_fname,' ',user_lname) from ".PREFIX."users
								where user_id = j.user_id) encoder,
							(select concat(user_fname,' ',user_lname) from ".PREFIX."users
								where user_id = j.driver_id) driver,
							concat(j.number,' ',j.street,' ',j.hood,' ',j.city,' ',j.state,' ',j.zip_id) as address,	
							j.comments,j.status,j.job_done
						from 
							".PREFIX."joborder as j,
							".PREFIX."customer as c
						where
							j.customer_id = c.customer_id and
							j.job_id='$jid'";
			
			$job_rs = $this->db->queryMe($sql);
			
			if(mysql_num_rows($job_rs)!=0){
				$job = $this->db->fetchAssoc($job_rs);
				echo '<table cellpadding="3" cellspacing="0" style="background:inherit;">';
				echo '<tr>';
				echo '<td width="100px"><b>Job ID</b></td>';
				echo '<td>'.$job['job_id'].'</td>';
				echo '</tr>';
				echo '<tr>';
				echo '<td ><b>Customer</b></td>';
				echo '<td>'.$job['customer'].'</td>';
				echo '</tr>';	
				echo '<tr>';
				echo '<td ><b>Job Address</b></td>';
				echo '<td>'.$job['address'].'</td>';
				echo '</tr>';	
				echo '<tr>';
				echo '<td ><b>Job Type</b></td>';
				echo '<td>'.mex::job_type($job['type']).'</td>';
				echo '</tr>';
				echo '<tr>';
				echo '<td ><b>Job Date</b></td>';
				echo '<td>'.$job['jobDate'].'</td>';
				echo '</tr>';
				echo '<tr>';
				echo '<td ><b>Date Done</b></td>';
				echo '<td>'.mex::empty_date("/",$job['jobDone']).'</td>';
				echo '</tr>';
				echo '<tr>';
				echo '<td ><b>Encoded By</b></td>';
				echo '<td>'.$job['encoder'].'</td>';
				echo '</tr>';
				if($job['driver']!=""){
				echo '<tr>';
				echo '<td ><b>Driver</b></td>';
				echo '<td>'.$job['driver'].'</td>';
				echo '</tr>';
				}
				echo '<tr>';
				echo '<td ><b>Job Status</b></td>';
				echo '<td>'.mex::jobStatus($job['job_id']).'</td>';
				echo '</tr>';
				if($job['comments']!=""){
				echo '<tr>';
				echo '<td  valign="top"><b>Comments</b></td>';
				echo '<td valign="top">'.mex::textToParagraph($job['comments']).'</td>';
				echo '</tr>';
				}
				echo '</table>';
				
				///check the boxes
				$sql = "select box_id,packing_no,consignee,delivery_address,status 
							from ".PREFIX."boxes 
							where job_id='$jid'";
				$box_rs  = $this->db->queryMe($sql);
				if(mysql_num_rows($box_rs)!=0){
					echo '<p class="sub_header">Included Boxes</p>';
					echo '<table cellpadding="3" cellspacing="0" width="100%" class="table_bordered">';
					echo '<tr class="table_headers">';
					echo '<td>#</td>';
					echo '<td>Packing No#</td>';
					echo '<td>Consignee</td>';
					echo '<td>Status</td>';
					echo '</tr>';
					$ctr=0;
					while($p=$this->db->fetchAssoc($box_rs)){
						$ctr++;
						if($ctr%2==0)
							$className="even";
						else
							$className="odd";
						echo '<tr class="'.$className.'">';
						echo '<td><b>'.$ctr.'</b>.</td>';
						echo '<td>'.$p['packing_no'].'</td>';
						echo '<td>'.$p['consignee'].'</td>';
						echo '<td>'.mex::boxStatus($p['box_id']).'</td>';
						echo '</tr>';
					}
					echo '</table>';
				}
				$this->db->freeMe($box_rs);
			}else{
				echo '<p align="center">Job Order #'.$jid.' cannot be found</p>';
			}
			$this->db->freeMe($job_rs);
		}
		
		
		
		function consigneeCopy(){
			$bid = mex::secure_post("bid");
			$sql = "select consignee,mobile_no,delivery_address,contact_no,code_id,
						(select country_id from ".PREFIX."destinationcodes  where code_id = b.code_id) country_id
						from ".PREFIX."boxes as b
						where
							box_id = '$bid'";
							
			$cons_rs = $this->db->queryMe($sql);
			header ("Content-Type:text/xml");
			
				$con_arr = $this->db->fetchAssoc($cons_rs);
				
				echo '<?xml version="1.0" encoding="ISO-8859-1"?>';
				echo '<parameters>';
				echo '<option value="consignee">'.$con_arr['consignee'].'</option>';
				echo '<option value="landline">'.$con_arr['contact_no'].'</option>';
				echo '<option value="mobile">'.$con_arr['mobile_no'].'</option>';
				echo '<option value="country">'.$con_arr['country_id'].'</option>';
				echo '</parameters>';
			
			
		}
		
		/*----------------------- ajax functions --------------------------- */
		
		//creating of job orders
		
		
		function selectAreas(){
		
			//select areas of a country .. show only <option>
			$country_id = mex::secure_get("country_id");
			$sql = "select area_id,area_name from ".PREFIX."area where country_id='$country_id' order by area_name ASC";
			$area_rs = $this->db->queryMe($sql);
			$arr = array();
			if(mysql_num_rows($area_rs)!=0){
				while($p=$this->db->fetchAssoc($area_rs)){
					$arr[] = $p;
				}
			}
			$this->db->freeMe($area_rs);
			echo json_encode($arr);
		}
		
		//show all the zipcodes by area
		function selectAreasZip(){
			//select zips of an area.
			$area_id = mex::secure_get("area");
			$sql = "select zip_id,code from ".PREFIX."zipcodes where area_id='$area_id' order by code ASC";
			$area_rs = $this->db->queryMe($sql);
			$arr = array();
			if(mysql_num_rows($area_rs)!=0){
				while($p=$this->db->fetchAssoc($area_rs)){
					$arr[] = $p;
				}
			}
			$this->db->freeMe($area_rs);
			echo json_encode($arr);
		}
		
		//show all the branches
		function getBranches(){
			//set zip into select format
			$country_id =mex::secure_get("country_id");
			$sql = "select branch_id,branch_name from ".PREFIX."branches where country_id = '$country_id' order by branch_name ASC";
			$branch_rs = $this->db->queryMe($sql);
						
			$arr = array();
			while($p=$this->db->fetchAssoc($branch_rs)){
				$arr[] = $p;
			}
			$this->db->freeMe($branch_rs);
			echo json_encode($arr);
		}
		
		//get all drivers based on countryid
		function getDrivers(){
			//get the drivers for the job order	
			$cid = intval(mex::secure_get("cid"));
			$cid = $cid?$cid:1;
			$sql = "select u.user_id,user_username,user_lname,user_fname
					from 
						".PREFIX."users as u 
						
					where user_level = 2";
					
			$rs = $this->db->queryMe($sql);
			$arr = array();
			while($p=$this->db->fetchAssoc($rs)){
				$arr[] = $p;
			}
			$this->db->freeMe($rs);
			echo json_encode($arr);
		}
		
		//get all the areas of the destination country
		function loaDest(){
			//set zip into select format
			$country_id =mex::secure_get("country_id");
			$sql = "select code_id,code,destination from 
						".PREFIX."destinationcodes
					where 
						country_id = '$country_id' 
					order by code ASC";
			
			$zip_rs = $this->db->queryMe($sql);
			$arr = array();
			while($p=$this->db->fetchAssoc($zip_rs)){
				$arr[] = $p;
			}
			$this->db->freeMe($zip_rs);
			echo json_encode($arr);
		}
		
		//ajax referral
		function referralIDNames(){
			//ajax request - output json
			$referralid = mex::secure_get("referralid");
			$json_arr = array();
			if($referralid){
				//select the name of the referral
				$sql = "select lname,fname,customer_id from 
							".PREFIX."referral left join 
							".PREFIX."customer using (customer_id)
							where acct_no = '$referralid'";
							
				$json_arr = $this->db->queryFirst($sql);
				
			}
			
			echo json_encode($json_arr);
			
		}
		
		
		function referral_search(){
			$keyword = mex::secure_get('keyword');
			$rows = mex::secure_get('rows');
			
			$rows=$rows?$rows:50;
			if(is_numeric($rows)){
				$limit = 'limit 0,'.$rows;
			}else{
				$limit = '';
			}
			
			$sql = "select count(distinct customer_id) as total 
						from 
							".PREFIX."customer left join 
							".PREFIX."referral using (customer_id)
						where 
							(lname like '%$keyword%' or fname like '%$keyword%')"; 
						
			$total_arr = $this->db->queryFirst($sql);
			$total = $total_arr['total'];
			
			$sql = "select customer_id,lname,fname,acct_no 
						from 
							".PREFIX."customer left join 
							".PREFIX."referral using (customer_id)
						where 
							(lname like '%$keyword%' or fname like '%$keyword%') 
						order by lname ASC $limit";
						
			$search_rs = $this->db->queryMe($sql);
			$count = mysql_num_rows($search_rs);	
			
			echo '<p>'.$count.' of '.$total.' records found</p>';
			if(mysql_num_rows($search_rs)!=0){
				echo '<table cellpadding="3" cellspacing="0" width="100%">';
				echo '<thead>';
				echo '<tr>';
				echo '<td>#</td>';
				echo '<th>Customer Name</th>';
				echo '<th>Acct No#</th>';
				echo '<th></th>';
				echo '</tr>';
				echo '</thead>';
				echo '<tbody>';
				$ctr=0;
				while($p=$this->db->fetchAssoc($search_rs)){
				$ctr++;
					if($ctr%2==0)
						$classname = "even";
					else
						$classname = "odd";
					
					$name = $p['lname'].', '.$p['fname'];
					$name = str_replace($keyword,'<b>'.$keyword.'</b>',$name);
					$name = str_replace(strtoupper($keyword),'<b>'.strtoupper($keyword).'</b>',$name);
					$name = str_replace(ucfirst($keyword),'<b>'.ucfirst($keyword).'</b>',$name);
					echo '<tr class="'.$classname.'">';
					echo '<td>'.$ctr.'. </td>';
					echo '<td>'.$name.'</td>';
					echo '<td><b>'.$p['acct_no'].'</b></td>';
					echo '<td><input type="button" value="USE THIS" onclick="useReferral(\''.$p['fname'].' '.$p['lname'].'\','.$p['customer_id'].');"></td>';
					echo '</tr>';
				}
				echo '</tbody>';
				echo '<table>';
			}else{
				echo '<p align="center">No Match Found</p>';
			}
			
			$this->db->freeme($search_rs);
			
		}
		/*----------------------------------------- generic calls -------------------------------------------------*/
		
		function getAreaName($area_id){
			$sql = "select area_name from ".PREFIX."area where area_id='$area_id'";
			
			$area_rs = $this->db->queryMe($sql);
			$area_arr = $this->db->fetchAssoc($area_rs);
			
			$this->db->freeMe($area_rs);
			return $area_arr['area_name'];
		}
		
		
	}
?>