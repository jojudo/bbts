//-----------------------------------------------	 name match ------------------------------
//request the name matches 
function populateClientDetails(cid){
    framework.ajaxGeneric({param:"action=client_details&cid="+cid,callback:"clientPopulate",submitType:'get',dataType:'json'});
	
	return false;
}

function clientPopulate(obj){
    var data = obj.data;
	$("#lname").val(data.details.lname);
	$("#fname").val(data.details.fname);
	$("#email").val(data.details.email);
	$("#address1").val(data.details.number);
	$("#address2").val(data.details.street);
	$("#city").val(data.details.city);
	$("#state").val(data.details.state);
	$("#zip").val(data.details.zip);
	$("#country").val(data.details.country);
	$("#notes").val(data.details.notes);

	$("#contact_home").val(data.details.phone_home);
	$("#contact_mobile").val(data.details.phone_home);
	$("#contact_work").val(data.details.phone_home);
	
	$('#country option[value="'+data.details.country_id+'"]').attr("selected","selected");
	$('#city option[value="'+data.details.city.toLowerCase()+'"]').attr("selected","selected");
	
	//use country of selected city as country if default country is unknown
	if($("#country").val()==0){
		var country = $('#city option[selected="selected"]').attr("country");
		$('#country option[value="'+country+'"]').attr("selected","selected");
	}
	
	//disable the first and last name 	
	$('.matchMe').attr("disabled","disabled");
	
	$("#lname").after('<div id="clear_newClient"><a href="#" onclick="clearMatch();return false;" title="Clear Details">Clear</a></div>');
	$("#fname").after('<input type="hidden" name="cid" id="cid"  value="'+data.details.customer_id+'" />');
	
	//focus on the notes after autocomplete 
	$("#contact_home1").focus();
	
}

$(function() {	
    $(".dated").datepicker();
    $(".matchMe").dynamicList({"selectCallback":"populateClientDetails","fieldIDs":["lname","fname"],"param":"action=name_match"});
    $(":input").inputmask();
 
});

