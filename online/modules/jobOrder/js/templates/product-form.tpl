<html>
<head>
	<meta http-equiv="Cache-Control" content="max-age=1800"/>
</head>
<body>
	<form method="post" name="frmNewProduct" id="frmNewProduct">
	<label for="jobType">Item Type</label>
	<select name="jobType" id="jobType">
		<option value="0">---</option>
		<option value="PU">Pick UP</option>
		<option value="DEL">Delivery</option>
		<option value="OTH">Other</option>
	</select>
	<label for="productTypes">Product</label>
	<select name="productTypes" id="productTypes">
		<option value="0">---</option>
	</select>
	<label for="cons_price">Price</label>
		<input type="text" name="cons_price" id="cons_price" size="40" autocomplete="off">	
		
	<div id="PUDetails">
		<label for="consignee">Packing NO#</label>
		<input type="text" name="cons_packing" id="cons_packing" size="40" autocomplete="off">
		
		<label for="consignee">Consignee</label>
		<input type="text" name="cons_name" id="cons_name" size="40" autocomplete="off">	
		
		<label for="cons_landline">Landline No#</label>
		<input type="text" name="cons_landline" id="cons_landline" size="40" autocomplete="off">	
		
		<label for="cons_mobile">Mobile No#</label>
		<input type="text" name="cons_mobile" id="cons_mobile" size="40" autocomplete="off">	
		
		<label for="cons_address1">Address1</label>
		<input type="text" name="cons_address1" id="cons_address1" size="40" autocomplete="off">	
		
		<label for="cons_address2">Address2</label>
		<input type="text" name="cons_address2" id="cons_address2" size="40" autocomplete="off">	
		
		<label for="cons_city">Town/City</label>
		<input type="text" name="cons_city" id="cons_city" size="40" autocomplete="off">	
		
		<label for="cons_province">Province</label>
		<input type="text" name="cons_province" id="cons_province" size="40">	
		
		<label for="cons_country">Country</label>
		<select name="cons_country" id="cons_country">
			<option value=""> --- </option>
			<option value="2">PHILIPPINES</option>
		</select>
		
	</div>
	<p align="right"><input type="submit" name="btnSubmit" value="Add Item"></p>
	</form>
</body>
</html>