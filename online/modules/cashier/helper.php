<?php
	class moduleHelper extends helperController{
		var $maxCount = 99;
		
		function __construct(){
			parent::__construct();
		}

		protected function main(){
			//show homepage template
			$args['profile'] = $this->datastore->get_profile();
			$args['count'] = $this->get_current_state();
			//default processing
			$this->set_mod_stylesheet('cashier');
			$this->set_mod_javascript('cashier');
			$this->ui->use_module_flat('frontline',$args);
		}
		
		function get_current_state(){
			if(file_exists('media'.DS.'count.json')){
				return $this->get_current_count();
			}else{
				return $this->create_counter_file(2);
			}
		}
		
		function get_current_count(){
			$text = file("media/count.json");
			$json_arr  = json_decode($text[0],TRUE);
			$count = $json_arr['count'];
			$count_date = date("m/d/Y",strtotime($json_arr['last_updated']));
			if($count_date!=date("m/d/Y")){
				//we return to zero when dates do not match
				$this->create_counter_file(0);
				$count = 0;
			}
			return $count;
			
		}
		
		function next_customer(){
			$current = $this->get_current_count();
			$current++;
			if($current>$this->maxCount) $current = 1;
			$this->create_counter_file($current);
			
			$args['count'] = $current;
			$args['success'] = TRUE;
			
			$this->json_output($args);
		}
		
		function same_customer(){
			$current = $this->get_current_count();
			$this->create_counter_file($current);
			
			$args['count'] = $current;
			$args['success'] = TRUE;
			
			$this->json_output($args);
		}
		
		function create_counter_file($count){
			$json_arr = array('count'=>$count,'last_updated'=>date('Y-m-d H:i:s'));
			$json_text = json_encode($json_arr);
			$fp = fopen("media/count.json","w+");
			if($fp)	{ $fw = fwrite($fp,$json_text);} else {echo "unable to create file";}
			fclose($fp);
			return 1;
		}
		
		
		function are_we_ready(){
			$args['ready'] = $this->check_if_ready();
			$args['success'] = TRUE;
			
			$this->json_output($args);
		}
		
		private function check_if_ready(){
			if(file_exists("media/ready.json")){
				$text = file("media/ready.json");
				$json_arr  = json_decode($text[0],TRUE);
				$count_date = strtotime($json_arr['last_updated']);
				$recheck = 60*60;
				//this would cannot communicate to the monitor
				if(date("U")>=($count_date+$recheck))
					return FALSE;
				else
					return TRUE;
			}else{	
				return FALSE;
			}
		}
		
	}

	
	
	
	
	