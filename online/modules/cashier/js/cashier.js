
function nextButtonClick(){
	//now do an ajax call
	var MaxNumber = 99;
	var ctr = ($("#counter span:first-child").html() * 1 )
	ctr++;
	if(ctr > MaxNumber) ctr=1;
	$("#counter").prepend('<span>'+ctr+'</span>');
	$("#counter span:nth-child(3)").remove();
	ajaxCallJson("action=next_customer&headerType=none",'GET','nextResult','hiddenloader');

}

function blinkCurrentNumber(repeat){
	for(var x=1;x<=repeat;x++){
		$("#counter span:first-child").fadeOut(300)
		$("#counter span:first-child").fadeIn(300)
	}
}

function nextResult(data){
	blinkCurrentNumber(2);
	/*if(data.success){
		$("#counter span:first-child").html('<span>'+data.count+'</span>');
	}*/
}

function buzzClick(){
	//now do an ajax call
	ajaxCallJson("action=same_customer&headerType=none",'GET','hiddenloader','hiddenloader');
	blinkCurrentNumber(2);

}

function hiddenloader(){
	;
}

function monitorCheck(){
	//now do an ajax call
	ajaxCallJson("action=are_we_ready&headerType=none",'GET','updateScreen','hiddenloader');

}

function updateScreen(data){
	if(data.ready){
		$("#buzz_button").show();
		$("#check_button").hide()
	}else{
		$("#buzz_button").hide();
		$("#check_button").show()
		createAlertAutoClose('<center>AD PAGE IS NOT YET READY!</center>',1000);
	}
}

$(function() {
	$("#next").bind("click",function(){nextButtonClick();});
	$("#buzz").bind("click",function(){buzzClick();});
	$("#check").bind("click",function(){monitorCheck();});
	monitorCheck();
});