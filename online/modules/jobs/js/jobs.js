function searchJobs(page){
	var dataSet = $("#frmjobSearch").serialize();
	var param = "action=job_search&page="+page+"&"+dataSet;
	//now define the ajax call and callback function
	ajaxCallJson(param,'GET','buildResultTable');
}

function buildResultTable(data){
	var str = '';
	var ctr=0;
	if(data.page!=1) ctr=(data.items*(data.page-1));
	$.each(data.jobs,function(index,value){
		ctr++;
		str+='<tr>';
		str+='<td>'+ctr+'</td>';
		str+='<td>'+value.job_id+'</td>';
		str+='<td>'+value.job_date+'</td>';
		str+='<td>'+value.lname+', '+value.fname+'</td>';
		str+='<td>'+value.number+' '+value.street+' '+value.hood+' '+value.city+' '+value.state+' '+value.zip+'</td>';
		str+='<td></td>';
		str+='<td></td>';
		str+='<td>';
		str+='<a href="'+fullPath+'index.php?mod='+mod+'&action=edit_data&cid='+value.job_id+'" class="button edit">Modify</a>'
		str+='<a href="#" class="button delete">Delete</a>'
		str+='</td>';
		str+='<tr>';
	})	
	
		
	pageStr = makePaging(data.pages,data.page);
	
	$("#search_result").find("tbody").html(str);
	$("#job_count").html(addCommas(data.rows));
	$("#container_paging").html(pageStr);

	$(".container_page").click(function(){searchJobs($(this).html());return false;})
	
	window.prev = data.prev;
	window.next = data.next
	
}

function makePaging(maxPages,currentpage){
	steps = 10;
	start = currentpage-steps;
	if(start<1) start=1;
	end = currentpage+steps;
	if(end>maxPages) end=maxPages;
	var pageStr = '<ul>';
	if(start>1)
		pageStr+='<li><a href="#" class="container_page" >1</a></li><li>...</li>';
		
	if(maxPages>1){	
		for(var i=start;i<end;i++){
			pageStr+='<li><a href="#" class="container_page';
			if(i==currentpage){
				pageStr+=' active';
			}

			pageStr+='" >'+i+'</a></li>';
		}
	}
	
	if(end<maxPages)
		pageStr+='<li>...</li><li><a href="#" class="container_page" >'+maxPages+'</a></li>';

		
	pageStr += '</ul>';
	return pageStr;
}

function addForm(){	
	createInfo('addDiv','...','Add Ad',300,500);
	loadJsTemplate('addDiv',modPath+'js/templates/advert-add.tpl','setupAddForm');
}



function addFormCallback(){
	//request the teams list		
	param = "action=resources";
	//now define the ajax call and callback function
	ajaxCallJson(param,'POST','setupAddForm');
}

function setupAddForm(data){
	//add data to select field
	$("#frmAdd").submit(function(){submitAddform();return false;});
	setupUploader();
	$("#btnBrowse").click(function(){setUpBrowser();})
	
}

function setUpBrowser(){
	if(requiredFields()){
		createInfo('browseDiv','...','Browser Folder',500,800);
		$("#browseDiv").parent().children(".ui-dialog-titlebar").css("display","none");
		loadJsTemplate('browseDiv',modPath+'js/templates/file-browser.tpl','fileBrowser')
	}
}

function requiredFields(){
	var form_arr = new Array();
	form_arr[0] = 'adname';
	
	return checkForm(form_arr);
		
}

function fileBrowser(){
	//request the teams list		
	param = "action=browse_media";
	//now define the ajax call and callback function
	ajaxCallJson(param,'GET','prepareBrowse');
}

function prepareBrowse(data){
	var body_str = '';
	var filename = '';
	$.each(data.media,function(index,value){
		if(value[1]=='Movie')
			filename= modPath+'images/movieclip.png';
		else
			filename = fullPath+'/media/'+value[0];
		
		body_str+='<a href="#" title="'+value[0]+'" class="browserObj"><img src="'+filename+'"> <span>'+value[1]+' : '+value[0]+'</span></a> ';
	})
	
	$("#browserCenter").html(body_str);
	
	$(".browserObj").click(function(){
		fromGallery($(this).prop("title"))
	})
	
	$("#btnBrowseCancel").click(function(){
		 dialog_closer("browseDiv");
	})
}

function fromGallery(filename){
	dialog_closer("browseDiv");
	submitAddform("browsefile="+filename);
}


function submitAddform(param){
	var form_arr = new Array();
	form_arr[0] = 'adname';
	
	if(checkForm(form_arr)){
		//passed password screening
		var dataSet = $("#frmAdd").serialize();
		if(param) param+="&";
		param += "action=add_data&"+dataSet;
		
		//now do an ajax call
		ajaxCallJson(param,'GET','submitAddCallback');
	}
		
}

function submitAddCallback(data){
	if(data.success){
		//createAlertAutoClose('New avertisement recorded successfully!',500);
		closeInfo('addDiv');
		appendData(data.insertedData);
	}else{
		alert(data.message);
		if(data.highlight)
			$("#"+data.highlight).css("background","#FFFF66");
			$("#"+data.highlight).focus();
	}
}

function appendData(data){
	if(data.is_manager){
		$("#m_"+data.team_id).remove();
	}
	
	var str = '<tr id="data-row-'+data.ad_id+'" line-id="'+data.ad_id+'">';
		str+='<td class="holder">+</td>';
		str+='<td>'+data.fileType+ ' : <span class="ad_name" >'+data.ad_name+'</span></td>';
		str+='<td>'+data.filename;		
		str+='<td>'+(data.fileType=="Movie"?"-":data.duration)+'</td>';
		str+='<td>';
			str+='<div class="buttongroup"><a href="#" line-id="'+data.ad_id+'" class="button-toggle '+(data.isEnabled?'active':'inactive')+'">'+(data.isEnabled?'PUBLISHED':'NOT PUBLISHED')+'</a></div>';
		str+='</td>';
		str+='<td>';
		str+='<a href="#" line-id="'+data.ad_id+'" class="link delete"></a>';
		str+='<input type="hidden" value="'+data.ad_id+'" name="order[]" />'
		str+'</td>';
		str+='</tr>';
		
	$(".data-table").find("tbody").prepend(str);
	rebind();
}


//delete user
function deleteRecord(obj){
	var data_id = obj.attr("line-id");
	$("#data-row-"+data_id).css("background","red");
	if(confirm("Are you sure you wish to remove this Record?")){
		param = "action=delete_data&data_id="+data_id;
		ajaxCallJson(param,'GET','deleteCallback');
	}else{
		$("#data-row-"+data_id).css("background","");
	}
}

function deleteCallback(data){
	if(data.success){
		$("#data-row-"+data.data_id).animate({
			opacity: 0,
			height: "toggle"
		  }, 500, function() {
			$(this).remove();
		  });
		
	}
}

//Save order
function saveOrder(){
	var dataSet = $("#frmOrder").serialize();
	param = "action=order_data&"+dataSet;
	
	//now do an ajax call
	ajaxCallJson(param,'POST','orderCallback','orderCallback');
}


function orderCallback(){
	console.log("we are sorting, but being silent");
	//be silent, do not notify
	;//createAlertAutoClose('Media Order Successfully Updated!',1000);
}


//activat account
function activateAccount(obj){
	var data_id = obj.attr("line-id");
	param = "action=activate_ad&data_id="+data_id;
	ajaxCallJson(param,'GET','activateCallback');

}

function activateCallback(data){
	var obj = $("#data-row-"+data.data_id);
	var linkObj = obj.find(".inactive");
	linkObj.removeClass("inactive");
	linkObj.addClass("active");
	linkObj.html("PUBLISHED");
	rebind();
}


//deactivate account
function deactivateAccount(obj){
	var data_id = obj.attr("line-id");
	param = "action=deactivate_ad&data_id="+data_id;
	ajaxCallJson(param,'GET','deactivateCallback');

}

function deactivateCallback(data){
	var obj = $("#data-row-"+data.data_id);
	var linkObj = obj.find(".active");
	linkObj.removeClass("active");
	linkObj.addClass("inactive");
	linkObj.html("NOT PUBLISHED");
	rebind();
}


//deactivate bindings
function bindings(){
	//bind them to bindings
	$(".edit").bind("click",function(){editRecord($(this));return false;});		
	$(".delete").bind("click",function(){deleteRecord($(this));return false;});		
	$(".inactive").bind("click",function(){activateAccount($(this));return false;});		
	$(".active").bind("click",function(){deactivateAccount($(this));return false;});		
	
	$("#sortable").sortable({
		placeholder: "sortPlaceholder",
		handle:".holder",
		stop:function(){ saveOrder(); }
	});
	
	$(".ad_name").bind("click",function(){editField($(this),"ad_name");return false;})
}

function editField(obj,fieldName){
	var val = obj.html();
	var editId = obj.parent().parent().attr("line-id")
	obj.unbind("click");
	
	obj.focusout(function(){updateField(obj,editId,fieldName)});
	obj.html('<input type="text" size="30" value="'+val+'" />');
	obj.find("input").focus();
}

function updateField(fldObj,editId,fieldName){
	var val = fldObj.find("input").val();
	fldObj.html(val);
	fldObj.unbind("focusout");
	
	fldObj.bind("click",function(){editField($(this),"ad_name");return false;})
	
	param = "action=update_data&data_id="+editId+"&fieldName="+fieldName+"&fieldVal="+val;
	ajaxCallJson(param,'GET','silentCallBack','silentCallBack');
}


$(function() {
	searchJobs(0);
	initDate("dated");
	$("#frmjobSearch").submit(function(){searchJobs(0);return false;});
	setArrowKeyPaging("searchJobs");
});