<?php
	class moduleHelper extends helperController{

		function __construct(){
			parent::__construct();
		}
		
		function main(){
			$this->set_mod_javascript();
			$this->set_mod_stylesheet();
			$this->ui->use_module_flat('job-search');
		}
		
	
		
		function job_search(){	
			$date = $this->datastore->pick('job_date');
			$name = $this->datastore->pick('customer_name');
			
			
			$page = intval($this->datastore->pick('page'));
			$page = $page?$page:1;
			$limit = 20;	
			
			if($page>1)
				$start = (($page-1)*$limit);
			else
				$start =0;
				
			$start=$start?$start:0;
						
			$table = PREFIX."joborder j inner join ".PREFIX."customer c using (customer_id) left join ".PREFIX."joborder_details d on (j.job_id=d.job_id)";
			$fields = "j.job_id,lname,fname,j.number,j.street,j.hood,j.city,j.state,j.zip_id,date_format(job_date,'%m/%d/%Y') job_date";
			$fields .= ",sum(if(d.job_type = 'PU', 1, 0)) pus, sum(if(d.job_type = 'DEL', 1, 0)) dels";
			$conditions = array();
			
			if($date){
				$job_date=date("Y-m-d",strtotime($date));
				$conditions["and"] = "j.job_date = '$job_date'";
			}
			if($name){
				$names_arr = explode(" ",$name);
				if(count($names_arr)>1){
					$conditions["and"] = "c.fname like '$names_arr[0]%'";
					$conditions["and"] = "c.lname like '$names_arr[1]%'";
				}else{
					$conditions["or"] = "c.fname like '$name%'";
					$conditions["or"] = "c.lname like '$name%'";
				}
			}
			
			$args['jobs'] = $this->db->selectQueryArr($table,"SQL_CALC_FOUND_ROWS $fields",$conditions,"job_date DESC","$start,$limit","j.job_id");
			$args['rows'] = $this->db->found_rows();
			
			$maxPages = ceil($args['rows']/$limit)+1;
			$next = $page+1;
			$next = $next<=$maxPages?$next:0;
			$prev = $page-1;
			$prev = $prev>0?$prev:0;
			
			
			$args['pages'] = $maxPages;
			$args['page'] = $page;
			$args['items'] = $limit;
			$args['next'] = $next;
			$args['prev'] = $prev;
			
			
			$this->json_output($args);
			
		}
				
		
	}
?>