<div id="search_container">
	<form method="POST" id="frmjobSearch">
	
	<div id="breadcrumb"><a href="index.php">Home</a> >> <b>Job Orders</b></div>
	<p>
		<label for="job_date">Job Date</label>
		<input type="text" name="job_date" id="job_date" class="dated"> 
		
		<label for="customer_name">Customer</label>
		<input type="text" name="customer_name" id="customer_name" size="50"> 
				
		<input type="submit" name="Search" id="btnSearch" value="Search" class="button search"> 
	</p>
	<div id="container_div">
	<div id="search_result">
		<h2><b>Total Records : <span id="job_count"></span></b></h2>
		<table width="100%" class="data-table" border="0" cellspacing="0">
			<thead>
			<tr class="table_headers">
				<td height="30px" >#</td>
				<td>Job ID</td>
				<td>Job Date</td>
				<td>Customer Name</td>
				<td>Job Address</td>
				<td>Details</td>
				<td>Status</td>
				<td>&nbsp;</td>
			</tr>
			</thead>
			<tbody>
			</tbody>
			<Tfoot></tfoot>
		</table>
	</div>
	<div id="container_paging" class="records_paging"></div>
</div>
	</form>
</div>