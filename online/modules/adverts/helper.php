<?php
	class moduleHelper extends helperController implements insertSet{
				
		function __construct(){
			parent::__construct();
		}
		
		protected function main(){
			if($this->mod_restrict_access(2)) return ;
			
			$adType =2;
			//whe in main go main_ads
			$table = 'ads A left join ad_positions P on (A.position_id = P.position_id) left join ad_type T on (A.ad_type_id = T.ad_type_id)';
			$args['adverts']['data'] = $this->db->selectQueryArr($table,"ad_id data_id,ad_name,duration,filename,type_name,type_name,body_text,isEnabled","A.ad_type_id<=$adType","`order` ASC");
			$args['adverts']['rows'] = $this->db->num_rows();
			
			$this->set_mod_javascript("adverts");
			$this->set_mod_stylesheet("advert");
			//set libraries to be used
			$this->ui->set_stylesheet(LIB_ADDR.US.'qqUploadedFileXhr'.US.'css.'.US.'qqUploadedFileXhr.css');
			$this->ui->set_javascript(LIB_ADDR.US.'qqUploadedFileXhr'.US.'js.'.US.'qqUploadedFileXhr.js');
			
			$this->ui->use_module_flat('ad-list',$args);
			
		}
		
		public function add_data(){
			$this->datastore->set_parameter_source('GET');
			$adname = $this->datastore->pick('adname');
			$duration = $this->datastore->pick('duration');
			$enabled = $this->datastore->pick('enabled');
			$qqfile = $this->datastore->pick('qqfile');
			$browsefile = $this->datastore->pick('browsefile');
			
			
			$enabled  = ($enabled)?1:0;

			$continue = true;
			
			//name is required
			if($adname ==''){
				$continue = false;
				$args['message'] = 'Please complete all fields';
			}
			
			//do not save unsupported file extensions
			$filename = $qqfile?$qqfile:$browsefile;
			$file_check = $this->getFileType($filename);
			
			if($file_check[0]=='unsupported'){
				$continue = FALSE;
				$args['message'] = $file_check[1]." is not yet supported, please select another one";
			}
			
			if($continue && $qqfile){
				//upload the file using the uploader
				$uploader = load_class('fileUploader');
				$result = $uploader->get_status();
				if(!isset($result['success'])) $result['success'] = FALSE;
				if(!$result['success']){
					$continue = FALSE;
					$args['message']  = $result['error'];
					$args['success']  = FALSE;
				}else{
					$filename = $result['filename'].'_'.date("YmdHis").".".$result['ext'];
					$ext = $result['ext'];
					//on success we move the file from the upload folder to the media folder
					$uploader->move_file("media".DS.$filename);
				}
			}
			
			$ext = isset($ext)?$ext:$file_check[1];
			
			if($continue){
				//let us get the extensions 
				$adType_arr = $this->db->selectQueryArr('ad_type',"type_name,ad_type_id,extensions");
				foreach($adType_arr as $adTypes){
					$extensions = explode(",",$adTypes['extensions']);
					if(in_array($ext,$extensions)){
						$ad_type_id = $adTypes["ad_type_id"];
						$type_name = $adTypes["type_name"];
						break;
					}
					//if it pasrsed through this is an unknown file
					$ad_type_id = 3;
				}
				
				//let us insert the ad now		
				$insertData = array('ad_name'=>$adname,'position_id'=>1,'body_text'=>'','duration'=>$duration,'ad_type_id'=>$ad_type_id,'isEnabled'=>$enabled,'filename'=>$this->datastore->sanitizer($filename));
				
				$this->db->insertQuery('ads',$insertData);
				$insertData['fileType'] = $type_name;
				$args['message'] = 'Insert success';
				$insertData['ad_id'] = $this->db->getInsertId();
				$args['insertedData'] = $insertData;
			}
			
			$args['success'] = $continue;
			$this->json_output($args);
		}
		
		public function browse_media(){
			$folder_files = scandir("media/");
			$skip_files = array(".","..","ready.json","count.json");
			
			foreach($folder_files as $file){
				if(!in_array($file,$skip_files)){
					$file_check = $this->getFileType($file);
					if($file_check[0]!='unsupported')
						$args['media'][] = array($file,$file_check[0]);
				}
			}

			$args['success'] = TRUE;
			
			$this->json_output($args);
		}
		
		private function getFileType($filename){
			$img_arr = explode(".",$filename);
			$ct= count($img_arr);
			$ext = strtolower($img_arr[($ct-1)]);
			
			if(!isset($this->adType_arr)){
				$this->adType_arr = $this->db->selectQueryArr('ad_type',"type_name,extensions");
			}
			
			foreach($this->adType_arr as $adTypes){
				$ext_types = explode(",",$adTypes['extensions']);
				$extensions = explode(",",$adTypes['extensions']);
				if(in_array($ext,$extensions)){
					return array($adTypes["type_name"],$ext);
				}
			}
			
			return array('unsupported',$ext);
		}
		
		//user update
		function edit_data(){
			;
		}
		
		function update_data(){
			$fieldName = $this->datastore->pick('fieldName');
			$fieldVal = $this->datastore->pick('fieldVal');
			$dataId = $this->datastore->pick('data_id');
			
			$fields = array($fieldName=>$fieldVal);
			
			$result = $this->db->updateQuery("ads",$fields,"ad_id=$dataId");
			$args['success'] = true;
			$this->json_output($args);
		}
		
		function delete_data(){
			$data_id = $this->datastore->pick('data_id');
			$this->db->deleteQuery("ads","ad_id=$data_id");
			
			$args['success'] = true;
			$args['data_id'] = $data_id;
			
			$this->json_output($args);
			
		}
		
		function resources(){
			$args['data'] = $this->db->selectQueryArr("ad_positions","position_id,position_name");
			$args['success'] = true;
			
			$this->json_output($args);
		}
		
		function activate_ad(){
			$data_id = $this->datastore->pick('data_id');
			$result = $this->db->updateQuery("ads",array("isEnabled"=>1),"ad_id=$data_id");
			if($result)
				$args['success'] = true;
			else
				$args['success'] = false;
				
			$args['data_id'] = $data_id;
			
			$this->json_output($args);
			
		}
		
		function deactivate_ad(){
			$data_id = $this->datastore->pick('data_id');('data_id');
			$result = $this->db->updateQuery("ads",array("isEnabled"=>0),"ad_id=$data_id");
			if($result)
				$args['success'] = true;
			else
				$args['success'] = false;
				
			$args['data_id'] = $data_id;
			
			$this->json_output($args);
			
		}
		
		function order_data(){
			//
			$this->datastore->set_parameter_source('POST');
			$order= $this->datastore->pick('order');
			$ctr=0;
			foreach($order as $ad_id){ $ctr++;
				$this->db->updateQuery("ads",array("`order`"=>$ctr),"ad_id=$ad_id");
			}
			
			$args['success'] = true;
			$this->json_output($args);
		}
		
	}
