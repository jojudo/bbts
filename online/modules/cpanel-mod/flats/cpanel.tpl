<div id="middle_content">
    <div id="cpanel">
        <?php foreach($boxes as $box) { ?>
        <div class="icon">
            <a href = "<?php echo transform_url($box['module'],$box['action']);?>">
                <img src="<?php echo mod_url();?>/images/<?php echo $box['img'];?>" border="0">
                <span><?php echo $box['label'];?></span>
            </a>
        </div>
        <?php } ?>
    </div>  
</div>