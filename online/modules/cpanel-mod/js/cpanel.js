function retreiveBulletin(){	 
	$.ajax({
		  type: "POST",
		  url: "index.php?mod=bulletin",
		  async: false,
		  data: "ptype=ajax&sub=frontpage",
		   success: function(data){
				if(data==0){
					sess_failure();
				}else{
					$("#bulletinDiv").html(data);
				}
		   },
		   error:function(http,url,error){alert("error code "+error)}
	})
}

function loadMessages(){
	$.ajax({
		  type: "GET",
		  url: "index.php?mod=emailManager",
		  async: false,
		  data: "ptype=ajax&action=api&limit=20",
		  success: function(xml){
				if(xml==0){
					//no output
					alert("no output");
				}else if(xml==1){
					$("#emailDiv").html('There are no new messages');
				}
				else{
					//process xml					
					var str='';
					$("#emailDiv").html('');
					$(xml).find('message').each(function(){
						str+='<li><a href="index.php?mod=emailManager" title="'+$(this).find('content').text().substr(100)+'... [click to view more]">';
						str+=$(this).find('sender').text()+" : ";
						str+=$(this).find('dated').text()+'</a></li>'+"\n"	
					});
					$("#emailDiv").append('<ul>'+str+'</ul>');
				}
		   },
		   error:function(http,url,error){alert("error code "+error)}
	})
}
$(function() {		
		$("#accordion").accordion({
			collapsible: true
		});
		loadMessages();

});