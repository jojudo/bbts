<?php
	class moduleHelper extends helperController{
		
		function __construct(){
			parent::__construct();
		}
		
		function main(){
			$this->quickView();
		}
		
		private function quickView(){
			
			//$this->set_mod_javascript();
			$this->set_mod_stylesheet();
			
            //get the cpanel boxes
            $this->db->set_table("cpanel");
            $this->db->set_fields("label,module,action,img");
            $this->db->set_orderby("`order`","ASC");
			$boxes = $this->db->select(); 
            
            //check if user access allows this box
			$custom_boxes = array();
			foreach($boxes as $box){
				if($this->permission->is_permitted($box['module'],$box['action'])){
					$custom_boxes[] = $box;
				}
			}
			$this->args['boxes'] = $custom_boxes;
			$this->args['messages'] = $this->getMessages();
           // echo '<pre>';
           // print_r($this->args);
            $this->set_mod_javascript('cpanel');
            $this->use_module_template('cpanel',$this->args);
			
		}
        
        private function getMessages(){
            
            $this->db->set_table("contactus");
            $this->db->set_fields("name,message,email,DATE_FORMAT(date_added,'%m/%d/%Y') added");
            $this->db->set_orderby("date_added","ASC");
            $this->db->set_condition("actiontaken!=1");
            
            
			return $this->db->select(); 
        }
	}
?>