<html>
<head>
	<meta http-equiv="Cache-Control" content="max-age=60"/>
</head>
<body>
	<form method="post" name="frmEditSalesman" id="frmEdit">
	<table cellpadding="3" cellspacing="0" width="100%">
		<tr>
			<td>First Name</td>
			<td><input type="text" name="fname" id="edit_salesman_fname" maxlength="20" size="30" autocomplete="off"></td>
		</tr>
		<tr>
			<td>Last Name</td>
			<td><input type="text" name="lname" id="edit_salesman_lname" size="30" autocomplete="off"></td>
		</tr>
		<tr>
			<td>Initials</td>
			<td><input type="text" name="initials" id="edit_salesman_initials" size="5" maxlength="3" autocomplete="off"></td>
		</tr>
		<tr>
			<td>Team</td>
			<td>
				<select name="team_id" id="edit_salesman_team">
					<option value="0">-- No Team --</option>
				</select>
			</td>
		</tr>
		<tr>
			<td></td>
			<td><input type="checkbox" id="edit_salesman_is_manager" name="salesman_is_manager" value="1">Sales Manager</td>
		</tr>
	</table>

	<p align="right"><input type="submit" name="btnSubmit" class="buttons" value="Update Salesman"></p>
	<input type="hidden" name="salesman_id" id="edit_salesman_id">
	</form>
</body>
</html>