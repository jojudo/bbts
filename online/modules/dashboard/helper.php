<?php
	class moduleHelper extends helperController{
		function __construct(){
			parent::__construct();
		}
		
		function main(){
			$this->quickView();
		}
		
		private function quickView(){
			add_vendor('bootstrap-daterangepicker','daterangepicker');
			add_vendor('moment','dmin/moment.min');
			add_vendor('bootstrap-daterangepicker','daterangepicker','css');
			
			$this->set_mod_javascript();
			$this->use_module_template('dashboard-1');
			
		}

	}
?>