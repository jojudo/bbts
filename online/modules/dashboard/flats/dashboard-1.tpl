 <!-- top tiles -->
           <div class="row top_tiles">
              <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                  <div class="icon"><i class="fa fa-caret-square-o-right"></i></div>
                  <div class="count">179</div>
                  <h3>Box Pick-up</h3>
                  <p>5% Up from previous week.</p>
                </div>
              </div>
              <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                  <div class="icon"><i class="fa fa-comments-o"></i></div>
                  <div class="count">179</div>
                  <h3>Box Orders</h3>
                  <p>-5% down from previous week.</p>
                </div>
              </div>
              <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                  <div class="icon"><i class="fa fa-sort-amount-desc"></i></div>
                  <div class="count">100</div>
                  <h3>Boxes Delivered</h3>
                  <p>5% up from previous week</p>
                </div>
              </div>
              <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                  <div class="icon"><i class="fa fa-check-square-o"></i></div>
                  <div class="count">10</div>
                  <h3>Messages</h3>
                  <p>5% up from previous week</p>
                </div>
              </div>
            </div>
          <!-- /top tiles -->

<div class="row">
	<div class="col-md-12">
		<div class="x_panel">
			 <div class="x_title">
				<h2>Transaction Summary <small>Weekly progress</small></h2>
				<div class="filter">
				  <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc">
					<i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
					<span>December 30, 2014 - January 28, 2015</span> <b class="caret"></b>
				  </div>
				</div>
				<div class="clearfix"></div>
			  </div>
            <h2>Shortcuts</h2>
        </div>
	</div>
</div>