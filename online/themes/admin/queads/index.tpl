<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>QAds Management Page </title>
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo $theme_url;?>/css/main.css" />
	<?php add_template('headinclude');?>
	<!-- custom headers here  -->
</head>
<body topmargin="0">
	<div id="container">
		<div id="banner"></div>
		<div id="top-nav"><?php widget('smartNav');?></div>
		<div id="inside-content"><?php echo $body;?></div>
		<div id="footer"><?php add_template('footer');?></div>
	</div>

</body>
</html>