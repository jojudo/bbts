<script language="javascript">
	var themePath = '<?php echo theme_url();?>/';
	var modPath = '<?php echo mod_url();?>/';
	var fullPath = '<?php echo admin_url();?>/';
	var mod = '<?php echo module();?>';
</script>	


<?php 
if(isset($javascript)){
    foreach($javascript as $javascript_url)
    { 
?>
<script language="javascript" src="<?php echo $javascript_url;?>"> </script>
<?php 
    }
} 

if(isset($stylesheet))
{
?>
        <?php foreach($stylesheet as $stylesheet_url){ ?>
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo $stylesheet_url;?>" />
    <?php } 

}?>
