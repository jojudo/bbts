<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Gentellela Alela! | </title>

    <!-- Bootstrap -->
    <link href="<?php echo vendor_url();?>/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo vendor_url();?>/font-awesome/css/font-awesome.min.css" rel="stylesheet">
	
	<?php if(isset($stylesheet)) { 
	foreach($stylesheet as $stylesheet_url){?>
	<link rel="stylesheet" type="text/css" media="screen" href="<?php echo $stylesheet_url;?>" />
    <?php } }?>


    <!-- Custom Theme Style -->
    <link href="<?php echo theme_url();?>/css/custom.min.css" rel="stylesheet">
  </head>