<!-- footer content -->
	<footer>
	  <div class="pull-right">
		Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
	  </div>
	  <div class="clearfix"></div>
	</footer>
	<!-- /footer content -->
  </div>
</div>

<!-- javascript variables -->
<script language="javascript">
	var themePath = "<?php echo theme_url();?>/";
	var modPath = "<?php echo mod_url();?>/";
	var fullPath = "<?php echo admin_url();?>/";
	var mod = "<?php echo module();?>";
</script>	


<!-- program specific javascripts -->
<?php 
if(isset($javascript)){
foreach($javascript as $javascript_url){ ?>
	<script src="<?php echo $javascript_url;?>"></script>
<?php } } ?>


<!-- Custom Theme Scripts -->
<script src="<?php echo theme_url();?>/js/custom.min.js"></script>