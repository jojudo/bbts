<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Millennium Express Admin Portal</title>
    
    <link href="<?php echo theme_url();?>/css/mexpress.css" rel="stylesheet">
    <link href="<?php echo theme_url();?>/bower_components/jquery-ui/themes/ui-lightness/jquery-ui.css" rel="stylesheet">
    
    <?php add_template('headinclude');?>
</head>

<body>

    <div id="horizon">

    	<div id="login-box">
	      	<form name="login_form" method="post" action="index.php">
	       	  	<div><img src="<?php echo theme_url();?>/images/icon_info.png" style="vertical-align:bottom;" hspace="3" vspace="2"><?php echo $error_msg;?></div>
	          	<p><label for="username">Username: </label><input name="username" type="text" size="25" maxlength="255" autocomplete = "off"></p>
	            <p><label for="password">Password: </label><input name="password" type="password" size="25" maxlength="255" autocomplete = "off"></p>
	            <p align="right"><a href="../index.php"><img src="<?php echo theme_url();?>/images/house.png" border="0" title="Homepage"></a><input type="submit" name="login" value="Login"></p>

	      	</form>

      </div>

    </div>

</body>

</html>