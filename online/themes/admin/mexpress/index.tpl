<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Millennium Express Admin</title>
    
    <link href="<?php echo theme_url();?>/css/mexpress-2.css" rel="stylesheet">
    <link href="<?php echo theme_url();?>/css/grid.css" rel="stylesheet">
    <link href="<?php echo theme_url();?>/bower_components/jquery-ui/themes/ui-lightness/jquery-ui.css" rel="stylesheet">
   <?php add_template('headinclude');?>
</head>
<body>
<div id="container">
    <div id="admin-body">
        <div id="banner">
            <a href="../index.php"><img src="<?php echo theme_url();?>/images/logo-admin.png" border="0"></a>
        </div>
        <div id="top-nav">
                <table width="100%">
                    <tr>
                        <td>
                            <div class="top-menu">
                                 <?php widget('smartNav');?>
                            </div>
                        </td>
                        <td align="right">
                                Welcome Back <b><?php echo $profile['user_fname'];?></b> (<?php echo date("m/d/Y h:i a");?>)
                                <a href="logout.php"><img src="<?php echo theme_url();?>/images/icon_profile.png" title="Edit Profile" border="0"></a>
                                <a href="logout.php"><img src="<?php echo theme_url();?>/images/icon_logout.png" title="Logout" border="0"></a>						
                        </td>
                    </tr>
                </table>
        </div>
        <div>
            <?php echo $body;?> 
        </div>
       
    </div>
    <div id="footer">
        Copyright &copy; <?php echo date("Y");?> Millennium Express & Travel INC. All rights reserved. 
    </div>
 </div>
 </body>
</html>