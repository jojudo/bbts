<script language="javascript">
	var themePath = '<?php echo theme_url();?>/';
	var modPath = '<?php echo mod_url();?>/';
	var fullPath = '<?php echo admin_url();?>/';
	var mod = '<?php echo module();?>';
</script>	

<!-- system assigned javascripts -->
<?php 
if(isset($javascript)){
    foreach($javascript as $javascript_url){ 
?>
		<script language="javascript" src="<?php echo $javascript_url;?>"> </script>
		

<?php 
    }
} 
?>
<!-- system assigned stylesheets -->
<?php
if(isset($stylesheet)){
	foreach($stylesheet as $stylesheet_url){ ?>
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo $stylesheet_url;?>" />
    <?php } 
}?>
