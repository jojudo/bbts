
$(function() {

    if(getCookie("hideSide")==1){
        $("#page-wrapper").addClass("page-hide");
        $("#side-menu").addClass("hide-side");
        $("#side-menu-min").addClass("handler-hidden")
        $("#side-menu-min").find("i").addClass("fa-caret-right");
        $("#side-menu-min").find("i").removeClass("fa-caret-left");
    }
    
   $("#side-menu-min").click(function(e) {
        e.preventDefault();
        $("#page-wrapper").toggleClass("page-hide",function(){
           setUnsetSidebarCookie();
        });

        $("#side-menu").toggleClass("hide-side");
        $(this).toggleClass("handler-hidden");
        $(this).find("i").toggleClass("fa-caret-left fa-caret-right");
    });
 

});

function setUnsetSidebarCookie(){
    if(getCookie("hideSide")==1)
        setCookie("hideSide",0);
    else
        setCookie("hideSide",1);
}

function setCookie(cname, cvalue) {
    var d = new Date();
    d.setTime(d.getTime() + (1*24*60*60*1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
    }
    return "";
}