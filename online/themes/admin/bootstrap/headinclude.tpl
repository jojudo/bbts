<?php if( isset($ui_vars) && is_array($ui_vars)){ ?>
<script language="javascript">
	<?php
	foreach($ui_vars as $varName => $varValue){
		echo 'var '.$varName.'="'.$varValue.'";'."\n";
	} ?>
</script>	
<?php } ?>

<?php 
if(isset($javascript)){
    foreach($javascript as $javascript_url)
    { 
?>
            <script language="javascript" src="<?php echo $javascript_url;?>"> </script>
<?php 
    }
} 

if(isset($stylesheet))
{
?>
    <?php foreach($stylesheet as $stylesheet_url){ ?>
            <link rel="stylesheet" type="text/css" media="screen" href="<?php echo $stylesheet_url;?>" />
    <?php } 

}?>
