<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Millennium Express &amp; Travel INC Balikbayan Boxes from Hawaii to the Philippines </title>
		
		<meta property="fb:page_id" content="175696702459695" />
		<meta name="google-site-verification" content="efyOf6uqO4dlnjNNug9U3iMk0Nu5vBWcyybwH_H6fwE" />
		<meta name="description" content="Send Balikbayan Boxes to your loved ones in the Philippines from Hawaii, track where they are through our tracking system">
		<meta name="keywords" content="balikbayan boxes,hawaii,philippines,balikbayan boxes to the philippines,hawaii to Philippines, boxes to Philippines,from Hawaii,
					balikbayan box door-to-door,door-to-door, shipping to the Philippines, web-based balikbayan box, internet balikbayan box, Travel to the philippines,
					jumbo box to the philippines,online tracking system">
		
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo theme_url();?>/css/style.css" />
		<?php add_template('headinclude');?>
	</head>
	<body>
		<div id="container">
			<div id="banner">
				<div id="logo">Millennium Express amp&; Travel INC</div>
				<div id="main-ads">Send Balikbayan Boxes to Metro Manila for as low as $53</div>
				<div id="main-tracking">
					<form method="get" action="index.php">
						<p><input type="text" name="id" id="keyword"><input type="submit" name="btnTrack" Value="Check"></p>
						<input type="hidden" name="page" value="track">
					</form>
				</div>
				<div id="main-license">NVOCC License #021386N. <a href="#">What's This?</a></div>
				<div id="main-nav">
					<ul>
						<li><a href="<?php echo full_url();?>/index.html">Home</a></li>
						<li><a href="<?php echo full_url();?>/services.html">Services</a></li>
						<li><a href="<?php echo full_url();?>/capability_statement.html">Government Services</a></li>
						<li><a href="<?php echo full_url();?>/faqs.html">FAQ's</a></li>
						<li><a href="<?php echo full_url();?>/aboutus.html">About Us</a></li>
						<li><a href="<?php echo full_url();?>/contactus.html">Contact Us</a></li>
					</ul>
				</div>
			</div>
			
			<!-- body -->
			<div id="body">
				<div id="content">
					<!-- content starts here -->
					<?php echo $body;?>
				</div>
			</div>
		</body>
		<div id="footer">
			<div id="credit-cards">
				<h4>Millennium Express Accept Visa and Mastercard</h4>
			</div>
			<div id="bottom-links">
				<ul>
					<li><a href="http://www.millenniumexpress.biz">Home</a></li>
					<li><a href="http://www.millenniumexpress.biz/services.html">Services</a></li>
					<li><a href="http://www.millenniumexpress.biz/usefulinfo.html">Useful Info</a></li>
					<li><a href="http://www.millenniumexpress.biz/faq.html">Faq's</a></li>
					<li><a href="http://www.millenniumexpress.biz/aboutus.html">About Us</a></li>
					<li><a href="http://www.millenniumexpress.biz/contactus.html">Contact Us</a></li>
					<li class="last"><a href="#" onclick="return false;"><img src="<?php echo theme_url();?>/images/icn-sitemap.gif" alt="sitemap" width="11" height="9" border="0" /> &nbsp; Site Map</a></li>
				</ul>
				<p>Copyright &copy; 2008 MillenniumExpress.biz All Rights Reserved.</p>
					<!-- AddThis Button BEGIN -->
                    <!--<div style="float:right;padding-right:10px;width:150px;">
					<div class="addthis_toolbox addthis_default_style addthis_32x32_style">
					<a class="addthis_button_preferred_1"></a>
					<a class="addthis_button_preferred_2"></a>
					<a class="addthis_button_compact"></a>
					</div>
					<script type="text/javascript">var addthis_config = {"data_track_clickback":true};</script>
					<script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#username=mexman"></script>
					</div>	-->	
                    <!-- AddThis Button END -->
			</div>
		</div>
	</body>
</html>