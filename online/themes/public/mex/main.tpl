<!-- left column -->
<div id="main-page">
	<div id="leftcol">
		<div>
			<h1><span class="white">Welcome To</span> <span class="orange">Millennium Express &amp; Travel INC</span></h1>
			
			<p>Millenium Express &amp; Travel INC is a Hawaii based cargo/freight forwarder servicing the Hawaii - Philippines route. We provide efficient and reliable wholesale and retail cargo delivery services from the island of Oahu to any destination point of the Philippines.
			
			<P>With Us, your Balikbayan Boxes will reach home fast, safe and fully trackable through our online tracking service. 
				Available in 3 sizes: Large, Medium & Small not to mention an array of Savings Packages, you will always have the 
				option that is right for you.
			</p>
			
			<!-- <script src="http://connect.facebook.net/en_US/all.js#xfbml=1"></script><fb:like href="http://www.millenniumexpress.biz" layout="button_count" show_faces="true" width="450" action="recommend" font="lucida grande"></fb:like> -->
		</div>
		
		<div class="line">&nbsp;</div>
		
		<div>
			<h1><span class="white">We've</span> <span class="orange">Moved</span></h1>
			<p>
				Please visit our new OAHU office at  <a href="https://www.google.com/maps/preview#!q=94-235+Hanawai+Circle%2C+Suite+8%2C+Waipahu%2C+Hawaii+96797&data=!1m4!1m3!1d807!2d-158.0074705!3d21.3841285!4m10!1m9!4m8!1m3!1d3227!2d-158.0076314!3d21.384372!3m2!1i1366!2i667!4f13.1" target="_new" title="click to view map">94-235 Hanawai Circle, Suite 8, Waipahu, Hawaii 96797</a>.<br />
				In MAUI inside <a href="https://www.google.com/maps/preview#!q=Kahului+Shopping+Center&data=!1m4!1m3!1d6475!2d-156.470144!3d20.891123!4m29!2m16!1m14!1s0x0%3A0xbe6b0f8b4da005ca!3m8!1m3!1d6475!2d-156.470144!3d20.891123!3m2!1i1366!2i667!4f13.1!4m2!3d20.8902277!4d-156.4686906!5e4!6s65+W.+Kaahumanu+Avenue+Unit+3+Kahului%2C+Maui+96732!5m11!1m10!1s65+W.+Kaahumanu+Avenue+Unit+3+Kahului%2C+Maui+96732!4m8!1m3!1d807!2d-158.0074705!3d21.3841285!3m2!1i1366!2i667!4f13.1" title="click to view map" target="_new"> TJ's Oriental Food Mart 65 W. Kaahumanu Avenue Unit 3 Kahului, Maui 96732</a>. <br />
				You can call us at <b>808-699-4329</b>
			</p>
			<p>
				For delivery status inquiries you can call us at <b>818-357-2346</b> or <b>818-237-5264</b><br />
				or send as an email to <a href="http://millenniumexpress.biz/contactus.html">millenniumexpress@gmail.com</a>
			</p>
		</div>
		<h1></h1>
		<div class="double">
			<div>
				<h1><span class="white">Our</span> <span class="orange">Advantage</span></h1>
				<p>Millenium Express &amp; Travel INC is an OTI-NVOCC (Ocean Transport Intermediary-Non Vessel Operating Common Carrier) and is licensed by the U.S. Federal Maritime Commission. This allow us to issue our own Bill of Landing which enables cargo consignees to receive their shipping documents and process required paperwork ahead of time.</p>
			</div>
			<div>
				<h1><span class="white">Our</span> <span class="orange">Destinations</span></h1>
				<p><a href="http://www.millenniumexpress.biz/services.html#destination"><img src="<?php echo theme_url();?>/images/destination-map.gif" alt="click here to view our destinations" border="0" /></a></p>
			</div>
		</div>
		
	</div>
	<!-- /left column -->

	<!-- right column -->
	<div id="rightcol">
		<div class="boes" id="login-box">
			<?php widget('loginBox');?>
		</div>
		
		 <div class="boxes" id="pick-up">
			<h2>Pick-up Schedule</h2>
			<div class="overlay"></div>
			<div>
				<p>Generally box request deliveries are done in the mornings and early afternoons. Please allow a 4-hour window for pick-ups and deliveries. In some cases, requests made later in the day may be served the following day.</p>
			</div>
		</div>
		
		 <div class="boxes" id="travel-services">
			<h2>Travel Services</h2>
			<div>
				<p>We got very competitive rates for your travel needs</p>
				<span class="call_button">Call 808.699.4329</span>
				<span class="yellow">for quotation</span>
			</div>
			
		</div>
		
		 <div class="boxes" id="government-services">
			<h2>Government Services</h2>
			<div>
				<ul>
					<li><b>FREIGHT SERVICES</b> - From Pre-move to Post-Move, we handle planning, organizing and Preparation... <a href="http://millenniumexpress.biz/capability_statement.html">[more]</a></li>
					<li><b>TRASH SERVICES</b> -Curbside garbage and rubbish collection of non-hazardous solid waste services <a href="http://millenniumexpress.biz/capability_trash.html">[more]</a></li>
				</ul>
			</div>
		</div>
	</div>
</div>
<!-- /right column -->
<div class="clear">&nbsp;</div>