<?
$css = 'index.css';
include('header.inc.php');

?>
                <!-- left column -->
                <div id="leftcol">
                    <div id="welcome">
                        <h1>Welcome To Millennium Express &amp; Travel LLC</h1>
                        <p>Millenium Express &amp; Travel LLC is a Hawaii based cargo/freight forwarder servicing the Hawaii - Philippines route. We provide efficient and reliable wholesale and retail cargo delivery services from the island of Oahu to any destination point of the Philippines.</p>
                    </div>
                    <div id="advantage">
                        <h1>Our Advantage</h1>
                        <p>Millenium Express &amp; Travel LLC is an OTI-NVOCC (Ocean Transport Intermediary-Non Vessel Operating Common Carrier) and is licensed by the U.S. Federal Maritime Commission. This allow us to issue our own Bill of Landing which enables cargo consignees to receive their shipping documents and process required paperwork ahead of time.</p>
                    </div>
                    <div id="destinantion">
                        <h1>Our Destinations</h1>
                        <p><a href="#"><img src="images/destination-map.gif" alt="click here to view our destinations" border="0" /></a></p>
                    </div>
                    <div id="rates">
                        <h1>Our Rates</h1>
                        <table width="100%" cellpadding="5">
                          <tr>
                            <th scope="col">Shipment Item</th>
                            <th scope="col">Destination</th>
                            <th scope="col">Rates*</th>
                            <th scope="col">Days of arrival</th>
                          </tr>
                          <tr>
                            <td>Balikbayan Box</td>
                            <td>Luzon (except Bicol / Mindoro)</td>
                            <td>$33</td>
                            <td>25 - 30 days</td>
                          </tr>
                          <tr>
                            <td>Balikbayan Box</td>
                            <td>Luzon (Bicol / Mindoro)</td>
                            <td>$45</td>
                            <td>25 - 30 days</td>
                          </tr>
                          <tr>
                            <td>Balikbayan Box</td>
                            <td>Visayas / Mindanao</td>
                            <td>$45</td>
                            <td>30 - 40 days</td>
                          </tr>
                        </table>
                        <p><a href="#">more info</a></p>
                        <p>Shipping cost only. Cost of box not included</p>
                  </div>
                </div>
                <!-- /left column -->
                
                <!-- right column -->
                <div id="rightcol">
                    <div id="special-offers">
                        <h1>Special Offers</h1>
                        <p><img src="images/temp-special-offers.png" alt="temporary" width="270" height="124" border="0" /></p>
                  </div>
                    <div id="other-services">
                        <h1>Other Services</h1>
                        <p><img src="images/temp-otherservices.png" alt="temporary" width="270" height="124" border="0" /></p>
                  </div>
                    <div id="pickup-schedule">
                        <h1>Pick-up Schedule</h1>
                        <p>Generally box request deliveries are done in the mornings and early afternoons. Please allow a 4-hour window for pick-ups and deliveries. In some cases, requests made later in the day may be served the following day.</p>
                    </div>
                </div>
                <!-- /right column -->
<?
	include('footer.inc.php');
?>