<?

$css = 'services.css';

include('header.inc.php');



?>

                <!-- left column -->

                <div id="leftcol">

                	<div id="services">

                    	<h1>Services</h1>
				
			  <h3>Government Services:</h3>

				<ul>
					<li><a href="http://millenniumexpress.biz/capability_statement.html">Freight Services</a></li>
					<li><a href="http://millenniumexpress.biz/capability_trash.html">Trash Services</a></li>
</ul>
                        <h3>Retail Services:</h3>



                        <p>Balikbayan Box Forwarding<br/>

                        Off-Size Boxes and Other Packages Forwarding</p>

                        

                        <h4>Also Accepts Shipment for:</h4>

                        <ul>

                            <li>Merchandise and Supplies</li>

                            <li>Appliances and Furniture</li>

                            <li>Second-Hand Vehicles</li>

                            <li>Returning Residence Household Goods and Personal Effects</li>

                        </ul>

                        

                        <h3>Wholesale Services:</h3>

                        <h4>Full Container Loads (FCLs):</h4>

                        <ul>

                            <li>Standard 20-footer with maximum of ___ tons (19'5" long x 7'8" wide x 7'9" high)</li>

                            <li>Standard 40-footer with maximum of ___ tons  (39'6" long x 7'8" wide x 7'9" high)</li>

                            <li>Standard 45-footer with maximum of ___ tons</li>

                        </ul>

                        

                        <h3>Delivery Service Arrangements:</h3>

                        <ul>

                            <li>Door-to-Door</li>

                            <li>Door-to-Port</li>

                            <li>Port-to-Port</li>

                            <li>Port-to-Door</li>

                        </ul>

                    </div>

                </div>

                <!-- /left column -->

                

                <!-- right column -->

                <div id="rightcol">

                    <div id="more-rates">

                    	<h1>More Rates</h1>

                        <h3>Off-Size Boxes:</h3>

                        	<h4>To compute for shipping cost:</h4>

                            <ul>

                                <li>VOLUME FACTOR = Length x Width x Height / 1720</li>

                                <li>Shipping Cost = VOLUME FACTOR X $14</li>

                            </ul>

                            <h4 class="top">Example: Box size: L= 20 W=20 H= 12</h4>

                            <ul>

                                <li>VOLUME FACTOR = 2.79 (4800/1720)</li>

                                <li>Shipping Cost = $39.06 ( $14 x 2.79)</li>

                            </ul>

                        

                        <h3>Appliances / Furnitures:</h3>

                        <table border="0" width="100%">

                          <tr>

                            <th scope="row">Washing Machine/Dryer</th>

                            <td>$250 per</td>

                          </tr>

                          <tr>

                            <th scope="row">Refrigerator</th>

                            <td>$300 to $500 (depending on size)</td>

                          </tr>

                          <tr>

                            <th scope="row">Plasma TV</th>

                            <td>$300 to $500 (depending on size)</td>

                          </tr>

                          <tr>

                            <td colspan="2" class="mat">Mattresses</th>

                          </tr>

                          <tr>

                            <th scope="row">Double</th>

                            <td>$250</td>

                          </tr>

                          <tr>

                            <th scope="row">Queen</th>

                            <td>$300</td>

                          </tr>

                          <tr>

                            <th scope="row">King</th>

                            <td>$400</td>

                          </tr>

                        </table>

                        <!--

                        <h3>Other Shipments Accepted:</h3>

                        <ul>

                            <li>Household Goods</li>

                            <li>Vehicles</li>

                            <li>Returning Residents household goods</li>

                        </ul>

                        -->

                        <p>Note: No additional inserts on container accepted (i.e. rice, etc)</p>

                    </div>

                </div>

                <!-- /right column -->

                <div class="clear"></div>

                <div id="destination">

                    	<h1><a name="destination">Destination</a></h1>

                        <ul>

                            <li>Abra</li>

                            <li>Agusan del Norte</li>

                            <li>Agusan del Sur</li>

                            <li>Aklan</li>

                            <li>Albay</li>

                            <li>Antique</li>

                            <li>Apayao</li>

                            <li>Aurora</li>

                            <li>Basilan</li>

                            <li>Bataan</li>

                            <li>Batanes</li>

                            <li>Batangas</li>

                            <li>Biliran</li>

                            <li>Benguet</li>

                            <li>Bohol</li>

                            <li>Bukidnon</li>

                            <li>Bulacan</li>

                            <li>Cagayan</li>

                            <li>Camarines Norte</li>

                            <li>Camarines Sur</li>

                            <li>Camiguin</li>

                            <li>Capiz</li>

                            <li>Catanduanes</li>

                            <li>Cavite</li>

                            <li>Cebu</li>

                            <li>Compostela</li>

                            <li>Davao del Norte</li>

                            <li>Davao del Sur</li>

                            <li>Davao Oriental</li>

                            <li>Dinagat Islands</li>

                            <li>Eastern Samar</li>

                            <li>Guimaras</li>

                            <li>Ifugao</li>

                            <li>Ilocos Norte</li>

                            <li>Ilocos Sur</li>

                            <li>Iloilo</li>

                            <li>Isabela</li>

                            <li>Kalinga</li>

                            <li>Laguna</li>

                            <li>Lanao del Norte</li>

                            <li>Lanao del Sur</li>

                            <li>La Union</li>

                            <li>Leyte</li>

                            <li>Maguindanao Marinduque</li>

                            <li>Masbate</li>

                            <li>Mindoro Occidental</li>

                            <li>Mindoro Oriental</li>

                            <li>Misamis Occidental Misamis Oriental</li>

                            <li>Mountain Province</li>

                            <li>Negros Occidental</li>

                            <li>Negros Oriental</li>

                            <li>North Cotabato</li>

                            <li>Northern Samar</li>

                            <li>Nueva Ecija</li>

                            <li>Nueva Vizcay</li>

                            <li>Palawan</li>

                            <li>Pampanga</li>

                            <li>Pangasinan</li>

                            <li>Quezon</li>

                            <li>Quirino</li>

                            <li>Rizal</li>

                            <li>Romblon</li>

                            <li>Samar</li>

                            <li>Sarangani</li>

                            <li>Shariff Kabunsuan</li>

                            <li>Siquijor</li>

                            <li>Sorsogon</li>

                            <li>South Cotabato</li>

                            <li>Southern Leyte</li>

                            <li>Sultan Kudarat</li>

                            <li>Sulu</li>

                            <li>Surigao del Norte</li>

                            <li>Surigao del Sur</li>

                            <li>Tarlac</li>

                            <li>Tawi-Tawi</li>

                            <li>Zambales</li>

                            <li>Zamboanga del Norte</li>

                            <li>Zamboanga del Sur</li>

                            <li>Zamboanga Sibugay</li>

                        </ul>

                    </div>

<?

	include('footer.inc.php');

?>