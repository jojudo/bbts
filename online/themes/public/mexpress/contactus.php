<?php

$process->contact();

$css = 'contactus.css';

$js = 'main.js';

include('header.inc.php');



?>

                <!-- left column -->

                <div id="leftcol">

                    <div id="contact-form">

                        <h1>Contact Us</h1>

                        <table>

                        <form name="formname" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>?page=contactus" onsubmit="return formchecking();">

                          <tr>

                            <td colspan="2" scope="row" class="status"><? echo($alert); ?></td>

                          </tr>

                          <tr>

                            <th scope="row"><label for="name">*Name:</label></th>

                            <td><input name="name" type="text" size="32" maxlength="80" value="<?php echo $_SESSION['contactname'];?>"  id="name"/></td>

                          </tr>

                          <tr>

                            <th scope="row"><label for="email">*Email Address:</label></th>

                            <td><input name="email" type="text" size="32" maxlength="80" value="<?php echo $_SESSION['contactemail'];?>" id="email" /></td>

                          </tr>

                          <tr>

                            <th scope="row"><label for="contact">Contact Number:</label></th>

                            <td><input name="contact" type="text" size="32" maxlength="80" value="<?php echo $_SESSION['contactcontact'];?>"  /> (optional)</td>

                          </tr>

                          <tr>

                            <th scope="row" class="align-top"><label for="message">*Message:</label></th>

                            <td><textarea name="message" cols="50" rows="10" id="message"><?php echo $_SESSION['contactmsg'];?></textarea></td>

                          </tr>

                          <tr>

                            <th scope="row">&nbsp;</th>

                            <td><input name="terms" type="checkbox" class="noborder" value="agree" id="chkagree"/>

                             Yes, I read and agree to the <a href="#" onclick="openWindow('?page=privacy','500','500','privacy');return false;">Privacy Policy</a></td>

                          </tr>

                          <tr>

                            <th scope="row">&nbsp;</th>

                            <td>

								<input name="clear" type="image" class="noborder" value="clear" src="<?php echo THEME_ADDR;?>images/btn-clear.png" alt="Clear" onclick="return false;"/>

								<input name="submit" type="image" class="noborder" value="send" src="<?php echo THEME_ADDR;?>images/btn-send.png" alt="Send" />

								<input type="hidden" name="process" value="contact">

								<br />*Required

							</td>

                          </tr>

                          </form>

                        </table>

						

							 

                    </div>

                </div>

                <!-- /left column -->

                

                <!-- right column -->

                <div id="rightcol">

                    <div id="call-visit">

                    	<h1>Call or Visit us</h1>

                        <dl>

                            <dt>OAHU Address:</dt>

                            <dd>94-235 Hanawai Circle, Suite 8</dd>

                            <dd> Waipahu, HI 96797</dd>

                            <dt>MAUI Address:</dt>

                            <dd>inside TJ's Oriental Food Mart</dd>

				<dd>65 W. Kaahumanu Avenue Unit 3 </dd>
				
				<dd>(Kahului Shopping Center) Kahului, Maui  96732 </dd> 



                            <dt>Telephone No:</dt>

                            <dd>808-699-4329</dd>

                          

                            <!--  <dt>Fax:</dt>

                            <dd>(800) 395-1658</dd> -->

                            <dt>Philippines:</dt>

                            <dd>818-357-2346</dd>
                            <dd>818-237-5264</dd>
				
   			 	<dt>PH Mobile Number:</dt>
				 <dd>09258770909</dd>


                            <dt>Email:</dt>

                            <dd>millenniumexpress@gmail.com</dd>

                        </dl>

                    </div>

                </div>

                <!-- /right column -->

<?

	include('footer.inc.php');

?>