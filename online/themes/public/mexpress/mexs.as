﻿package {
	import flash.display.MovieClip;
	import flash.events.*;
	import flash.net.navigateToURL;
	import flash.net.URLRequest;
	import fl.transitions.TweenEvent;

	public class mex extends MovieClip {
	public var tn:String;
		
		public function mex() {
			//trace("hello");
			
			btn_home.navLabel.navLabelText.text = "Home";
			btn_services.navLabel.navLabelText.text = "Services";
			btn_useful.navLabel.navLabelText.text = "Useful Info";
			btn_faqs.navLabel.navLabelText.text = "Faq's";
			btn_about.navLabel.navLabelText.text = "About Us";
			btn_contact.navLabel.navLabelText.text = "Contact Us";
			
			btn_home.addEventListener(MouseEvent.CLICK, goHome);
			btn_services.addEventListener(MouseEvent.CLICK, goServices);
			btn_useful.addEventListener(MouseEvent.CLICK, goUseful);
			btn_faqs.addEventListener(MouseEvent.CLICK, goFaqs);
			btn_about.addEventListener(MouseEvent.CLICK, goAbout);
			btn_contact.addEventListener(MouseEvent.CLICK, goContact);
			
			
			btn_home.addEventListener(MouseEvent.MOUSE_OVER, btnOver);
			btn_services.addEventListener(MouseEvent.MOUSE_OVER, btnOver);
			btn_useful.addEventListener(MouseEvent.MOUSE_OVER, btnOver);
			btn_faqs.addEventListener(MouseEvent.MOUSE_OVER, btnOver);
			btn_about.addEventListener(MouseEvent.MOUSE_OVER, btnOver);
			btn_contact.addEventListener(MouseEvent.MOUSE_OVER, btnOver);
			
		
			mcBalikbayan.btnMore.addEventListener(MouseEvent.CLICK, goRates);
			//trace(mcTracking.btnTrack);
			mcTracking.btnTrack.addEventListener(MouseEvent.CLICK, goTracking);
			//btn_whats.addEventListener(MouseEvent.CLICK, goContact);
			//btn_sitemap.addEventListener(MouseEvent.CLICK, goContact);
		}
		
		private function btnOver(event:MouseEvent):void
		{
			event.currentTarget.gotoAndPlay(2);
			event.currentTarget.addEventListener(MouseEvent.MOUSE_OUT, btnOut);
		}
		
		private function btnOut(event:MouseEvent):void
		{
			event.currentTarget.gotoAndPlay(16);
		}
		
		public function goTracking(event:MouseEvent):void
		{
			if(mcTracking.trackNo.text) {
				tn = mcTracking.trackNo.text;
				var request:URLRequest = new URLRequest("index.php?page=track&id=" + tn);
				navigateToURL(request,"_self");
				mcTracking.trackNo.text = "";
			}
		}
		
		private function goHome(event:MouseEvent):void
		{
			var request:URLRequest = new URLRequest("index.php");
			navigateToURL(request,"_self");
		}
		
		private function goServices(event:MouseEvent):void
		{
			var request:URLRequest = new URLRequest("index.php?page=services");
			navigateToURL(request,"_self");
		}
		
		private function goUseful(event:MouseEvent):void
		{
			var request:URLRequest = new URLRequest("index.php?page=usefulinfo");
			navigateToURL(request,"_self");
		}
		
		private function goFaqs(event:MouseEvent):void
		{
			var request:URLRequest = new URLRequest("index.php?page=faqs");
			navigateToURL(request,"_self");
		}
		
		private function goAbout(event:MouseEvent):void
		{
			var request:URLRequest = new URLRequest("index.php?page=aboutus");
			navigateToURL(request,"_self");
		}
		
		private function goContact(event:MouseEvent):void
		{
			var request:URLRequest = new URLRequest("index.php?page=contactus.php");
			navigateToURL(request,"_self");
		}
		private function goRates(event:MouseEvent):void
		{
			var request:URLRequest = new URLRequest("index.php?page=rates");
			navigateToURL(request,"_self");
		}
	}
	
	
}