<?php
	session_start();
	ini_set("session.gc_maxlifetime", "18000"); 
	
	include_once("mil/includes/settings.inc.php");
	include_once("mil/includes/class.phpmailer.php");
	include_once("mil/includes/class.customEmailer.php");
	include_once("mil/includes/class.site_settings.php");
	include_once("mil/includes/class.db.php");
	include_once("mil/includes/functions.php");
	
	$db = new db();
	$user_arr = array();
	
	if($db->conn)
	{
		$user_id = $_SESSION['user_id'];
		
		//get the username and name of user
		$sql = "select user_username, 
					   concat(user_lname,', ',user_fname) name
					   from me_users
					  where
					  	user_id = '$user_id'";
		
		$rs = $db->queryMe($sql);
		$user_arr = $db->fetchAssoc($rs);
		$db->freeMe($rs);
	}
	
?>