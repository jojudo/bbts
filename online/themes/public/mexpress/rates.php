<?
$css = 'rates.css';
include('header.inc.php');
?>
                <!-- left column -->
                <div id="leftcol">
                	<div id="rates">
                        <h1>Our Affordable Rates</h1>
                        <table width="100%" cellpadding="5">
                          <tr>
                            <th width="20%" scope="col">From</th>
                            <th width="35%" colspan="3" scope="col">To Luzon</th>
                            <th width="45%" colspan="3" scope="col">To Visayas, Mindanao, Mindoro and Bicol</th>
                          </tr>
                          <tr style="font-weight:bold;">
                          	<td></td>
                          	<td>Regular</td>
                          	<td>Jumbo</td>
                          	<td>Bulilit</td>
                          	<td>Regular</td>
                          	<td>Jumbo</td>
                          	<td>Bulilit</td>
                          </tr>
                          <tr>
                            <td>Oahu</td>
                           	<td>$45</td>
                           	<td>$63</td>
                           	<td>$35</td>
                           	<td>$65</td>
                           	<td>$90</td>
                           	<td>$50</td>
                          </tr>
                          <tr>
                            <td>Maui</td>
                            <td>$55</td>
                            <td>$80</td>
                            <td>$40</td>
                            <td>$80</td>
                            <td>$105</td>
                            <td>$60</td>
                          </tr>
                          <tr>
                            <td>Kaua'i</td>
                            <td>$55</td>
                            <td>$75</td>
                            <td>$40</td>
                            <td>$75</td>
                            <td>$100</td>
                            <td>$60</td>
                          </tr>
                          <tr>
                            <td>Big Island Kona</td>
                            <td>$65</td>
                            <td>$90</td>
                            <td>$40</td>
                            <td>$100</td>
                            <td>$130</td>
                            <td>$70</td>
                          </tr>
                        </table>
                        <p>* all rates inclusive of tax. Empty Regular box @ $6/ea. Empty Jumbo box @ $8/ea. Empty Bulilit box @ $3.50/ea.</p>
                        	<dl>
                            	<dt>Days of shipment</dt>
                                <dd>Luzon: 30 - 35 days</dd>
                                <dd>Visayas, Mindanao, Mindoro and Bicol: 45 days</dd>
                            </dl>
                        <p>From date of shipment from honolulu port.</p>
                        <p>Loading every thursday.</p>
                  </div>
                </div>
                <!-- /left column -->
                
                <!-- right column -->
                <div id="rightcol">
                    <div id="more-savings">
                    	<h1>More Savings</h1>
                        <ul>
                        	<li>Send 4 boxes Get 1 Free Bulilit Box  <span class="yellow">(one-time pickup only &amp;  single consignee address)</span></li>
                            <li>Send 7 boxes &amp; pay only for 6 <span class="yellow">(one-time pick up &amp; one single consignee address)</span></li>
                            <li>Send 10 boxes (accumulated) &amp; send the 11<sup>th</sup> box for free <span class="yellow">(accompanied by 1 standard box)</span></li>
                        </ul>
                    </div>
                </div>
                <!-- /right column -->
<?
	include('footer.inc.php');
?>