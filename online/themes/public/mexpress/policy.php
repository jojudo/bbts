<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Millennium Express Privacy Policy </title>
	<link rel="stylesheet" type="text/css" media="screen" href="<?php echo  THEME_ADDR."styles".DS."policy.css"; ?>" />
	<link href="<?php echo THEME_ADDR;?>favicon.ico" rel="SHORTCUT ICON" />
</head>
<body>
<h1>Privacy Policy</h1>
<p>Keeping customer information private is a priority for <b>Millennium Express INC</b>. To enable us to provide you with best solutions to your problems, we need to collect certain information from you. However, we want to emphasize that we are committed to maintaining the privacy of this information in accordance with law. All individuals with access to personal information about our clients are required to follow this policy.</p>
<p>We collect non-public personal information about you from information we receive from you on applications or other forms, that which you may provide during visits to our website, and information about your transactions with us.</p>
<p>We do not disclose any non-public personal information about our clients or former clients to any non-affiliated entity except as described below and otherwise permitted by law. We may disclose all of the information we collect, as described above, to companies that assist us in the servicing or administration of the product that you have requested or authorized.</p>
<p>When information is shared with companies that perform services on our behalf, we protect against the subsequent disclosure of that information with a confidentiality agreement.</p>
<p>In no event do we disclose your personal information to companies that will use that information to contact you about their own products or services.</p>
<p>We restrict access to non-public information about you to those persons who need such information to provide products or services to you. We maintain physical, electronic and procedural safeguards that comply with federal regulation to guard your non-public personal information.</p>
<p>We are providing you this privacy notice to inform you of what personal information we collect about you and how we treat that information. We hope this privacy notice answers any questions you may have regarding our treatment of your personal information and reassures you of our dedication to keeping your personal information secure.</p>
<center><img src="<?php echo THEME_ADDR;?>/images/btn-agree.png"></center>
</body>
</html>