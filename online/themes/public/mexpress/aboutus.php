<?
$css = 'aboutus.css';
include('header.inc.php');

?>
                <!-- left column -->
                <div id="leftcol">
                	<div id="aboutus">
                    	<h1>About Us</h1>
                        <p>Millennium Express &amp; Travel LLC began operations in 2000 is headquartered in Waipahu, Hawaii (County of Honolulu). We are centrally located amidst major distribution points in the island of Oahu that offer short distances and more direct delivery and pick-up services.</p>
                        <p>We offer an unparalleled range of services to meet our customer&rsquo;s varied shipping requirements. We accept retail shipment of balikbayan boxes, off-size boxes and packages or commodities, as well as wholesale container shipments from the island of Oahu to any destination point in the Philippine Islands.</p>
                        <p>We aim to provide efficiency, reliability and wider reach to all our wholesale and retail clients at highly competitive rates and prices. We maintain personalized service and offer flexible responses to customer requirements so they are assured of fast, friendly, reliable, and efficient cargo handling and delivery services.</p>
                        <p>Millennium Express &amp; Travel LLC is a <em>NVOCC</em> (Non Vessel Operating Common Carrier) and is licensed by the <em>U.S Federal Maritime Commission</em>. (OTI-NVOCC License No. 021386N)</p>
                    </div>
                </div>
                <!-- /left column -->
                
                <!-- right column -->
                <div id="rightcol">
                    <!--
                    <div id="sub">
                    	<h3>sub menu</h3>
                    </div>
                    -->
                </div>
                <!-- /right column -->
<?
	include('footer.inc.php');
?>