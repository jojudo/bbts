<?php
$css = 'contactus.css';
$js = 'contact.js';
include('header.inc.php');

?>
                <!-- left column -->
                <div id="leftcol">
                    <div id="contact-form">
                        <h1>Contact Us</h1>
						<br />
                        <h2>Your Message has been Sent!</h2>
						<br />
                        <p>
							Thank you very much for taking the time and writing us a message. We will be contacting you through the 
							details you provided. Please allow 1-2 days for us to respond.
						</p>
						<br />
						<p>
							Sincerely,<br />
							<b>Millennium Express Admin</b>
						</p>
                    </div>
                </div>
                <!-- /left column -->
                
                <!-- right column -->
                <div id="rightcol">
                    <div id="call-visit">
                    	<h1>Call or Visit us</h1>
                        <dl>
                            <dt>Address:</dt>
                            <dd>94-208 Pupuole St. #102</dd>
                            <dd>Waipahu, HI 96797</dd>
                            
                            <dt>Telephone No:</dt>
                            <dd>(808) 699-4329</dd>
                            <dd>(866) 694-7793</dd>
                            
                            <dt>Fax:</dt>
                            <dd>(800) 395-1658</dd>
                            
                            <dt>Email:</dt>
                            <dd>millenniumexpress@gmail.com</dd>
                            <dd>admin@millenniumexpress.biz</dd>
                        </dl>
                    </div>
                </div>
                <!-- /right column -->
<?
	include('footer.inc.php');
?>