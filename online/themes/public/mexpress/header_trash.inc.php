	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Millennium Express &amp; Travel INC Balikbayan Boxes from Hawaii to the Philippines<?php echo strtoupper($param);?></title>
	<style type="text/css" media="screen">
	<!--
		@import url("<?php echo  THEME_ADDR."styles".DS.$css; ?>");
	-->
	</style>
	<script language="javascript">AC_FL_RunContent = 0;</script>
	<script src="<?php echo THEME_ADDR;?>js/AC_RunActiveContent.js" language="javascript"></script>
	<?php echo $GLOBALS['main_jscripts'];?>
	<?php echo$GLOBALS['main_stylesheets'];?>	

	<!--[if lt IE 7]>
	    <script defer type="text/javascript" src="pngfix.js"></script>
	<![endif]-->
	
	<meta property="fb:page_id" content="175696702459695" />
	<meta name="google-site-verification" content="efyOf6uqO4dlnjNNug9U3iMk0Nu5vBWcyybwH_H6fwE" />
	<meta name="description" content="Send Balikbayan Boxes to your loved ones in the Philippines from Hawaii, track where they are through our tracking system">
	<meta name="keywords" content="balikbayan boxes,hawaii,philippines,balikbayan boxes to the philippines,hawaii to Philippines, boxes to Philippines,from Hawaii,
				balikbayan box door-to-door,door-to-door, shipping to the Philippines, web-based balikbayan box, internet balikbayan box, Travel to the philippines,
				jumbo box to the philippines,online tracking system">
	<link href="<?php echo THEME_ADDR;?>favicon.ico" rel="SHORTCUT ICON" />

	<!-- asynchronous analytics update made by joey 02/23/2011 -->
		<script type="text/javascript">

		  var _gaq = _gaq || [];
		  _gaq.push(['_setAccount', 'UA-20658591-1']);
		  _gaq.push(['_trackPageview']);

		  (function() {
			var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		  })();

		</script>
	<!-- asynchronous analytics update made by joey 02/23/2011 -->

	</head>
<body>

	<div id="container">
    	
    	<div id="banner">
        <!-- flash banner -->
			<script language="javascript">
            if (AC_FL_RunContent == 0) {
                alert("This page requires AC_RunActiveContent.js.");
            } else {
                AC_FL_RunContent(
                    'codebase', 'http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,0,0',
                    'width', '960',
                    'height', '314',
                    'src', 'banner',
                    'quality', 'high',
                    'pluginspage', 'http://www.macromedia.com/go/getflashplayer',
                    'align', 'middle',
                    'play', 'true',
                    'loop', 'true',
                    'scale', 'showall',
                    'wmode', 'transparent',
                    'devicefont', 'false',
                    'id', 'banner',
                    'bgcolor', '#ffffff',
                    'name', 'banner',
                    'menu', 'false',
                    'allowFullScreen', 'false',
                    'allowScriptAccess','sameDomain',
                    'movie', '<?php echo THEME_ADDR;?>images/banner_trash',
                    'salign', ''
                    ); //end AC code
            }
            </script>
            <noscript>
            <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,0,0" width="960" height="314" id="banner" align="middle">
              <param name="allowScriptAccess" value="sameDomain" />
              <param name="allowFullScreen" value="false" />
              <param name="movie" value="<?php echo THEME_ADDR;?>/images/banner_trash.swf" />
              <param name="menu" value="false" />
              <param name="quality" value="high" />
              <param name="wmode" value="transparent" />
              <param name="bgcolor" value="#ffffff" />
              <embed src="themes/mex/images/banner_trash.swf" menu="false" quality="high" wmode="transparent" bgcolor="#ffffff" width="960" height="314" name="banner" align="middle" allowScriptAccess="sameDomain" allowFullScreen="false" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" />
            </object>
            </noscript>
        <!-- /flash banner -->
        </div>
        
        <!-- body -->
        <div id="body">
        
            <div id="content">