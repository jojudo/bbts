<?php
//include_once("public.auth.php");
$css = 'capability.css';
include('header_contracts.inc.php');
?>
                <!-- left column -->
                <div id="leftcol">
                    <div id="capability">
                        <h1>company overview</h1>
						
                        <p>
							Founded in 2000, Millennium Express provides freight forwarding,
							household goods and office/modular furniture
							relocation, delivery, transportation, logistics and warehousing solutions
							for residential, commercial, corporate and government customers.
							The company is a Federal Maritime Commission approved cargo shipper, and also a travel agency
							and money remittance center
							<br />
							<br />
						</p>
                    </div>
                    <div id="project-types">
                        <h1>Project Types</h1>
							<ul>
								<li>From Pre-move to Post-Move, we handle planning, organizing and Preparation</li>
								<li>Disassembly & Assembly of home and office modular furniture</li>
								<li>Packing, Crating, Containerization, local drayage, delivery, Unpacking storage for personal property shipments</li>
								<li>Furnishing of equipment, facilities, supplies for preparation of personal property for movement or drayage</li>
								<li>Appliance and Automobile Shipment</li>
							</ul>
							<p>&nbsp;</p>
                    </div>
                    <div id="key-clients">
                        <h1>Key Clients</h1>
							<ul>
								<li>
									Business to Consumers-over 16, 000 residential customers and
									released over 39 forty-footer containerized vans annually and
									delivered over 22,000 cargoes comprising of various personal/
									commercial packages annually to the Philippine
								</li>
								<li>B2B/B2G- Philippine Consulate, Don Quijote Department Stores, Metro Bank Remittance Center and Various SmallBusinesses</li>
							</ul>
						<p>&nbsp</p>
                    </div>
                    <p>&nbsp;</p>
                   <div id="alliance_org">
                        <h1>Alliance Organization / agency coordination</h1>
                        <p>
							<ul>
								<li>YK Line-Nippon Yusen Kaisha</li>
								<li>Matson Inc.</li>
								<li> XPEDX Inc.</li>
								<li>Philippines Airlines</li>
								<li>U.S. Customs & Border Protection</li>
								<li>Philippine Consulate Hawaii</li>
								<li>Philippine Customs</li>
								<li>Young Brothers Limited</li>
								<li>Don Quijote Department Stores</li>	
							</ul>
						</p>
                  </div>
                </div>
                <!-- /left column -->
                
                <!-- right column -->
                <div id="rightcol">
					<div id="company-data">
                        <h1>COMPANY DATA</h1>
						<div>
							Small Business<br />
							Woman-Owned <br />
							HUB-Zoned Area <br />
							DUNS #: 832240167<br />
							CAGE #:  5RAC7<br />
							FMC OTI/NVOCC#:	021386N  
						</div>
					</div>
					
                    <div id="naics-code">
                        <h1>Naics code</h1>
						<div>
							321920-Shipping Cases,
							Drums, Wood
							322211/423840-Shipping
							Containers
							484210-Used Household & 
							Office Goods Moving
							488510-Freight 
							Transportation Arrangement
							488991-Packing & Crating
						</div>
					</div>
					
					 <div id="special-item">
                        <h1>SPECIAL ITEM NUMBERS OFFERED</h1>
						<div>
							SIN 653-7-Move Management Services
							SIN 451-99-New Services
							SIN 653-1- Relocation Service Package
							SIN	653-3-Relocation Software, Technology Tools and	Services
						</div>
					</div>
					
                    <div id="gsa-schedule">
                        <h1>GSA Schedule 48</h1>
                        <p>Transportation, Delivery &  Relocation Solutions (Applied For) </p>
                    </div>
	                 <div id="onsite-facilities">
                        <h1>On-Site Facilities  </h1>
                        <p>
							Warehouses-5,000 sq. ft.<br />
							(Philippines) 2,000 sq.ft. Pearl City, Hawaii <br />
							Fleet of Delivery Trucks <br />
							Retail Stores: Waipahu, Maui and Big Island,  Hawaii <br />
						</p>
                       
                  </div> 
				 
				  <div id="other-services">
                        <h1>Other Services</h1>
                        <p> <a href="http://millenniumexpress.biz/capability_trash.html">
							Millennium Express in partnership with Big Island Clean Living LTD provides curbside garbage and rubbish
							 collection of  non-hazardous solid waste services
							 to community subdivisions and <br/>
							government agencies 
							</a>
						</p>
                  </div>
				  
                </div>
                <!-- /right column -->
<?
	include(THEME_FOLDER.'footer.inc.php');
?>
