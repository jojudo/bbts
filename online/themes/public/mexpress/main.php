<?php
//include_once("public.auth.php");
$css = 'index.css';
include('header.inc.php');
?>
                <!-- left column -->
                <div id="leftcol">
                    <div id="welcome">
                        <h1>Welcome To Millennium Express &amp; Travel INC</h1>
						
                        <p>Millenium Express &amp; Travel INC is a Hawaii based cargo/freight forwarder servicing the Hawaii - Philippines route. We provide efficient and reliable wholesale and retail cargo delivery services from the island of Oahu to any destination point of the Philippines.
						<br /><br/>
						With Us, your Balikbayan Boxes will reach home fast, safe and fully trackable through our online tracking service. 
							Available in 3 sizes: Large, Medium & Small not to mention an array of Savings Packages, you will always have the 
							option that is right for you.
							<br /><br />
							<script src="http://connect.facebook.net/en_US/all.js#xfbml=1"></script><fb:like href="http://www.millenniumexpress.biz" layout="button_count" show_faces="true" width="450" action="recommend" font="lucida grande"></fb:like>

						</p>
                    </div>
			
			<p>&nbsp;</p>
		     <div id="we-moved">
                        <h1>We Moved</h1>
                        <p>
				Please visit our new OAHU office at  <a href="https://www.google.com/maps/preview#!q=94-235+Hanawai+Circle%2C+Suite+8%2C+Waipahu%2C+Hawaii+96797&data=!1m4!1m3!1d807!2d-158.0074705!3d21.3841285!4m10!1m9!4m8!1m3!1d3227!2d-158.0076314!3d21.384372!3m2!1i1366!2i667!4f13.1" target="_new" title="click to view map">94-235 Hanawai Circle, Suite 8, Waipahu, Hawaii 96797</a>.<br />
				In MAUI inside <a href="https://www.google.com/maps/preview#!q=Kahului+Shopping+Center&data=!1m4!1m3!1d6475!2d-156.470144!3d20.891123!4m29!2m16!1m14!1s0x0%3A0xbe6b0f8b4da005ca!3m8!1m3!1d6475!2d-156.470144!3d20.891123!3m2!1i1366!2i667!4f13.1!4m2!3d20.8902277!4d-156.4686906!5e4!6s65+W.+Kaahumanu+Avenue+Unit+3+Kahului%2C+Maui+96732!5m11!1m10!1s65+W.+Kaahumanu+Avenue+Unit+3+Kahului%2C+Maui+96732!4m8!1m3!1d807!2d-158.0074705!3d21.3841285!3m2!1i1366!2i667!4f13.1" title="click to view map" target="_new"> TJ's Oriental Food Mart 65 W. Kaahumanu Avenue Unit 3 Kahului, Maui 96732</a>. <br />
				You can call us at <b>808-699-4329</b> <br />
				For delivery status inquiries you can call us at <b>818-357-2346</b> or <b>818-237-5264</b><br />
				or send as an email to <a href="http://millenniumexpress.biz/contactus.html">millenniumexpress@gmail.com</a>
	 		    </p>
                    </div>
			<h1></h1>
                    <div id="advantage">
                        <h1>Our Advantage</h1>
                        <p>Millenium Express &amp; Travel INC is an OTI-NVOCC (Ocean Transport Intermediary-Non Vessel Operating Common Carrier) and is licensed by the U.S. Federal Maritime Commission. This allow us to issue our own Bill of Landing which enables cargo consignees to receive their shipping documents and process required paperwork ahead of time.</p>
                    </div>
                    <div id="destinantion">
                        <h1>Our Destinations</h1>
                        <p><a href="http://www.millenniumexpress.biz/services.html#destination"><img src="<?php echo THEME_ADDR;?>images/destination-map.gif" alt="click here to view our destinations" border="0" /></a></p>
                    </div>
		

                   <!--  
                   <div id="rates">
                        <h1>Our Affordable Rates</h1>
    			   
                       <table width="100%" cellpadding="5">
                          <tr>
                            <th scope="col">From</th>
                            <th scope="col">Manila</th>
                            <th colspan="3" scope="col">Rest of Central Luzon</th>
                            <th colspan="3" scope="col">To Visayas, Mindoro and Bicol</th>
                          </tr>
                          <tr style="font-weight:bold;">
                          	<td></td>
                          	<td>Regular</td>
                          	<td>Regular</td>
                          	<td>Jumbo</td>
                          	<td>Bulilit</td>
                          	<td>Regular</td>
                          	<td>Jumbo</td>
                          	<td>Bulilit</td>
                          </tr>
                          <tr>
                            <td>Oahu</td>
                           	<td>$45</td>
                           	<td>$45</td>
                           	<td>$65</td>
                           	<td>$35</td>
                           	<td>$70</td>
                           	<td>$95</td>
                           	<td>$50</td>
                          </tr>
                          <tr>
                            <td>Maui</td>
                            <td>$55</td>
                            <td>$60</td>
                            <td>$80</td>
                            <td>$40</td>
                            <td>$80</td>
                            <td>$105</td>
                            <td>$60</td>
                          </tr>
                          <tr>
                            <td>Kaua'i</td>
                            <td>$55</td>
                            <td>$60</td>
                            <td>$80</td>
                            <td>$40</td>
                            <td>$75</td>
                            <td>$100</td>
                            <td>$60</td>
                          </tr>
                          <tr>
                            <td>Big Island Kona</td>
                            <td>$60</td>
                            <td>$65</td>
                            <td>$90</td>
                            <td>$40</td>
                            <td>$100</td>
                            <td>$130</td>
                            <td>$70</td>
                          </tr>
                        </table>
                        <p>*all rates inclusive of tax. Empty Regular box @ $7/ea. Empty Jumbo box @ $9/ea. Empty Bulilit box @ $5/ea.</p>
                        <p>*remote areas may be subject to a surcharge of $5 * surcharge applies on 1 pick up</p>
                        
                        	<dl>
                            	<dt>Days of shipment</dt>
                                <dd>Luzon: 30 - 45 days</dd>
                                <dd>Visayas, Mindanao, Mindoro and Bicol: 45 days</dd>
                            </dl>
                        <p>From date of shipment from honolulu port.</p>
                        <p>Loading every thursday.</p>

                        
                  </div>
-->
                </div>
                <!-- /left column -->
                
                <!-- right column -->
                <div id="rightcol">
                	<div id="loginbox">
                	<form name="login" method="post" action="mex/index.php">	
                        <fieldset>
                        	<legend>Members Login</legend>
                        	<?php
                        		if(!$_SESSION['client_authenticated']){
                        	?>
                            <p><label for="username">username:</label><input name="username" type="text" size="18" maxlength="32" autocomplete="off" /></p>
                            <p><label for="password">password:</label><input name="password" type="password" size="18" maxlength="32" autocomplete="off" /></p>
                            <p style="text-align: center;"><input name="login" type="submit" value="login" /></p>
                            <?php
                        		}
                        		else 
                        		{
                            ?>
                            	<p style="text-align:center;">You are already logged in</b></p>
                            	<p></p>
                            	<p style="text-align:center;">
                            		<a href="<?php echo FULL_ADDR.DS.ADMIN.DS."index.php";?>" style="color:#FFFFFF;">Members Area</a>
                            		<a href="<?php echo FULL_ADDR.DS.ADMIN.DS."logout.php";?>" style="color:#FFFFFF;">Log Out</a>
                            	</p>
                            <?
                        		}
                        	?>
                            
                        </fieldset>
                       </form>
                        <p></p>
                  </div>	
					 <div id="pickup-schedule">
                        <h1>Pick-up Schedule</h1>
                        <p>Generally box request deliveries are done in the mornings and early afternoons. Please allow a 4-hour window for pick-ups and deliveries. In some cases, requests made later in the day may be served the following day.</p>
						<div id="pu_guy"></div>
                    </div>
					
					 <div id="travel-services">
                        <h1>Travel Services</h1>
                        <p>We got very competitive rates for your travel needs</p>
                        <h3>Call 808.699.4329</h3>
                        <p class="yellow">for quotation</p>
					</div>
					
					 <div id="government-services">
                        <h1>Government Services</h1>
						<div>
							<ul>
								<li><b>FREIGHT SERVICES</b> - From Pre-move to Post-Move, we handle planning, organizing and Preparation... <a href="http://millenniumexpress.biz/capability_statement.html">[more]</a></li>
								<li><b>TRASH SERVICES</b> -Curbside garbage and rubbish collection of non-hazardous solid waste services <a href="http://millenniumexpress.biz/capability_trash.html">[more]</a></li>
							</ul>
						</div>
					</div>
					
                   
					
	                
                </div>
                <!-- /right column -->
<?
	include(THEME_FOLDER.'footer.inc.php');
?>
