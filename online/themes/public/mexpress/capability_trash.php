<?php
//include_once("public.auth.php");
$css = 'capability2.css';
include('header_trash.inc.php');
?>
                <!-- left column -->
                <div id="leftcol">
                    <div id="capability">
                        <h1>company overview</h1>
						
                        <p>
							Millennium Express in partnership with Big Island Clean Living LTD provides curbside garbage and rubbish collection 
							of non-hazardous solid waste services to community subdivisions and government agencies (Federal, State, and City), 
							schools, hospitals and businesses.    Big Island Clean Living LTD is bonded and licensed as a garbage and refuse collector/provider and a 
							contracted trash collector of  the City and County of Honolulu.     
							<br />
							<br />
						</p>
                    </div>
                    <div id="project-types">
                        <h1>Project Types</h1>
							<ul>
								<li>Curbside refuse, garbage and rubbish collection of non-hazardous solid waste (garbage) within a local area </li>
								<li>Bulky Item Collection Services</li>
								<li>Curbside Paint Disposal Services</li>
								<li>Curbside Hauling of Unwanted Passenger Tire Services</li>
								<li>Curbside Hauling of Unwanted Car Batteries </li>
								<li>Recycling & Hauling of mixed recycling materials within a local area</li>
								<li>We provide trash bins for low monthly rental</li>
								<li>We offer several efficient, economic and environmentally responsible trash disposal options for residential customers</li>
							</ul>
							<p>&nbsp;</p>
                    </div>
                    <div id="key-clients">
                        <h1>Key Clients</h1>
							<ul>
								<li>
									Business to Consumers-the company serviced over 10,000 households on a weekly basis (Paradise Park, Big Island, Hawaii)  
								</li>
								<li>B2B/B2G- Contracted with the City and County of Honolulu (On-Call Basis) servicing sectors from Hawaii Kai to Downtown 
									Honolulu. And sectors in Pearl City, Aiea, Waipahu, Ewa Beach And Makakilo, Kapolei, areas. </li>
							</ul>
						<p>&nbsp</p>
                    </div>
                    <p>&nbsp;</p>
                   <div id="alliance_org">
                        <h1>Alliance Organization / agency coordination</h1>
                        <p>
							<ul>
								<li>City & County of Honolulu </li>
								<li>Waimanalo Sanitary Landfill</li>
								<li>PVT Landfill</li>
								<li>Covanta CRRB</li>
							</ul>
						</p>
                  </div>
                </div>
                <!-- /left column -->
                
                <!-- right column -->
                <div id="rightcol">
					<div id="company-data">
                        <h1>COMPANY DATA</h1>
						<div>
							Small Business <br />
							Woman-Owned <br />
							HUB-Zoned Area <br />
							DUNS #: 832240167 <br />
							CAGE #:  5RAC7 <br />
							City of Honolulu Business Occupational License (Garbage & Rubbish Collection) No.- 0074850 <br />
							FMC OTI / NVOCC NO.�021386N
						</div>
					</div>
					
                    <div id="naics-code">
                        <h1>Naics code</h1>
						<div>
							562111-Solid Waste Collection 
						</div>
					</div>
					
					 <div id="special-item">
                        <h1>SPECIAL ITEM NUMBERS OFFERED</h1>
						<div>
							SIN 618-02-Trash Collector 
						</div>
					</div>
					
                    <div id="gsa-schedule">
                        <p>GSA Schedule 23V-Trash Collector (Applied For)</p>
                    </div>
	                 <div id="onsite-facilities">
                        <h1>On-Site Facilities  </h1>
                        <p>
							Motorpool /Warehouses-2,000 sq. ft. Pearl City, Hawaii <br />
							(Philippines) 2,000 sq.ft. Pearl City, Hawaii <br />
							Line of Garbage Trucks  (Rear-Loaders) & Flatbeds<br />
						</p>
                       
                  </div>
                </div>
                <!-- /right column -->
<?
	include(THEME_FOLDER.'footer.inc.php');
?>
