<?

$css = 'faqs.css';

include('header.inc.php');



?>

                <!-- left column -->

                <div id="leftcol">

                	<div id="faqs">

                    	<h1>Frequently Asked Question&rsquo;s</h1>

                        <ol>

                          <li>Schedules?</li>

                            <p>We ship out every Thursday.</p> 

                            <p>For box request delivery and pick up schedules, please call 866-694-7793 (toll-free) or (808) 699-4329</p>

                            

                            <li>Do I have to visit your office for booking reservation?</li>

                            <p>You may book by visiting our office, via emails, fax or through Customer Interaction Center 866-694-7793 by just giving us the following details:</p>

                            <ul>

                                <li>shipper name</li>

                                <li>complete address</li>

                                <li>contact number - phone nos. (home, office, mobile, pager)</li>

                                <li>contact person - for pick up</li>

                                <li>consignee</li>

                                <li>service arrangement - door/door, door/pier, pier/pier, pier/door</li>

                                <li>quantity of booking &ndash; boxes or no. of containers required 20, 40, 45 - footers</li>

                                <li>commodity classification - (please see commodity classification under services and rates)</li> 

                                <li>special instructions</li>

                            </ul>

                            

                            <li>What are your prevailing rates?</li>

                            <p>Please see our <a href="rates.php">RATES</a>.</p>

                            

                            <li>What ports do you serve?</li>

                            <p>We accept boxes and cargo from the islands of Oahu, Maui, Lanai, Kona-Big island and Kauai. We ship out from the port of Honolulu and deliver to any destination point in the philippines</p>

                            

                            <li>What are the container specifications?</li>

                            <p>Containers come in three sizes:</p>

                                

                                <ul>

                                	<li>20" footer</li>

                                    <li>40" footer</li>

                                    <li>45" footer</li>

                                </ul>

                            

                            <li>Who should I contact to do business with you?</li>

                            <p>You may call us at 866-694-7793 (toll-free) or (808) 699-4329, send a fax at 800-395-1658, or email us at millenniumexpress@gmail.com.  You may also visit our warehouse office at:</p>

                            

                            <p>94-208 Pupuole St. #102, Waipahu, HI 96797</p>

                            

                            <li>Is there a cut-off time for booking or can I book anytime?</li>

                            <p>We load every Thursday, so we advise customers to have their boxes/cargo delivered or picked up prior to our loading schedule.</p>

                            

                            <li>What are the available payment modes?</li>

                          	<p>Payment could be:</p>

                            <ul>

                                <li>pre-paid</li>

                                <li>on-account</li>

                                <li>collect (upon verification of approval from consignee)</li>

                                <li>thru VISA and Mastercard</li>

                            </ul>

                            

                            <li>What is your lead-time?</li>

                            <p>The lead time depends on the distance between port of origin and port of destination.  Shipments to Luzon may take 25 to 30 days, while Visayas and Mindanao shipments take 30 to 40 days.</p>

                            

                            <li>How will I know if my shipment was loaded?</li>

                            <p>You may check status of your shipment by entering your packing list number into our online tracking system on our website homepage. You will know whether your shipment has been loaded if there is a check mark on the field labelled "Shipment Loaded to Container".</p>

                            

                            <li>How will I know if my shipment has been delivered?</li>

                            <p>You may check status of your shipment by entering your packing list number into our online tracking system on our website homepage. You will know whether your shipment has been delivered if there is a check mark on the field labelled "Shipment Received by Consignee".</p>

                            

                            <li>Can we have a copy of our proof of payment?</li>

                            <p>Yes.  However, for collect payment, documents are sent to destination or consignee upon payment.</p>

                            <p class="note">All services are affected by weather and the ability of the carrier to perform their duty</p>

                        </ol>

                       

                  </div>

                </div>

                <!-- /left column -->

                

                <!-- right column -->

                <div id="rightcol">

                	<!--

                    <div id="sub">

                    	<h3>sub menu</h3>

                    </div>

                    -->

                </div>

                <!-- /right column -->

<?

	include('footer.inc.php');

?>