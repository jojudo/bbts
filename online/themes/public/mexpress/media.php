<?
$css = 'media.css';
include('header.inc.php');
?>
                <!-- left column -->
                <div id="leftcol">
                	<div id="media">
						<h1>Our Affordable Rates</h1>
						<div id="mediaplayer">Loading Player... </div>
							<script type="text/javascript" src="<?php echo THEME_ADDR;?>js/jwplayer.js"></script>
							<script type="text/javascript">
								jwplayer("mediaplayer").setup({
								flashplayer: "<?php echo THEME_ADDR;?>js/player.swf",
								playlist: [
								{ duration: 147, file: "<?php echo THEME_ADDR;?>media/kitv-philbox.flv", image: "<?php echo THEME_ADDR;?>media/kitv-philbox.png", title:"Millennium Express Delivers PB Boxes" },
								{ duration: 540, file: "<?php echo THEME_ADDR;?>media/abc-breakthrough.flv", image: "<?php echo THEME_ADDR;?>media/abc-protandim.jpg",title:"Protandim in ABC" },
								{ duration: 710, file: "<?php echo THEME_ADDR;?>media/breakthrough.flv", image: "<?php echo THEME_ADDR;?>media/breakthrough.jpg", title:"A Breakthrough" },
								
								],
								"playlist.position": "bottom",
								"playlist.size": 100,
								height: 400,
								width: 600
								});
							</script>
						</div>
					</div>
                <!-- /left column -->
                
                <!-- right column -->
                <div id="rightcol">
                    <div id="download">
                    	<h1>Download</h1>
                        <ul>
                        	<li><span class="yellow"><a href="<?php echo THEME_ADDR;?>media/abc-breakthrough.flv">Protandim in ABC News  </a></span></li>
                            <li><span class="yellow"><a href="<?php echo THEME_ADDR;?>media/breakthrough.flv">Protandim: A Breakthrough </a></span></li>
                        </ul>
                    </div>
                </div>
                <!-- /right column -->
<?
	include('footer.inc.php');
?>