<?php
$process->public_tracking();
$css = 'track.css';

UI::set_js(FULL_ADDR.'/libraries/jquery/jquery-1.5.2.js');
UI::set_js(THEME_ADDR.'js/easySlider1.7.js');
UI::set_js(THEME_ADDR.'js/slider.js');

include('header.inc.php');


?>
    <!-- left column -->
    <div id="onecol">
    	<h1>Tracking Your Packages</h1>
    	<div id="photo">   
    		<?php echo $photoline;?>
    	</div>
    	<div id="status">
     		 <h4><span class="yellow">Tracking Number:</span> <?php echo $boxid;?> <span style="font-size:10px;">Not your package? <a href="http://millenniumexpress.biz/trackall/<?php echo $boxid;?>.html" class="yellow">click here</a></span></h4> 
     		 <?php echo $statusline;?>
     	</div>
    </div>
    
    <div id="disclaimer">
    	<p>*Above dates are determined from information provided by ocean vessel carrier
    	and may change without prior notice</p>
 	</div>
    <!-- /left column -->
    
    <!-- right column -->
    <div id="rightcol">
    	<!--
        <div id="sub">
        	<h3>sub menu</h3>
        </div>
        -->
    </div>
    <!-- /right column -->
<?
	include('footer.inc.php');
?>