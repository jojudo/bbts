$(document).ready(function(){ 
	if($("#photo li").length>1){
		$("#photo").easySlider({
			auto: true,
			prevId:'prevBtn',
			prevText: 'Previous',
			nextId: 'nextBtn',
			nextText: 'Next',
			continuous:	true,
			controlsShow: true,
			speed: 		800,
			pause:		5000,
			numeric: true
		});
	}
		
});