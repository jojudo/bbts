<?php

/**
 * Millennium Express INC.
 *
 * Proprietary TRacking Sysytem
 *
 * @package		MEX
 * @author		Joey Toga
 * @copyright	Copyright (c) 2009 - 2015, Millennium Express INC..
 * @license		---
 * @link		http://www.millenniumexpress.biz
 * @since		Version 3.0
 * @filesource
 */

// ------------------------------------------------------------------------

	session_start();
	define('INSTANCE',1);
	//define environment
	define('ENVIRONMENT', 'development');	
        
    //define the base of this file
    define('BASE',dirname(__FILE__));
    
	//let us define the folders
	$system_path = "system";
	$app_path = "public";

    //save individual global values	
	$server_name = rtrim($_SERVER['SERVER_NAME']);
	$addr_arr = explode('/',$_SERVER['SCRIPT_NAME']);
	array_pop($addr_arr);
	
	$current_folder = implode('/',$addr_arr);	
	$current_folder = LTRIM($current_folder,DIRECTORY_SEPARATOR);
    
	require_once $system_path."\core.php";	

