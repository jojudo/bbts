<?php 
/**
 * Millennium Express INC.
 *
 * Proprietary TRacking Sysytem
 *
 * @package		MEX
 * @author		Joey Toga
 * @copyright	Copyright (c) 2009 - 2015, Millennium Express INC..
 * @license		---
 * @link		http://www.millenniumexpress.biz
 * @since		Version 3.0
 * @filesource
 * @description This file loads all the common libraries for use anywhere in the system
 */

// ------------------------------------------------------------------------

defined('INSTANCE') or die("No Direct access allowed!");

//setup static refences
define('DS',DIRECTORY_SEPARATOR);
define('US','/');

//let us define external libraries path
define('LIBPATH', BASE.DS.'libraries'.DS);

//let us define the internal classes path
define('CLASSPATH', BASE.DS.'classes'.DS);

//let us define the modules path
define('MODPATH', BASE.DS.'modules'.DS);

define('SYSPATH', str_replace("\\", DS, realpath($system_path)));
define('APPATH', str_replace("\\", DS, realpath($app_path)));
    

//include class loader    
include_once SYSPATH.DS.'common'.DS.'class_loader.php';

//load custom error 
register_shutdown_function( "check_for_fatal" );
set_error_handler("myErrorHandler");
ini_set( "display_errors", "off" );
error_reporting( E_ALL );

//password encryption functions
if (!function_exists('password_hash')) {
    include SYSPATH.DS.'common'.DS.'password_functions.php';
}


