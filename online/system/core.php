<?php 
/**
 * Millennium Express INC.
 *
 * Proprietary TRacking Sysytem
 *
 * @package		MEX
 * @author		Joey Toga
 * @copyright	Copyright (c) 2009 - 2015, Millennium Express INC..
 * @license		---
 * @link		http://www.millenniumexpress.biz
 * @since		Version 3.0
 * @filesource 
 * @description	core function
 */

// ------------------------------------------------------------------------
	if ( ! defined('INSTANCE')) exit('No direct script access allowed');
	require_once $system_path.'\chipset.php';
	
	
	//load configuration file
    if (file_exists(BASE.DS."configuration.php")) {
        include BASE.DS."configuration.php";
    } else {
        if (file_exists(BASE.DS."installation".DS."index.php")) {
            header('location:'.BASE.DS.'installation'.DS.'index.php');
            exit();
        } else {
            exit("Configuration not Installed");
        }
     }
	 
	//set up error environment
	if(defined($config['environment'])){
		switch ($config['environment']){
			case 'development':
				error_reporting(1);
			break;
			case 'testing':
			case 'production':
				error_reporting(0);
			break;
			default:
				exit('The application environment is not set or correctly assigned.');
		}
	}
	
	//we are gonna start inserting config to our datastore
	$datastore = load_class('datastore');	
	$datastore->set_config($config);
	
	
    //add to config global URL variables
	$datastore->add_config('server_name',$server_name);
	$datastore->add_config('current_folder',$current_folder);
	$datastore->add_config('full_url',"http://".$server_name.$current_folder);
    $datastore->add_config('this_file', pathinfo(__FILE__, PATHINFO_BASENAME));
	  
	
    //require the APP resources
	include_once APPATH.DS.'functions.php';
	include_once APPATH.DS.'loader.php';
    
    //proceed to process output
	include_once APPATH.DS.'processor.php';