<?php

if ( ! function_exists('load_class'))
	{
		function &load_class($class)
		{
			static $_classes = array();
			$prefix = 'class';
			
			// Does the class exist?  If so, we're done...
			if (isset($_classes[$class]))
			{
				return $_classes[$class];
			}

			$name = FALSE;

			
			if (file_exists(CLASSPATH.DS.$prefix.'.'.$class.'.php'))
			{
				$name = $class;

				if (class_exists($name) === FALSE)
				{
					require(CLASSPATH.DS.$prefix.'.'.$class.'.php');
				}
			}else{
					trigger_error("Class $class does not exist",E_USER_ERROR);
			}
		

			// Did we find the class?
			if ($name === FALSE)
			{
				// Note: We use exit() rather then show_error() in order to avoid a
				// self-referencing loop with the Excptions class
				exit('Unable to locate the specified class: '.$class.'.php');
			}

			// Keep track of what we just loaded
			is_loaded($class);

			$_classes[$class] = new $name();
			return $_classes[$class];
		}
	}
    
 //loading of library    
	if ( ! function_exists('load_lib'))
	{	
		function &load_lib($folder,$filename='',$classname='',$instantiate=FALSE){
			//folder ex: 'folder/subfolder' 
			$filename = $filename?$filename:$folder;
			$classname = $classname?$classname:$filename;
			
			static $_classes = array();
			
			// Does the library exist?  If so, we're done...
			//we separate the library classes from the core classes
			if (isset($_classes[$classname]))
			{
				return $_classes[$classname];
			}

			$name = FALSE;
			$folder = LIBPATH.DS.$folder;
			
			if (file_exists($folder.DS.$filename.'.php'))
			{
				$name = $classname;

				if (class_exists($name) === FALSE)
				{
					require($folder.DS.$classname.'.php');
				}
			}
		

			// Did we find the class?
			if ($name === FALSE)
			{
				// Note: We use exit() rather then show_error() in order to avoid a
				// self-referencing loop with the Excptions class
				exit('Unable to locate the library class '.$class.' in '.$folder);
			}

			// Keep track of what we just loaded
			if($instantiate===TRUE){
				is_loaded($classname);

				$_classes[$classname] = new $name();
				return $_classes[$classname];
			}else{
				return $instantiate;
			}
			
		}
	}
	
	if ( ! function_exists('load_interface'))
	{
		function load_interface($classname){
			if (file_exists(CLASSPATH.'interfaces'.DS.'interface.'.$classname.'.php'))
			{
				if (!class_exists($classname)){
					include CLASSPATH.'interfaces'.DS.'interface.'.$classname.'.php';
				}
			}else{
				//warning here
				echo CLASSPATH.'interfaces'.DS.'interface.'.$classname.'.php'.'file does not exist';
			}
		}
	}
	
	if ( ! function_exists('is_loaded'))
	{
		function &is_loaded($class = '')
		{
			static $_is_loaded = array();

			if ($class != '')
			{
				$_is_loaded[strtolower($class)] = $class;
			}

			return $_is_loaded;
		}
	}
	if ( ! function_exists('myErrorHandler'))
	{
		function myErrorHandler($errno, $errstr, $errfile, $errline)
		{
			$error_message = '';
			if (!(error_reporting() & $errno)) {
				// This error code is not included in error_reporting
				return;
			}

			switch ($errno) {
			case E_USER_ERROR:
				$error_message =  "<b>ERROR</b> [$errno] $errstr<br />\n";
				$error_message .=  "  Fatal error on line $errline in file $errfile";
				$error_message .=  ", PHP " . PHP_VERSION . " (" . PHP_OS . ")<br />\n";
				$error_message .=  "Aborting...<br />\n";
				echo $error_message;
				exit(1);
				break;

			case E_USER_WARNING || 2:
				$error_message .=  "<b>WARNING</b> [$errno] $errstr ; file: $errfile  <br />\n";
				break;

			case E_USER_NOTICE:
				$error_message .=  "<b>NOTICE</b> [$errno] $errstr <br />\n";
				break;

			default:
				$error_message .=  "Unknown error type: [$errno] $errstr <br /> on line $errline in file $errfile \n";
				break;
			}
			
			
			
			$datastore = load_class("datastore");
			$datastore->add_config('system_message',array($error_message));
			
			/* Don't execute PHP internal error handler */
			return true;
		}
	}
	
	function check_for_fatal()
	{
		$error = error_get_last();
		if ( $error["type"] == E_ERROR ){
			myErrorHandler(E_USER_ERROR, $error["message"], $error["file"],0);
		}
		
		return true;
	}
