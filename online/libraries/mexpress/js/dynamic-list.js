/* ========================================================================
 * Autocomplete: dynamicList.v1.js
 * --- 
 * ========================================================================
 */
    
    
(function($){
     
     var dynamicListCounter = 0;
     var dynamicListClass = 'dropList';
     var dynamicListItemClass = 'dynamicListItem';
     
     var DynamicList  = function(element,options)
     {
         var elem = $(element);
         var obj = this;
         var objID = "";
         var clickEvent = {};
         var settings = $.extend({
               param: 'defaultValue'
         }, options || {});
         var count=0;   
         var dynamicListInterval;
         var dataSet = {};
         
         //public method
         this.publicMethod = function()
         {
           console.log(elem.val())  
         };
         
         //private method
         var privateMethod = function()
         {
             console.log("action for "+objID);
         };
         
         
         var createResultsDiv = function()
         {
             if(!$("#"+objID).length){
                elem.after('<div id="'+objID+'" class="'+dynamicListClass+'">Loading Results...</div>')
            }
         } 
        
         var initDynamicList = function ()
         {
             if(clickEvent.which === 40)
             {
                //;nameListNextHighlight();
                console.log("do not start search");
             }
             else
             {
                if(elem.val().length>=1)
                {
                    //window.matchInterval = setInterval(function(){window[callback]();clearInterval(window.matchInterval);;},(interval))
                    createResultsDiv();
                    clearTimeout(dynamicListInterval);
                    dynamicListInterval = setTimeout(function(){ajaxDataSearch(0);},500);
                }
             }
         }
          
         var ajaxDataSearch = function(page){
            console.log("ajax request started  ");
            var param="";
            for	(index = 0; index < options.fieldIDs.length; index++) {
                param += options.fieldIDs[index]+'='+$("#"+options.fieldIDs[index]).val();
                param +='&';
            }
            param+=options.param;
            param+='&page='+page;
            framework.ajaxGeneric({param:param,callbackObj:obj,loader:'',submitType:'get',dataType:'json'});
                        
         }
         
         this.nextPage = function(page){
             ajaxDataSearch(resultSet.data.next);
         } 
         
         this.prevPage = function(page){
             ajaxDataSearch(resultSet.data.prev);
         }
         
         this.callback = function(data){
             resultSet = data
             this.populateResults();
         }
         
         this.populateResults = function(){
            
            var contentStr = ''; 
            
            if(resultSet.data.prev>=1) contentStr += '<span id="move-left"><a href="#" class="result-prev"><< Previous</a> </span>';
            if(resultSet.data.next>=1) contentStr += '<span id="move-right"><a href="#" class="result-next">Next >></a></span>';

            contentStr += '<ul class="matchUl">';
            $.each(resultSet.data.names,function(index,value){
                contentStr+='<li><a href="#" listId="'+value.customer_id+'" class="'+dynamicListItemClass+'">'+value.lname+', '+value.fname+'</a></li>';
            });
            contentStr+='</ul>'

            $("#"+objID).html(contentStr);
            
            $("#"+objID+" .result-next").click(function(){
                obj.nextPage();
            }) 
            
            $("#"+objID+" .result-prev").click(function(){
                obj.prevPage();
            })
            
            $("#"+objID+" ."+dynamicListItemClass).click(function(){
                window[options.selectCallback]($(this).attr("listId"));
                $("#"+objID).hide("fade",500);
            })
            
            $("#"+objID).show("fade");           
         }
         
         elem.on('keyup', function (e) {
           clickEvent = e;
           initDynamicList();            
         });
         
         //default action
         dynamicListCounter++;
         objID = "dynamicList"+dynamicListCounter;
        
     };
     
     $.fn.dynamicList = function(options) {
         return this.each(function()
         {
            var element = $(this);
            
            // Return early if this element already has a plugin instance
            if (element.data('DynamicList')) return;
            
            // pass options to plugin constructor
            var dynamicList = new DynamicList(this, options);
            
            //Store plugin object in this element's data
            element.data('DynamicList', DynamicList);


         });   

      };
})(jQuery);

