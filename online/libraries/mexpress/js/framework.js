/**
 * Created by IT on 16/07/2015.q
 */

function framework(defaults){
    this.defaults =defaults;
    this.defaults.dialogClass = 'modals';
    this.defaults.development = true
}



//ajax Calls
framework.ajaxGeneric = function(options){

    //param,callback,loader,submitType,headertype,datatype
    if(options.loader)
        window[options.loader]();
   
    $.ajax({
        type: options.submitType,
        url: fullPath+"index.php",
        async: true,
        data: options.param+"&mod="+mod,
        dataType: options.dataType,
        success: function(data)
        {
            if(data.failure==1)
            {
                if(data.session==0)
                    this.quick_login_form(data);
                else
                    alert(data.msg);
            }
            else
            {           
                if($.type(options)=='object')
                {
                    if($.type(options.callbackObj)!='object')
                        window[options.callback]({'data':data});
                    else 
                       options.callbackObj.callback({'data':data});
                }
               
            }
        },
        error:function(html,url,error){framework.closeautoloadinginfo();alert("Problem Loading page, please try again");}
    })
}

framework.closeautoloadinginfo = function(){
    $("#windowloader").dialog('close');
}

framework.createDialog = function(params){
    //id,content,title,height,width,obj
	if(!$("#"+params.id).length){
		params.obj.after("<div id='"+params.id+"' class='modals'></div>");
		$("#"+params.id).dialog({
			bgiframe: true,
			height: params.height,
			width: params.width,
			modal: true,
			title:params.title,
			show: {
				effect: "fade",
				duration: 300
			 },
			 hide: {
				effect: "fade",
				duration: 300
			 },
			 position:"middle"
		});
		$("#"+params.id).html(params.content);
		
		return 1;
	}else{
		$("#"+params.id).dialog('open');
		$("#"+params.ids).dialog('option','position','middle');
			
		return 0;
	}
}

//----- javascript templates Start -----
framework.loadtemplate = function(params){
    //params {template=text,callback = text,passover  = obj, callbackobj=obj}
	var ext = '.tpl?'+Math.random(99999);
	$.get(modPath+'js/templates/'+params.template+ext, function (template){		
		params.passover.template = template;
        if($.type(params.callbackObj)!='object'){
            window[params.callback](params.passover);
        }
        else{
            params.callbackObj.callback(params.passover);
        }
	})
}

framework.templateFillWithData = function(template,data){
	$.each(data,function(index,value){
		if($.type(value)=='object'){
			template = templateData(template,value)
		}else{
			value = value!=null?value:'';
			while(template.indexOf('{'+index+'}')!=-1){
				template = template.replace('{'+index+'}',value);
			}
		}
	})	
	
	return template;
}

framework.loopStr = function(template,data){
	if(data.length>0){		
		template = template.replace('{{DATASTART}}','');
		template = template.replace('{{DATAEND}}','');
		var start = template.indexOf('{{LOOPSTART}}')*1+13;
		var end = template.indexOf('{{LOOPEND}}')*1;
		var len = template.length;
		//GET THE line to loop
		var loopStr = template.substring(start,end);
		
		if(start==12){
			loopStr = template;
		}

		var liner = loopStr;
		var ctr=0;
		var returnStr= '';
		$.each(data,function(index,value){
			ctr++;
			if(typeof value=='object'){
				liner = loopStr;
				$.each(value,function(index2,value2){
					value2 = value2!=null?value2:'';
					while(liner.indexOf('{'+index2+'}')!=-1){
						liner = liner.replace('{'+index2+'}',value2);
					}
				})
				
				liner = liner.replace('{num}',ctr);
				returnStr+=liner;
			}else{
				liner = liner.replace('{'+index+'}',value);
				alert(index);
			}
				
		})
	
		if(start==12){
			return liner;
		}else{
			return template.substring(0,(start-13)) + returnStr + template.substring(end+11,len);
		}
	}else{
		var start = template.indexOf('{{DATASTART}}')*1+13;
		var end = template.indexOf('{{DATAEND}}')*1;
		return template.substring(0,(start-13)) + template.substring(end+11,len);	
	}
}

//----- javascript templates End -----
