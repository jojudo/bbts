/**
 * Created by IT on 16/07/2015.q
 */

function framework(defaults){
    this.defaults= defaults;
    this.defaults.dialogClass = 'modals';
}

//creation of notes
framework.handleNotes = function(msg){
    if(this.options.environment=="development")
        console.log(msg);
}

//Floating dialogs
framework.createInfo = function(options) {
    //divID,content,title,ht,wd/
    if (!$("#"+options.divID).length){
        $("#body").append("<div id='" + options.divID + "' class='" + this.defaults.dialogClass +"'></div>");
        $("#" + options.divID).dialog({
            bgiframe: true,
            modal: true,
            position: "middle",
            height: options.ht,
            width: options.wd,
            title: options.title
        });
        $("#" + options.divID).html(options.content);

        return 1;
    }else{
        $("#" + divID).dialog('open');
        $("#" + divID).dialog('option', 'position', 'middle');

        return 0;
    }
}

framework.autoloadinginfo = function(textMessage){
    if(textMessage==null)
        textMessage = "Loading...";

    this.loadingInfo('windowloader','<div class="loader">'+textMessage+'<div>','',100,450);
}

framework.closeautoloadinginfo = function(){
    $("#windowloader").dialog('close');
}

framework.loadingInfo = function(options){
    //divID,content,mtitle,ht,wd
    createInfo(options.divID,options.content,options.mtitle,options.ht,options.wd);
    $("#"+divID).parent().children(".ui-dialog-titlebar").css("display","none");
    $("#"+divID).dialog({ closeOnEscape: false });
}


framework.closeInfo = function(divID){
    $("#"+divID).dialog('close');
}

framework.createAlertAutoClose = function (options){
    createInfo("windowAutoClose",options.content,'',100,500)
    $("#windowAutoClose").parent().children(".ui-dialog-titlebar").hide();
    //time it for closeing
    window.intervalue = setInterval(function(){$("#windowAutoClose").dialog("close");clearInterval(intervalue)},options.interval);
}

framework.errorAlert = function(msg){
    this.createInfo("windowError",content,'',150,500)
    $("#windowError").parent().children(".ui-dialog-titlebar").hide();
    $("#windowError").html("<p>"+msg+"</p>");
    $("#windowError").append('<a href="#" class="button" onclick="this.dialog_closer(\'windowError\');return false;">OK</a>');
}

framework.dialogCloseButton = function (options){
    var str = options.msg+'<br />';
    str+='<input type="button" value="CLOSE" onclick="dialog_closer(\''+foptions.ID+'\');">';
    $("#"+ID).html('<p class="loader">'+str+'</p>');
}

framework.dialogYesNo(options){
    //msg,yescallback,nocallback
    if(options.nocallback==undefined)
        options.nocallback = "_silentfunction";

    createInfo("windowYesNo",content,'',150,500)
    $("#windowYesNo").parent().children(".ui-dialog-titlebar").hide();
    $("#windowYesNo").html("<p>"+msg+"</p>");
    $("#windowYesNo").append('<a href="#" class="button" id="btnYes" onclick="'+options.yescallback+'();this.dialog_closer(\'windowYesNo\');return false;">YES</a>');
    $("#windowYesNo").append('<a href="#" class="button" onclick="dialog_closer(\'windowYesNo\');'+nocallback+'();return false;">NO</a>');
    //time it for closeing

    $("#btnYes").focus();
}

//paging

framework.classNamePaging = function(options){
    //className,page,pages
    var pageStr ;
    if(options.pages>=1){
        pageStr = '<ul>';
        for(var i=1;i<=pages;i++){
            pageStr+='<li><a href="#" class="'+options.className;
            if(i==options.page){
                pageStr+=' active';
            }

            pageStr+='" >'+i+'</a></li>';
        }
        pageStr += '</ul>';
    }

    return pageStr;
}

framework.setArrowKeyPaging = function (callback){
    $(window).keydown(function(e){
        if(e.which==39){
            if(next){
                window[callback](next)
                e.preventDefault();
            }
        }
        else if(e.which==37){
            if(prev){
                window[callback](prev)
                e.preventDefault();
            }
        }
        else ;
    })
}


//ajax Calls
framework.ajaxGeneric = function(options){

    //param,callback,loader,submitType,headertype
    if(options.loader)
        window[options.loader]();
    else
        this.autoloadinginfo("Processing request, please wait...");

    $.ajax({
        type: options.submitType,
        url: fullPath+"index.php"
        async: true,
        data: "headerType="+options.headerType+"&"+options.param,
        success: function(data){
            if(data.failure==1){
                if(data.session==0)
                    this.quick_login_form(data);
                else
                    alert(data.msg);
            }else{
                if(options.callback!='')
                    window[options.callback](data);
                else
                    this.closeautoloadinginfo();
            }
        },
        error:function(html,url,error){this.closeautoloadinginfo();alert("Problem Loading page, please try again");}
    })
}


//checking
$framework.checkForm = function(form_arr){
    var cont=true;
    for (var counter=0; counter<form_arr.length; counter++)
    {
        var objText=form_arr[counter];
        var obj = $("#"+objText);

        if(obj.val()=='' || obj.val()==0)
        {
            obj.addClass("input-warning");
            obj.focus();
            cont=false;
        }
        else
        {
            obj.removeClass("input-warning");
        }

    }

    if(!cont)
        alert("Some required fields are missing!");

    return cont;
}


//session failure
framework.sessionFailure = function(){
    $(".modals").dialog("close");
    var html = '<div class="loader"><img src="images/loader.gif"> Loading Content...</div>';
    createInfo('expiryForm',html,'Session Expired',200,400);

    $.ajax({
        type: "POST",
        url: "api/user-api.php",
        async: true,
        data: "action=expired",
        success: function(data){
            $("#expiryForm").html(data);
            $("#loginForm").submit(function(){expireLogin();return false;});
        },
        error:function(a,b,c){alert('problem loading');}
    })
}