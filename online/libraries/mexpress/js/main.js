
function _handleNotes(msg){
	if(environment=="development")
		console.log(msg);
}

function insertToBody(divID,content){
	if(!$("#"+divID).length){
		$("#container").append("<div id='"+divID+"' class='printFrame'>"+content+"</div>");
	}else{
		$("#"+divID).html(content);
	}
}

function createInfo(divID,content,mtitle,ht,wd){
	if(!$("#"+divID).length){
		$("#container").append("<div id='"+divID+"' class='modals'></div>");
		$("#"+divID).dialog({
			bgiframe: true,
			height: ht,
			width: wd,
			modal: true,
			title:mtitle,
			show: {
				effect: "fade",
				duration: 200
			 },
			 hide: {
				effect: "fade",
				duration: 300
			 },
			 position:"middle"
		});
		$("#"+divID).html(content);
		
		return 1;
	}else{
		$("#"+divID).dialog('open');
		$("#"+divID).dialog('option','position','middle');
			
		return 0;
	}
}

function closeInfo(divID){
	$("#"+divID).dialog('close');
}


//load javascript template
function loadJsTemplate(elementId, url, callback, data, overwrite)
{
    $("#" + elementId).load(url + '?p=' + Math.floor(Math.random() * 11), function (response, status, xhr) {
        if (status == "error") {
            var msg = "Template not found ";
            $("#" + elementId).html(msg + xhr.status + " " + xhr.statusText);
        } else {
            if (callback != '' && callback != 'none')
                window[callback](data);
        }
    });
}

//template load
function __jsTemplate(template,callback,loader,closer){
	var ext = '.js.tpl';
	loader = loader?loader:'_silentfunction';
	closer = closer?closer:'_silentfunction';
	window[loader]()
	$.get(modPath+'js/templates/'+template+ext, function (template){
		window[closer]()
		window[callback](template);
	})
}

function classNamePaging(className,page,pages){
	var pageStr ;
	if(pages>=1){
		pageStr = '<ul>';
		for(var i=1;i<=pages;i++){
			pageStr+='<li><a href="#" class="'+className;
			if(i==page){
				pageStr+=' active';
			}

			pageStr+='" >'+i+'</a></li>';
		}
		pageStr += '</ul>';
	}

	return pageStr;
}

function setArrowKeyPaging(callback){
	$(window).keydown(function(e){
		if(e.which==39){
			if(next){
				window[callback](next)
				e.preventDefault();
			}
		}
		else if(e.which==37){
			if(prev){
				window[callback](prev)
				e.preventDefault();
			}
		}
		else ;
	})
}
//generic Ajax call
function ajaxGeneric(param,callback,loader){
	if(loader)
		window[loader]();
	else
		autoloadinginfo("Processing request, please wait...");

	$.ajax({
	  type: submitType,
	  url: fullPath+"index.php?mod="+mod,
	  async: true,
	  data: 'headerType=none&'+param,
	  success: function(data){
		if(data.failure==1){
			if(data.session==0)
				quick_login_form(data);
			else
				alert(data.msg);
		}else{
			if(callback!='' && callback!='none')
				window[callback](data);
				//alert(callback);
			else
				closeautoloadinginfo();
		}
	   },
	   error:function(html,url,error){closeautoloadinginfo();alert("Problem Loading page, please try again");}
	})
}

function ajaxCallJson(param,submitType,callback,loader){
	if(loader)
		window[loader]();
	else
		autoloadinginfo("Processing request, please wait...");

	$.ajax({
	  type: submitType,
	  url: fullPath+"index.php?mod="+mod,
	  async: true,
	  data: 'headerType=json&'+param,
	  dataType:'JSON',
	  success: function(data){
		closeautoloadinginfo();
		if(data.failure==1){
			if(data.session==0)
				quick_login_form(data);
			else
				alert(data.msg);
		}else{
			if(callback!='' && callback!='none')
				window[callback](data);
				//alert(callback);
			else
				;//no action
		}
	   },
	   error:function(html,url,error){closeautoloadinginfo();alert("Problem Loading page, please try again");}
	})
}

//function automated login form
function quick_login_form(jsonObj){
	//load javascript template
	var template = '<p id="errormsg">'+jsonObj.msg+'</p>';
	template += '<form method="post" id="quick_login_form">';
	template += '<label for="quick_username">Username</label><input type="text" id="quick_username" name="quick_username" class="quick_field"/>';
	template += '<label for="quick_password">Username</label><input type="password" id="quick_password" name="quick_password" class="quick_field"/>';
	template += '<p><input type="submit" value="Quick Login" class="button" /></p>';
	template += '</form>';

	createInfo("quicklogin",template,'Please Login',250,470);
	$("#quicklogin").parent().children(".ui-dialog-titlebar").hide();

	$("#quick_login_form").submit(function(){quick_login();return false;});
}

function quick_login(){
	var username = $("#quick_username").val();
	var password = $("#quick_password").val();

	ajaxCallJson('username='+username+'&password='+password,'POST','quick_login_callback');
}

function quick_login_callback(obj){
	if(obj.session==1){
		$("#quicklogin").dialog('close');
	}else{
		$("#errormsg").html(obj.msg);
	}
}

function autoloadinginfo(textMessage){
	if(textMessage==null)
		textMessage = "Loading...";

	loadingInfo('windowloader','<div class="loader">'+textMessage+'<div>','',100,450);
}

function closeautoloadinginfo(){
	$("#windowloader").dialog('close');
}

function loadingInfo(divID,content,mtitle,ht,wd){
	createInfo(divID,content,mtitle,ht,wd);
	$("#"+divID).parent().children(".ui-dialog-titlebar").css("display","none");
	$("#"+divID).dialog({ closeOnEscape: false });
}

function createAlertAutoClose(content,interval){
	createInfo("windowAutoClose",content,'',100,500)
	$("#windowAutoClose").parent().children(".ui-dialog-titlebar").hide();
	//time it for closeing
	window.intervalue = setInterval(function(){$("#windowAutoClose").dialog("close");clearInterval(intervalue)},interval);
}

function errorAlert(msg){
	createInfo("windowError",content,'',150,500)
	$("#windowError").parent().children(".ui-dialog-titlebar").hide();
	$("#windowError").html("<p>"+msg+"</p>");
	$("#windowError").append('<a href="#" class="button" onclick="dialog_closer(\'windowError\');return false;">OK</a>');
}

function dialog_close_button(ID,msg){
	var str = msg+'<br />';
	str+='<input type="button" value="CLOSE" onclick="dialog_closer(\''+ID+'\');">';
	$("#"+ID).html('<p class="loader">'+str+'</p>');
}

function dialogYesNo(msg,yescallback,nocallback){
	if(nocallback==undefined)
		nocallback = "_silentfunction";

	createInfo("windowYesNo",content,'',150,500)
	$("#windowYesNo").parent().children(".ui-dialog-titlebar").hide();
	$("#windowYesNo").html("<p>"+msg+"</p>");
	$("#windowYesNo").append('<a href="#" class="button" id="btnYes" onclick="'+yescallback+'();dialog_closer(\'windowYesNo\');return false;">YES</a>');
	$("#windowYesNo").append('<a href="#" class="button" onclick="dialog_closer(\'windowYesNo\');'+nocallback+'();return false;">NO</a>');
	//time it for closeing

	$("#btnYes").focus();
}

function addHighlightClass(obj){
	obj.addClass("highlight");
}

function removeHighlightClass(obj){
	obj.removeClass("highlight");
}

function _silentfunction() {};

function dialog_closer(ID){
	$("#"+ID).dialog('close');
}

//form checking ... enter arrays of ids to check
function checkForm(form_arr){
	var cont=true;
	for (var counter=0; counter<form_arr.length; counter++)
	{
		var objText=form_arr[counter];
		var obj = $("#"+objText);

		if(obj.val()=='' || obj.val()==0)
		{
			obj.addClass("input-warning");
			obj.focus();
			cont=false;
		}
		else
		{
			obj.removeClass("input-warning");
		}

	}

   	if(!cont)
   		alert("Some required fields are missing!");

	return cont;
}

//session failure
function sess_failure(){
	$(".modals").dialog("close");
	var html = '<div class="loader"><img src="images/loader.gif"> Loading Content...</div>';
	createInfo('expiryForm',html,'Session Expired',200,400);

	$.ajax({
	  type: "POST",
	  url: "api/user-api.php",
	  async: true,
	  data: "action=expired",
	   success: function(data){
			$("#expiryForm").html(data);
			$("#loginForm").submit(function(){expireLogin();return false;});
	   },
	   error:function(a,b,c){alert('problem loading');}
	 })
}

function expireLogin(){
	var dataSet = $("#loginForm").serialize();
	$("#loginForm :input").attr("disabled","disabled");
	$.ajax({
	  type: "POST",
	  url: "api/user-api.php",
	  async: true,
	  data: "action=login&"+dataSet,
	  success: function(data){
			if(data==0){
				alert('Username and Password combination not found');
				$("#loginForm :input [type==text]").val('');
				$("#loginForm :input").attr("disabled","");
			}else if(data==1){
				$("#expiryForm").dialog('close');
			}else{
				$("#loginForm :input  [type==text]").val('');
				$("#loginForm :input").attr("disabled","");
			}

	   }
	 })
}


function confirmMe(message,url){
 var answer = confirm(message)
 if (answer)
	window.location = url
}

//initialize a date
function initDate(className){
	$("."+className).datepicker();
}

//number manipulation
function IsNumeric(s){
	return Number(s).toString() != "NaN";
}

function addCommas(nStr)
{
    nStr += '';
    var x = nStr.split('.');
    var x1 = x[0];
    var x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}

function str_split(str,con)
{
	var strs = str.split(con);

	return strs;
}

function addslashes(str) {
	str=str.replace(/\\/g,'\\\\');
	str=str.replace(/\'/g,'\\\'');
	str=str.replace(/\"/g,'\\"');
	str=str.replace(/\0/g,'\\0');
	return str;
}

function toNumber(strVal){
	num = strVal.replace(",","");
	num = Number(num);

	return num;
}

//------------------------ DYNAMIC AJAX LIST ------------------
function initDynamicList(dialogId,pegId){
	if(!$("#"+dialogId).length){
		createDynamicListDiv(dialogId);
	}
	var listDivObj  = $("#"+dialogId);
	
	//position the dialog
	listDivObj.css("top",pegObj.position().top);	
	listDivObj.css("left",pegObj.position().left);	
}

//creation of the Div container for the list, this should only be called once
function createDynamicListDiv(dialogId){
	$("body").append('<div id="'+dialogId+'">matchDiv</div>');
		
	//click effects
	$(document).click(function(){
	//alert("outside click");
		hideDynamicList()
	});
	
	//do not hide if clicked in list
	listDivObj.click(function(e){
		e.stopPropagation();
	})	
	
	listKeyDown();
}

function hideDynamicList(dialogId){
	$("#"+dialogId).remove();
	window.liSelected = null;
}