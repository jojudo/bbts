<?php 
	defined('INSTANCE') or die("No Direct access allowed!");
	
	abstract class helperController{
		function __construct(){
			$this->db = load_class('db');	
			$this->ui = load_class('ui');
			$this->datastore = load_class('datastore');
			$this->permission = load_class('permission');
            
			$this->profile = $this->datastore->get_config('profile')[0];
			$this->environment = $this->ui->get_header_type();
			
			//default access to restricted
			$this->access_level = 1;
			
			//any method for retrieving action
			$this->datastore->set_parameter_source("ALL");
			$action = $this->datastore->pick("action");
			$action = $action?$action:"main";
			
            $this->mod = $this->datastore->get_config('mod');
			
            //we bring the default to get for all modules
			$this->datastore->set_parameter_source("GET");
			$this->$action();
		}
		
		abstract protected function main();
				
		function __call($method,$args){
			if($this->environment=="none"){
				$output_arr['msg'] = "The pages you are looking for cannot be found";
				$output_arr['failure'] = 1;
				echo $this->json_output($output_arr);
			}else{
                $this->ui->load_theme_page("404",array('method'=>$method,'module'=>$this->datastore->get_config("mod")));
			}
		}
		
		private function set_args($varName,$varValue){
			$this->args[$varName] = $varValue;
		}
		
	
		function set_mod_access($level){	
			$this->access_level = $level;
		}
		
		function mod_access_level(){
			return $this->access_level;
		}
		
		function mod_restrict_access($maxlevel,$msg=''){
			$msg = $msg?$msg:'You do not have permission to access this page';
			if($this->profile['user_level_id']>$maxlevel){
				$this->ui->load_theme_errorfile($msg);
				return TRUE;
			}
			
			return FALSE;
			
		}

		function json_output($json_arr=array()){
			if(!count($json_arr)) $json_arr = $this->args;
			$args['data_arr'] = $json_arr;				
			$this->ui->load_theme_page_solo("json",array('body'=>$json_arr));
		}
		
		function set_mod_javascript($filename=""){
			$filename = $filename?$filename:$this->mod;
			$this->ui->set_javascript($this->datastore->get_config("mod_url").US.$this->mod.US.'js'.US.$filename.'.js');
		}
		
		function set_mod_stylesheet($filename=""){
			$filename = $filename?$filename:$this->mod;
			$this->ui->set_stylesheet($this->datastore->get_config("mod_url").US.$this->mod.US.'css'.US.$filename.'.css');
		}
		
		function set_mod_flat_folder($folder="flats"){
			$this->ui->set_module_flat_folder($folder);
		}
		
		function add_libraries($filename,$folder,$type='js'){
			$file = str_replace(".",US,$filename);

			$this->ui->set_javascript($this->datastore->get_config('lib_url').US.$file.'.'.$type);
		}
		
		function add_vendor($folder,$filename,$ext='js'){
			if($ext=='js')
			{
				$this->ui->set_javascript($this->datastore->get_config("full_url").US."themes".US."vendors".US.$folder.US.$filename.".$ext");
			}
			else
			{
				$this->ui->set_stylesheet($this->datastore->get_config("full_url").US."themes".US."vendors".US.$folder.US.$filename.".$ext");
			}
		}
		
        function set_libraries($folder,$filename,$ext)
        {
            $this->ui->set_javascript($this->datastore->get_config('lib_url').US.$folder.US.$filename.'.'.$ext);
        }
        
        function use_module_template($filename,$args=array()){
            $args['mod'] = $this->datastore->get_config("mod");            
            $this->ui->load_module_page($filename,$args);
        }
        
		
	}
?>