<?php
	interface insertSet{
		//insert record
		public function save();
		
		//select all data for edit
		public function edit();
		
		//update data record
		public function update();
		
		//delete data
		public function delete();
	}