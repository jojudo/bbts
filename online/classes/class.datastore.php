<?php 
	/**
 * Millennium Express INC.
 *
 * Proprietary TRacking Sysytem
 *
 * @package		MEX
 * @author		Joey Toga
 * @copyright	Copyright (c) 2009 - 2015, Millennium Express INC..
 * @license		---
 * @link		http://www.millenniumexpress.biz
 * @since		Version 3.0
 * @filesource
 * @description 
			data storeclass is used to store all variables into 
			one object so it can be used all around the system
			when instance is initialized.
	
 */
	class datastore{ 
		protected $parameter_basket, $config;
		
		function __construct()
		{
			//add all available variables into the object
			$this->__parameter_picking();
		}
		
		private function __parameter_picking(){
			//insert into basket all variable sources
			$this->parameter_basket = array();
			
			if(isset($_GET))
			{
				$this->parameter_basket['GET'] = $this->__parameter_cleaning($_GET);  
			}
			if(isset($_POST))
			{
				$this->parameter_basket['POST'] = $this->__parameter_cleaning($_POST);  
			}
			
			$this->parameter_basket['ALL'] = array_merge($this->parameter_basket['POST'],$this->parameter_basket['GET']);
				
		}
		
		
		
		function set_parameter_source($source="GET")
		{
			 //set the default parameter source, if unknown we will get it from GET
			if(in_array($source,array('GET','POST','ALL','REWRITE')))
			{
				$this->parameter_source = $source;
			}


		}
        
		function get_parameter_source()
		{
			 if(isset($this->parameter_source)) return $this->parameter_source;
             else return "GET";
		}
		
		//retrieve parameters 
		function pick($paramName)
		{
			if (isset($this->parameter_basket[$this->get_parameter_source()][$paramName]))
			{
				return $this->parameter_basket[$this->get_parameter_source()][$paramName];
			}
			else
			{
				return false;
			}
		}
		
		//bulk retrieve parameter basket
		function get_basket($type='GET'){
			return $this->parameter_basket[$type];
		}
		
		//clean up variables and add into the variable container
		private function __parameter_cleaning($var_arr)
		{
			$returnVal = array();
			foreach($var_arr as $index=>$value)
			{	
				if(is_array($value) && count($value))
					$returnVal[$index] = $this->parameter_cleaning($value);
				else
					$returnVal[$index] = $this->sanitizer($value);
					
			}
			return $returnVal;
		}
		
		//clean up variables
		function sanitizer($val){
	 		$var= $val;
	 		$var = trim($var);
	 		$var = strip_tags($var); 		
	 		
	 		return $var;
	 	}
		
		
		
		//record the config to be used all throughout the system
		function set_config($config_arr=array()){
			//we will only allow config to be set once
			if(count($this->config)===0)
			{
				$this->config = $config_arr;
				return true;
			}
			else
			{	
				echo "config is already set, I am full ".__FILE__;
				return false;
			}
		}
        
        function update_config($subgroup,$value){
            if(isset($this->config[$subgroup]))
			{
				$this->config[$subgroup] = $value;
			}
        }
		
		//add config variables after set config has been initiated
		function add_config($group,$configVal,$subgroup=""){
			if($subgroup)
			{
				$this->config[$group][$subgroup][] = $configVal;
			}
			else
			{	
				if(is_array($configVal))
				{
					$this->config[$group][] = $configVal;
				}
				else
				{
					$this->config[$group] = $configVal;
				}
				
			}
		}
		
		//retrieve variables based on subgroup
		function get_config($subgroup=""){
			//if subgroup is undefined, return blank. strictly subgroup only do not return all
			if(!$subgroup){
				return array();
			}
			
			//if subgroup is found give the values else return an empty array
			if(isset($this->config[$subgroup])){
				return $this->config[$subgroup];
			}else{
				return array();
			}
		}
				
		function vomit(){
			//this is debug method, only for testing
			echo '<pre>vomiting config....';
			print_r($this->config);
            
            echo 'vomiting basket....';
			print_r($this->parameter_basket);
		}
	
	}
