<?php /**
 * Millennium Express INC.
 *
 * Proprietary TRacking o
 *
 * @package		MEX
 * @author		Joey Toga
 * @copyright	Copyright (c) 2009 - 2015, Millennium Express INC..
 * @license		---
 * @link		http://www.millenniumexpress.biz
 * @since		Version 3.0
 * @filesource
 * @description 
		ui class does the processing of templates and variables, however displays is 
        handed over to the jail class to limit access to the datastore class.
	@dependecy  class.jail.php
				class.datastore.php
	
 */

defined('INSTANCE') or die("No Direct access allowed!");
class ui{
    protected 	$stylesheet, 
                $javascript, 
                $datastore, 
                $headerType,
                $variableCabinet; //storage variable to handover to the jail class
    
    function __construct(){
        //initialize the datastore to be usable in ui
        $this->datastore = load_class('datastore');
        $this->outputTypes = $this->datastore->get_config('output');
		
		$this->set_header_type();
    }
        
    //public call : break variable to inserted into variable cabinet
    public function store_ui_variable($variableSet=array()){
        if(!empty($variableSet))
        {
            foreach($variableSet as $setIndex=>$setValue)
            {
                $this->open_cabinet($setIndex,$setValue);
            }
        }
        else
        {
            trigger_error("store_ui_variable parameter needs to be array", E_USER_ERROR);
        }
    }
    
    //public call: set variables to be accessible into the templates via variable cabinet
    public function open_cabinet($variableName,$variableValue,$arraySet = false){
        if($arraySet)
        {            
            $this->variableCabinet[$variableName][] = $variableValue; 
        }
        else 
        {
            $this->variableCabinet[$variableName] = $variableValue;
        }
    }
    
        
   
    //----------------------- START TEMPLATE LOADING FUNCTIONS ---------------------------------
    
    //PUBLIC WEBSITE : load the  page in the templates folder using the base template
    public function load_theme_page($filename,$args=array()){
        if(count($args)>0) $this->store_ui_variable($args);
        
        $this->__ready_jail();        
        $this->ui_start();
        $this->jail->open_gate($filename);
        $this->ui_end();
    }
    
    //make $body accessible into the main template (index)
    public function load_theme($body){
        $this->open_cabinet("body",$body);

        //prepare the template jail
        $this->__ready_jail();

        //select the to use and use it
        if(array_key_exists($this->headerType,$this->outputTypes))
        {
            
            $this->jail->open_gate($this->headerType,false);
        }
        else 
        {
            $this->jail->open_gate("index");
        }
    }
  
    //load a template without using the parent template. echo the output immediately
    public function load_theme_page_solo($filename,$args=array()){
        if(count($args)>0)
        {
            $this->store_ui_variable($args);
        }
        
        $this->__ready_jail();
        
        $this->ui_start();
        $this->jail->open_gate($filename);
        echo $this->ui_end(false);
    }
    
    //allow modules to use own templates
    public function load_module_page($filename,$moduleVariables=array()){
        $this->__ready_jail();
        
        //limit resources to module specifications
        $this->jail->set_resources($moduleVariables);
        $this->ui_start();
		
		if(array_key_exists($this->headerType,$this->outputTypes)){
            $this->jail->open_gate($this->headerType,false);
        }
        else{
			$this->jail->set_jail_cell($this->datastore->get_config('mod_folder').DS.$this->datastore->get_config('mod').DS.$this->load_module_flat_folder());
			$this->jail->open_gate($filename);
		}
		
        $this->ui_end();
        
    }
    
	public function set_module_flat_folder($tpl_folder='flats'){
		$this->tpl_folder = $tpl_folder;
	}
	
	private function load_module_flat_folder(){
		if(!isset($this->tpl_folder)){
			$this->set_module_flat_folder();
		}
		return $this->tpl_folder;
	}
    //----------------------- END TEMPLATE LOADING FUNCTIONS ---------------------------------
   //------------------------ START DISPLAY METHODS ------------------------------------------
        
    //prepare the limited object to display the output.
    private function __ready_jail(){
        if(!isset($this->jail))
        {
            $this->jail = load_class("jail");
        }
        
        //everytime this method is invoked, variables are overwritten
        $this->jail->set_jail_cell($this->datastore->get_config('theme_folder'));
        $this->jail->set_jail_cellBars($this->datastore->get_config('tplExtension'));
        $this->jail->set_resources($this->variableCabinet);
        
    }
   
    //mark the beginning of output buffering
    public function ui_start(){
        if(!@ob_start("ob_gzhandler")) ob_start();
                
    }

    //end output buffering, flag to TRUE  if output it to index, FALSE to return as variable
    public function ui_end($outputFlag=true){
        if($outputFlag)
        {				
            $this->load_theme(ob_get_clean());	
        }
        else 
        {
            return ob_get_clean();
        }
    }
    
    //return header type
    public function get_header_type(){
        return $this->headerType;
    }
    
    //define what type of output we are going to serve
    public function set_header_type(){
        $this->headerType = $this->datastore->pick("headerType","ALL");
        $this->headerType = $this->headerType?$this->headerType:"html";
    }
	
	//create the link for the javascripts
    public function set_javascript($link){
        if($this->datastore->get_config('environment')=="development") $link.='?temp='.rand(1,9999999);
        $this->open_cabinet('javascript',$link,true);
    }
        
    //---------------------------- END DISPLAY METHODS ----------------------
    
     
    //---------------------------- START CANCELLED FUNCTIONS ----------------------
    function overwrite_header($headerType){
        trigger_error("overwrite header cancelled ".__FILE__);
        //$this->headerType = $headerType;
    }
            
    function load_theme_errorfile($msg){
       /* trigger_error("oload_theme_errorfile cancelled ".__FILE__);
        $args['msg'] = $msg;
        print $buffer = $this->load_theme_page_solo('errorfile',$args);*/
    }		
    
    //create the link for the stylesheets
    public function set_stylesheet($link){
        if($this->datastore->get_config('environment')=="development") $link.='?temp='.rand(1,9999999);
        $this->open_cabinet('stylesheet',$link,true);
    }
    
    
    
    //---------------------------- END CANCELLED FUNCTIONS ----------------------
}
