<?php 
	defined('INSTANCE') or die("No Direct access allowed!");
	class auth{
		protected $profile, $db , $errorMsg ;
		
		function __construct()
		{
			$this->db = load_class('db');
			$this->ui = load_class('ui');
            $this->datastore = load_class('datastore');
            
            $this->sess_name = $this->datastore->get_config('sessions')['prefix'];			
		}
		
		function logged_in_user()
		{
			if($this->__is_session_registered())
			{
				$this->__verify_existing_login();
				return $this->__get_profile_details($this->user_id);
			}
			else
			{
				return array();
			}
			
		}
		
		function authenticate(){
			$this->datastore->set_parameter_source("POST");
			
			//check if new session
			if(!$this->__is_session_registered())
			{
				//check if correct login details 
				if($this->__verify_new_login())
				{				
					$this->__login_success_redirect();					
				}
				else
				{
					//load the login form if incorrect details
					$this->__load_login_form();
				}
			}
			else{
				//determine if user wants to log out
				$this->__clean_logout_check();
				//check if session is still valid, if not load log in form
				if(!$this->__verify_existing_login()) $this->__load_login_form();
			}
		}
		
		function __verify_new_login(){
			//check records in database
			$username = $this->datastore->pick("username");
			$password = $this->datastore->pick("password");
				
			if($username=='' && $password==''){
				$this->errorMsg = "Please enter username and password";
				return false;
			}
			
            $this->db->set_table("users");
            $this->db->set_fields("user_id,user_password");
            $this->db->set_condition("user_username='$username'");
			$results = $this->db->select();
            
          
			if(count($results)){
                $result = $results[0];
                
				if(!$this->__password_check($password,$result['user_password'])){
					//$this->__record_strike($result['user_id'],$result['strikes']);
					$this->errorMsg = 'wrong username-password combination';
					return false;
				}
				
				//reaching here means everything was passed
				$this->user_id = $result['user_id'];
				$this->__register_session();
				$this->__save_to_profile();
				return true;
				
			}else{
				$this->errorMsg = 'Invalid Username and Password combination';
				return false;
			}
			
		}
		
		private function __password_check($password,$password_check){
			if(md5($password) == $password_check)
				return true;
			else
				return false;
		}
		
		private function __verify_existing_login(){
			$this->user_id = $_SESSION[$this->sess_name.'user_id'];
			
			$ip_address = $_SERVER['REMOTE_ADDR'];
			$last_access = $_SESSION[$this->sess_name.'last_access'];
			$session_id = session_id();

			//let us verify session timeout before checking the database for records
			if($last_access+$this->datastore->get_config('session_timeout') < date("U") && $this->user_id){
				$this->errorMsg = 'YOur session has timed out!';
				return false;
			}
			
			$this->__save_to_profile();			
			return true;
				
		}
		
		private function __register_session(){
			$ip_address = $_SERVER['REMOTE_ADDR'];
			$_SESSION[$this->sess_name.'authenticated'] = 1;
			$_SESSION[$this->sess_name.'last_access'] = date("U");
			$_SESSION[$this->sess_name.'user_id'] = $this->user_id;
			$session_id = session_id().md5($this->user_id.date("ymd"));
			
			$_SESSION[$this->sess_name.'session_id'] = $session_id;
			
			
			//update user login time, session_id and ip address
            $this->db->set_table("users");
            $this->db->set_fields('last_log',date("Y-m-d H:i:s"));
            $this->db->set_fields('last_action',date("Y-m-d H:i:s"));
            $this->db->set_fields('ip_add',$ip_address);
            $this->db->set_condition('user_id='.$this->user_id);
            $this->db->update();
           
		}
		
		//save a copy of the current users profile to the datastore class	
		private function  __save_to_profile()
		{	
			$this->datastore->add_config('profile',$this->__get_profile_details($this->user_id));
		}
		
		private function __get_profile_details($user_id)
		{
			if(intval($user_id))
			{
                $this->db->set_table("users u");
                $this->db->set_join("userlevels l","l.level_id = u.user_level");
                $this->db->set_join("branches b","u.branch_id = b.branch_id");
                $this->db->set_fields("u.user_id,u.user_username as username");
                $this->db->set_fields("u.user_lname,u.user_fname,u.ip_add");
                $this->db->set_fields("u.last_action,l.level_id,l.level_name");
                $this->db->set_fields("u.branch_id,b.branch_name,b.country_id");
                $this->db->set_condition("u.user_id=$user_id");
                return $this->db->select()[0];
				
			}
			else
			{
				return array();
			}
		}
		
 		private function __is_session_registered(){
			return isset($_SESSION[$this->sess_name.'authenticated']);
		}
				
		//load login form
		private function __load_login_form($msg=""){
			//clearing just to make sure
			$this->__clear_session();
			
			$msg = $msg?$msg:$this->errorMsg;
			$data_arr['error_msg'] = $msg;
			$data_arr['session'] = 0;
			$data_arr['failure'] = 1;
			
			
			if($this->ui->get_header_type()=="json"){
				//Json output
				$json_arr['body'] = $data_arr;
				$this->ui->load_theme_page_solo("json",$json_arr);				
			}else{
				$this->ui->load_theme_page_solo('login',$data_arr);
			}
            
            exit();
		}
		
		function verify_strike_status($user_id,$strike,$last_action){
			if($strike>=$this->datastore->get_config('strike_limit')){
				//stroke of  bad luck
				if($last_action+$this->datastore->get_config('strike_wait')<=date("U")){
					//remove strikes and allow user to proceed
					$this->db->updateQuery(PREFIX."users",array('last_action'=>date("U"),'strikes'=>0),"user_id=".$result['user_id']);
					return false;
				}else{
					//$this->load_login_form("You have used your fail allowance, please try again after "+($this->datastore->get_config('strike_wait')/60)+" minutes");
					return true;
				}
			}else{
				//empty strikes 
				$this->db->updateQuery(PREFIX."users",array('last_action'=>date("U"),'strikes'=>0),"user_id=".$result['user_id']);
				return false;
			}
			
			
		}
	
		private function __record_strike($user_id,$strike){
			$ip_address = $_SERVER['REMOTE_ADDR'];
			$this->msg = 'You have used '.$strike++.' of '.$this->datastore->get_config('strike_limit');
			//update last action to mark when to start counting from strikeut
			$this->db->updateQuery(PREFIX."users",array('last_action'=>date("U"),'ip_address'=>$ip_address,'strikes'=>$strike),"user_id=".$user_id);
			
		}
		
		
		private function __clear_session(){
			session_unset();
			session_regenerate_id(); 
		}
		
		private function __clean_logout_check(){
			$this->datastore->set_parameter_source("ALL");
			$logout = $this->datastore->pick("logout");
			if($logout=="logout"){
				$this->__clear_session();
				header("location:index.php");
				exit();
			}
		}

		private function __login_success_redirect(){
			
			if($this->ui->get_header_type()=="json"){
				//Json output
				$data_arr['session'] =1;
				$json_arr['body'] = $data_arr;
				$this->ui->load_theme_page_solo("json",$json_arr);				
			}else{
				echo $this->ui->get_header_type();
				header("location:index.php?login=true");
			}	
		}
	}
?>