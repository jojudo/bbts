<?php 

class widgetParent{
	protected $ui,
			  $datastore,
			  $db,
			  $widgetFolder;
			  
	function __construct(){
		$this->ui = load_class("ui");
		$this->datastore  = load_class("datastore");
		$this->db = load_class("db");
		$this->profile = load_class("auth")->logged_in_user();
		$this->widgetFolder = BASE.DS."widgets".DS.$this->widgetName;
	}
	
	public function widget_template($templateName,$arguments=array()){
		if(count($arguments)) extract($arguments);
		include $this->widgetFolder.DS."flats".DS.$templateName.".".$this->datastore->get_config('tplExtension');
	}
    
    public function widget_javascript($filename){
         $this->ui->set_javascript($this->datastore->get_config('full_url').'/widgets/'.$this->widgetName.'/js/'.$filename.'.js');
    }
    
    public function widget_css($filename){
         $this->ui->set_stylesheet($this->datastore->get_config('full_url').'/widgets/'.$this->widgetName.'/css/'.$filename.'.css');
    }
}

?>