<?php
/**
 * Millennium Express INC.
 *
 * Proprietary TRacking system
 *
 * @package		MEX
 * @author		Joey Toga
 * @copyright	Copyright (c) 2009 - 2015, Millennium Express INC..
 * @license		---
 * @link		http://www.millenniumexpress.biz
 * @since		Version 3.0
 * @filesource
 * @description templates run inside the jail object to prevent any global variables to be  
                accessible through the templates.
	@dependecy  none
	
 */
class jail{
	protected  $cellWall,
			   $cellResources,
			   $cellBars;
			   
	function __construct() {;} 
    
	function open_gate($cellRoom,$extract=true)
	{
        if(isset($this->cellResources)) extract($this->cellResources);
		
		if(file_exists($this->cellWalls.DS.$cellRoom.'.'.$this->cellBars))
		{
			include $this->cellWalls.DS.$cellRoom.'.'.$this->cellBars;
		}
		else
		{
			include $this->cellWalls.DS.'404.'.$this->cellBars;
		}
	}
	
	function set_jail_cell($cellName)
	{
		$this->cellWalls = $cellName;
	}
	
	function set_jail_cellBars($barName)
	{
		$this->cellBars = $barName;
	}
	
	function set_resources($rawResource)
	{
		$this->cellResources = $rawResource;
	}

}