<?php
class db{

	protected $host, $username, $password, $database, $rs, $db, $sql, $conn, $error;
	
	function __construct()
	{
		$datastore = load_class('datastore');

		$this->username = $datastore->get_config('db')['username'];
		$this->password = $datastore->get_config('db')['password'];
		$this->database = $datastore->get_config('db')['database'];		
		$this->driver = $datastore->get_config('db')['driver'];		
		$this->prefix = $datastore->get_config('db')['prefix'];
		
		$this->connect_db();
	}
	    
	function connect_db()
	{
		try{
			$this->conn = new PDO($this->driver.':host='.$this->host.';dbname='.$this->database.';charset=utf8', $this->username, $this->password);		
			$this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$this->conn->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
			
		}catch(PDOException $ex){
			echo $this->error = $ex->getMessage();
			exit();
		}
	}
	
    function set_use_prefix($option=true){
        $this->useprefix=true;
    }
    function use_prefix(){
        if(isset($this->useprefix)) return $this->userprefix;
        else return true;
    }
    
	function is_connected(){
		return $this->conn;
	}
    
    function set_table($tblName){
        //clear query when this is called
        $this->__clear_query();
        
        $prefix = $this->use_prefix()?$prefix=$this->prefix:"";
        $this->table = $prefix.$tblName;
    }
    
    function set_fields($fields){
        if(func_num_args()==2)
        {
           $this->fields[func_get_arg(0)]=func_get_arg(1); 
        }
        else
        {
           $this->fields[]= $fields; 
        }
     }
    
    function set_join($table,$field,$join="left")
    {
        $prefix = $this->use_prefix()?$prefix=$this->prefix:"";
        $this->joins[] = array('table'=>$prefix.$table,'field'=>$field,'join'=>$join);
    }

    
    function set_orderby($field,$orderby){
        $this->orderby[] = "$field $orderby";
    }
    
    function set_limit($start,$rows){
        $this->limit = "$start,$rows";
    }
    
    function set_groupby($fieldName, $ascdesc){
        $this->groupby[] =  "$fieldName,$ascdesc";
    }
    
    function set_condition($condition)
    {
        $this->conditions[] = $condition;
    }
    
    function select($getResult = true)
    {
        $this->sql = "select ".implode(",",$this->fields);
        $this->sql .=" from ".$this->table." ";

        if(isset($this->joins) && count($this->joins)>0)
        {
            foreach($this->joins as $join)
            {
                 $this->sql .= $join['join'].' join '.$join['table'].' on ('.$join['field'].')';
            }
        }
        
        if(isset($this->conditions)) $this->sql .=" where ".implode(" and ",$this->conditions);
        if(isset($this->orderby)) $this->sql.=' order by '.implode(",",$this->orderby);
        if(isset($this->groupby)) $this->sql.=' group by '.implode(",",$this->groupby);
        if(isset($this->limit)) $this->sql.=' limit '.$this->limit;
        
        $this->selectQuery();
        
        if($getResult) return $this->queryResults();
       
    }
    
    function insert()
    {
        if(func_num_args()>0)
        {
            list($table,$data) = func_get_args();
        }
        else
        {
            $table = $this->table;
            $data = $this->fields;
        }
               
		//string, array
		if(is_array($data) && count($data)){
			foreach($data as $field=>$value){
				$fields_arr[] = $field;
				$values_arr[] = $value;
			}
			$fields = implode(",",$fields_arr);
			$values = implode("','",$values_arr);
			
            //clean sql for multiple inserts
			$this->sql = "";
			$this->sql .= "INSERT INTO $table ($fields) VALUES ('$values');";
            $this->queryType = "insert";
            $result = $this->execute();
		}
		else
        {
            $result = false;
        }
        
        return array("success"=>$result);
    }
    
    
    
    
    function update(){
        $this->sql = "update ";
        $this->sql .= $this->table;
		$this->sql .=' set ';
		$ctr=0;
		foreach($this->fields as $field=>$value){
			$ctr++;
			if($ctr!=1){
				$this->sql.=" , ";
			}
			$this->sql .=" $field='$value' ";
		}
        if(count($this->conditions)) {
			$this->sql .=" where ".implode(" and ",$this->conditions);
			return $this->execute();
		}else {
			$this->error = 'Update query cannot be without conditions';
			return false;
			
		}
    }
    
    function insertQuery()
    {
        if(is_array($this->fields)){
            foreach($this->fields as $field=>$value){
                $fields_arr[] = $field;
                $values_arr[] = $value;
            }
            
            $fields = implode(",",$fields_arr);
            $values = implode("','",$values_arr);
        }
        
        $this->sql .= "insert into ";
        $this->sql .= implode(",",$this->tables);
        $this->sql .="($fields) values ('$values');";
        return $this->sql;
       
    }
    
    
   //---------------------------------------------------------------- 
    //delete query
	function deleteQuery()
    {
		$this->sql = "delete from";
        $this->sql =  implode(",",$this->tables);
        if(count($this->conditions)) $this->sql .=" where ".implode(" and ",$this->conditions);
		return  $this->sql();
	}
    
    
    //clean function;
    function clearQuery(){
        $this->sql = "";
    }
   
	//transactional mysql query call
	function actionQuery(){
		try {  
			$this->conn->beginTransaction();
			$this->rowCount = $this->conn->exec($this->sql);
			$this->conn->commit();
            $this->__clear_query();
			return true;
			
		} catch (Exception $e) {
			$this->conn->rollBack();
			$this->error = $e->getMessage();
            $this->__clear_query();
			return false;
			
		}	
	}
	
    //select mysql query call
    function selectQuery(){	
		try {
			//connect as appropriate as above
			$this->rs = $this->conn->query($this->sql); //invalid query!
			//$this->rowCount = $this->rs->fetchColumn();
            $this->__clear_query();
			return $this->rs;
			
		} catch(PDOException $ex) {
			$this->error = $ex->getMessage().$this->sql;
            $this->__clear_query();
			return false;
		}		
	
	}
	
	function rawSql($sql){
		$this->sql = $sql;
	}
    
	function execute(){
       if($this->queryType=="select") return $this->selectQuery();
       else return $this->actionQuery();
	}
	
    function queryResults(){
        return $this->rs->fetchAll(PDO::FETCH_ASSOC);
    }
    
	function getInsertId(){
		return $this->latestInsertedId;
	}

	function num_rows(){
		return $this->rowCount;
	}
	
	function found_rows(){
        $this->sql = "select FOUND_ROWS() as found";
        $this->selectQuery();
		return $this->queryResults()[0]['found'];
		
		
	}
	
	function show_sql($returnAsVar=false){
        if($returnAsVar) return $this->sql;
		else echo $this->sql;
	}
	
	function getErrorMsg(){
		return $this->error;
	}
    
    private function __clear_query(){
        unset($this->fields);
        unset($this->table);
        unset($this->joins);
        unset($this->orderby);
        unset($this->conditions);
        unset($this->groupby);
        
    }
}

?>