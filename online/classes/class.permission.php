<?php

class permission{
    
    protected $permission;
    
	function __construct(){
		$this->datastore = load_class("datastore");
		$this->db = load_class("db");
        
		$this->profile = $this->datastore->get_config("profile")[0];
		$this->__load_permissions();
	}
	
	private function __load_permissions(){
		//select user-group permission 
		if(!isset($this->permission_set))
        {
			//run this block if permission is not set
            $this->db->set_table("userlevelaccess");
            $this->db->set_fields("module,allowed_actions");
            $this->db->set_condition("level_id=".$this->profile['level_id']);
            $permissions = $this->db->select();
			$this->permission_set = array();
			
            foreach($permissions as $permit){
				$this->permission_set[$permit['module']] = explode(',',strtolower($permit['allowed_actions']));
			}
		}
		
        return ;
	}
	
	public function is_permitted($module,$action){
		if(array_key_exists('all',$this->permission_set)){
			return true;
		}
		
		
		if(isset($permission_set[$module]) && in_array(strtolower($action),$permission_set[$module]))
			return true;
		else
			return false;
	}

}

?>
