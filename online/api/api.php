<?php
	session_start();
	define('INSTANCE',1);
	
	chdir('../');
	
	//define environment
	define('ENVIRONMENT', 'development');	
	
	//setup static refences
	define('DS',DIRECTORY_SEPARATOR);
	define('US','/');
	
	//let us define the location of main processors!
	$system_path = realpath('system').DS;
	define('SYSPATH', str_replace("\\", DS, $system_path));
	define('APIPATH', str_replace("\\", DS, realpath('api').DS));
	
	//define this file
	define('SELF', pathinfo(__FILE__, PATHINFO_BASENAME));
	
	//define the base of this file
	$base_arr = explode(DS,dirname(__FILE__));
	array_pop($base_arr);
	
	define('BASE',implode(US,$base_arr));
	//load the mother of all boards
	
	//basic resources
	require_once SYSPATH.'motherboard.php';
	
	//operational resources
	require SYSPATH.'startupClasses.php';
	
	//api resources
	require APIPATH.'api.class.php';
	require APIPATH.'api.processor.php';
	
?>

