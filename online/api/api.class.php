<?php 
	class api{
		
		function __construct(){
			//processing resources
			$this->db = load_class('db');
			$this->ui  = load_class('ui');
			$this->datastore = load_class('datastore');
			$this->api_run_module();
		
		}
		
		private function api_run_module(){
			$mod = $this->datastore->pick("mod");
			if(file_exists(MODPATH.$mod.DS."helper.php")){
				//mod helper to be used as parent
				require MODPATH.$mod.DS."helper.php";
				
				//now that module path and helper is present we execute the api helper
				require APIPATH.'api.helper.php';
				
				//instantiate api helper
				$helper = new apiHelper();
				
				//set permissions for API use
				$helper->api_record_permissions();
				
				$action = $this->datastore->pick("action");
				
				$helper->api_run($action);
				//if($helper->api_verified($action)) $helper->$action();
				
			
			}else{
				echo 'Page not found';
			}
						
		}
		
		function return_parameters(){
			;
		}
	}
?>