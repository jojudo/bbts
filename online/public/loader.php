<?php 
/**
 * Millennium Express INC.
 *
 * Proprietary Tracking Sysytem
 *
 * @package		MEX
 * @author		Joey Toga
 * @copyright	Copyright (c) 2009 - 2015, Millennium Express INC..
 * @license		---
 * @link		http://www.millenniumexpress.biz
 * @since		Version 3.0
 * @filesource  public/loader.php
 * @description Object that handles the output for the public folder
 
 */

// ------------------------------------------------------------------------
	if ( ! defined('INSTANCE')) exit('No direct script access allowed');
	class loader{
		
		function __construct(){
			$this->boot();
		}
		
		private function boot()
		{
			//instantiate objects
			$this->ui  = load_class('ui');
			$this->datastore = load_class('datastore');

			$page = $this->datastore->pick("page");
			$this->page = $page?$page:'main';
			$this->__public_page_view();
		}
		
			
		private function __public_page_view()
		{
			$this->ui->load_theme_page($this->page);
		}
			
	}
?>