<?php
/**
 * Millennium Express INC.
 *
 * Proprietary Tracking Sysytem
 *
 * @package		MEX
 * @author		Joey Toga
 * @copyright	Copyright (c) 2009 - 2015, Millennium Express INC..
 * @license		---
 * @link		http://www.millenniumexpress.biz
 * @since		Version 3.0
 * @filesource  public/processor.php
 * @description this files defines the required global variables for this folder and instantiates the loader.
 */

// ------------------------------------------------------------------------

	if ( ! defined('INSTANCE')) exit('No direct script access allowed');
		
	//this can be made dynamic
	$datastore->add_config('theme',$config['theme']['public']);
	$datastore->add_config('theme_folder',BASE.DS."themes".DS."public".DS.$config['theme']['public']);
	$datastore->add_config('theme_url',$datastore->get_config("full_url").US."themes".US."public".US.$config['theme']['public']);
	
	
	$publicProcess = new loader();