<?php 
	if ( ! defined('INSTANCE')) exit('No direct script access allowed');
	function widget($widgetName,$method="",$param=array())
    {
        //allows widgets to be used in templates for public processing
        include(CLASSPATH."class.widget.php");
		if(file_exists(BASE.DS.'widgets'.DS.$widgetName.DS.'widget.php')){
			$ui = load_class("ui");
			
			$ui->ui_start();
			include(BASE.DS.'widgets'.DS.$widgetName.DS.'widget.php');
			$widget = new widget();
            if($method!="") $widget->$method($param);
			echo $ui->ui_end(false);
		}
	}
	
    //allows a different template to be included into the current template
	function add_template($template){
		$ui = load_class("ui");	
		$ui->load_theme_page_solo($template);		
	}
    
    function full_url(){
        return load_class("datastore")->get_config('full_url');
    }

    function theme_url(){
        return load_class("datastore")->get_config('theme_url');
    }
?>
