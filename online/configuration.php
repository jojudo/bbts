<?php 
//bring down the site
$config['offline'] = 0;
$config['offline_msg'] = 'Im sorry site is under construction';
$config['session_timeout'] = 86400; // will be leaving session on for 24 hours
$config['strike_limit'] = 5; //5 wrong password
$config['strike_wait'] = 300; //5 minutes wait for fresh 5 strike bat
$config['url_rewrite'] = 1; //turn on rewrite or not

$config['timezone'] = 'Pacific/Samoa';
$config['environment'] = 'development';
$config['tplExtension'] = 'tpl';

//$config['theme']['admin'] = 'mexpress';
$config['theme']['admin'] = 'Gentellela';
$config['theme']['public'] = 'mex';

//database settings
$config['db']['host'] = 'localhost';
$config['db']['database'] = 'mexpress_27122015';
$config['db']['driver'] = 'mysql';
$config['db']['username'] = 'root';
$config['db']['password'] = '';
$config['db']['prefix'] = 'me_';

//session settings
$config['sessions']['prefix'] = 'me_';

//extras
$config['data']['limit'] = 20;

//in megabytes = 10MB
$config['upload']['limit'] = 60971520;
$config['upload']['dir'] = 'upload';

//supported output
$config['output']['json']= 1;
$config['output']['xml'] = 1;

/*--------------- YOU CAN ADD CUSTOM SETTINGS FROM HERE ------------------*/

?>