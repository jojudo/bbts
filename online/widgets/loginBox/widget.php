<?php 

class widget extends widgetParent{
	protected $widgetName;
	
	function __construct(){
        $this->widgetName = "loginBox";
		parent::__construct();
       
        $this->auth = load_class("auth");
		$this->__widget_run();
       
		
	}
	
	private function __widget_run(){
        $user_details = $this->auth->logged_in_user();
		if(!empty($user_details)>0)
        {            
			$this->widget_template("welcome_box",array('user'=>$user_details));
		}else{
			$this->widget_template("login_box",array('full_addr'=>$this->datastore->get_config("full_url")));

		}
		
	}
}

?>