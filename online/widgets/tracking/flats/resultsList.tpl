 <div id="tracking-container">
     <div id="tracking-photo">   
        <img src="http://millenniumexpress.biz/mex/images/temp-photo.jpg" alt="Package Arrival Picture" border="0" hspace="10"/>
     </div>
     <div id="tracking-status">
         <h4><span class="yellow">Tracking Number:</span> <?php echo $packing_no;?> <span style="font-size:10px;">Not your package? <a href="http://millenniumexpress.biz/trackall/103.html" class="yellow">click here</a></span></h4> 
         <ul>
         <?php foreach($events as $event) { ?>
            <li> <a class="<?php echo $event['status'];?>"><?php echo $event['event'];?></a></li>
         <?php } ?>   
        </ul>     	
    </div>
    <div class="clear"></div>
    
    <div id="disclaimer">
        <p>*Above dates are determined from information provided by ocean vessel carrier
        and may change without prior notice</p>
    </div>
</div>
