<?php 

class widget extends widgetParent{
	protected $widgetName = 'tracking';
	
	function __construct(){
		parent::__construct();
		$this->__widget_run();
		
	}
	
	private function __widget_run(){
        $this->track_box();
		
	}
    
    function track_box(){
        $this->widget_css("resultslist");
        
        if($this->datastore->pick("id")){
            $this->db->set_table("event_tracking e");
            $this->db->set_join("boxes b","b.box_id = e.box_id","inner");
            $this->db->set_fields("replace(e.event,'({date})',if(e.event_date='0000-00-00 00:00:00','',concat('<span class=\"yellow\">(',date_format(e.event_date,'%m/%d/%Y'),')</span>'))) event");
            $this->db->set_fields("e.event_date");
            $this->db->set_fields("if(e.event_date<now(),if(e.event_date='0000-00-00 00:00:00','uncompleted','completed'),'uncompleted') status");
            $this->db->set_condition("packing_no='".$this->datastore->pick('id')."'");
            $results = $this->db->select();
        }
         
        if(isset($results) && count($results) >0)
        {
            $this->widget_template("resultsList",array('packing_no'=>$this->datastore->pick('id'),'events'=>$results));
        } 
        else
        {
            
             $this->widget_template("notfound",array('packing_no'=>$this->datastore->pick('id')));
        }
        
    }
    
    private function get_tracking_events()
    {
        //returns tracking event array
         $this->db->set_table("boxes");
         $this->db->set_fields("1");
         $this->db->set_condition("packing_no='".$this->datastore->pick('id')."'"); 
         
         
    }
    
    private function check_packing_exists(){
        //returns true
        
    }
    private function create_tracking_details(){
        //attempt to create tracking details for existing packing_no
    }
	
}

?>