<?php 
class widget extends widgetParent{
	protected $widgetName = 'smartNav';
	
	function __construct(){
		parent::__construct();
		$this->__widget_run();
		
	}
	
	private function __widget_run(){
		$this->args['nav'] = $this->__build_nav();
       	$this->args['full_url'] = $this->datastore->get_config("full_url");
		$this->args['folder_url'] = $this->datastore->get_config("folder_url");
        $this->widget_template("admin_navigation",$this->args);
		
	}
	
	private function __build_nav($parentid=0){
		$this->permissions = load_class("permission");
        
        $this->db->set_table("nav");
        $this->db->set_fields("text,module,action,navid,param");
        $this->db->set_condition("parentid = $parentid");
        $this->db->set_orderby("`order`","ASC");
        
        $main_nav = $this->db->select();
		$return_arr = array();
		$line_arr = array();
		
		foreach($main_nav as $nav){
			
			
			if($this->permissions->is_permitted($nav['module'],$nav['action']))
            {
				$line_arr['text'] = $nav['text'];
				$line_arr['mod'] = $nav['module'];
				$line_arr['action'] = $nav['action'];
                $line_arr['param'] = $this->__split_param($nav['param']);
				$line_arr['subnav'] = $this->__build_nav($nav['navid']);

				if($parentid){
					$line_arr['current_page'] = ($this->datastore->get_config('mod')==$nav['module'])?true:false;
					if($line_arr['current_page']) $this->current_tab = true;
				}else{
					$line_arr['current_tab'] = isset($this->current_tab)?$this->current_tab:false;
					$this->current_tab = false;
				}
				
				$return_arr[] = $line_arr;
			}
		}
			
		
		return	$return_arr;
	}
	  
    function __split_param($params)
    {
        $params_arr = explode(';',$params);
        $return_arr = array();
        foreach($params_arr as $items){
            $line_arr = explode(':',$items);
            if(count($line_arr)==2)$return_arr[$line_arr[0]] = $line_arr[1];
        }
        
        return $return_arr;
    }
	
	

}

?>