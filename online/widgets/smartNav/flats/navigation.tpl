 
   
     <div class="sidebar-nav navbar-collapse">
         <ul class="nav" id="side-menu">
               <!-- <li class="sidebar-search">
                    <div class="input-group custom-search-form">
                        <input type="text" class="form-control" placeholder="Search...">
                        <span class="input-group-btn">
                        <button class="btn btn-default" type="button">
                            <i class="fa fa-search"></i>
                        </button>
                    </span>
                    </div>
                    <!-- /input-group 
                </li> -->
                <?php 	foreach($nav as $item){ ?>
                <li >                    
                    <a href="<?php echo ($item['mod']?transform_url($item['mod']):'#');?>">
                        <i class="fa <?php echo (isset($item['param']['class'])?$item['param']['class']:'');?> fa-fw"></i> <?php echo $item["text"];?>
                        <?php if(count($item['subnav'])){?><span class="fa arrow"></span><?php } ?>
                    </a>
                    <?php if(count($item['subnav'])){ ?>
                    <ul class="nav nav-second-level">
                    <?php foreach($item['subnav'] as $subnav){ ;?>
                        <li>
                            <a href="<?php echo ($subnav['mod']?transform_url($subnav['mod']):'#');?>"><?php echo $subnav['text'];?></a>
                        </li>
                     <?php } ?>
                    </ul>
                    <?php } ?>
                    <!-- /.nav-second-level -->
                </li>
                <?php } ?>
          </ul>
        </div>