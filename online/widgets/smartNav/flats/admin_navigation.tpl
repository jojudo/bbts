<ul class="nav side-menu">
	<?php foreach($nav as $item){ ?>
	<li <?php echo ($item['current_tab'])?'class="active"':'' ?> >
	
		<a <?php echo $item['mod']?'href="'.transform_url($item['mod']).'"':''?> >
			<i class="fa <?php echo (isset($item['param']['class'])?$item['param']['class']:'fa-home');?> fa-fw"></i> 
			<?php echo $item["text"];?>
			<?php if(count($item['subnav'])){?><span class="fa fa-chevron-down"></span><?php } ?>
		</a>
		<!-- /.nav-second-level -->
		<?php if(count($item['subnav'])){ ?>
			<ul class="nav child_menu" <?php echo ($item['current_tab'])?'style="display:block;"':'' ?> >
			<?php foreach($item['subnav'] as $subnav){ ;?>
				<li <?php echo ($subnav['current_page'])?'class="current-page"':'' ?>>
					<a href="<?php echo ($subnav['mod']?transform_url($subnav['mod']):'#');?>"><?php echo $subnav['text'];?></a>
				</li>
			 <?php } ?>
			</ul>
		<?php } ?>
		
	</li>
	<?php } ?>
</ul>