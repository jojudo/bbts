<div id="contact-form">
    <h2>Your Message has been Sent!</h2>
    <br />
    <p>
        Thank you very much for taking the time and writing us a message. We will be contacting you through the 
        details you provided. Please allow 1-2 days for us to respond.
    </p>
    <br />
    <p>
        Sincerely,<br />
        <b>Millennium Express Admin</b>
    </p>
</div>