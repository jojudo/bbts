<?php 

class widget extends widgetParent{
	protected $widgetName = 'contactUs';
	
	function __construct(){
		parent::__construct();
		$this->__widget_run();
		
	}
	
	private function __widget_run(){
        $this->process_contact();
		
	}
    
    function process_contact(){
           $return_arr = array();
           $this->datastore->set_parameter_source('POST');
           $this->widget_javascript("verify");
                    
   
           if($this->datastore->pick("submit")){
               
                $continue = true;
                if(!filter_var($this->datastore->pick("email"),FILTER_VALIDATE_EMAIL)){ $continue = false; $message="Wrong Email";} 
                if(!$this->datastore->pick("name")) {$continue = false; $message="No Name";}
                if(!$this->datastore->pick("contact")) {$continue = false; $message="No contact";}
                if(!$this->datastore->pick("message")) {$continue = false;$message="No message";}
                               
                if($continue)
                {
                    $this->db->set_table('contactus');
                    $this->db->set_fields('email',$this->datastore->pick("email"));
                    $this->db->set_fields('contact_no',$this->datastore->pick("contact"));
                    $this->db->set_fields('date_added',date("Y-m-d H:i:s"));
                    $this->db->insert();                    
                }
                else
                {
                    echo "did not continue".$message;
                }
                
                $this->widget_template("thankyou");
            }
            else
            {
                $this->widget_template("contactUsForm");
            }
           
        
    }

		
}

?>