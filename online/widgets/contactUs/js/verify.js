function formchecking(){
	var form_arr = new Array();
	form_arr[0] = "name";
	form_arr[1] = "email";
	form_arr[2] = "message";

	
	if(!autoformchecking(form_arr)){
		alert("Please fill-in the highlighted fields");
		return false;
	}else{
		if(!document.getElementById('chkagree').checked){
			alert("You must read and agree to the 'Privacy Policy'");
			return false;
		}else{
			return true;
		}
		
	}

}

function autoformchecking(form_arr) 
{
	var cont=true;
	for (var counter=0; counter<form_arr.length; counter++)
	{
		objText=form_arr[counter];
		if(document.getElementById(objText)!=null)
		{
			objs = document.getElementById(objText);
			if(objs.type=="select-one")
			{
				eVal = objs[objs.selectedIndex].value
				if(eVal==0)
					eVal="";
			}
			else
			{
				eVal = objs.value;
			}
				
			
			if(eVal.length==0)
			{
				objs.style.background="#FFFF66";
				objs.focus();
				cont=false;
			}
			else
			{
				objs.style.background="";
			}
		}		
	}
	   		
	return cont;
}